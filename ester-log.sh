#!/bin/bash
if [ "$1" = "" ] || [ "$1" = "-h" ]
then
  echo "reseignez un niveau d'information"
  echo "-d : debug"
  echo "-t : trace"
  echo "-i : info"
  echo "-e : error"
  echo "-a : all"
  echo "-c : clear files"
  exit
fi

case "$1" in
  -d | --debug)
    echo "===debug==="
    sudo tail /var/log/tomcat9/ESTER-debug.log -n 500
  ;;

  -t | --trace)
    echo "===trace==="
    sudo tail /var/log/tomcat9/ESTER-trace.log -n 500
  ;;

  -i | --info)
    echo "===info==="
    sudo tail /var/log/tomcat9/ESTER-info.log -n 500
  ;;

  -e | --error)
    echo "===error==="
    sudo tail /var/log/tomcat9/ESTER-error.log -n 500
  ;;

  -a | --all)
    echo "===all==="
    sudo tail /var/log/tomcat9/ESTER.log -n 500
  ;;

  -c | --clear)
    echo "===clear files==="
    sudo rm /var/log/tomcat9/ESTER.log
    sudo rm /var/log/tomcat9/ESTER-debug.log
    sudo rm /var/log/tomcat9/ESTER-info.log
    sudo rm /var/log/tomcat9/ESTER-trace.log
    sudo rm /var/log/tomcat9/ESTER-error.log

  ;;

  * | --debug)
    echo "Option invalide !"
    echo "-h for help"
  ;;
esac
package fr.univangers.masterinfo.ester.utils;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Map;

/**
 * Classe utilitaire pour exécuter des commandes sur le terminal (Windows, Linux, etc.)
 *
 * @since 07/09/2020
 */
public class ProcessUtils {

    public static final String HOST_KEY = "host";
    public static final String PORT_KEY = "port";
    public static final String DB_KEY = "db";
    public static final String FILE_PATH_KEY = "file";
    public static final String FILE_FORMAT_KEY = "format";
    public static final String COLLECTION_KEY = "collection";
    public static final String APPEND_KEY = "append";
    public static final String EXEC_KEY = "exec";
    /**
     * Log de la classe
     */
    private static final Logger LOG = LogManager.getLogger(ProcessUtils.class);

    /**
     * On interdit l'instanciation
     */
    private ProcessUtils() {

    }

    /**
     * Exécuter une commande
     *
     * @param command
     * @return le résultat de la commande
     */
    public static String execCommand(final String... command) {
        final StringBuilder sb = new StringBuilder();

        final ProcessBuilder processBuilder = new ProcessBuilder();
        processBuilder.command(command);

        try {
            final Process process = processBuilder.start();
            final BufferedReader reader = new BufferedReader(
                    new InputStreamReader(process.getInputStream()));

            String line;
            while ((line = reader.readLine()) != null) {
                sb.append(line);
            }

            final int exitCode = process.waitFor();


        } catch (final IOException | InterruptedException e) {
            LOG.error(e.getMessage(), e);
        }

        final String result = sb.toString();


        return result;
    }

    /**
     * Exécuter une commande mongoimport
     *
     * @param properties
     * @return le résultat de la commande
     */
    public static String execMongoImportCommand(final Map<String, Object> properties) {
        final String host = (String) properties.get(HOST_KEY);


        final String port = (String) properties.get(PORT_KEY);
        final String db = (String) properties.get(DB_KEY);
        final String filePath = (String) properties.get(FILE_PATH_KEY);
        final String fileFormat = (String) properties.get(FILE_FORMAT_KEY);
        final String collection = (String) properties.get(COLLECTION_KEY);
        final boolean append = (boolean) properties.get(APPEND_KEY);
        final String[] exec = (String[]) properties.get(EXEC_KEY);


        final StringBuilder sb = new StringBuilder();
        sb.append("tr \";\" \"\\t\" < ").append(filePath);
        sb.append(" | mongoimport");
        sb.append(" --host ").append(host);
        sb.append(" --port ").append(port);
        sb.append(" --db ").append(db);
        sb.append(" --collection ").append(collection);
        sb.append(" --type ").append(fileFormat);
        sb.append(" --headerline ");

        if (!append) {
            sb.append(" --drop ");
        }

        final String command = sb.toString();

        exec[2] = command;

        final String result = execCommand(exec);

        return result;
    }
}

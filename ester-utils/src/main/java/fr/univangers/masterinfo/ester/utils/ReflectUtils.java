package fr.univangers.masterinfo.ester.utils;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.lang.annotation.Annotation;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Classe utilitaire sur les méta-données d"une classe
 *
 * @since 07/09/2020
 */
public class ReflectUtils {

    /**
     * Log de la classe
     */
    private static final Logger LOG = LogManager.getLogger(ReflectUtils.class);

    /**
     * On interdit l'instanciation
     */
    private ReflectUtils() {

    }

    /**
     * Créer une nouvelle instance à partir d'un constructeur vide
     *
     * @param clazz
     * @param <T>
     * @return une nouvelle instance
     */
    public static <T> T newInstance(final Class<T> clazz) {
        try {
            return clazz.getDeclaredConstructor().newInstance();
        } catch (IllegalAccessException | InstantiationException | IllegalArgumentException
                | InvocationTargetException | NoSuchMethodException | SecurityException e) {
            LOG.error(e.getMessage(), e);
            return null;
        }
    }

    /**
     * Créer une nouvelle instance à partir d'un constructeur avec arguments
     *
     * @param clazz
     * @param types
     * @param values
     * @param <T>
     * @return une nouvelle instance
     */
    public static <T> T newInstance(final Class<T> clazz, final Class<?>[] types,
                                    final Object[] values) {
        try {
            final Constructor<T> constructor = clazz.getConstructor(types);
            return constructor.newInstance(values);
        } catch (NoSuchMethodException | IllegalAccessException | InstantiationException
                | InvocationTargetException e) {
            LOG.error(e.getMessage(), e);
            return null;
        }
    }

    /**
     * Récupérer la valeur d'un champ à partir d'une instance
     *
     * @param instance
     * @param field
     * @param <T>
     * @return la valeur du champ
     */
    @SuppressWarnings("unchecked")
    public static <T> T getValue(final Object instance, final Field field) {
        try {
            field.setAccessible(true);
            final T value = (T) field.get(instance);
            field.setAccessible(false);
            return value;
        } catch (final IllegalAccessException e) {
            LOG.error(e.getMessage(), e);
            return null;
        }
    }

    /**
     * Modifier la valeur d'un champ à partir d'une instance
     *
     * @param instance
     * @param field
     * @param value
     * @param <T>
     */
    public static <T> void setValue(final Object instance, final Field field, final T value) {
        try {
            field.setAccessible(true);
            field.set(instance, value);
            field.setAccessible(false);
        } catch (final IllegalAccessException e) {
            LOG.error(e.getMessage(), e);
        }
    }

    /**
     * Récupérer tous les champs déclarés d'une classe et des super classes
     *
     * @param type
     * @return la liste de tous les champs
     */
    public static List<Field> getAllDeclaredField(final Class<?> type) {
        return getAllDeclaredField(new ArrayList<>(), type);
    }

    /**
     * Récupérer tous les champs déclarés d'une classe et des super classes
     *
     * @param fields
     * @param type
     * @return la liste de tous les champs
     */
    private static List<Field> getAllDeclaredField(final List<Field> fields, final Class<?> type) {
        fields.addAll(Arrays.asList(type.getDeclaredFields()));

        if (type.getSuperclass() != null) {
            getAllDeclaredField(fields, type.getSuperclass());
        }

        return fields;
    }

    /**
     * Récupérer tous les champs déclarés d'une classe et des super classes à partir d'une annotaion
     * déclarée
     *
     * @param type
     * @param annotation
     * @return la liste de tous les champs
     */
    public static List<Field> getAllDeclaredField(final Class<?> type,
                                                  final Class<? extends Annotation> annotation) {
        return getAllDeclaredField(new ArrayList<>(), type, annotation);
    }

    /**
     * Récupérer tous les champs déclarés d'une classe et des super classes à partir d'une annotaion
     * déclarée
     *
     * @param fields
     * @param type
     * @param annotation
     * @return la liste de tous les champs
     */
    private static List<Field> getAllDeclaredField(final List<Field> fields, final Class<?> type,
                                                   final Class<? extends Annotation> annotation) {
        for (final Field field : type.getDeclaredFields()) {
            if (field.isAnnotationPresent(annotation)) {
                fields.add(field);
            }
        }

        if (type.getSuperclass() != null) {
            getAllDeclaredField(fields, type.getSuperclass(), annotation);
        }

        return fields;
    }
}

package fr.univangers.masterinfo.ester.service.impl;

import fr.univangers.masterinfo.ester.dao.bean.MemberEsterBean;
import fr.univangers.masterinfo.ester.dao.bean.UserRole;
import fr.univangers.masterinfo.ester.service.contract.AccountServiceContract;
import fr.univangers.masterinfo.ester.service.exception.EsterServiceException;
import fr.univangers.masterinfo.ester.service.form.account.CreateAccountForm;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.transaction.Transactional;

import static org.junit.Assert.assertArrayEquals;

/**
 * Test sur le service compte
 *
 * @author Clémence DURAND
 * @version 1.0
 * @date 17/10/2020
 */
@ContextConfiguration(locations = "classpath:spring-service-test.xml")
@RunWith(SpringJUnit4ClassRunner.class)
@Transactional
public class CreateAccountServiceImplTest {

    /**
     * Le service compte à tester
     */
    @Autowired
    private AccountServiceContract accountService;

    /**
     * Test
     *
     * @throws EsterServiceException
     */
    @Test
    @Rollback(true)
    public void testGetRolesAdmin() throws EsterServiceException {
        final CreateAccountForm createAccountForm = new CreateAccountForm();
        final MemberEsterBean memberEsterBean = new MemberEsterBean();
        memberEsterBean.setRole(UserRole.ADMIN);
        createAccountForm.setSessionUser(memberEsterBean);

        // Tableau des roles attendus
        final UserRole[] expectedsRoles = UserRole.values();

        // Tableau des roles récupérés dans la fonction
        final UserRole[] actualsRoles = this.accountService.getRoles(createAccountForm);

        assertArrayEquals(expectedsRoles, actualsRoles);
    }

    @Test
    @Rollback(true)
    public void testGetRolesAssistant() throws EsterServiceException {
        // TODO
    }

    @Test
    @Rollback(true)
    public void testGetRolesInfirmier() throws EsterServiceException {
        // TODO
    }

    @Test
    @Rollback(true)
    public void testGetRolesMedecin() throws EsterServiceException {
        // TODO
    }

    @Test
    @Rollback(true)
    public void testGetRolesPreventeur() throws EsterServiceException {
        // TODO
    }

    @Test
    @Rollback(true)
    public void testGetRolesSalarie() throws EsterServiceException {
        // TODO
    }
}

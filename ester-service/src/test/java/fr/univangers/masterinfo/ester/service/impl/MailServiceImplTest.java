package fr.univangers.masterinfo.ester.service.impl;

import fr.univangers.masterinfo.ester.service.contract.MailServiceContract;
import fr.univangers.masterinfo.ester.service.exception.EsterServiceException;
import fr.univangers.masterinfo.ester.service.form.account.CreateAccountForm;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.transaction.Transactional;

import static org.junit.Assert.assertTrue;

/**
 * Test sur le service mail
 *
 * @version 1.0
 * @date 04/10/2020
 */
@ContextConfiguration(locations = "classpath:spring-service-test.xml")
@RunWith(SpringJUnit4ClassRunner.class)
@Transactional
public class MailServiceImplTest {

    /**
     * Le service mail à tester
     */
    @Autowired
    private MailServiceContract mailService;

    /**
     * Test d'un envoi de mail de création de compte
     *
     * @throws EsterServiceException
     */
    @Test
    @Rollback(true)
    public void testSendCreateAccount() throws EsterServiceException {
        final CreateAccountForm form = new CreateAccountForm();
        form.setMailAccount("theo.mahauda@gmail.com");

        this.mailService.sendCreateAccountAsync(form);
        assertTrue(true);
    }
}

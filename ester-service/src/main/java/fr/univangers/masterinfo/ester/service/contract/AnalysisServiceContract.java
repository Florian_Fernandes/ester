package fr.univangers.masterinfo.ester.service.contract;

import fr.univangers.masterinfo.ester.dao.bean.EmployeeBean;
import fr.univangers.masterinfo.ester.dao.bean.QuestionBean;
import fr.univangers.masterinfo.ester.dao.bean.ResultBean;
import fr.univangers.masterinfo.ester.dao.exception.EsterDaoException;
import fr.univangers.masterinfo.ester.service.exception.EsterServiceException;
import fr.univangers.masterinfo.ester.service.form.analysis.AccessWebSiteAnalysisForm;
import fr.univangers.masterinfo.ester.service.form.analysis.CompareDataReferencesAnalysisForm;

import java.util.List;

/**
 * Contrat pour gérer les analyses statistiques
 *
 * @version 1.0
 * @date 04/10/2020
 */
public interface AnalysisServiceContract extends AbstractServiceContract {

    /**
     * Récuperer score globale par employee et questionnaire
     *
     * @param form
     * @return score globale par employee et questionnaire
     * @throws EsterServiceException
     */
    int getScoresGolablsByEmployeeAndQuestionnary(AccessWebSiteAnalysisForm form)
            throws EsterServiceException;

    /**
     * Récuperer score globale par employee et questionnaire (compareDataReferance)
     *
     * @param form
     * @return score globale par employee et questionnaire
     * @throws EsterServiceException
     */
    int getScoresGolablsByEmployeeAndQuestionnary(CompareDataReferencesAnalysisForm form)
            throws EsterServiceException;

    /**
     * Récupérer sous forme de table d'entier tous les scores (résultats) des
     * salariés qui sont liés au questionnaire sélectionner à l'exception du score
     * de salarié sélectionner
     *
     *
     * @return list de tous les scores (résultats)
     * @throws EsterServiceException
     */
    List<Long> getlistScoreResults(String dbName, String variables) throws EsterServiceException;

    /**
     * Récupérer le pourcentage d'apparition de la valeur de la variable
     *
     * @param tableReferenceName
     * @param variable
     * @param value
     * @return
     * @throws EsterDaoException
     */
    long getPercentageValueVariable(String tableReferenceName, String variable, int value) throws EsterServiceException;

    /**
     * Récupérer la liste des questions d'un questionnaire
     *
     * @param form
     * @return liste des questions d'un questionnaire
     * @throws EsterServiceException
     */
    List<QuestionBean> getQuestionsPerGroup(AccessWebSiteAnalysisForm form)
            throws EsterServiceException;

    /**
     * Récupérer les résultats d'un salarié
     *
     * @param form
     * @return résultat d'un salarié
     * @throws EsterServiceException
     */
    ResultBean getResultByEmployeeAndQuestionnary(AccessWebSiteAnalysisForm form) throws EsterServiceException;

    /**
     * Récupérer les employees avec leur réponses
     *
     * @param form
     * @return résultat d'un salarié
     * @throws EsterServiceException
     */
    List<EmployeeBean> getEmployeeWithAnswer(AccessWebSiteAnalysisForm form) throws EsterServiceException;
}
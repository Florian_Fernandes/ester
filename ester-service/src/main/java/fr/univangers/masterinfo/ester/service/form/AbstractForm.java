package fr.univangers.masterinfo.ester.service.form;

import fr.univangers.masterinfo.ester.dao.bean.UserBean;
import fr.univangers.masterinfo.ester.service.form.notification.Notification;
import fr.univangers.masterinfo.ester.service.form.notification.NotificationLevel;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Formulaire d'une page
 *
 * @version 1.0
 * @date 04/10/2020
 */
public abstract class AbstractForm implements Serializable {

    /**
     * Serial Version UID
     */
    private static final long serialVersionUID = 1L;

    /**
     * L'identifiant de l'entité
     */
    protected String id;

    /**
     * L'utilisateur courrant connecté au site
     */
    protected UserBean sessionUser;

    /**
     * La liste des notifications à afficher à l'utilisateur
     */
    protected List<Notification> notifications = new ArrayList<>();

    /**
     * Reinitialise les champs du formulaire
     */
    public void reset() {
        this.notifications = new ArrayList<>();
    }

    /**
     * @param msg
     * @param level
     */
    public void notify(final String msg, final NotificationLevel level) {
        final Notification notify = new Notification();
        notify.setLevel(level);
        notify.setMessage(msg);
        this.notifications.add(notify);
    }

    /**
     * @return the id
     */
    public String getId() {
        return this.id;
    }

    /**
     * @param id the id to set
     */
    public void setId(final String id) {
        this.id = id;
    }

    /**
     * @return the sessionUser
     */
    public UserBean getSessionUser() {
        return this.sessionUser;
    }

    /**
     * @param sessionUser the sessionUser to set
     */
    public void setSessionUser(final UserBean sessionUser) {
        this.sessionUser = sessionUser;
    }

    /**
     * @return the notifications
     */
    public List<Notification> getNotifications() {
        return this.notifications;
    }

    /**
     * @param notifications the notifications to set
     */
    public void setNotifications(final List<Notification> notifications) {
        this.notifications = notifications;
    }
}

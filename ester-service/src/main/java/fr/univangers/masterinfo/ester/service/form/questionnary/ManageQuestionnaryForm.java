package fr.univangers.masterinfo.ester.service.form.questionnary;

import fr.univangers.masterinfo.ester.dao.bean.QuestionnaryOperation;
import fr.univangers.masterinfo.ester.service.form.AbstractForm;
import fr.univangers.masterinfo.ester.service.form.Form;
import fr.univangers.masterinfo.ester.service.form.Input;

import java.io.InputStream;
import java.io.OutputStream;

/**
 * @author etudiant
 */
@Form(name = ManageQuestionnaryForm.MANAGE_QUESTIONNARY_FORM)
public class ManageQuestionnaryForm extends AbstractForm {

    /**
     * Nom du formulaire accessible dans la requete via la JSP
     */
    public static final String MANAGE_QUESTIONNARY_FORM = "manageQuestionnaryForm";
    /**
     * Mapping idQuestionnary entre le champ input HTML / attribut Java
     */
    public static final String INPUT_ID_QUESTIONNARY = "idQuestionnary";
    /**
     * Serial Version UID
     */
    private static final long serialVersionUID = 1L;
    /**
     * idQuestionnary
     */
    @Input(name = INPUT_ID_QUESTIONNARY)
    private String idQuestionnary;
    /**
     * /**
     * L'action effectué (save, update, etc.)
     */
    private QuestionnaryOperation operation;

    /**
     * Le flux pour écrire dans la réponse
     */
    private OutputStream os;

    /**
     * Le flux pour lire les données de la réponse
     */
    private InputStream in;

    /**
     * Le contenu JSON renvoyé par le client
     */
    private String jsonContent;

    /**
     * @return idQuestionnary
     */
    public String getIdQuestionnary() {
        return this.idQuestionnary;
    }

    /**
     * @param idQuestionnary
     */
    public void setIdQuestionnary(final String idQuestionnary) {
        this.idQuestionnary = idQuestionnary;
    }

    /**
     * @return the operation
     */
    public QuestionnaryOperation getOperation() {
        return this.operation;
    }

    /**
     * @param operation the operation to set
     */
    public void setOperation(final QuestionnaryOperation operation) {
        this.operation = operation;
    }

    /**
     * @return the os
     */
    public OutputStream getOs() {
        return this.os;
    }

    /**
     * @param os the os to set
     */
    public void setOs(final OutputStream os) {
        this.os = os;
    }

    /**
     * @return the in
     */
    public InputStream getIn() {
        return this.in;
    }

    /**
     * @param in the in to set
     */
    public void setIn(final InputStream in) {
        this.in = in;
    }

    /**
     * @return the jsonContent
     */
    public String getJsonContent() {
        return this.jsonContent;
    }

    /**
     * @param jsonContent the jsonContent to set
     */
    public void setJsonContent(final String jsonContent) {
        this.jsonContent = jsonContent;
    }
}

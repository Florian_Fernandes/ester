package fr.univangers.masterinfo.ester.service.impl;

import fr.univangers.masterinfo.ester.service.contract.AbstractServiceContract;

/**
 * Service générique implémentant le CRUD
 *
 * @version 1.0
 * @date 04/10/2020
 */
public abstract class AbstractServiceImpl implements AbstractServiceContract {

}

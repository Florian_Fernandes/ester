package fr.univangers.masterinfo.ester.service.impl;

import fr.univangers.masterinfo.ester.dao.bean.PictureBean;
import fr.univangers.masterinfo.ester.dao.bean.VideoBean;
import fr.univangers.masterinfo.ester.dao.contract.PictureDaoContract;
import fr.univangers.masterinfo.ester.dao.contract.VideoDaoContract;
import fr.univangers.masterinfo.ester.dao.criteria.PictureCriteria;
import fr.univangers.masterinfo.ester.dao.criteria.VideoCriteria;
import fr.univangers.masterinfo.ester.dao.exception.EsterDaoException;
import fr.univangers.masterinfo.ester.service.contract.DocumentServiceContract;
import fr.univangers.masterinfo.ester.service.exception.EsterServiceException;
import fr.univangers.masterinfo.ester.service.pojo.PicturePojo;
import fr.univangers.masterinfo.ester.service.pojo.VideoPojo;
import org.hibernate.ogm.datastore.mongodb.type.GridFS;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Service pour gérer les données (références et salariés)
 *
 * @version 1.0
 * @date 04/10/2020
 */
@Service
public class DocumentServiceImpl extends AbstractServiceImpl implements DocumentServiceContract {

    /**
     * DAO Video
     */
    @Autowired
    private VideoDaoContract videoDao;

    /**
     * DAO Image
     */
    @Autowired
    private PictureDaoContract pictureDao;

    @Override
    public void uploadVideo(final VideoPojo videoPojo) throws EsterServiceException {
        final VideoBean videoBean = new VideoBean();
        final GridFS gridFs = new GridFS(videoPojo.getFileInputStream());
        videoBean.setGridFs(gridFs);
        videoBean.setCode(videoPojo.getCode());

        try {
            this.videoDao.create(videoBean);
        } catch (final EsterDaoException e) {
            throw new EsterServiceException(e.getMessage(), "Erreur dans la recherche en base de données");
        }
    }

    @Override
    public VideoPojo downloadVideo(final String code) throws EsterServiceException {
        try {
            final VideoCriteria videoCriteria = new VideoCriteria();
            videoCriteria.setCode(code);

            final VideoBean videoBean = this.videoDao.findVideoByCode(videoCriteria);

            final VideoPojo videoPojo = new VideoPojo();
            videoPojo.setCode(videoBean.getCode());
            videoPojo.setFileInputStream(videoBean.getGridFs().getInputStream());

            return videoPojo;
        } catch (final EsterDaoException e) {
            throw new EsterServiceException(e.getMessage(), "Erreur dans la recherche en base de données");
        }
    }

    @Override
    public void uploadPicture(final PicturePojo picturePojo) throws EsterServiceException {
        final PictureBean pictureBean = new PictureBean();
        final GridFS gridFs = new GridFS(picturePojo.getFileInputStream());
        pictureBean.setGridFs(gridFs);
        pictureBean.setCode(picturePojo.getCode());

        try {
            this.pictureDao.create(pictureBean);
        } catch (final EsterDaoException e) {
            throw new EsterServiceException(e.getMessage(), "Erreur dans la recherche en base de données");
        }
    }

    @Override
    public PicturePojo downloadPicture(final String code) throws EsterServiceException {
        try {
            final PictureCriteria pictureCriteria = new PictureCriteria();
            pictureCriteria.setCode(code);

            final PictureBean pictureBean = this.pictureDao.findPictureByCode(pictureCriteria);

            final PicturePojo picturePojo = new PicturePojo();
            picturePojo.setCode(pictureBean.getCode());
            picturePojo.setFileInputStream(pictureBean.getGridFs().getInputStream());

            return picturePojo;
        } catch (final EsterDaoException e) {
            throw new EsterServiceException(e.getMessage(), "Erreur dans la recherche en base de données");
        }
    }
}

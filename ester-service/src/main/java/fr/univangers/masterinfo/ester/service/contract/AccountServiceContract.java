package fr.univangers.masterinfo.ester.service.contract;

import fr.univangers.masterinfo.ester.dao.bean.*;
import fr.univangers.masterinfo.ester.service.exception.EsterServiceException;
import fr.univangers.masterinfo.ester.service.form.account.CreateAccountForm;
import fr.univangers.masterinfo.ester.service.form.account.EmployeeAccountForm;
import org.hibernate.service.spi.ServiceException;

import java.util.ArrayList;
import java.util.List;

/**
 * Contrat pour gérer les comptes
 *
 * @version 1.0
 * @date 04/10/2020
 */
public interface AccountServiceContract extends AbstractServiceContract {

    /**
     * Récupère la liste des rôles pour créer un compte selon l'utilisateur connecté
     *
     * @param createAccountForm
     * @return roles
     * @throws EsterServiceException
     */
    UserRole[] getRoles(CreateAccountForm createAccountForm) throws EsterServiceException;

    /**
     * Enregistrer un utilisateur dans la base de données
     *
     * @param createAccountForm
     * @throws EsterServiceException
     */
    void registerUser(CreateAccountForm createAccountForm) throws EsterServiceException;

    /**
     * Vérification qu'une adresse mail n'est pas déjà relié à un utilisateur en
     * base de données
     *
     * @param createAccountForm
     * @return vrai si le mail existe
     * @throws EsterServiceException
     */
    boolean checkEmailExist(CreateAccountForm createAccountForm) throws EsterServiceException;

    /**
     * Vérification qu'un login n'est pas déjà relié à un utilisateur en
     * base de données
     *
     * @param createAccountForm
     * @return vrai si le login exist
     * @throws EsterServiceException
     */
    boolean checkLoginExist(CreateAccountForm createAccountForm) throws EsterServiceException;

    /**
     * Récupérer la liste de tous les salariés
     *
     * @return la liste de tous les questionnaires
     * @throws ServiceException
     */
    List<EmployeeBean> findAll() throws EsterServiceException;

    /**
     * Récupérer une liste de membre ester médecin et admin
     *
     * @return
     * @throws EsterServiceException
     */
    List<MemberEsterBean> findMemberEsterAdminAndMedecin() throws EsterServiceException;

    /**
     * Récupérer tous les membres ester à l'exception de l'utilisateur courant
     * @param ID
     * @return
     */
    List<MemberEsterBean> findAllMemberEsterExceptCurrentUser(String ID) throws EsterServiceException;

    /**
     * Récupérer un salarié via son ID passé en paramètre
     *
     * @param ID
     * @return
     * @throws EsterServiceException
     */
    EmployeeBean findEmployeeByID(String ID) throws EsterServiceException;

    /**
     * Créer un login basé sur le nom et le prenom d'un membre ester
     *
     * @param CreateAccountForm
     * @return String
     * @throws EsterServiceException
     */
    void createLoginFromName(CreateAccountForm CreateAccountForm) throws EsterServiceException;

    /**
     * Check si l'utilisateur peut accéder à un service
     * return null si authorisé
     * return le chemin de la jsp sinon (pour se connecté ou error)
     */
    String checkIfUserCanAccess(UserBean user, UserRole minRole);

    /**
     * Modifie le compte salarié
     *
     * @param form
     * @throws EsterServiceException
     */
    void modifyEmployee(EmployeeAccountForm form) throws EsterServiceException;

    ArrayList<EmployeeQuestionnaires> getMappingEmployee(List<EmployeeBean> employeeBeans);

    class EmployeeQuestionnaires {
        public EmployeeBean employeeBean;
        public ArrayList<QuestionnaryBean> questionnaireAnswered;
        public ArrayList<QuestionnaryBean> questionnaireNotAnswered;

        public EmployeeQuestionnaires(EmployeeBean employeeBean, ArrayList<QuestionnaryBean> questionnaireAnswered, ArrayList<QuestionnaryBean> questionnaireNotAnswered) {
            this.employeeBean = employeeBean;
            this.questionnaireAnswered = questionnaireAnswered;
            this.questionnaireNotAnswered = questionnaireNotAnswered;
        }

        public EmployeeBean getEmployeeBean() {
            return employeeBean;
        }

        public void setEmployeeBean(EmployeeBean employeeBean) {
            this.employeeBean = employeeBean;
        }

        public ArrayList<QuestionnaryBean> getQuestionnaireAnswered() {
            return questionnaireAnswered;
        }

        public void setQuestionnaireAnswered(ArrayList<QuestionnaryBean> questionnaireAnswered) {
            this.questionnaireAnswered = questionnaireAnswered;
        }

        public ArrayList<QuestionnaryBean> getQuestionnaireNotAnswered() {
            return questionnaireNotAnswered;
        }

        public void setQuestionnaireNotAnswered(ArrayList<QuestionnaryBean> questionnaireNotAnswered) {
            this.questionnaireNotAnswered = questionnaireNotAnswered;
        }
    }
}
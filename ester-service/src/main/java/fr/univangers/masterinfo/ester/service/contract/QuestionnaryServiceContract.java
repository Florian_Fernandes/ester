package fr.univangers.masterinfo.ester.service.contract;

import fr.univangers.masterinfo.ester.dao.bean.EmployeeBean;
import fr.univangers.masterinfo.ester.dao.bean.MemberEsterBean;
import fr.univangers.masterinfo.ester.dao.bean.QuestionnaryBean;
import fr.univangers.masterinfo.ester.service.exception.EsterServiceException;
import fr.univangers.masterinfo.ester.service.form.questionnary.AnswerQuestionnaryForm;
import fr.univangers.masterinfo.ester.service.form.questionnary.AssignQuestionnaryForm;
import fr.univangers.masterinfo.ester.service.form.questionnary.ShareQuestionnaryForm;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * Contrat pour gérer les questionnaires
 *
 * @version 1.0
 * @date 04/10/2020
 */
public interface QuestionnaryServiceContract extends AbstractServiceContract {

    /**
     * Récupérer la liste de tous les questionnaires
     *
     * @return la liste de tous les questionnaires
     * @throws EsterServiceException
     */
    List<QuestionnaryBean> getListQuestionnary() throws EsterServiceException;

    /**
     * Récupérer un questionnaire par son identifiant via un formulaire CompareDataReference
     *
     * @param id
     * @return
     * @throws EsterServiceException
     */
    QuestionnaryBean getQuestionnaryByID(final String id)
            throws EsterServiceException;

    /**
     * Sauvegarder un questionnaire
     *
     * @param questionnary
     * @return le questionnaire sauvegardé
     * @throws EsterServiceException
     */
    QuestionnaryBean saveQuestionnary(final QuestionnaryBean questionnary)
            throws EsterServiceException;


    /**
     * Sauvegarder ou mettre à jour un questionnaire à partir d'une requête http
     *
     * @param request
     * @return le questionnaire
     * @throws EsterServiceException
     */
    QuestionnaryBean saveOrUpdateQuestionnary(final HttpServletRequest request)
            throws EsterServiceException;


    /**
     * Mettre à jour un questionnaire
     *
     * @param questionnaryBean
     */
    void updateQuestionnary(final QuestionnaryBean questionnaryBean)
            throws EsterServiceException;

    /**
     * Mettre à jour un questionnaire
     *
     * @param questionnaryid
     */
    void publishQuestionnaryById(final String questionnaryid)
            throws EsterServiceException;

    /**
     * Supprimer un questionnaire
     *
     * @param questionnaryid
     */
    void deleteQuestionnaryById(final String questionnaryid)
            throws EsterServiceException;

    /**
     * Récupérer la liste des questionnaires non répondu d'un salarié
     *
     * @param form
     * @return les questionnaires
     * @throws EsterServiceException
     */
    Set<QuestionnaryBean> getQuestionnariesNoAnswered(AnswerQuestionnaryForm form)
            throws EsterServiceException;

    /**
     * Récupérer un questionnaire en format JSON par son identifiant
     *
     * @param form
     * @return le questionnaire en format JSON
     * @throws EsterServiceException
     */
    String getQuestionnaryAsJsonById(AnswerQuestionnaryForm form) throws EsterServiceException;

    /**
     * Récupérer un questionnaire en format objet par son identifiant
     *
     * @param form
     * @return le questionnaire en format JSON
     * @throws EsterServiceException
     */
    QuestionnaryBean getQuestionnaryAsObjectById(AnswerQuestionnaryForm form)
            throws EsterServiceException;

    /**
     * Enregistrer le résultat d'un questionnaire répondu par le salarié
     *
     * @param form
     * @throws EsterServiceException
     */
    void saveResultQuestionnary(final AnswerQuestionnaryForm form, int score) throws EsterServiceException;

    /**
     * Recupere la liste des questionnaires
     *
     * @throws EsterServiceException
     */
    List<QuestionnaryBean> findAll() throws EsterServiceException;

    /**
     * Recupere la liste des questionnaires publié
     *
     * @throws EsterServiceException
     */
    List<QuestionnaryBean> findJustPublished() throws EsterServiceException;

    List<QuestionnaryBean> findJustPublishedandMember(MemberEsterBean esterBean) throws EsterServiceException;

    List<QuestionnaryBean> findJustMember(MemberEsterBean esterBean) throws EsterServiceException;

    /**
     * Affecter un questionnaire a un salarié
     *
     * @param form
     * @throws EsterServiceException
     */
    void assignQuestionnaryToEmployee(AssignQuestionnaryForm form) throws EsterServiceException;

    /**
     * Affecter un droit de modification d'un questionnaire a un membre ester (admin
     * ou médecin)
     *
     * @param form
     * @throws EsterServiceException
     */
    void shareQuestionnaryToMemberEster(ShareQuestionnaryForm form) throws EsterServiceException;

    void shareQuestionnaryToMemberEsterOnCreation(String idQuestionnaryToShare, String idMemberEsterToChare)
            throws EsterServiceException;

    /*
     *supprimer un questionaire de la liste des questionaire repondu ou pas encore d'un employer
     *
     */
    void deleteQuestionnaryFromEmployee(QuestionnaryBean questionnary) throws EsterServiceException;

    /*
     *supprimer les question d'un questionnaire don les options
     */
    void deleteQuestionsFromQuestionnary(QuestionnaryBean questionnary) throws EsterServiceException;

    /*
     * la supprission des resultats d'un questionnaire et les reponse
     */
    void deleteResultatsFromQuestionnary(QuestionnaryBean questionnary) throws EsterServiceException;

    /*
     * la supprission des reponse qui n'ont pas de données
     */
    void deleteallAnsewersWithNotData() throws EsterServiceException;

    /*
     * la supprission des resultats qui n'ont pas de données
     */
    void deleteallResultsWithNotData() throws EsterServiceException;

    /*
     * la supprission des option qui de questions
     */
    void deleteallOptionsWithNotData() throws EsterServiceException;

    boolean verfierMember(MemberEsterBean memberEsterBean, QuestionnaryBean questionnaryBean) throws EsterServiceException;
}
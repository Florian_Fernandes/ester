package fr.univangers.masterinfo.ester.service.form.questionnary;

import fr.univangers.masterinfo.ester.dao.bean.QuestionBean;
import fr.univangers.masterinfo.ester.dao.bean.QuestionOptionBean;
import fr.univangers.masterinfo.ester.dao.bean.QuestionnaryBean;
import fr.univangers.masterinfo.ester.service.form.AbstractForm;
import fr.univangers.masterinfo.ester.service.form.Form;
import fr.univangers.masterinfo.ester.service.form.Input;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * @author etudiant
 */
@Form(name = AnswerQuestionnaryForm.ANSWER_QUESTIONNARY_FORM)
public class AnswerQuestionnaryForm extends AbstractForm {

    /**
     * Nom du formulaire accessible dans la requete via la JSP
     */
    public static final String ANSWER_QUESTIONNARY_FORM = "answerQuestionnaryForm";
    /**
     * Mapping questionnaire entre le champ input / attribut java
     */
    public static final String INPUT_SELECTED_QUESTIONNARY = "selectedQuestionnary";
    /**
     * Mapping questionnaire entre le champ input / attribut java
     */
    public static final String INPUT_QUESTIONNARY_ANSWERED = "questionnaryAnswered";
    /**
     * Serial Version UID
     */
    private static final long serialVersionUID = 1L;
    /**
     * Liste des questionnaires non répondus
     */
    private Set<QuestionnaryBean> questionnariesNoAnswered = new HashSet<>();

    /**
     * Le questionnaire sélectionné par le salarié dans la liste
     */
    @Input(name = INPUT_SELECTED_QUESTIONNARY)
    private String selectedQuestionnary;

    private QuestionnaryBean questionnary;

    /**
     * Le questionnaire sélectionné au format JSON
     */
    private String jsonQuestionnary;

    /**
     * Le questionnaire est-il répondu ?
     */
    @Input(name = INPUT_QUESTIONNARY_ANSWERED)
    private boolean questionnaryAnswered;

    /**
     * Liste des options sélectionnés par l'utilisateur dans les questions
     */
    private Set<QuestionOptionBean> questionsOptionSelected = new HashSet<>();

    /**
     * Liste des question courtes et longues
     */
    private Map<QuestionBean, String> questionsSelected = new HashMap<>();

    /**
     * @return the questionnariesNoAnswered
     */
    public Set<QuestionnaryBean> getQuestionnariesNoAnswered() {
        return this.questionnariesNoAnswered;
    }

    /**
     * @param questionnariesNoAnswered the questionnariesNoAnswered to set
     */
    public void setQuestionnariesNoAnswered(final Set<QuestionnaryBean> questionnariesNoAnswered) {
        this.questionnariesNoAnswered = questionnariesNoAnswered;
    }

    /**
     * @return the selectedQuestionnary
     */
    public String getSelectedQuestionnary() {
        return this.selectedQuestionnary;
    }

    /**
     * @param selectedQuestionnary the selectedQuestionnary to set
     */
    public void setSelectedQuestionnary(final String selectedQuestionnary) {
        this.selectedQuestionnary = selectedQuestionnary;
    }

    /**
     * @return the jsonQuestionnary
     */
    public String getJsonQuestionnary() {
        return this.jsonQuestionnary;
    }

    /**
     * @param jsonQuestionnary the jsonQuestionnary to set
     */
    public void setJsonQuestionnary(final String jsonQuestionnary) {
        this.jsonQuestionnary = jsonQuestionnary;
    }

    /**
     * @return the questionnaryAnswered
     */
    public boolean isQuestionnaryAnswered() {
        return this.questionnaryAnswered;
    }

    /**
     * @param questionnaryAnswered the questionnaryAnswered to set
     */
    public void setQuestionnaryAnswered(final boolean questionnaryAnswered) {
        this.questionnaryAnswered = questionnaryAnswered;
    }

    /**
     * @return the questionsOptionSelected
     */
    public Set<QuestionOptionBean> getQuestionsOptionSelected() {
        return this.questionsOptionSelected;
    }

    /**
     * @param questionsOptionSelected the questionsOptionSelected to set
     */
    public void setQuestionsOptionSelected(final Set<QuestionOptionBean> questionsOptionSelected) {
        this.questionsOptionSelected = questionsOptionSelected;
    }

    /**
     * @return the questionnary
     */
    public QuestionnaryBean getQuestionnary() {
        return this.questionnary;
    }

    /**
     * @param questionnary the questionnary to set
     */
    public void setQuestionnary(final QuestionnaryBean questionnary) {
        this.questionnary = questionnary;
    }

    /**
     * @return the question selected
     */
    public Map<QuestionBean, String> getQuestionsSelected() {
        return questionsSelected;
    }

    /**
     * @param questionsSelected the questionnary to set
     */
    public void setQuestionsSelected(Map<QuestionBean, String> questionsSelected) {
        this.questionsSelected = questionsSelected;
    }
}

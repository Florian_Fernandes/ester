package fr.univangers.masterinfo.ester.service.form.validation;

import fr.univangers.masterinfo.ester.service.form.AbstractForm;
import fr.univangers.masterinfo.ester.service.form.notification.NotificationLevel;
import org.apache.commons.lang3.StringUtils;

import java.util.regex.Pattern;

/**
 * Validation d'une adresse mail
 *
 * @author Clémence DURAND
 * @version 1.0
 * @date 24/10/2020
 */
public class MailValidation {

    /**
     * Expression régulière pour valider une adresse mail
     */
    private static final String REGEX_MAIL = "^(.+)@(.+)$";

    /**
     * Machine à état pour vérifier la validité d'une adresse mail
     */
    private static final Pattern PATTERN_MAIL = Pattern.compile(REGEX_MAIL);

    /**
     * Longueur maximum d'une adresse mail
     */
    private static final int LENGTH_MAIL = 255;

    /**
     * Constructeur privé
     */
    private MailValidation() {

    }

    /**
     * @param form
     * @param mail
     * @return vrai si une adresse mail est valide. Faux sinon.
     */
    public static boolean isMailValid(final AbstractForm form, final String mail) {

        if (StringUtils.isBlank(mail)) {
            form.notify("Le mail n'est pas renseigné !", NotificationLevel.ERROR);
            return false;
        }
        if (mail.length() > LENGTH_MAIL) {
            form.notify("Le mail est trop long !", NotificationLevel.ERROR);
            return false;
        }
        if (!PATTERN_MAIL.matcher(mail).matches()) {
            form.notify("L’adresse mail ne respecte pas le format attendu !",
                    NotificationLevel.ERROR);
            return false;
        }
        return true;
    }
}

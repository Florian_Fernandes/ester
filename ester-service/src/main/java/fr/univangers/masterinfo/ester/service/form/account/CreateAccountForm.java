package fr.univangers.masterinfo.ester.service.form.account;

import fr.univangers.masterinfo.ester.dao.bean.UserRole;
import fr.univangers.masterinfo.ester.service.form.AbstractForm;
import fr.univangers.masterinfo.ester.service.form.Form;
import fr.univangers.masterinfo.ester.service.form.Input;
import fr.univangers.masterinfo.ester.service.form.validation.order.One;
import fr.univangers.masterinfo.ester.service.form.validation.order.Three;
import fr.univangers.masterinfo.ester.service.form.validation.order.Two;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * Formulaire première connexion
 *
 * @author Clémence DURAND
 * @version 1.0
 * @date 14/10/2020
 */
@Form(name = CreateAccountForm.CREATE_ACCOUNT_FORM)
public class CreateAccountForm extends AbstractForm {

    /**
     * Nom du formulaire accessible dans la requete pour la JSP
     */
    public static final String CREATE_ACCOUNT_FORM = "createAccountForm";
    /**
     * Mapping select role entre le champ input HTML / attribut Java
     */
    public static final String SELECT_ROLE_ACCOUNT = "selectedRole";
    /**
     * Mapping email entre le champ input HTML / attribut Java
     */
    public static final String INPUT_MAIL_ACCOUNT = "mailAccount";
    /**
     * Mapping login entre le champ input HTML / attribut Java
     */
    public static final String INPUT_LOGIN_ACCOUNT = "loginAccount";
    /**
     * Mapping login entre le champ input HTML / attribut Java
     */
    public static final String INPUT_NAME_ACCOUNT = "nameAccount";
    /**
     * Mapping login entre le champ input HTML / attribut Java
     */
    public static final String INPUT_FAMILY_NAME_ACCOUNT = "familyNameAccount";
    /**
     * Mapping sexe entre le champ input HTML / attribut Java
     */
    public static final String INPUT_SEXE_ACCOUNT = "sexeAccount";
    /**
     * Mapping annee entre le champ input HTML / attribut Java
     */
    public static final String INPUT_BIRTH_ACCOUNT = "birthAccount";
    /**
     * Mapping PCS entre le champ input HTML / attribut Java
     */
    public static final String INPUT_PCS_ACCOUNT = "PCSAccount";
    /**
     * Mapping NAF entre le champ input HTML / attribut Java
     */
    public static final String INPUT_NAF_ACCOUNT = "NAFAccount";
    /**
     * Mapping commentaires entre le champ input HTML / attribut Java
     */
    public static final String INPUT_COMMENTAIRE_ACCOUNT = "CommentaireAccount";
    /**
     * Serial Version UID
     */
    private static final long serialVersionUID = 1L;
    /**
     * Liste des rôles
     */
    private UserRole[] roles = new UserRole[0];

    /**
     * Le type de compte
     */
    @Input(name = SELECT_ROLE_ACCOUNT)
    @NotNull(message = "{role.not.null.msg}", groups = One.class)
    private UserRole selectedRole;

    /**
     * Le mail
     */
    @Input(name = INPUT_MAIL_ACCOUNT)
    @NotBlank(message = "{mail.not.blank.msg}", groups = One.class)
    @Size(message = "{mail.size.msg}", groups = Two.class)
    @Email(message = "{mail.pattern.msg}", regexp = "{mail.pattern.regexp}", groups = Three.class)
    private String mailAccount;

    /**
     * Le login
     */
    @Input(name = INPUT_LOGIN_ACCOUNT)
    @NotBlank(message = "{login.not.blank.msg}", groups = One.class)
    @Size(message = "{login.size.msg}", groups = Two.class)
    private String loginAccount;

    /**
     * Le prénom
     */
    @Input(name = INPUT_NAME_ACCOUNT)
    @NotBlank(message = "{login.not.blank.msg}", groups = One.class)
    @Size(message = "{login.size.msg}", groups = Two.class)
    private String nameAccount;

    /**
     * Le nom
     */
    @Input(name = INPUT_FAMILY_NAME_ACCOUNT)
    @NotBlank(message = "{login.not.blank.msg}", groups = One.class)
    @Size(message = "{login.size.msg}", groups = Two.class)
    private String familyNameAccount;

    /**
     * Le sexe
     */
    @Input(name = INPUT_SEXE_ACCOUNT)
    @NotBlank(message = "{sexe.not.blank.msg}", groups = One.class)
    @Size(message = "{sexe.size.msg}", groups = Two.class)
    private String sexeAccount;

    /**
     * L'année de naissance
     */
    @Input(name = INPUT_BIRTH_ACCOUNT)
    @NotBlank(message = "{birth.not.blank.msg}", groups = One.class)
    @Size(message = "{birth.size.msg}", groups = Two.class)
    private String birthAccount;

    /**
     * Le PCS
     */
    @Input(name = INPUT_PCS_ACCOUNT)
    @NotBlank(message = "{pcs.not.blank.msg}", groups = One.class)
    @Size(message = "{pcs.size.msg}", groups = Two.class)
    private String PCSAccount;

    /**
     * Le NAF
     */
    @Input(name = INPUT_NAF_ACCOUNT)
    @NotBlank(message = "{naf.not.blank.msg}", groups = One.class)
    @Size(message = "{naf.size.msg}", groups = Two.class)
    private String NAFAccount;

    /**
     * Les commentaires
     */
    @Input(name = INPUT_COMMENTAIRE_ACCOUNT)
    @NotBlank(message = "{commentaire.not.blank.msg}", groups = One.class)
    @Size(message = "{commentaire.size.msg}", groups = Two.class)
    private String CommentaireAccount;

    /**
     * @return the selectedRole
     */
    public UserRole getSelectedRole() {
        return this.selectedRole;
    }

    /**
     * @param selectedRole the selectedRole to set
     */
    public void setSelectedRole(final UserRole selectedRole) {
        this.selectedRole = selectedRole;
    }

    /**
     * @return the mailAccount
     */
    public String getMailAccount() {
        return this.mailAccount;
    }

    /**
     * @param mailAccount the mailAccount to set
     */
    public void setMailAccount(final String mailAccount) {
        this.mailAccount = mailAccount;
    }

    /**
     * @return the roles
     */
    public UserRole[] getRoles() {
        return this.roles;
    }

    /**
     * @param roles the roles to set
     */
    public void setRoles(final UserRole[] roles) {
        this.roles = roles;
    }

    /**
     * @return the login
     */
    public String getLoginAccount() {
        return this.loginAccount;
    }

    /**
     * @param loginAccount the login to set
     */
    public void setLoginAccount(final String loginAccount) {
        this.loginAccount = loginAccount;
    }

    public String getNameAccount() {
        return nameAccount;
    }

    public void setNameAccount(String nameAccount) {
        this.nameAccount = nameAccount;
    }

    public String getFamilyNameAccount() {
        return familyNameAccount;
    }

    public void setFamilyNameAccount(String familyNameAccount) {
        this.familyNameAccount = familyNameAccount;
    }

    /**
     * @return the sexe
     */
    public String getSexeAccount() {
        return this.sexeAccount;
    }

    /**
     * @param sexeAccount the sexe to set
     */
    public void setSexeAccount(final String sexeAccount) {
        this.sexeAccount = sexeAccount;
    }

    /**
     * @return the birth
     */
    public String getBirthAccount() {
        return this.birthAccount;
    }

    /**
     * @param birthAccount the birth to set
     */
    public void setBirthAccount(final String birthAccount) {
        this.birthAccount = birthAccount;
    }

    /**
     * @return the pcs
     */
    public String getPCSAccount() {
        return this.PCSAccount;
    }

    /**
     * @param PCSAccount the pcs to set
     */
    public void setPCSAccount(final String PCSAccount) {
        this.PCSAccount = PCSAccount;
    }

    /**
     * @return the naf
     */
    public String getNAFAccount() {
        return this.NAFAccount;
    }

    /**
     * @param NAFAccount the naf to set
     */
    public void setNAFAccount(final String NAFAccount) {
        this.NAFAccount = NAFAccount;
    }

    /**
     * @return the commentaire
     */
    public String getCommentaireAccount() {
        return this.CommentaireAccount;
    }

    /**
     * @param CommentaireAccount the commentaire to set
     */
    public void setCommentaireAccount(final String CommentaireAccount) {
        this.CommentaireAccount = CommentaireAccount;
    }
}

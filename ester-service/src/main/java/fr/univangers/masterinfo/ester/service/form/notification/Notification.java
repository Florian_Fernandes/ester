package fr.univangers.masterinfo.ester.service.form.notification;

import java.io.Serializable;

/**
 * Message de notification à afficher à l'utilisateur
 *
 * @version 1.0
 * @date 04/10/2020
 */
public class Notification implements Serializable {

    /**
     * Serial Version UID
     */
    private static final long serialVersionUID = 1L;

    /**
     * Le niveau d'alerte
     */
    private NotificationLevel level;

    /**
     * Le message à afficher
     */
    private String message;

    /**
     * @return the level
     */
    public NotificationLevel getLevel() {
        return this.level;
    }

    /**
     * @param level the level to set
     */
    public void setLevel(final NotificationLevel level) {
        this.level = level;
    }

    /**
     * @return the message
     */
    public String getMessage() {
        return this.message;
    }

    /**
     * @param message the message to set
     */
    public void setMessage(final String message) {
        this.message = message;
    }
}

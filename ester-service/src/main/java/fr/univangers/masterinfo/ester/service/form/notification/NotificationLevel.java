package fr.univangers.masterinfo.ester.service.form.notification;

/**
 * Les niveaux d'alertes
 *
 * @version 1.0
 * @date 04/10/2020
 */
public enum NotificationLevel {
    /**
     * Erreur
     */
    ERROR,
    /**
     * Avertissement
     */
    WARNING,
    /**
     * Succès
     */
    SUCCESS;
}

package fr.univangers.masterinfo.ester.service.exception;

/**
 * Exception de la couche service
 *
 * @version 1.0
 * @date 04/10/2020
 */
public class EsterServiceException extends Exception {

    /**
     * Serial Version UID
     */
    private static final long serialVersionUID = 1L;

    /**
     * ce message nous servira à informer l'utilisateur du type d'erreur que le programme a rencontré
     */
    private String helperMessage;

    /**
     * @param message
     */
    public EsterServiceException(final String message) {
        super(message);
        helperMessage = "";
    }


    /**
     * Constructeur
     *
     * @param message
     * @param helperMessage
     */
    public EsterServiceException(final String message, final String helperMessage) {
        super(message);
        this.helperMessage = helperMessage;
    }

    public String getHelperMessage() {
        return helperMessage;
    }

    public boolean hasHelperMessage() {
        return !(helperMessage.isEmpty());
    }
}

package fr.univangers.masterinfo.ester.service.contract;

import fr.univangers.masterinfo.ester.service.form.AbstractForm;

/**
 * Contrat pour gérer de la validation (formulaire, etc.)
 *
 * @version 1.0
 * @date 04/10/2020
 */
public interface ValidatorServiceContract extends AbstractServiceContract {
    /**
     * Vérifier si tous les champs d'un formulaire sont valides
     *
     * @param form
     * @return vrai ou faux
     */
    boolean formIsValid(AbstractForm form);
}

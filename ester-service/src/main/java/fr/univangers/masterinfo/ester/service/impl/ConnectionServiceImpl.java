package fr.univangers.masterinfo.ester.service.impl;

import fr.univangers.masterinfo.ester.dao.bean.EmployeeBean;
import fr.univangers.masterinfo.ester.dao.bean.MemberEsterBean;
import fr.univangers.masterinfo.ester.dao.bean.UserBean;
import fr.univangers.masterinfo.ester.dao.bean.UserGroup;
import fr.univangers.masterinfo.ester.dao.contract.EmployeeDaoContract;
import fr.univangers.masterinfo.ester.dao.contract.MemberEsterDaoContract;
import fr.univangers.masterinfo.ester.dao.criteria.EmployeeCriteria;
import fr.univangers.masterinfo.ester.dao.criteria.MemberEsterCriteria;
import fr.univangers.masterinfo.ester.dao.exception.EsterDaoException;
import fr.univangers.masterinfo.ester.service.contract.ConnectionServiceContract;
import fr.univangers.masterinfo.ester.service.exception.EsterServiceException;
import fr.univangers.masterinfo.ester.service.form.connection.LoginConnectionForm;
import fr.univangers.masterinfo.ester.service.form.notification.Notification;
import fr.univangers.masterinfo.ester.service.form.notification.NotificationLevel;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Service pour connecter un utilisateur
 *
 * @version 1.0
 * @date 04/10/2020
 */
@Service
public class ConnectionServiceImpl extends AbstractServiceImpl
        implements ConnectionServiceContract {

    /**
     * Log de la classe
     */
    private static final Logger LOG = LogManager.getLogger(ConnectionServiceImpl.class);

    /**
     * La DAO membre ESTER
     */
    @Autowired
    private MemberEsterDaoContract memberEsterDao;


    /**
     * La DAO salarié
     */
    @Autowired
    private EmployeeDaoContract employeeDao;

    @Override
    public UserBean findUser(final LoginConnectionForm loginForm) throws EsterServiceException {
        if (loginForm == null) {
            throw new EsterServiceException("Login form is null", "Erreur dans la recherche en base de données pour trouver un utilisateur");
        }

        try {
            if (UserGroup.MEMBRE_ESTER == loginForm.getGroup()) {
                return this.findMemberEster(loginForm);
            } else if (UserGroup.SALARIE == loginForm.getGroup()) {
                return this.findEmployee(loginForm);
            } else {
                throw new EsterServiceException("Role unknown : " + loginForm.getGroup());
            }
        } catch (final EsterDaoException e) {
            throw new EsterServiceException(e.getMessage(), e.getHelperMessage());
        }
    }

    /**
     * Générer un message d'erreur sur l'identifiant
     *
     * @param loginForm
     */
    private void generateErrorNotificationLogin(final LoginConnectionForm loginForm) {
        final Notification notify = new Notification();
        notify.setLevel(NotificationLevel.ERROR);
        notify.setMessage("L'identifiant est inconnu !");
        loginForm.getNotifications().add(notify);
    }

    /**
     * Générer un message d'erreur sur le mot de passe
     *
     * @param loginForm
     */
    private void generateErrorNotificationPassword(final LoginConnectionForm loginForm) {
        final Notification notify = new Notification();
        notify.setLevel(NotificationLevel.ERROR);
        notify.setMessage("Le mot de passe est incorrect !");
        loginForm.getNotifications().add(notify);
    }

    /**
     * Générer un message de succès de connexion
     *
     * @param loginForm
     */
    private void generateSuccessNotificationConnection(final LoginConnectionForm loginForm) {
        final Notification notify = new Notification();
        notify.setLevel(NotificationLevel.SUCCESS);
        notify.setMessage("Vous êtes connecté. Vous allez être redirigé sur votre tableau de bord");
        loginForm.getNotifications().add(notify);
    }

    /***
     * Vérifier la connexion d'un membre ester
     *
     * @param loginForm
     * @return le membre ester
     * @throws EsterDaoException
     */
    private MemberEsterBean findMemberEster(final LoginConnectionForm loginForm)
            throws EsterDaoException {
        // On crée le critère
        final MemberEsterCriteria memberEsterCriteria = new MemberEsterCriteria();
        memberEsterCriteria.setLoginOrEmail(loginForm.getLoginOrEmail());

        // On vérifie si l'utilisateur existe
        final boolean exist = this.memberEsterDao.existMemberEsterByLoginOrEmail(memberEsterCriteria);


        if (!exist) {
            this.generateErrorNotificationLogin(loginForm);
            return null;
        }

        // On le récupère
        final MemberEsterBean memberEsterBean = this.memberEsterDao
                .findMemberEsterByLoginOrEmail(memberEsterCriteria);


        if (memberEsterBean == null) {
            return null;
        }

        // On vérifie le mot de passe s'ils sont égaux

        final boolean equals = StringUtils.equals(memberEsterBean.getPassword(),
                loginForm.getPassword());


        if (!equals) {
            this.generateErrorNotificationPassword(loginForm);
            return null;
        }

        // A la fin on affiche succès à l'utilisateur

        this.generateSuccessNotificationConnection(loginForm);

        return memberEsterBean;
    }

    /***
     * Vérifier la connexion d'un salarié
     *
     * @param loginForm
     * @return le salarié
     * @throws EsterDaoException
     */
    private EmployeeBean findEmployee(final LoginConnectionForm loginForm) throws EsterDaoException {
        // On crée le critère
        final EmployeeCriteria employeeCriteria = new EmployeeCriteria();
        employeeCriteria.setLogin(loginForm.getLoginOrEmail());

        // On vérifie si l'utilisateur existe

        final boolean exist = this.employeeDao.existEmployeeById(employeeCriteria);


        if (!exist) {
            this.generateErrorNotificationLogin(loginForm);
            return null;
        }

        // On le récupère

        final EmployeeBean employeeBean = this.employeeDao.findEmployee(employeeCriteria);


        this.generateSuccessNotificationConnection(loginForm);

        return employeeBean;
    }
}

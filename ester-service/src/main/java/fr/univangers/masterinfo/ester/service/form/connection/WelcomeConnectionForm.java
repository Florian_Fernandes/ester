package fr.univangers.masterinfo.ester.service.form.connection;

import fr.univangers.masterinfo.ester.service.form.AbstractForm;
import fr.univangers.masterinfo.ester.service.form.Form;

/**
 * Formulaire de bienvenu
 *
 * @version 1.0
 * @date 04/10/2020
 */
@Form(name = WelcomeConnectionForm.WELCOME_CONNECTION_FORM)
public class WelcomeConnectionForm extends AbstractForm {

    /**
     * Nom du formulaire accessible dans la requete via la JSP
     */
    public static final String WELCOME_CONNECTION_FORM = "welcomeConnectionForm";
    /**
     * Serial Version UID
     */
    private static final long serialVersionUID = 1L;
}

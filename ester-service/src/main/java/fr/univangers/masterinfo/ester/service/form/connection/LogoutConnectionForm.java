package fr.univangers.masterinfo.ester.service.form.connection;

import fr.univangers.masterinfo.ester.service.form.AbstractForm;
import fr.univangers.masterinfo.ester.service.form.Form;

/**
 * Formulaire de déconnexion
 *
 * @version 1.0
 * @date 04/10/2020
 */
@Form(name = LogoutConnectionForm.LOGOUT_CONNECTION_FORM)
public class LogoutConnectionForm extends AbstractForm {

    /**
     * Nom du formulaire accessible dans la requete pour la JSP
     */
    public static final String LOGOUT_CONNECTION_FORM = "logoutConnectionForm";
    /**
     * Serial Version UID
     */
    private static final long serialVersionUID = 1L;
}

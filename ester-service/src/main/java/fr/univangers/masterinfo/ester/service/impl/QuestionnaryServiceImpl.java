package fr.univangers.masterinfo.ester.service.impl;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import fr.univangers.masterinfo.ester.dao.bean.*;
import fr.univangers.masterinfo.ester.dao.contract.*;
import fr.univangers.masterinfo.ester.dao.exception.EsterDaoException;
import fr.univangers.masterinfo.ester.service.contract.DataServiceContract;
import fr.univangers.masterinfo.ester.service.contract.QuestionnaryServiceContract;
import fr.univangers.masterinfo.ester.service.exception.EsterServiceException;
import fr.univangers.masterinfo.ester.service.form.questionnary.AnswerQuestionnaryForm;
import fr.univangers.masterinfo.ester.service.form.questionnary.AssignQuestionnaryForm;
import fr.univangers.masterinfo.ester.service.form.questionnary.ManageQuestionnaryForm;
import fr.univangers.masterinfo.ester.service.form.questionnary.ShareQuestionnaryForm;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.*;
import java.util.Map.Entry;
import java.util.stream.Collectors;

/**
 * Service pour gérer les questionnaires
 *
 * @version 1.0
 * @date 04/10/2020
 */
@Service
public class QuestionnaryServiceImpl extends AbstractServiceImpl
        implements QuestionnaryServiceContract {

    /**
     * Log de la classe
     */
    private static final Logger LOG = LogManager.getLogger(QuestionnaryServiceImpl.class);
    /**
     * L'entité manageur qui gère les entités
     */
    @PersistenceContext
    protected EntityManager entityManager;
    /**
     * DAO question
     */
    @Autowired
    QuestionDaoContract questionDaoContract;
    /**
     * DAO questionOption
     */
    @Autowired
    QuestionOptionDaoContract questionOptionDaoContract;
    /**
     * DAO Resultat
     */
    @Autowired
    ResultDaoContract resultDaoContract;
    /**
     * DAO Ansewer
     */
    @Autowired
    AnswerDaoContract answerDaoContract;
    /**
     * DAO questionnaire
     */
    @Autowired
    private QuestionnaryDaoContract questionnaryDao;
    /**
     * DAO question
     */
    @Autowired
    private QuestionDaoContract questionDao;
    /**
     * DAO sous-question
     */

    @Autowired
    private QuestionOptionDaoContract questionOptionDao;
    /**
     * DAO employe
     */
    @Autowired
    private EmployeeDaoContract employeeDao;
    /**
     * DAO membre ester
     */
    @Autowired
    private MemberEsterDaoContract memberEsterDao;
    /**
     * DAO résultat
     */
    @Autowired
    private ResultDaoContract resultDao;
    /**
     * Service reference
     */
    @Autowired
    private DataServiceContract referenceService;
    /**
     * Conversion json to object et inversement
     */
    @Autowired
    private ObjectMapper mapper;

    @Override
    public List<QuestionnaryBean> getListQuestionnary() throws EsterServiceException {
        try {
            final List<QuestionnaryBean> questionnaries = this.questionnaryDao.read();
            return questionnaries;
        } catch (final EsterDaoException e) {
            throw new EsterServiceException(e.getMessage(), e.getHelperMessage());
        }
    }

    @Override
    public QuestionnaryBean getQuestionnaryByID(final String id)
            throws EsterServiceException {

        if (id == null) {
            return null;
        }

        try {
            final QuestionnaryBean questionnary = this.questionnaryDao.read(id);
            return questionnary;
        } catch (final EsterDaoException e) {
            throw new EsterServiceException(e.getMessage(), e.getHelperMessage());
        }
    }

    @Override
    public QuestionnaryBean saveQuestionnary(final QuestionnaryBean questionnary) throws EsterServiceException {
        try {
            this.questionnaryDao.create(questionnary);

            return questionnary;
        } catch (final EsterDaoException e) {
            throw new EsterServiceException(e.getMessage(), e.getHelperMessage());
        }
    }

    @Override
    public QuestionnaryBean saveOrUpdateQuestionnary(final HttpServletRequest request) throws EsterServiceException {
        /************************************/
        /**     CREATION QUESTIONNAIRE     **/
        /************************************/

        // si la requête est une modification, on supprime l'ancien questionnaire

        if (request.getParameter("action").equals("modify")) {
            try {
                QuestionnaryBean ancien_questionnaire = questionnaryDao.findQuestionnaryByName(request.getParameter("nom_questionnaire"));

                questionnaryDao.delete(ancien_questionnaire);
            } catch (Exception e) {
            }
        }

        // On crée un nouveau questionnaire

        final QuestionnaryBean questionnaire = new QuestionnaryBean();

        // On définit à null son ID

        questionnaire.setId(null);

        // On définit son nom et son code (pour l'instant identiques)

        // on boucle sur le nom tant qu'il est identique à un autre questionnaire (on ajoute _Copie à chaque fois)

        String nom_questionnaire = request.getParameter("nom_questionnaire");

        try {
            while (questionnaryDao.findQuestionnaryByName(nom_questionnaire) != null) {
                nom_questionnaire = nom_questionnaire + "_Copie";
            }
        } catch (Exception e) {
        }

        questionnaire.setName(nom_questionnaire);

        questionnaire.setCode(nom_questionnaire);

        // On définit sa version

        questionnaire.setVersion(1);

        // On définit son attribut published à false

        questionnaire.setPublished(false);

        // On définit ses dates de création et modification à la date du jour

        questionnaire.setDateSubmission(new Date());

        questionnaire.setDateLastModified(new Date());

        // On définit sa base de référence

        questionnaire.setReference(request.getParameter("choix_reference"));

        /************************************/
        /**       CREATION QUESTIONS       **/
        /************************************/

        // On crée maintenant les questions et on les ajoute au questionnaire

        // Arbre contenant les questions

        final Set<QuestionBean> questions = new TreeSet<>(QuestionBean.COMPARATOR_QUESTION);

        // On parcourt toutes les questions du questionnaire

        for (int i = 1; i <= (Integer.parseInt(request.getParameter("nb_questions"))); i++) {
            // si c'est une question courte ou longue

            if (request.getParameter("type_question_" + String.valueOf(i)).equals("courte") || request.getParameter("type_question_" + String.valueOf(i)).equals("longue")) {
                // on crée une nouvelle question

                final QuestionBean question = new QuestionBean();

                // On définit à null son ID

                question.setId(null);

                // on définit son type à SHORTANSWER ou LONGANSWER

                if (request.getParameter("type_question_" + String.valueOf(i)).equals("courte")) {
                    question.setType(QuestionType.SHORTANSWER);
                } else {
                    question.setType(QuestionType.LONGANSWER);
                }

                // on définit sa position

                question.setPosition(i);

                // on définit son intitulé

                question.setName(request.getParameter("question_" + String.valueOf(i)));

                // On définit le questionnaire auquel est rattachée la question

                question.setQuestionnary(questionnaire);

                // on ajoute la question dans l'arbre des questions

                questions.add(question);
            }

            // sinon c'est une question à choix multiples

            else {
                // on crée une nouvelle question

                final QuestionBean question = new QuestionBean();

                // On définit à null son ID

                question.setId(null);

                // on définit son type à RADIO, SELECT ou CHECKBOX

                if (request.getParameter("type_question_" + String.valueOf(i)).equals("radio")) {
                    question.setType(QuestionType.RADIO);
                } else if (request.getParameter("type_question_" + String.valueOf(i)).equals("liste")) {
                    question.setType(QuestionType.SELECT);
                } else if (request.getParameter("type_question_" + String.valueOf(i)).equals("case")) {
                    question.setType(QuestionType.CHECKBOX);
                }

                // on définit sa position

                question.setPosition(i);

                // on définit son intitulé

                question.setName(request.getParameter("question_" + String.valueOf(i)));

                // on définit son seuil

                question.setSeuil(Integer.parseInt(request.getParameter("seuil_question_" + String.valueOf(i))));

                // on définit sa variable de référence

                question.setVariable(request.getParameter("variable_question_" + String.valueOf(i)));

                // on définit le questionnaire auquel est rattachée la question

                question.setQuestionnary(questionnaire);

                /************************************/
                /**    CREATION SOUS-QUESTIONS     **/
                /************************************/

                // on définit chacune de ses sous-questions

                final Set<QuestionOptionBean> sous_questions = new TreeSet<>(QuestionOptionBean.COMPARATOR_QUESTION_OPTION);

                for (int j = 1; j <= Integer.parseInt(request.getParameter("nb_sous_questions_" + String.valueOf(i))); j++) {
                    // on crée une nouvelle sous-question

                    final QuestionOptionBean sous_question = new QuestionOptionBean();

                    // On définit à null son ID

                    sous_question.setId(null);

                    // on définit sa position

                    sous_question.setPosition(j);

                    // on définit son intitulé

                    sous_question.setValue(request.getParameter("sous_question_" + String.valueOf(j) + "_" + String.valueOf(i)));

                    // on définit son score

                    sous_question.setScore(Integer.parseInt(request.getParameter("score_sous_question_" + String.valueOf(j) + "_" + String.valueOf(i))));

                    // on définit son coefficient

                    sous_question.setCoeff(Integer.parseInt(request.getParameter("coefficient_sous_question_" + String.valueOf(j) + "_" + String.valueOf(i))));

                    // on définit la valeur de la variable de référence

                    sous_question.setValeur(Integer.parseInt(request.getParameter("valeur_variable_sous_question_" + String.valueOf(j) + "_" + String.valueOf(i))));

                    // on définit la question auquelle est rattachée la sous-question

                    sous_question.setQuestion(question);

                    // on ajoute la sous-question dans l'arbre des sous-questions

                    sous_questions.add(sous_question);
                }

                // on ajoute les sous-questions à la question

                question.setQuestionOptions(sous_questions);

                // on ajoute la question dans l'arbre des questions

                questions.add(question);
            }
        }

        // on ajoute les questions au questionnaire

        questionnaire.setQuestions(questions);

        /**************************************/
        /**     QUESTIONNAIRE DANS LA BDD    **/
        /**************************************/

        try {
            saveQuestionnary(questionnaire);
        } catch (EsterServiceException e) {
        }

        return questionnaire;
    }

    /**
     * Mettre à jour un questionnaire
     *
     * @param questionnaryBean
     * @return
     * @throws EsterServiceException
     */
    @Override
    public void updateQuestionnary(QuestionnaryBean questionnaryBean) throws EsterServiceException {
        try {
            questionnaryDao.update(questionnaryBean);
        } catch (final EsterDaoException e) {
            throw new EsterServiceException(e.getMessage(), e.getHelperMessage());
        }
    }

    /**
     * Mettre à jour un questionnaire
     *
     * @param questionnaryid
     */
    @Override
    public void publishQuestionnaryById(String questionnaryid) throws EsterServiceException {
        QuestionnaryBean questionnaryBean = this.getQuestionnaryByID(questionnaryid);
        questionnaryBean.setPublished(true);
        this.updateQuestionnary(questionnaryBean);
    }

    /**
     * Supprimer un questionnaire
     *
     * @param questionnaryid
     */
    @Override
    public void deleteQuestionnaryById(String questionnaryid) throws EsterServiceException {
        //on recupere le question avec son Id
        final QuestionnaryBean removequestionnryBean = this.getQuestionnaryByID(questionnaryid);

        try {
            //la supprission des resultats et les reponses
            deleteResultatsFromQuestionnary(removequestionnryBean);

            //suprissions des question  de questionnaire (question normale et avec option)
            deleteQuestionsFromQuestionnary(removequestionnryBean);

            //la supprission de questionnaire de la liste des questionnaires d'un employe
            deleteQuestionnaryFromEmployee(removequestionnryBean);

            //la supprion de questionnaire apres avoir traiter tous les cas
            questionnaryDao.delete(removequestionnryBean);

            //la supprission des reponse de plus (sans données)
            deleteallAnsewersWithNotData();

            //la supprission des resultats de plus (sans donées)
            deleteallResultsWithNotData();

            //suprission des option de plus (sans données ou sans question)
            deleteallOptionsWithNotData();

        } catch (final EsterDaoException e) {
            throw new EsterServiceException(e.getMessage());
        }
    }

    @Override
    public Set<QuestionnaryBean> getQuestionnariesNoAnswered(final AnswerQuestionnaryForm form)
            throws EsterServiceException {
        try {

            final String idEmp = form.getSessionUser().getId();
            final EmployeeBean employee = this.employeeDao.read(idEmp);
            return employee.getQuestionnariesNoAnswered();

        } catch (final EsterDaoException e) {
            throw new EsterServiceException(e.getMessage(), e.getHelperMessage());
        }
    }

    @Override
    public QuestionnaryBean getQuestionnaryAsObjectById(final AnswerQuestionnaryForm form)
            throws EsterServiceException {
        try {

            final String idQuestionnary = form.getSelectedQuestionnary();
            final QuestionnaryBean objQuestionnary = this.questionnaryDao.read(idQuestionnary);
            return objQuestionnary;

        } catch (final EsterDaoException e) {
            throw new EsterServiceException(e.getMessage(), e.getHelperMessage());
        }
    }

    @Override
    public String getQuestionnaryAsJsonById(final AnswerQuestionnaryForm form)
            throws EsterServiceException {
        try {

            final QuestionnaryBean objQuestionnary = this.getQuestionnaryAsObjectById(form);
            final String jsonQuestionnary = this.mapper.writeValueAsString(objQuestionnary);
            return jsonQuestionnary;

        } catch (final JsonProcessingException e) {
            throw new EsterServiceException(e.getMessage());
        }
    }

    @Override
    public void saveResultQuestionnary(final AnswerQuestionnaryForm form, int score)
            throws EsterServiceException {
        try {
            final EmployeeBean employee = (EmployeeBean) form.getSessionUser();
            final QuestionnaryBean questionnary = form.getQuestionnary();

            // On crée d'abord le résultat avec les réponses
            final ResultBean result = new ResultBean();
            result.setScoreGlobal(score);
            result.setDateResult(new Date());
            final Map<AnswerBean, Set<QuestionOptionBean>> options = new HashMap<>();

            // On regroupe les options sélectionnées par le salarié par leur question
            Map<QuestionBean, Set<QuestionOptionBean>> optionsSelectedPerQuestion = new HashMap<>();

            for (QuestionOptionBean option : form.getQuestionsOptionSelected()) {
                if (optionsSelectedPerQuestion.containsKey(option.getQuestion())) {
                    optionsSelectedPerQuestion.get(option.getQuestion()).add(option);
                }
                else {
                    Set<QuestionOptionBean> set = new HashSet<>();
                    set.add(option);
                    optionsSelectedPerQuestion.put(option.getQuestion(), set);
                }
            }

            for (QuestionBean question : optionsSelectedPerQuestion.keySet()) {
                AnswerBean answer = new AnswerBean();
                answer.setResult(result);
                result.getAnswers().add(answer);
                options.put(answer, optionsSelectedPerQuestion.get(question));
            }




            final Map<AnswerBean, QuestionBean> questionBeanMap = new HashMap<>();
            for (final Entry<QuestionBean, String> questionBean : form.getQuestionsSelected().entrySet()) {
                QuestionBean questionBean1 = questionBean.getKey();
                String responses = questionBean.getValue();
                final AnswerBean answer = new AnswerBean();
                answer.setReponsesEmploye(responses);
                answer.setResult(result);
                answer.setResponseID(questionBean1.getId());
                result.getAnswers().add(answer);
                questionBeanMap.put(answer, questionBean1);
            }

            this.resultDao.create(result);

            // Puis on met à jour les associations

            result.setEmployee(employee);
            employee.getResults().add(result);
            result.setQuestionnary(questionnary);
            questionnary.getResults().add(result);

            for (final Entry<AnswerBean, Set<QuestionOptionBean>> option : options.entrySet()) {
                final AnswerBean answer = option.getKey();
                final Set<QuestionOptionBean> responses = option.getValue();
                answer.setEmployee(employee);
                employee.getAnswers().add(answer);
                answer.setResponse(responses);
                for (QuestionOptionBean response: responses) {
                    response.getAnswers().add(answer);
                }
            }

            for (final Entry<AnswerBean, QuestionBean> questionBean : questionBeanMap.entrySet()) {
                final AnswerBean answer = questionBean.getKey();
                answer.setEmployee(employee);
                employee.getAnswers().add(answer);
            }

            // On passe le questionnaire à répondu
            employee.getQuestionnariesNoAnswered().remove(questionnary);
            employee.getQuestionnariesAnswered().add(questionnary);

            questionnary.getEmployeeNoAnswered().remove(employee);
            questionnary.getEmployeeAnswered().add(employee);

            // On met à jour les objets
            this.employeeDao.update(employee);
            // this.questionnaryDao.update(questionnary);
            // this.resultDao.update(result);

            form.setSessionUser(employee);
        } catch (final EsterDaoException e) {
            throw new EsterServiceException(e.getMessage(), e.getHelperMessage());
        }
    }

    @Override
    public List<QuestionnaryBean> findAll() throws EsterServiceException {
        try {
            return this.questionnaryDao.read();
        } catch (final EsterDaoException e) {
            throw new EsterServiceException(e.getMessage(), e.getHelperMessage());
        }
    }

    @Override
    public List<QuestionnaryBean> findJustPublished() throws EsterServiceException {
        try {
            List<QuestionnaryBean> questionnaryBeanListPublished;
            questionnaryBeanListPublished = this.questionnaryDao.read().stream().filter(p -> p.getPublished()).collect(Collectors.toList());
            return questionnaryBeanListPublished;
        } catch (final EsterDaoException e) {
            throw new EsterServiceException(e.getMessage(), e.getHelperMessage());
        }
    }

    @Override
    public List<QuestionnaryBean> findJustPublishedandMember(MemberEsterBean esterBean) throws EsterServiceException {
        try {
            List<QuestionnaryBean> questionnaryBeanListMember = this.findJustPublished();
            questionnaryBeanListMember = this.questionnaryDao.read().stream().filter(p -> p.getMemberAdmins().contains(esterBean)).collect(Collectors.toList());
            return questionnaryBeanListMember;
        } catch (final EsterDaoException e) {
            throw new EsterServiceException(e.getMessage(), e.getHelperMessage());
        }
    }

    @Override
    public List<QuestionnaryBean> findJustMember(MemberEsterBean esterBean) throws EsterServiceException {
        try {
            List<QuestionnaryBean> questionnaryBeanListMember = questionnaryDao.read();
            questionnaryBeanListMember = this.questionnaryDao.read().stream().filter(p -> p.getMemberAdmins().contains(esterBean)).collect(Collectors.toList());
            return questionnaryBeanListMember;
        } catch (final EsterDaoException e) {
            throw new EsterServiceException(e.getMessage(), e.getHelperMessage());
        }
    }

    @Override
    public void assignQuestionnaryToEmployee(AssignQuestionnaryForm form)
            throws EsterServiceException {
        try {
            String idQuestionnary = form.getSelectQuestionnary();
            String idEmploye = form.getSelectEmployee();

            // On recupere le questionnaire de la base avec son id
            QuestionnaryBean questionnaryObj = questionnaryDao.read(idQuestionnary);

            // On recupere l'employee de la base avec son id
            EmployeeBean employeeObj = employeeDao.read(idEmploye);

            // On affecte le questionnaire à l'employee
            employeeObj.getQuestionnariesNoAnswered().add(questionnaryObj);
            questionnaryObj.getEmployeeNoAnswered().add(employeeObj);

            // On met a jour le questionnaire dans la base
            questionnaryDao.update(questionnaryObj);

            // On met a jour l'employee dans la base
            employeeDao.update(employeeObj);
        } catch (final EsterDaoException e) {
            throw new EsterServiceException(e.getMessage(), e.getHelperMessage());
        }
    }

    @Override
    public void shareQuestionnaryToMemberEster(ShareQuestionnaryForm form)
            throws EsterServiceException {
        try {

            String idQuestionnary = form.getSelectQuestionnary();


            String idMemberEster = form.getSelectMemberEster();


            // On recupere le questionnaire de la base avec son id
            QuestionnaryBean questionnaryObj = questionnaryDao.read(idQuestionnary);


            // On recupere l'employee de la base avec son id
            MemberEsterBean memberEsterObj = memberEsterDao.read(idMemberEster);


            //on ajoute le membreEster à la liste des membre qui ont le droit sur le questionnaire
            questionnaryObj.getMemberAdmins().add(memberEsterObj);
            // On met a jour lquestionnaire dans la base
            questionnaryDao.update(questionnaryObj);

        } catch (final EsterDaoException e) {
            throw new EsterServiceException(e.getMessage(), e.getHelperMessage());
        }

    }
    /*
     * L'affectation d'un questionnaire à une personne
     */

    @Override
    public void shareQuestionnaryToMemberEsterOnCreation(String idQuestionnaryToShare, String idMemberEsterToChare)
            throws EsterServiceException {
        try {

            String idQuestionnary = idQuestionnaryToShare;


            String idMemberEster = idMemberEsterToChare;


            // On recupere le questionnaire de la base avec son id
            QuestionnaryBean questionnaryObj = questionnaryDao.read(idQuestionnary);


            // On recupere l'employee de la base avec son id
            MemberEsterBean memberEsterObj = memberEsterDao.read(idMemberEster);


            // On partage le droit avec le membre ester
            questionnaryObj.getMemberAdmins().add(memberEsterObj);

            // On met a jour lquestionnaire dans la base
            questionnaryDao.update(questionnaryObj);


        } catch (final EsterDaoException e) {
            throw new EsterServiceException(e.getMessage(), e.getHelperMessage());
        }

    }

    /*
     *supprimer un questionaire de la liste des questionaire repondu ou pas encore d'un employer
     *
     */
    @Override
    public void deleteQuestionnaryFromEmployee(QuestionnaryBean questionnary) throws EsterServiceException {

        //je recupere la liste des employer qui ont repondu et pas repondu
        Set<EmployeeBean> em1 = questionnary.getEmployeeNoAnswered();
        em1.addAll(questionnary.getEmployeeAnswered());
        try {
            //je parcour chaque employer et je supprime le questionnaire dans ca liste des questionnaire repondu et pas repondu
            if (!em1.isEmpty()) {

                for (EmployeeBean em : em1) {
                    em.removequestionnary(questionnary);
                    employeeDao.update(em);
                }

            }
        } catch (final EsterDaoException e) {
            throw new EsterServiceException(e.getMessage());
        }

    }

    /*
     *supprimer les question d'un questionnaire don les options
     */
    @Override
    public void deleteQuestionsFromQuestionnary(QuestionnaryBean questionnary) throws EsterServiceException {
        //on recupere toutes les questions d'un questionnaire
        Set<QuestionBean> questions = questionnary.getQuestions();
        try {
            if (!questions.isEmpty()) {

                for (QuestionBean question : questions) {
                    //on recupere les options d'une question
                    Set<QuestionOptionBean> options = question.getQuestionOptions();
                    if (options.size() > 0) {
                        for (QuestionOptionBean option : options) {
                            //on supprime toutes les options
                            option.setQuestion(null);
                            option.setCode(questionOptionDaoContract.read(option.getId()).getCode());
                            option.setCoeff(questionOptionDaoContract.read(option.getId()).getCoeff());
                            option.setPosition(questionOptionDaoContract.read(option.getId()).getPosition());
                            option.setScore(questionOptionDaoContract.read(option.getId()).getScore());
                            option.setValue(questionOptionDaoContract.read(option.getId()).getValue());
                            this.questionOptionDaoContract.update(option);
                            this.questionOptionDaoContract.delete(option);
                        }
                    }
                    //on supprime la question apres avoir supprimer ses options et ses reponses
                    this.questionDaoContract.delete(question);

                }

            }

        } catch (final EsterDaoException e) {
            throw new EsterServiceException(e.getMessage());
        }
    }

    /*
     * la supprission des resultats d'un questionnaire et les reponse
     */
    @Override
    public void deleteResultatsFromQuestionnary(QuestionnaryBean questionnary) throws EsterServiceException {
        //on recupere tous les resultats
        Set<ResultBean> results = questionnary.getResults();
        try {
            if (!results.isEmpty()) {
                for (ResultBean result : results) {
                    result.setQuestionnary(null);
                    result.setEmployee(null);
                    result.setScoreGlobal(resultDaoContract.read(result.getId()).getScoreGlobal());
                    result.setDateResult(resultDaoContract.read(result.getId()).getDateResult());
                    //on recupere tous les reponses
                    Set<AnswerBean> answers = result.getAnswers();
                    for (AnswerBean answer : answers) {
                        answer.setResult(null);
                        answer.setEmployee(null);
                        answer.setResponse(null);
                        answerDaoContract.update(answer);
                        answerDaoContract.delete(answer);
                        ;
                    }
                    resultDaoContract.update(result);
                    resultDaoContract.delete(result);

                }

            }

        } catch (final EsterDaoException e) {
            throw new EsterServiceException(e.getMessage());
        }
    }

    @Override
    public void deleteallAnsewersWithNotData() throws EsterServiceException {
        try {
            List<AnswerBean> allansewers = answerDaoContract.read();
            for (AnswerBean answer : allansewers) {
                if (answer.getEmployee() == null && answer.getResult() == null && answer.getResponse() == null)
                    answerDaoContract.delete(answer);
            }
        } catch (final EsterDaoException e) {
            throw new EsterServiceException(e.getMessage());
        }
    }

    @Override
    public void deleteallResultsWithNotData() throws EsterServiceException {
        try {
            List<ResultBean> resultBeanList = resultDaoContract.read();
            for (ResultBean result : resultBeanList) {
                if (result.getQuestionnary() == null && result.getEmployee() == null)
                    resultDaoContract.delete(result);
            }
        } catch (final EsterDaoException e) {
            throw new EsterServiceException(e.getMessage());
        }
    }

    @Override
    public void deleteallOptionsWithNotData() throws EsterServiceException {
        try {
            List<QuestionOptionBean> optionBeanList = questionOptionDaoContract.read();
            for (QuestionOptionBean option : optionBeanList) {
                if (option.getQuestion() == null)
                    questionOptionDaoContract.delete(option);
            }
        } catch (final EsterDaoException e) {
            throw new EsterServiceException(e.getMessage());
        }
    }

    @Override
    public boolean verfierMember(MemberEsterBean memberEsterBean, QuestionnaryBean questionnaryBean) throws EsterServiceException {
        Set<MemberEsterBean> memberEsterBeans = questionnaryBean.getMemberAdmins();
        for (MemberEsterBean memberEsterBean1 : memberEsterBeans) {
            if (memberEsterBean1.equals(memberEsterBean))
                return true;
        }

        return false;
    }

}

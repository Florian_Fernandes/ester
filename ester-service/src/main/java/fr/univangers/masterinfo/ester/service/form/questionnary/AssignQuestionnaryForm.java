package fr.univangers.masterinfo.ester.service.form.questionnary;

import fr.univangers.masterinfo.ester.dao.bean.EmployeeBean;
import fr.univangers.masterinfo.ester.dao.bean.QuestionnaryBean;
import fr.univangers.masterinfo.ester.service.form.AbstractForm;
import fr.univangers.masterinfo.ester.service.form.Form;
import fr.univangers.masterinfo.ester.service.form.Input;

import java.util.ArrayList;
import java.util.List;

/**
 * @author etudiant
 */
@Form(name = AssignQuestionnaryForm.ASSIGN_QUESTIONNARY_FORM)
public class AssignQuestionnaryForm extends AbstractForm {

    /**
     * Nom du formulaire accessible dans la requete via la JSP
     */
    public static final String ASSIGN_QUESTIONNARY_FORM = "assignQuestionnaryForm";
    /**
     * Mapping select Employee entre le champ input HTML / attribut Java
     */
    public static final String EMPLOYEE_SELECT = "selectEmployee";
    /**
     * Mapping select questionnary entre le champ input HTML / attribut Java
     */
    public static final String QUESTIONNARY_SELECT = "selectQuestionnary";
    /**
     * Serial Version UID
     */
    private static final long serialVersionUID = 1L;
    /**
     * Employe selectionné
     */
    @Input(name = EMPLOYEE_SELECT)
    private String selectEmployee;

    /**
     * Questionnaire selectionné
     */
    @Input(name = QUESTIONNARY_SELECT)
    private String selectQuestionnary;

    /**
     * Liste des questionnaires
     */
    private List<QuestionnaryBean> questionnaries = new ArrayList<>();

    /**
     * Liste des employés
     */
    private List<EmployeeBean> employees = new ArrayList<>();

    /**
     * @return the questionnaries
     */
    public List<QuestionnaryBean> getQuestionnaries() {
        return this.questionnaries;
    }

    /**
     * @param questionnaries the questionnaries to set
     */
    public void setQuestionnaries(List<QuestionnaryBean> questionnaries) {
        this.questionnaries = questionnaries;
    }

    /**
     * @return the selectEmployee
     */
    public String getSelectEmployee() {
        return this.selectEmployee;
    }

    /**
     * @param selectEmployee the selectEmployee to set
     */
    public void setSelectEmployee(String selectEmployee) {
        this.selectEmployee = selectEmployee;
    }

    /**
     * @return the selectQuestionnary
     */
    public String getSelectQuestionnary() {
        return this.selectQuestionnary;
    }

    /**
     * @param selectQuestionnary the selectQuestionnary to set
     */
    public void setSelectQuestionnary(String selectQuestionnary) {
        this.selectQuestionnary = selectQuestionnary;
    }

    /**
     * @return the employees
     */
    public List<EmployeeBean> getEmployees() {
        return this.employees;
    }

    /**
     * @param employees the employees to set
     */
    public void setEmployees(List<EmployeeBean> employees) {
        this.employees = employees;
    }
}

package fr.univangers.masterinfo.ester.service.contract;

import fr.univangers.masterinfo.ester.service.exception.EsterServiceException;
import fr.univangers.masterinfo.ester.service.pojo.PicturePojo;
import fr.univangers.masterinfo.ester.service.pojo.VideoPojo;

/**
 * Contrat pour gérer tout type de documents (vidéo, images, etc.)
 *
 * @version 1.0
 * @date 04/10/2020
 */
public interface DocumentServiceContract extends AbstractServiceContract {

    /**
     * Enregistrer une vidéo
     *
     * @param video
     * @throws EsterServiceException
     */
    void uploadVideo(VideoPojo video) throws EsterServiceException;

    /**
     * Télécharger une vidéo par son code
     *
     * @param code
     * @return la vidéo
     * @throws EsterServiceException
     */
    VideoPojo downloadVideo(String code) throws EsterServiceException;

    /**
     * Enregistrer une image
     *
     * @param picture
     * @throws EsterServiceException
     */
    void uploadPicture(PicturePojo picture) throws EsterServiceException;

    /**
     * Télécharger une image par son code
     *
     * @param code
     * @return l'image
     * @throws EsterServiceException
     */
    PicturePojo downloadPicture(String code) throws EsterServiceException;
}
package fr.univangers.masterinfo.ester.service.form.account;

import fr.univangers.masterinfo.ester.dao.bean.UserSexe;
import fr.univangers.masterinfo.ester.service.form.AbstractForm;
import fr.univangers.masterinfo.ester.service.form.Form;
import fr.univangers.masterinfo.ester.service.form.Input;
import fr.univangers.masterinfo.ester.service.form.validation.order.One;

import javax.validation.constraints.NotBlank;

;

/**
 * Formulaire pour récupérer les informations sur les employés
 **/
@Form(name = EmployeeAccountForm.EMPLOYEE_ACCOUNT_FORM)
public class EmployeeAccountForm extends AbstractForm {

    /**
     * Nom du formulaire accessible dans la requete pour la JSP
     */
    public static final String EMPLOYEE_ACCOUNT_FORM = "employeeAccountForm";
    /**
     *
     */
    public static final String INPUT_LOGIN = "login";
    /**
     *
     */
    public static final String INPUT_SEXE = "sexe";
    /**
     *
     */
    public static final String INPUT_BIRTH_YEAR = "birthYear";
    /**
     *
     */
    public static final String INPUT_SELECTED_PCS = "selectedPCS";
    /**
     *
     */
    public static final String INPUT_SELECTED_NAF = "selectedNAF";
    /**
     *
     */
    public static final String INPUT_COMMENTAIRE = "commentaire";
    /**
     * Serial Version UID
     */
    private static final long serialVersionUID = 1L;
    /**
     *
     */
    @Input(name = INPUT_LOGIN)
    @NotBlank(message = "{login.not.blank.msg}", groups = One.class)
    private String login;

    /**
     *
     */
    @Input(name = INPUT_SEXE)
    private UserSexe sexe;

    /**
     *
     */
    @Input(name = INPUT_BIRTH_YEAR)
    private String birthYear;

    /**
     *
     */
    @Input(name = INPUT_SELECTED_PCS)
    private String selectedPCS;

    /**
     *
     */
    @Input(name = INPUT_SELECTED_NAF)
    private String selectedNAF;

    /**
     *
     */
    @Input(name = INPUT_COMMENTAIRE)
    private String commentaire;

    /**
     * @return the login
     */
    public String getLogin() {
        return this.login;
    }

    /**
     * @param login the login to set
     */
    public void setLogin(final String login) {
        this.login = login;
    }

    /**
     * @return the sexe
     */
    public UserSexe getSexe() {
        return this.sexe;
    }

    /**
     * @param sexe the sexe to set
     */
    public void setSexe(final UserSexe sexe) {
        this.sexe = sexe;
    }

    /**
     * @return the birthYear
     */
    public String getBirthYear() {
        return this.birthYear;
    }

    /**
     * @param birthYear the birthYear to set
     */
    public void setBirthYear(final String birthYear) {
        this.birthYear = birthYear;
    }

    /**
     * @return the selectedPCS
     */
    public String getSelectedPCS() {
        return this.selectedPCS;
    }

    /**
     * @param selectedPCS the selectedPCS to set
     */
    public void setSelectedPCS(final String selectedPCS) {
        this.selectedPCS = selectedPCS;
    }

    /**
     * @return the selectedNAF
     */
    public String getSelectedNAF() {
        return this.selectedNAF;
    }

    /**
     * @param selectedNAF the selectedNAF to set
     */
    public void setSelectedNAF(final String selectedNAF) {
        this.selectedNAF = selectedNAF;
    }

    /**
     * @return the commentaire
     */
    public String getCommentaire() {
        return this.commentaire;
    }

    /**
     * @param commentaire the commentaire to set
     */
    public void setCommentaire(final String commentaire) {
        this.commentaire = commentaire;
    }
}

package fr.univangers.masterinfo.ester.service.pojo;

import java.io.InputStream;

public class VideoPojo extends AbstractPojo {

    /**
     * Code unique de la vidéo afin de la retrouver
     */
    private String code;

    /**
     * Le flux vidéo
     */
    private InputStream fileInputStream;

    /**
     * @return the fileInputStream
     */
    public InputStream getFileInputStream() {
        return this.fileInputStream;
    }

    /**
     * @param fileInputStream the fileInputStream to set
     */
    public void setFileInputStream(final InputStream fileInputStream) {
        this.fileInputStream = fileInputStream;
    }

    /**
     * @return the code
     */
    public String getCode() {
        return this.code;
    }

    /**
     * @param code the code to set
     */
    public void setCode(final String code) {
        this.code = code;
    }
}

package fr.univangers.masterinfo.ester.service.impl;

import fr.univangers.masterinfo.ester.dao.bean.MemberEsterBean;
import fr.univangers.masterinfo.ester.dao.contract.MemberEsterDaoContract;
import fr.univangers.masterinfo.ester.dao.criteria.MemberEsterCriteria;
import fr.univangers.masterinfo.ester.dao.exception.EsterDaoException;
import fr.univangers.masterinfo.ester.service.contract.MailServiceContract;
import fr.univangers.masterinfo.ester.service.exception.EsterServiceException;
import fr.univangers.masterinfo.ester.service.form.account.CreateAccountForm;
import freemarker.template.Configuration;
import freemarker.template.Template;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;
import org.springframework.ui.freemarker.FreeMarkerTemplateUtils;

import javax.mail.internet.MimeMessage;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;

/**
 * Service pour gérer les mails
 *
 * @version 1.0
 * @date 04/10/2020
 */
@Service
public class MailServiceImpl implements MailServiceContract {

    /**
     * Template associé à la création d'un nouveau compte
     */
    private static final String TEMPLATE_CREATE_ACCOUNT = "mail-create-account.html";

    /**
     * Objet création de compte
     */
    private static final String SUBJECT_CREATE_ACCOUNT = "[ESTER] - Création de compte";

    /**
     * Objet confirmation de création de compte après avoir validé le mot de passe
     */
    private static final String SUBJECT_VALIDATE_CREATE_ACCOUNT = "[ESTER] - Confirmation création de compte";

    /**
     * Objet demande de mot de passe oublié
     */
    private static final String SUBJECT_FORGOT_PASSWORD_ACCOUNT = "[ESTER] - Mot de passe oublié";

    /**
     * Objet confirmation changement de mot de passe
     */
    private static final String SUBJECT_VALIDATE_FORGOT_PASSWORD_ACCOUNT = "[ESTER] - Confirmation changement de mot de passe";

    /**
     * Objet qui permet d'envoyer des messages
     */
    @Autowired
    private JavaMailSender mailSender;

    /**
     * Objet qui permet de charger un fichier template
     */
    @Autowired
    private Configuration freemarker;

    /**
     * Objet permettant de récupérer le compte Ester créé
     */
    @Autowired
    private MemberEsterDaoContract memberEsterDao;

    @Override
    public void sendCreateAccountSync(final CreateAccountForm form) throws EsterServiceException {
        try {

            final MemberEsterCriteria memberEsterCriteria = new MemberEsterCriteria();
            memberEsterCriteria.setLoginOrEmail(form.getMailAccount());
            MemberEsterBean membre = memberEsterDao.findMemberEsterByLoginOrEmail(memberEsterCriteria);

            final Map<String, Object> model = new HashMap<>();
            model.put("account", form.getSelectedRole().getName());
            model.put("identifiant", membre.getLogin());
            model.put("password", membre.getPassword());
            model.put("linkConnection", "https://developer.mozilla.org/fr/docs/Web/HTML/Element/a");
            model.put("linkPassword", "https://developer.mozilla.org/fr/docs/Web/HTML/Element/a");
            model.put("employee", true);

            final Template template = this.freemarker.getTemplate(TEMPLATE_CREATE_ACCOUNT);
            final String content = FreeMarkerTemplateUtils.processTemplateIntoString(template,
                    model);

            final MimeMessage mimeMessage = this.mailSender.createMimeMessage();
            final MimeMessageHelper message = new MimeMessageHelper(mimeMessage,
                    StandardCharsets.UTF_8.name());
            message.setSubject(SUBJECT_CREATE_ACCOUNT);
            message.setFrom("ester.chu.angers@gmail.com");
            message.setTo(form.getMailAccount());
            message.setText(content, true);

            this.mailSender.send(mimeMessage);
        } catch (final EsterDaoException e) {
            throw new EsterServiceException(e.getMessage(), e.getHelperMessage());
        } catch (final Exception e) {
            throw new EsterServiceException(e.getMessage());
        }
    }

    @Override
    public void sendCreateAccountAsync(CreateAccountForm form) throws EsterServiceException {
        Thread t = new Thread() {
            public void run() {
                try {
                    sendCreateAccountSync(form);
                } catch (EsterServiceException e) {
                    e.printStackTrace();
                }
            }
        };
        t.start();
    }
}

package fr.univangers.masterinfo.ester.service.contract;

import fr.univangers.masterinfo.ester.dao.bean.ReferenceBean;
import fr.univangers.masterinfo.ester.service.exception.EsterServiceException;
import fr.univangers.masterinfo.ester.service.form.data.ImportReferencesDataForm;
import fr.univangers.masterinfo.ester.service.form.data.RemoveReferencesDataForm;

import java.util.List;

/**
 * Contrat pour gérer les données (références et salariés)
 *
 * @version 1.0
 * @date 04/10/2020
 */
public interface DataServiceContract extends AbstractServiceContract {
    /**
     * Ajouter des données de references
     *
     * @param form
     * @throws EsterServiceException
     */
    void addReference(final ImportReferencesDataForm form) throws EsterServiceException;

    /**
     * Mettre à jour les données de references
     *
     * @param form
     * @throws EsterServiceException
     */
    //void updateReference(ModifyReferencesDataForm form) throws EsterServiceException;

    /**
     * supprimer les données de references
     *
     * @param form
     * @throws EsterServiceException
     */
    void removeReference(RemoveReferencesDataForm form) throws EsterServiceException;

    /**
     * renvoie le referencebean correspondant à l'id passé en paramètres
     *
     * @param id
     * @return
     * @throws EsterServiceException
     */
    ReferenceBean findReferenceById(String id) throws EsterServiceException;

    /**
     * Recuperer toutes les données de references
     *
     * @throws EsterServiceException
     */
    List<ReferenceBean> findAll() throws EsterServiceException;

}

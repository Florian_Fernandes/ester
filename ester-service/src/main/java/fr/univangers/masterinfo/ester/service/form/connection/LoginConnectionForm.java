package fr.univangers.masterinfo.ester.service.form.connection;

import fr.univangers.masterinfo.ester.dao.bean.UserGroup;
import fr.univangers.masterinfo.ester.service.form.AbstractForm;
import fr.univangers.masterinfo.ester.service.form.Form;
import fr.univangers.masterinfo.ester.service.form.Input;
import fr.univangers.masterinfo.ester.service.form.validation.order.One;
import fr.univangers.masterinfo.ester.service.form.validation.order.Three;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;

/**
 * Formulaire de connexion
 *
 * @version 1.0
 * @date 04/10/2020
 */
@Form(name = LoginConnectionForm.LOGIN_CONNECTION_FORM)
public class LoginConnectionForm extends AbstractForm {

    /**
     * Nom du formulaire accessible dans la requete pour la JSP
     */
    public static final String LOGIN_CONNECTION_FORM = "loginConnectionForm";
    /**
     * Mapping login ou email entre le champ input HTML / attribut Java
     */
    public static final String INPUT_LOGIN_OR_EMAIL = "loginOrEmail";
    /**
     * Mapping mot de passe entre le champ input HTML / attribut Java
     */
    public static final String INPUT_PASSWORD = "password";
    /**
     * Mapping groupe entre le champ input HTML / attribut Java
     */
    public static final String INPUT_GROUP = "group";
    /**
     * Serial Version UID
     */
    private static final long serialVersionUID = 1L;
    /**
     * Le login
     */
    @Input(name = INPUT_LOGIN_OR_EMAIL)
    @NotBlank(message = "Le champ login {javax.validation.constraints.NotBlank.message}", groups = One.class)
    @Email(message = "{mail.pattern.msg}", regexp = "{mail.pattern.regexp}", groups = Three.class)
    private String loginOrEmail;

    /**
     * Le mot de passe
     */
    @Input(name = INPUT_PASSWORD)
    @NotBlank(message = "{mail.not.blank.msg}", groups = One.class)
    private String password;

    /**
     * Le rôle
     */
    @Input(name = INPUT_GROUP)
    private UserGroup group;

    /**
     * @return the loginOrEmail
     */
    public String getLoginOrEmail() {
        return this.loginOrEmail;
    }

    /**
     * @param loginOrEmail the loginOrEmail to set
     */
    public void setLoginOrEmail(final String loginOrEmail) {
        this.loginOrEmail = loginOrEmail;
    }

    /**
     * @return the password
     */
    public String getPassword() {
        return this.password;
    }

    /**
     * @param password the password to set
     */
    public void setPassword(final String password) {
        this.password = password;
    }

    /**
     * @return the group
     */
    public UserGroup getGroup() {
        return this.group;
    }

    /**
     * @param group the group to set
     */
    public void setGroup(final UserGroup group) {
        this.group = group;
    }
}

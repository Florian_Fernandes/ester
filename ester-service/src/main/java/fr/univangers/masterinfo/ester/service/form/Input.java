/**
 *
 */
package fr.univangers.masterinfo.ester.service.form;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * Tag qui permet d'identifier les champs d'un formulaire
 *
 *
 * @date 04/10/2020
 * @version 1.0
 */
@Documented
@Retention(RUNTIME)
@Target(FIELD)
public @interface Input {
    /**
     * @return Le nom du champ
     */
    String name();
}

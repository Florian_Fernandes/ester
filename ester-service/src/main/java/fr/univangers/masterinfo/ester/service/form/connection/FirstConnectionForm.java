package fr.univangers.masterinfo.ester.service.form.connection;

import fr.univangers.masterinfo.ester.service.form.AbstractForm;
import fr.univangers.masterinfo.ester.service.form.Form;
import fr.univangers.masterinfo.ester.service.form.Input;

/**
 * Formulaire première connexion
 *
 * @author Clémence DURAND
 * @version 1.0
 * @date 22/10/2020
 */
@Form(name = FirstConnectionForm.FIRST_CONNECTION_FORM)
public class FirstConnectionForm extends AbstractForm {

    /**
     * Nom du formulaire accessible dans la requete pour la JSP
     */
    public static final String FIRST_CONNECTION_FORM = "firstConnectionForm";
    /**
     * Mapping mdp entre le champ input / attribut java
     */
    public static final String INPUT_PASSWORD = "password";
    /**
     * Mapping confirmation mdp entre le champ input / attribut java
     */
    public static final String INPUT_CONFIRM_PASSWORD = "confirmPassword";
    /**
     * Serial Version UID
     */
    private static final long serialVersionUID = 1L;
    /**
     * Le mot de passe
     */
    @Input(name = INPUT_PASSWORD)
    private String password;

    /**
     * Le mot de passe confirmé
     */
    @Input(name = INPUT_CONFIRM_PASSWORD)
    private String confirmPassword;
}

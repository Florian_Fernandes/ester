package fr.univangers.masterinfo.ester.service.form.data;

import fr.univangers.masterinfo.ester.dao.bean.EmployeeBean;
import fr.univangers.masterinfo.ester.service.form.AbstractForm;
import fr.univangers.masterinfo.ester.service.form.Form;

import java.util.ArrayList;
import java.util.List;

@Form(name = EmployeeDataForm.EMPLOYEE_DATA)
public class EmployeeDataForm extends AbstractForm {
    /**
     * Nom du formulaire accessible dans la requete via la JSP
     */
    public static final String EMPLOYEE_DATA = "EMPLOYEE_DATA";
    /**
     * Serial Version UID
     */
    private static final long serialVersionUID = 1L;
    /**
     * Liste des employés
     */
    private List<EmployeeBean> employees = new ArrayList<>();

    /**
     * @return the employees
     */
    public List<EmployeeBean> getEmployees() {
        return this.employees;
    }

    /**
     * @param employees the employees to set
     */
    public void setEmployees(List<EmployeeBean> employees) {
        this.employees = employees;
    }
}

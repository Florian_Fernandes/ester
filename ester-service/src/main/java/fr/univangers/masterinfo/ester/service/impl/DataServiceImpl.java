package fr.univangers.masterinfo.ester.service.impl;

import fr.univangers.masterinfo.ester.dao.bean.FileFormat;
import fr.univangers.masterinfo.ester.dao.bean.ReferenceBean;
import fr.univangers.masterinfo.ester.dao.contract.QuestionnaryDaoContract;
import fr.univangers.masterinfo.ester.dao.contract.ReferenceDaoContract;
import fr.univangers.masterinfo.ester.dao.criteria.ReferenceCriteria;
import fr.univangers.masterinfo.ester.dao.exception.EsterDaoException;
import fr.univangers.masterinfo.ester.service.contract.DataServiceContract;
import fr.univangers.masterinfo.ester.service.exception.EsterServiceException;
import fr.univangers.masterinfo.ester.service.form.data.ImportReferencesDataForm;
import fr.univangers.masterinfo.ester.service.form.data.RemoveReferencesDataForm;
import org.apache.commons.io.FileUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;

/**
 * Service pour gérer les données (références et salariés)
 *
 * @version 1.0
 * @date 04/10/2020
 */
@Service
public class DataServiceImpl extends AbstractServiceImpl implements DataServiceContract {
    /**
     * Logger
     */
    private static final Logger LOG = LogManager.getLogger(DataServiceImpl.class);

    /**
     * ReferenceDao
     */
    @Autowired
    private ReferenceDaoContract referenceDao;

    /**
     * QuestionnaryDao
     */
    @Autowired
    private QuestionnaryDaoContract questionnaryDao;

    @Override
    public void addReference(final ImportReferencesDataForm form) throws EsterServiceException {
        try {

            final InputStream is = form.getFile();
            final String filePath = form.getRoutePathServer() + form.getNomBDD() + ".csv";
            final File newfile = new File(filePath);

            FileUtils.copyToFile(is, newfile);

            final ReferenceCriteria criteria = new ReferenceCriteria();
            criteria.setFile(newfile);
            criteria.setFormatfile(FileFormat.TSV);
            criteria.setVariables(form.getVariablesBDD());
            final String collection = form.getNomBDD().toLowerCase();
            criteria.setCollection(collection);

            // ajout de la table de référence a la table t_reference
            final ReferenceBean newref = new ReferenceBean();
            newref.setFormat(criteria.getFormatfile());
            newref.setFile(new File(filePath));
            newref.setName(collection);
            newref.setVariables(criteria.getVariables());
            this.referenceDao.create(newref);

            this.referenceDao.addReference(criteria);//ajout de la collection de données de ref a la bdd

            newfile.delete();
        } catch (final IOException e) {
            throw new EsterServiceException(e.getMessage());
        } catch (final EsterDaoException e) {
            throw new EsterServiceException(e.getMessage(), e.getHelperMessage());
        }
    }

    @Override
    public void removeReference(RemoveReferencesDataForm form) throws EsterServiceException {
        try {
            final ReferenceBean ref = this.referenceDao.read(form.getSelectReference());
            this.referenceDao.removeReferenceTable(ref);
            this.referenceDao.delete(ref);

        } catch (EsterDaoException e) {
            throw new EsterServiceException(e.getMessage(), e.getHelperMessage());
        }
    }

    @Override
    public ReferenceBean findReferenceById(String id) throws EsterServiceException {
        try {
            return this.referenceDao.findReference(id);
        } catch (final EsterDaoException e) {
            throw new EsterServiceException(e.getMessage(), e.getHelperMessage());
        }
    }

    @Override
    public List<ReferenceBean> findAll() throws EsterServiceException {
        try {
            return this.referenceDao.read();
        } catch (final EsterDaoException e) {
            throw new EsterServiceException(e.getMessage(), e.getHelperMessage());
        }
    }
}

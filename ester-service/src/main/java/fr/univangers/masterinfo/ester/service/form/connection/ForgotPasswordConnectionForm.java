package fr.univangers.masterinfo.ester.service.form.connection;

import fr.univangers.masterinfo.ester.service.form.AbstractForm;
import fr.univangers.masterinfo.ester.service.form.Form;
import fr.univangers.masterinfo.ester.service.form.Input;

/**
 * Formulaire d'oubli de mot de passe
 *
 * @author Clémence DURAND
 * @version 1.0
 * @date 22/10/2020
 */
@Form(name = ForgotPasswordConnectionForm.FORGOT_PASSWORD_CONNECTION_FORM)
public class ForgotPasswordConnectionForm extends AbstractForm {

    /**
     * Nom du formulaire accessible dans la requete pour la JSP
     */
    public static final String FORGOT_PASSWORD_CONNECTION_FORM = "forgotPasswordConnectionForm";
    /**
     * Mapping email entre le champ input / attribut java
     */
    public static final String INPUT_EMAIL = "email";
    /**
     * Serial Version UID
     */
    private static final long serialVersionUID = 1L;
    /**
     * L'email
     */
    @Input(name = INPUT_EMAIL)
    private String email;

    /**
     * @return the email
     */
    public String getEmail() {
        return this.email;
    }

    /**
     * @param email the email to set
     */
    public void setEmail(final String email) {
        this.email = email;
    }
}

package fr.univangers.masterinfo.ester.service.impl;

import fr.univangers.masterinfo.ester.dao.bean.*;
import fr.univangers.masterinfo.ester.dao.contract.EmployeeDaoContract;
import fr.univangers.masterinfo.ester.dao.contract.MemberEsterDaoContract;
import fr.univangers.masterinfo.ester.dao.criteria.EmployeeCriteria;
import fr.univangers.masterinfo.ester.dao.criteria.MemberEsterCriteria;
import fr.univangers.masterinfo.ester.dao.exception.EsterDaoException;
import fr.univangers.masterinfo.ester.service.contract.AccountServiceContract;
import fr.univangers.masterinfo.ester.service.exception.EsterServiceException;
import fr.univangers.masterinfo.ester.service.form.account.CreateAccountForm;
import fr.univangers.masterinfo.ester.service.form.account.EmployeeAccountForm;
import fr.univangers.masterinfo.ester.service.form.notification.NotificationLevel;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;


/**
 * Service pour gérer les comptes
 *
 * @version 1.0
 * @date 04/10/2020
 */
@Service
public class AccountServiceImpl extends AbstractServiceImpl implements AccountServiceContract {

    /**
     * Log de la classe
     */
    private static final Logger LOG = LogManager.getLogger(AccountServiceImpl.class);

    /**
     * La DAO membre ESTER
     */
    @Autowired
    private MemberEsterDaoContract memberEsterDao;

    /**
     * La DAO salarié
     */
    @Autowired
    private EmployeeDaoContract employeeDao;

    @Override
    public UserRole[] getRoles(final CreateAccountForm createAccountForm)
            throws EsterServiceException {
        UserRole[] listeRoles = new UserRole[0];

        switch (createAccountForm.getSessionUser().getRole()) {
            case ADMIN:
                return UserRole.values();

            case PLURIDISCIPLINAIRE:
                listeRoles = new UserRole[1];
                listeRoles[0] = UserRole.SALARIE;
                return listeRoles;

            case MEDECIN:
                listeRoles = new UserRole[2];
                listeRoles[0] = UserRole.PLURIDISCIPLINAIRE;
                listeRoles[1] = UserRole.SALARIE;

                return listeRoles;

            default:
                return listeRoles;

        }
    }

    @Override
    public boolean checkEmailExist(final CreateAccountForm createAccountForm)
            throws EsterServiceException {
        switch (createAccountForm.getSelectedRole()) {
            case SALARIE:
                return this.researchMailEmployee(createAccountForm);
            default:
                return this.researchMailMemberEster(createAccountForm);
        }
    }

    @Override
    public boolean checkLoginExist(CreateAccountForm createAccountForm) throws EsterServiceException {
        switch (createAccountForm.getSelectedRole()) {
            case SALARIE:
                return this.researchLoginEmployee(createAccountForm);
            default:
                return this.researchLoginMemberEster(createAccountForm);
        }
    }

    /**
     * Recherche d'un mail d'un salarié
     *
     * @param createAccountForm
     * @return vrai si l'adresse mail d'un salarié existe. Faux dans le cas
     * contraire
     * @throws EsterServiceException
     */
    private boolean researchMailEmployee(final CreateAccountForm createAccountForm)
            throws EsterServiceException {
        final EmployeeCriteria employeeCriteria = new EmployeeCriteria();
        employeeCriteria.setEmail(createAccountForm.getMailAccount());
        try {
            if (this.employeeDao.existEmployeeByEmail(employeeCriteria)) {
                createAccountForm.notify("L’adresse mail est déjà définie pour un compte !",
                        NotificationLevel.ERROR);
                return true;
            } else {
                return false;
            }
        } catch (final EsterDaoException e) {
            throw new EsterServiceException(e.getMessage(), e.getHelperMessage());
        }
    }


    /**
     * Recherche un mail d'un membre ESTER
     *
     * @param createAccountForm
     * @return vrai si l'adresse mail d'un membre ESTER existe. Faux dans le cas
     * contraire
     * @throws EsterServiceException
     */
    private boolean researchMailMemberEster(final CreateAccountForm createAccountForm)
            throws EsterServiceException {
        final MemberEsterCriteria memberEsterCriteria = new MemberEsterCriteria();
        memberEsterCriteria.setEmail(createAccountForm.getMailAccount());
        try {
            if (this.memberEsterDao.existMemberEsterByMail(memberEsterCriteria)) {
                createAccountForm.notify("L’adresse mail est déjà définie pour un compte.",
                        NotificationLevel.ERROR);
                return true;
            } else {
                return false;
            }
        } catch (final EsterDaoException e) {
            throw new EsterServiceException(e.getMessage(), e.getHelperMessage());
        }
    }

    /**
     * Recherche le login d'un salarié
     *
     * @param createAccountForm
     * @return vrai si le login d'un salarié existe. Faux dans le cas
     * contraire
     * @throws EsterServiceException
     */
    private boolean researchLoginEmployee(final CreateAccountForm createAccountForm)
            throws EsterServiceException {
        final EmployeeCriteria employeeCriteria = new EmployeeCriteria();
        employeeCriteria.setLogin(createAccountForm.getLoginAccount());
        try {
            if (this.employeeDao.existEmployeeByLogin(employeeCriteria)) {
                createAccountForm.notify("Ce login est déjà défini pour un compte !",
                        NotificationLevel.ERROR);
                return true;
            } else {
                return false;
            }
        } catch (final EsterDaoException e) {
            throw new EsterServiceException(e.getMessage(), e.getHelperMessage());
        }
    }

    /**
     * Recherche le login d'un membre ESTER
     *
     * @param createAccountForm
     * @return vrai si le login d'un membre ESTER existe. Faux dans le cas
     * contraire
     * @throws EsterServiceException
     */
    private boolean researchLoginMemberEster(final CreateAccountForm createAccountForm)
            throws EsterServiceException {
        final MemberEsterCriteria memberEsterCriteria = new MemberEsterCriteria();
        memberEsterCriteria.setLogin(createAccountForm.getLoginAccount());
        try {
            if (this.memberEsterDao.existMemberEsterByLogin(memberEsterCriteria)) {
                createAccountForm.notify("Le login est déjà défini pour un compte.",
                        NotificationLevel.ERROR);
                return true;
            } else {
                return false;
            }
        } catch (final EsterDaoException e) {
            throw new EsterServiceException(e.getMessage(), e.getHelperMessage());
        }
    }

    @Override
    public void registerUser(final CreateAccountForm createAccountForm)
            throws EsterServiceException {
        switch (createAccountForm.getSelectedRole()) {
            case SALARIE:
                String login = this.CreateLogin();
                this.registerEmployee(createAccountForm, login);
                createAccountForm.notify("Le compte du salarié à été crée avec le login suivant : " + login, NotificationLevel.SUCCESS);
                break;
            default:
                createLoginFromName(createAccountForm);
                this.registerMemberEster(createAccountForm);
                createAccountForm.notify("Le compte a été créé avec succès !", NotificationLevel.SUCCESS);
                break;
        }
    }

    /**
     * Renvoie un mot de passe généré aléatoire
     */
    private String CreatePassword() {
        String caracteres = "ABCDEFGHIJKLMNOPQRSTUVWXYZ" + "abcdefghijklmnopqrstuvwxyz"
                + "0123456789";
        StringBuilder builder = new StringBuilder(10);

        for (int m = 0; m < 10; m++) {
            int caractere = (int) (caracteres.length() * Math.random());

            builder.append(caracteres.charAt(caractere));
        }

        return builder.toString();
    }

    /**
     * Renvoie un login généré aléatoire
     */
    private String CreateLogin() {
        String caracteres = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        StringBuilder builder = new StringBuilder(9);

        for (int m = 0; m < 9; m++) {
            int caractere = (int) (caracteres.length() * Math.random());
            builder.append(caracteres.charAt(caractere));
        }
        builder.insert(6, '-');
        builder.insert(3, '-');
        return builder.toString();
    }

    /**
     * Enregistre un membre ESTER
     *
     * @param createAccountForm
     * @throws EsterServiceException
     */
    private void registerMemberEster(final CreateAccountForm createAccountForm)
            throws EsterServiceException {
        final MemberEsterBean memberEsterBean = new MemberEsterBean();
        memberEsterBean.setGroup(UserGroup.MEMBRE_ESTER);
        memberEsterBean.setRole(createAccountForm.getSelectedRole());
        memberEsterBean.setEmail(createAccountForm.getMailAccount());
        memberEsterBean.setLogin(createAccountForm.getLoginAccount());
        memberEsterBean.setPassword(CreatePassword());

        try {
            this.memberEsterDao.create(memberEsterBean);
        } catch (final EsterDaoException e) {
            throw new EsterServiceException(e.getMessage(), e.getHelperMessage());
        }
    }

    /**
     * Enregistre un salarié
     *
     * @param createAccountForm
     * @param login             le login du salarié
     * @return String revoit l'id du salarié
     * @throws EsterServiceException
     */
    private void registerEmployee(final CreateAccountForm createAccountForm, String login)
            throws EsterServiceException {
        final EmployeeBean employeeBean = new EmployeeBean();

        employeeBean.setGroup(UserGroup.SALARIE);
        employeeBean.setRole(createAccountForm.getSelectedRole());
        employeeBean.setLogin(login);

        switch (createAccountForm.getSexeAccount()) {
            case "homme":
                employeeBean.setSexe(UserSexe.HOMME);
                break;
            case "femme":
                employeeBean.setSexe(UserSexe.FEMME);
                break;
            default:
                employeeBean.setSexe(UserSexe.AUTRE);
        }

        String birthYear = createAccountForm.getBirthAccount();
        String pcs = createAccountForm.getPCSAccount();
        String naf = createAccountForm.getNAFAccount();
        String commentaire = createAccountForm.getCommentaireAccount();

        if(birthYear != null && !birthYear.isBlank()) {
            // Format déprécié, faut trouver autre chose qu'un Date
            Date date = new Date();
            date.setYear(Integer.parseInt(createAccountForm.getBirthAccount()) - 1900);
            employeeBean.setBirth(date);
        }
        if(pcs != null && !pcs.isBlank())
            employeeBean.setPcs(pcs);
        if(naf != null && !naf.isBlank())
            employeeBean.setNaf(naf);
        if(commentaire != null && !commentaire.isBlank())
            employeeBean.setCommentaires(commentaire);

        try {
            this.employeeDao.create(employeeBean);
        } catch (final EsterDaoException e) {
            throw new EsterServiceException(e.getMessage(), e.getHelperMessage());
        }
    }

    @Override
    public List<EmployeeBean> findAll() throws EsterServiceException {
        try {
            return this.employeeDao.read();
        } catch (final EsterDaoException e) {
            throw new EsterServiceException(e.getMessage(), e.getHelperMessage());
        }
    }

    @Override
    public List<MemberEsterBean> findMemberEsterAdminAndMedecin() throws EsterServiceException {
        try {
            MemberEsterCriteria memberEsterCriteria = new MemberEsterCriteria();
            memberEsterCriteria.getRoles().add(UserRole.ADMIN);
            memberEsterCriteria.getRoles().add(UserRole.MEDECIN);

            return this.memberEsterDao.findMemberEsterByRoles(memberEsterCriteria);
        } catch (final EsterDaoException e) {
            throw new EsterServiceException(e.getMessage(), e.getHelperMessage());
        }
    }

    @Override
    public List<MemberEsterBean> findAllMemberEsterExceptCurrentUser(String ID) throws EsterServiceException {
        try {
            MemberEsterCriteria memberEsterCriteria = new MemberEsterCriteria();
            memberEsterCriteria.setId(ID);

            return this.memberEsterDao.findAllExceptCurrentUser(memberEsterCriteria);
        } catch (final EsterDaoException e) {
            throw new EsterServiceException(e.getMessage(), e.getHelperMessage());
        }

    }

    @Override
    public EmployeeBean findEmployeeByID(String ID) throws EsterServiceException {
        try {
            EmployeeCriteria employeeCriteria = new EmployeeCriteria();
            employeeCriteria.setId(ID);

            return this.employeeDao.findEmployeeByID(employeeCriteria);
        } catch (final EsterDaoException e) {
            throw new EsterServiceException(e.getMessage(), e.getHelperMessage());
        }
    }

    @Override
    public void createLoginFromName(CreateAccountForm CreateAccountForm) throws EsterServiceException {
        int nbFails = 0;
        String prenom = CreateAccountForm.getNameAccount();
        String nom = CreateAccountForm.getFamilyNameAccount();
        do {
            String subStringPrenom = nbFails == 0 ? prenom.substring(0, 1) : prenom.substring(0, 1) + nbFails;
            nbFails++;
            CreateAccountForm.setLoginAccount(nom + subStringPrenom);
        } while (this.checkLoginExist(CreateAccountForm));
    }

    @Override
    public String checkIfUserCanAccess(UserBean user, UserRole minRole) {
        String ERROR_JSP = "/WEB-INF/jsp/librairies/error.jsp";
        String LOGIN_CONNECTION_JSP = "/WEB-INF/jsp/connection/loginConnection.jsp";

        // si on n'a pas de role minimum alors tout le monde peut accéder à la page
        if(minRole == null) return null;

        // si le user n'existe pas on redirige le client vers la page de connection
        if (user == null) {
            return LOGIN_CONNECTION_JSP;
        }

        UserRole role = user.getRole();

        // on regarde si le current user à les privileges minimaux
        if (getSecurityLevel(minRole) > getSecurityLevel(role)) {
            return ERROR_JSP;
        }

        return null;
    }

    private int getSecurityLevel(UserRole role) {
        int securityLevel;

        switch (role){
            case ADMIN: securityLevel = 4; break;
            case MEDECIN: securityLevel = 3; break;
            case PLURIDISCIPLINAIRE: securityLevel = 2; break;
            case SALARIE: securityLevel = 1; break;
            default: securityLevel = 0;
        }

        return securityLevel;
    }

    /**
     * Modifie le compte salarié
     *
     * @param form
     * @throws EsterServiceException
     */
    @Override
    public void modifyEmployee(EmployeeAccountForm form) throws EsterServiceException {
        final EmployeeCriteria employeeCriteria = new EmployeeCriteria();
        employeeCriteria.setLogin(form.getLogin());
        EmployeeBean employeeBean;
        try {
            employeeBean = employeeDao.findEmployee(employeeCriteria);

            employeeBean.setSexe(form.getSexe());
            String birthYear = form.getBirthYear();
            String pcs = form.getSelectedPCS();
            String naf = form.getSelectedNAF();
            String commentaire = form.getCommentaire();
            if(birthYear != null && !birthYear.isBlank()) {
                // Format déprécié, faut trouver autre chose qu'un Date
                Date date = new Date();
                date.setYear(Integer.parseInt(birthYear) - 1900);
                employeeBean.setBirth(date);
            }
            employeeBean.setPcs(pcs);
            employeeBean.setNaf(naf);
            employeeBean.setCommentaires(commentaire);

            this.employeeDao.update(employeeBean);
        } catch (EsterDaoException e) {
            throw new EsterServiceException(e.getMessage(), e.getHelperMessage());
        }
    }

    @Override
    public ArrayList<EmployeeQuestionnaires> getMappingEmployee(List<EmployeeBean> employeeBeans) {
        ArrayList<EmployeeQuestionnaires> mappingEmployee = new ArrayList<>();
        for (EmployeeBean employee : employeeBeans) {
            Set<QuestionnaryBean> setAnswered = employee.getQuestionnariesAnswered();
            Set<QuestionnaryBean> setNotAnswered = employee.getQuestionnariesNoAnswered();

            ArrayList<QuestionnaryBean> questionnaireAnsBeans = new ArrayList<>();
            ArrayList<QuestionnaryBean> questionnaireNotAnsBeans = new ArrayList<>();

            for (Iterator<QuestionnaryBean> it = setAnswered.iterator(); it.hasNext(); ) {
                QuestionnaryBean questionnaryBean = it.next();
                questionnaireAnsBeans.add(questionnaryBean);
            }

            for (Iterator<QuestionnaryBean> it = setNotAnswered.iterator(); it.hasNext(); ) {
                QuestionnaryBean questionnaryBean = it.next();
                questionnaireNotAnsBeans.add(questionnaryBean);
            }

            mappingEmployee.add(new EmployeeQuestionnaires(employee, questionnaireAnsBeans, questionnaireNotAnsBeans));
        }
        return mappingEmployee;
    }
}
package fr.univangers.masterinfo.ester.service.form.data;

import fr.univangers.masterinfo.ester.dao.bean.QuestionnaryBean;
import fr.univangers.masterinfo.ester.dao.bean.ReferenceBean;
import fr.univangers.masterinfo.ester.service.form.AbstractForm;
import fr.univangers.masterinfo.ester.service.form.Form;
import fr.univangers.masterinfo.ester.service.form.Input;
import fr.univangers.masterinfo.ester.service.form.notification.NotificationLevel;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * @author etudiant
 */
@Form(name = ImportReferencesDataForm.IMPORT_REFERENCES_DATA_FORM)
public class ImportReferencesDataForm extends AbstractForm {

    /**
     * Nom du formulaire accessible dans la requete via la JSP
     */
    public static final String IMPORT_REFERENCES_DATA_FORM = "importReferencesDataForm";
    /**
     * Mapping fichier import entre le champ input HTML / attribut Java
     */
    public static final String INPUT_FILE = "fileReference";
    /**
     * Mapping select nom BDD entre le champ input HTML / attribut Java
     */
    public static final String INPUT_DATABASE_NAME = "nomBDD";
    /**
     * Mapping select nom BDD entre le champ input HTML / attribut Java
     */
    public static final String INPUT_DATABASE_VARIABLES = "variablesBDD";
    /**
     * Serial version UID
     */
    private static final long serialVersionUID = 1L;
    /**
     * LOG
     */
    private static final Logger LOG = LogManager.getLogger(ImportReferencesDataForm.class);
    /**
     * Path to location of temporary file to mangoimport
     */
    private String routePathServer;

    /**
     * Liste des References
     */
    private List<ReferenceBean> references = new ArrayList<>();

    /**
     * Liste des Questionaries
     */
    private List<QuestionnaryBean> questionnaries = new ArrayList<>();

    /**
     * Nom de la BDD
     */
    @Input(name = INPUT_DATABASE_NAME)
    private String nomBDD;

    /**
     * Variables
     */
    @Input(name = INPUT_DATABASE_VARIABLES)
    private String variablesBDD;

    public String getVariablesBDD() {
        return variablesBDD;
    }

    public void setVariablesBDD(String variablesBDD) {
        this.variablesBDD = variablesBDD;
    }

    /**
     * Champ fichier import
     */
    @Input(name = INPUT_FILE)
    private InputStream file;

    public boolean isValid() {
        // Check File: exists, below 16Mb
        if (this.file == null) {
            this.notify("InputStream NULL", NotificationLevel.ERROR);
            return false;
        }

        try {
            // final byte[] bytearray = IOUtils.toByteArray(this.file);
            int size = this.file.available();
            if (size > 16000000) {
                this.notify("Fichier trop gros (>16MB)", NotificationLevel.ERROR);
                return false;
            }
            if (size <= 0) {
                this.notify("Fichier manquant", NotificationLevel.ERROR);
                return false;
            }
        } catch (final IOException e) {

            // throw new ServiceException(e.getMessage()); //<< pas conform au super
        }

        // Check name: exists, not used, 10char
        if (this.nomBDD == "") {
            this.notify("Nom manquant", NotificationLevel.ERROR);
            return false;
        }
        if (this.nomBDD.length() > 10) {
            this.notify("Nom trop long (>10char)", NotificationLevel.ERROR);
            return false;
        }

        final String collection = "t_" + this.getNomBDD().toLowerCase();


        for (final ReferenceBean b : this.references) {


            if (b.getName() == collection) {
                this.notify("Nom dejà utilisé", NotificationLevel.ERROR);
                return false;
            }
        }
        return true;
    }

    /**
     * @return the file
     */
    public InputStream getFile() {
        return this.file;
    }

    /**
     * @param file the file to set
     */
    public void setFile(final InputStream file) {
        this.file = file;
    }

    /**
     * @return nom de la BDD
     */
    public String getNomBDD() {
        return this.nomBDD;
    }

    /**
     * @param nomBDD
     */
    public void setNomBDD(final String nomBDD) {
        this.nomBDD = nomBDD;
    }

    /**
     * @return the questionnaries
     */
    public List<QuestionnaryBean> getQuestionnaries() {
        return this.questionnaries;
    }

    /**
     * @param questionnaries the questionnaries to set
     */
    public void setQuestionnaries(final List<QuestionnaryBean> questionnaries) {
        this.questionnaries = questionnaries;
    }

    /**
     * @return the routePathServer
     */
    public String getRoutePathServer() {
        return this.routePathServer;
    }

    /**
     * @param routePathServer the routePathServer to set
     */
    public void setRoutePathServer(final String routePathServer) {
        this.routePathServer = routePathServer;
    }

    /**
     * @return the references
     */
    public List<ReferenceBean> getReferences() {
        return this.references;
    }

    /**
     * @param references the references to set
     */
    public void setReferences(final List<ReferenceBean> references) {
        this.references = references;

    }

}

package fr.univangers.masterinfo.ester.service.impl;

import fr.univangers.masterinfo.ester.dao.bean.EmployeeBean;
import fr.univangers.masterinfo.ester.dao.bean.QuestionBean;
import fr.univangers.masterinfo.ester.dao.bean.ResultBean;
import fr.univangers.masterinfo.ester.dao.contract.ResultDaoContract;
import fr.univangers.masterinfo.ester.dao.criteria.ResultCriteria;
import fr.univangers.masterinfo.ester.dao.exception.EsterDaoException;
import fr.univangers.masterinfo.ester.service.contract.AnalysisServiceContract;
import fr.univangers.masterinfo.ester.service.exception.EsterServiceException;
import fr.univangers.masterinfo.ester.service.form.analysis.AccessWebSiteAnalysisForm;
import fr.univangers.masterinfo.ester.service.form.analysis.CompareDataReferencesAnalysisForm;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * Service pour analyser les données
 */
@Service
public class AnalysisServiceImpl extends AbstractServiceImpl implements AnalysisServiceContract {
    /**
     * La DAO Resultat
     */
    @Autowired
    private ResultDaoContract resultDao;
    /**
     * Log de la classe
     */
    private static final Logger LOG = LogManager
            .getLogger(AnalysisServiceImpl.class);

    @Override
    public int getScoresGolablsByEmployeeAndQuestionnary(final AccessWebSiteAnalysisForm form)
            throws EsterServiceException {
        final ResultCriteria resultCriteria = new ResultCriteria();
        // le choix de -2 est aléatoire pour éviter de donner 0
        int scoreGlobal = -2;
        resultCriteria.setIdEmployee(form.getIdEmployee());
        resultCriteria.setIdQuestionnary(form.getIdQuestionnary());
        try {
            scoreGlobal = this.resultDao.getScoreGlobalByEmployeeAndQuestionnary(resultCriteria);
        } catch (final EsterDaoException e) {
            throw new EsterServiceException(e.getMessage(), "Erreur dans la recherche en base de données pour le score");
        }

        return scoreGlobal;
    }

    @Override
    public List<Long> getlistScoreResults(String dbName, String variables)
            throws EsterServiceException {
        try {
            return this.resultDao.getlistScoreResults(dbName, variables);
        } catch (final EsterDaoException e) {
            throw new EsterServiceException(e.getMessage(), e.getHelperMessage());
        }
    }

    @Override
    public long getPercentageValueVariable(String tableReferenceName, String variable, int value)
            throws EsterServiceException {
        try {
            return this.resultDao.getPercentageValueVariable(tableReferenceName, variable, value);
        } catch (final EsterDaoException e) {
            throw new EsterServiceException(e.getMessage(), e.getHelperMessage());
        }
    }

    @Override
    public int getScoresGolablsByEmployeeAndQuestionnary(
            final CompareDataReferencesAnalysisForm form) throws EsterServiceException {
        final ResultCriteria resultCriteria = new ResultCriteria();
        // le choix de -2 est aléatoire pour éviter de donner 0
        int scoreGlobal = -2;
        resultCriteria.setIdEmployee(form.getIdEmployee());
        resultCriteria.setIdQuestionnary(form.getIdQuestionnary());
        try {
            scoreGlobal = this.resultDao.getScoreGlobalByEmployeeAndQuestionnary(resultCriteria);
        } catch (final EsterDaoException e) {
            throw new EsterServiceException(e.getMessage(), e.getHelperMessage());
        }

        return scoreGlobal;
    }

    @Override
    public List<QuestionBean> getQuestionsPerGroup(AccessWebSiteAnalysisForm form) throws EsterServiceException {
        final ResultCriteria resultCriteria = new ResultCriteria();
        resultCriteria.setIdEmployee(form.getIdEmployee());
        resultCriteria.setIdQuestionnary(form.getIdQuestionnary());
        List<QuestionBean> listeTypes = new ArrayList<>();

        try {
            listeTypes = this.resultDao.getQuestionsPerGroup(resultCriteria);
        } catch (final EsterDaoException e) {
            throw new EsterServiceException(e.getMessage(), e.getHelperMessage());
        }
        return listeTypes;
    }

    @Override
    public ResultBean getResultByEmployeeAndQuestionnary(
            final AccessWebSiteAnalysisForm form) throws EsterServiceException {
        final ResultCriteria resultCriteria = new ResultCriteria();
        ResultBean resultBean = new ResultBean();

        resultCriteria.setIdEmployee(form.getIdEmployee());
        resultCriteria.setIdQuestionnary(form.getIdQuestionnary());
        try {
            resultBean = this.resultDao.getResultByEmployeeAndQuestionnary(resultCriteria);
        } catch (final EsterDaoException e) {
            throw new EsterServiceException(e.getMessage(), e.getHelperMessage());
        }

        return resultBean;
    }

    @Override
    public List<EmployeeBean> getEmployeeWithAnswer(AccessWebSiteAnalysisForm form) throws EsterServiceException {
        final ResultCriteria resultCriteria = new ResultCriteria();
        resultCriteria.setIdEmployee(form.getIdEmployee());
        resultCriteria.setIdQuestionnary(form.getIdQuestionnary());
        List<EmployeeBean> listes = new ArrayList<>();

        try {
            listes = this.resultDao.getEmployeeWithAnswer(resultCriteria);
        } catch (final EsterDaoException e) {
            throw new EsterServiceException(e.getMessage(), e.getHelperMessage());
        }
        return listes;
    }

}

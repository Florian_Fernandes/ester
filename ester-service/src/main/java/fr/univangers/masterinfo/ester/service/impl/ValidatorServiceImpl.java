package fr.univangers.masterinfo.ester.service.impl;

import fr.univangers.masterinfo.ester.service.contract.ValidatorServiceContract;
import fr.univangers.masterinfo.ester.service.form.AbstractForm;
import fr.univangers.masterinfo.ester.service.form.notification.NotificationLevel;
import fr.univangers.masterinfo.ester.service.form.validation.order.Sequence;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.validation.ConstraintViolation;
import javax.validation.Validator;
import java.util.Set;

/**
 * Service pour faire de la validation
 *
 * @version 1.0
 * @date 04/10/2020
 */
@Service
public class ValidatorServiceImpl extends AbstractServiceImpl implements ValidatorServiceContract {

    /**
     * Validation des champs d'un formulaire
     */
    @Autowired
    protected Validator validator;

    @Override
    public boolean formIsValid(final AbstractForm form) {
        final Set<ConstraintViolation<AbstractForm>> constraints = this.validator.validate(form,
                Sequence.class);

        for (final ConstraintViolation<AbstractForm> constraint : constraints) {
            form.notify(constraint.getMessage(), NotificationLevel.ERROR);
        }

        return constraints.isEmpty();
    }

}

package fr.univangers.masterinfo.ester.service.form.connection;

import fr.univangers.masterinfo.ester.service.form.AbstractForm;
import fr.univangers.masterinfo.ester.service.form.Form;

/**
 * Formulaire tableau de bord
 *
 * @version 1.0
 * @date 04/10/2020
 */
@Form(name = DashboardConnectionForm.DASHBOARD_CONNECTION_FORM)
public class DashboardConnectionForm extends AbstractForm {

    /**
     * Nom du formulaire accessible dans la requete pour la JSP
     */
    public static final String DASHBOARD_CONNECTION_FORM = "dashboardConnectionForm";
    /**
     * Serial Version UID
     */
    private static final long serialVersionUID = 1L;
}

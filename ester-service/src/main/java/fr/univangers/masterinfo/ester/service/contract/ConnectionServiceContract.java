package fr.univangers.masterinfo.ester.service.contract;

import fr.univangers.masterinfo.ester.dao.bean.UserBean;
import fr.univangers.masterinfo.ester.service.exception.EsterServiceException;
import fr.univangers.masterinfo.ester.service.form.connection.LoginConnectionForm;

/**
 * Contrat pour connecter un utilisateur
 *
 * @version 1.0
 * @date 04/10/2020
 */
public interface ConnectionServiceContract extends AbstractServiceContract {

    /**
     * Retrouver un utilisateur selon le formulaire de connexion
     *
     * @param form
     * @return L'utilisateur selon des critères
     * @throws EsterServiceException
     */
    UserBean findUser(LoginConnectionForm form) throws EsterServiceException;

}
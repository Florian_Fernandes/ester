package fr.univangers.masterinfo.ester.service.contract;

import fr.univangers.masterinfo.ester.service.exception.EsterServiceException;
import fr.univangers.masterinfo.ester.service.form.account.CreateAccountForm;

/**
 * Contrat pour gérer les comptes
 *
 * @version 1.0
 * @date 04/10/2020
 */
public interface MailServiceContract extends AbstractServiceContract {

    /**
     * Envoyer un email de création de compte
     *
     * @param form
     * @throws EsterServiceException
     */
    void sendCreateAccountSync(CreateAccountForm form) throws EsterServiceException;

    void sendCreateAccountAsync(CreateAccountForm form) throws EsterServiceException;
}
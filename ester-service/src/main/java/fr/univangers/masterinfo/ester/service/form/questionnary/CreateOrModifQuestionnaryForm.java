package fr.univangers.masterinfo.ester.service.form.questionnary;

import fr.univangers.masterinfo.ester.dao.bean.ReferenceBean;
import fr.univangers.masterinfo.ester.service.form.AbstractForm;
import fr.univangers.masterinfo.ester.service.form.Form;
import fr.univangers.masterinfo.ester.service.form.Input;

import java.util.ArrayList;
import java.util.List;

@Form(name = fr.univangers.masterinfo.ester.service.form.questionnary.CreateOrModifQuestionnaryForm.CREATE_OR_MODIF_QUESTIONNARY)
public class CreateOrModifQuestionnaryForm extends AbstractForm {
    /**
     * Nom du formulaire accessible dans la requete via la JSP
     */
    public static final String CREATE_OR_MODIF_QUESTIONNARY = "createOrModifQuestionnaryForm";
    /**
     *
     */
    public static final String INPUT_QUESTIONNARY_NAME = "nouveau questionnaire";
    /**
     * Mapping select questionnary entre le champ input HTML / attribut Java
     */
    public static final String SELECT_REFERENCE = "selectReference";
    /**
     * Serial Version UID
     */
    private static final long serialVersionUID = 1L;
    /**
     *
     */
    @Input(name = INPUT_QUESTIONNARY_NAME)
    private String questionnary_name;
    /**
     * Liste des References
     */
    private List<ReferenceBean> references = new ArrayList<>();
    /**
     * Questionnaire selectionné
     */
    @Input(name = SELECT_REFERENCE)
    private String selectReference;

    /**
     * @return the selectReference
     */
    public String getSelectReference() {
        return this.selectReference;
    }

    /**
     * @param selectReference the selectReference to set
     */
    public void setSelectReference(final String selectReference) {
        this.selectReference = selectReference;
    }

    /**
     * @return the references
     */
    public List<ReferenceBean> getReferences() {
        return this.references;
    }

    /**
     * @param references the references to set
     */
    public void setReferences(final List<ReferenceBean> references) {
        this.references = references;
    }

    /**
     * @return the questionnary name
     */
    public String getQuestionnaryName() {
        return this.questionnary_name;
    }

    /**
     * @param questionnary_name the questionnary_name to set
     */
    public void setQuestionnaryName(final String questionnary_name) {
        this.questionnary_name = questionnary_name;
    }
}

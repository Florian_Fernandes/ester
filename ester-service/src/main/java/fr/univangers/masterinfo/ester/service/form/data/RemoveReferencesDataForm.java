package fr.univangers.masterinfo.ester.service.form.data;

import fr.univangers.masterinfo.ester.dao.bean.ReferenceBean;
import fr.univangers.masterinfo.ester.service.form.AbstractForm;
import fr.univangers.masterinfo.ester.service.form.Form;
import fr.univangers.masterinfo.ester.service.form.Input;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.List;

/**
 * @author etudiant
 */
@Form(name = RemoveReferencesDataForm.REMOVE_REFERENCES_DATA_FORM)
public class RemoveReferencesDataForm extends AbstractForm {

    /**
     * Nom du formulaire accessible dans la requete via la JSP
     */
    public static final String REMOVE_REFERENCES_DATA_FORM = "removeReferencesDataForm";
    /**
     * Mapping select questionnary entre le champ input HTML / attribut Java
     */
    public static final String SELECT_REFERENCE = "selectReference";
    /**
     * Serial version UID
     */
    private static final long serialVersionUID = 1L;
    /**
     * LOG
     */
    private static final Logger LOG = LogManager.getLogger(RemoveReferencesDataForm.class);
    /**
     * Liste des References
     */
    private List<ReferenceBean> references = new ArrayList<>();

    /**
     * reference selectionné
     */
    @Input(name = SELECT_REFERENCE)
    private String selectReference;

    /**
     * @return the selectReference
     */
    public String getSelectReference() {
        return this.selectReference;
    }

    /**
     * @param selectReference the selectReference to set
     */
    public void setSelectReference(final String selectReference) {
        this.selectReference = selectReference;
    }

    /**
     * @return the references
     */
    public List<ReferenceBean> getReferences() {
        return this.references;
    }

    /**
     * @param references the references to set
     */
    public void setReferences(final List<ReferenceBean> references) {
        this.references = references;
    }

}

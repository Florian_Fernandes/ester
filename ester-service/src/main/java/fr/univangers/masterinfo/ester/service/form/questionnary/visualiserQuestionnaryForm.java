package fr.univangers.masterinfo.ester.service.form.questionnary;

import fr.univangers.masterinfo.ester.dao.bean.QuestionBean;
import fr.univangers.masterinfo.ester.service.form.AbstractForm;
import fr.univangers.masterinfo.ester.service.form.Form;
import fr.univangers.masterinfo.ester.service.form.Input;

import java.util.Set;

@Form(name = fr.univangers.masterinfo.ester.service.form.questionnary.visualiserQuestionnaryForm.VISUALISER_QUESTIONNARY)
public class visualiserQuestionnaryForm extends AbstractForm {
    /**
     * Nom du formulaire accessible dans la requete via la JSP
     */
    public static final String VISUALISER_QUESTIONNARY = "visualiserQuestionnaryForm";
    /**
     *
     */
    public static final String INPUT_QUESTIONNARY_NAME = "Visualiser";
    /**
     * Serial Version UID
     */
    private static final long serialVersionUID = 1L;
    /**
     *
     */
    @Input(name = INPUT_QUESTIONNARY_NAME)
    private String questionnary_name;

    /**
     * Liste des questions
     */
    private Set<QuestionBean> questions;

    /**
     * @return the questionnary name
     */
    public String getQuestionnaryName() {
        return this.questionnary_name;
    }

    /**
     * @param questionnary_name the questionnary_name to set
     */
    public void setQuestionnaryName(final String questionnary_name) {
        this.questionnary_name = questionnary_name;
    }

    /**
     * @return the list of questions
     */
    public Set<QuestionBean> getQuestions() {
        return this.questions;
    }

    /**
     * @param questions the questions to set
     */
    public void setQuestions(final Set<QuestionBean> questions) {
        this.questions = questions;
    }

}

package fr.univangers.masterinfo.ester.service.form.validation.order;

import com.fasterxml.jackson.databind.ser.std.StdKeySerializers.Default;

import javax.validation.GroupSequence;

/**
 * @author etudiant
 */
@GroupSequence({Default.class, One.class, Two.class, Three.class, Four.class, Five.class,
        Six.class, Seven.class, Eigth.class, Nine.class, Ten.class})
public interface Sequence {

}

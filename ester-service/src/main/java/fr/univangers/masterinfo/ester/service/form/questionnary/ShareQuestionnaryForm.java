package fr.univangers.masterinfo.ester.service.form.questionnary;

import fr.univangers.masterinfo.ester.dao.bean.MemberEsterBean;
import fr.univangers.masterinfo.ester.dao.bean.QuestionnaryBean;
import fr.univangers.masterinfo.ester.service.form.AbstractForm;
import fr.univangers.masterinfo.ester.service.form.Form;
import fr.univangers.masterinfo.ester.service.form.Input;

import java.util.ArrayList;
import java.util.List;

/**
 * @author etudiant
 */
@Form(name = ShareQuestionnaryForm.SHARE_QUESTIONNARY_FORM)
public class ShareQuestionnaryForm extends AbstractForm {

    /**
     * Nom du formulaire accessible dans la requete via la JSP
     */
    public static final String SHARE_QUESTIONNARY_FORM = "shareQuestionnaryForm";
    /**
     * Mapping select MemberEster entre le champ input HTML / attribut Java
     */
    public static final String MEMBER_ESTER_SELECT = "selectMemberEster";
    /**
     * Mapping select questionnary entre le champ input HTML / attribut Java
     */
    public static final String QUESTIONNARY_SELECT = "selectQuestionnary";
    /**
     * Serial Version UID
     */
    private static final long serialVersionUID = 1L;
    /**
     * Membre Ester selectionné
     */
    @Input(name = MEMBER_ESTER_SELECT)
    private String selectMemberEster;

    /**
     * Questionnaire selectionné
     */
    @Input(name = QUESTIONNARY_SELECT)
    private String selectQuestionnary;

    /**
     * Liste des questionnaires
     */
    private List<QuestionnaryBean> questionnaries = new ArrayList<>();

    /**
     * Liste des membres ester
     */
    private List<MemberEsterBean> membersEster = new ArrayList<>();

    /**
     * @return the selectMemberEster
     */
    public String getSelectMemberEster() {
        return this.selectMemberEster;
    }

    /**
     * @param selectMemberEster the selectMemberEster to set
     */
    public void setSelectMemberEster(String selectMemberEster) {
        this.selectMemberEster = selectMemberEster;
    }

    /**
     * @return the selectQuestionnary
     */
    public String getSelectQuestionnary() {
        return this.selectQuestionnary;
    }

    /**
     * @param selectQuestionnary the selectQuestionnary to set
     */
    public void setSelectQuestionnary(String selectQuestionnary) {
        this.selectQuestionnary = selectQuestionnary;
    }

    /**
     * @return the questionnaries
     */
    public List<QuestionnaryBean> getQuestionnaries() {
        return this.questionnaries;
    }

    /**
     * @param questionnaries the questionnaries to set
     */
    public void setQuestionnaries(List<QuestionnaryBean> questionnaries) {
        this.questionnaries = questionnaries;
    }

    /**
     * @return the membersEster
     */
    public List<MemberEsterBean> getMembersEster() {
        return this.membersEster;
    }

    /**
     * @param membersEster the membersEster to set
     */
    public void setMembersEster(List<MemberEsterBean> membersEster) {
        this.membersEster = membersEster;
    }
}

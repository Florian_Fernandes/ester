package fr.univangers.masterinfo.ester.service.form;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * Tag qui permet d'identifier les formulaires
 *
 * @version 1.0
 * @date 04/10/2020
 */
@Retention(RUNTIME)
@Target(TYPE)
@Documented
public @interface Form {
    /**
     * @return Le nom du formulaire
     */
    String name();
}

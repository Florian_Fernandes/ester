package fr.univangers.masterinfo.ester.service.form.questionnary;


import fr.univangers.masterinfo.ester.dao.bean.QuestionnaryBean;
import fr.univangers.masterinfo.ester.service.form.AbstractForm;
import fr.univangers.masterinfo.ester.service.form.Form;

import java.util.ArrayList;
import java.util.List;

@Form(name = fr.univangers.masterinfo.ester.service.form.questionnary.ConfigQuestionnaryForm.CONFIG_QUESTIONNARY)
public class ConfigQuestionnaryForm extends AbstractForm {
    /**
     * Nom du formulaire accessible dans la requete via la JSP
     */
    public static final String CONFIG_QUESTIONNARY = "configQuestionnaryForm";
    /**
     * Serial Version UID
     */
    private static final long serialVersionUID = 1L;
    /**
     * Liste des questionnaires
     */
    private List<QuestionnaryBean> questionnaries = new ArrayList<>();

    /**
     * @return the questionnaries
     */
    public List<QuestionnaryBean> getQuestionnaries() {
        return this.questionnaries;
    }

    /**
     * @param questionnaries the questionnaries to set
     */
    public void setQuestionnaries(List<QuestionnaryBean> questionnaries) {
        this.questionnaries = questionnaries;
    }
}
package fr.univangers.masterinfo.ester.service.form.analysis;

import fr.univangers.masterinfo.ester.dao.bean.ReferenceBean;
import fr.univangers.masterinfo.ester.service.form.AbstractForm;
import fr.univangers.masterinfo.ester.service.form.Form;
import fr.univangers.masterinfo.ester.service.form.Input;

import java.util.ArrayList;
import java.util.List;

/**
 * @author etudiant
 */
@Form(name = CompareDataReferencesAnalysisForm.COMPARE_DATA_REFERENCES_ANALYSIS_FORM)
public class CompareDataReferencesAnalysisForm extends AbstractForm {

    /**
     * Nom du formulaire accessible dans la requete via la JSP
     */
    public static final String COMPARE_DATA_REFERENCES_ANALYSIS_FORM = "compareDataReferencesAnalysisForm";
    /**
     * Mapping idEmployee entre le champ input HTML / attribut Java
     */
    public static final String INPUT_ID_EMPLOYEE = "idEmployee";
    /**
     * Mapping idQuestionnary entre le champ input HTML / attribut Java
     */
    public static final String INPUT_ID_QUESTIONNARY = "idQuestionnary";
    /**
     * Mapping select questionnary entre le champ input HTML / attribut Java
     */
    public static final String SELECT_REFERENCE = "selectReference";
    /**
     * Liste des References
     */
    private List<ReferenceBean> references = new ArrayList<>();

    /**
     * idEmployee
     */
    @Input(name = INPUT_ID_EMPLOYEE)
    private String idEmployee;
    /**
     * idQuestionnaire
     */
    @Input(name = INPUT_ID_QUESTIONNARY)
    private String idQuestionnary;
    /**
     * reference selectionné
     */
    @Input(name = SELECT_REFERENCE)
    private String selectReference;

    /**
     * @return idEmployee
     */
    public String getIdEmployee() {
        return this.idEmployee;
    }

    /**
     * @param idEmployee
     */
    public void setIdEmployee(final String idEmployee) {
        this.idEmployee = idEmployee;
    }

    /**
     * @return idQuestionnary
     */
    public String getIdQuestionnary() {
        return this.idQuestionnary;
    }

    /**
     * @param idQuestionnary
     */
    public void setIdQuestionnary(final String idQuestionnary) {
        this.idQuestionnary = idQuestionnary;
    }

    /**
     * @return the references
     */
    public List<ReferenceBean> getReferences() {
        return this.references;
    }

    /**
     * @param references the references to set
     */
    public void setReferences(final List<ReferenceBean> references) {
        this.references = references;
    }

    /**
     * @return the selectReference
     */
    public String getSelectReference() {
        return this.selectReference;
    }

    /**
     * @param selectReference the selectReference to set
     */
    public void setSelectReference(final String selectReference) {
        this.selectReference = selectReference;
    }
}

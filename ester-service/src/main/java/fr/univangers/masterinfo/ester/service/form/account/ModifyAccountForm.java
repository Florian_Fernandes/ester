package fr.univangers.masterinfo.ester.service.form.account;

import fr.univangers.masterinfo.ester.dao.bean.UserRole;
import fr.univangers.masterinfo.ester.dao.bean.UserSexe;
import fr.univangers.masterinfo.ester.service.form.AbstractForm;
import fr.univangers.masterinfo.ester.service.form.Form;
import fr.univangers.masterinfo.ester.service.form.Input;
import fr.univangers.masterinfo.ester.service.form.validation.order.One;
import fr.univangers.masterinfo.ester.service.form.validation.order.Three;
import fr.univangers.masterinfo.ester.service.form.validation.order.Two;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * Formulaire première connexion
 *
 * @author Clémence DURAND
 * @version 1.0
 * @date 24/10/2020
 */
@Form(name = ModifyAccountForm.MODIFY_ACCOUNT_FORM)
public class ModifyAccountForm extends AbstractForm {

    /**
     * Nom du formulaire accessible dans la requete pour la JSP
     */
    public static final String MODIFY_ACCOUNT_FORM = "modifyAccountForm";
    /**
     *
     */
    public static final String INPUT_SELECTED_ROLE = "selectedRole";
    /**
     *
     */
    public static final String INPUT_LOGIN = "login";
    /**
     *
     */
    public static final String INPUT_MAIL = "mail";
    /**
     *
     */
    public static final String INPUT_ACTUAL_PASSWORD = "actualPassword";
    /**
     *
     */
    public static final String INPUT_NEW_PASSWORD = "newPassword";
    /**
     *
     */
    public static final String INPUT_CONFIRM_PASSWORD = "confirmPassword";
    /**
     *
     */
    public static final String INPUT_SEXE = "sexe";
    /**
     *
     */
    public static final String INPUT_BIRTH_YEAR = "birthYear";
    /**
     *
     */
    public static final String INPUT_SELECTED_PCS = "selectedPCS";
    /**
     *
     */
    public static final String INPUT_SELECTED_NAF = "selectedNAF";
    /**
     *
     */
    public static final String INPUT_COMMENTAIRE = "commentaire";
    /**
     *
     */
    public static final String INPUT_SELECTED_REGION = "selectedRegion";
    /**
     *
     */
    public static final String INPUT_SELECTED_DEPARTEMENT = "selectedDepartement";
    /**
     * Serial Version UID
     */
    private static final long serialVersionUID = 1L;
    /**
     * Liste des rôles
     */
    private UserRole[] roles = new UserRole[0];

    /**
     *
     */
    @Input(name = INPUT_SELECTED_ROLE)
    @NotNull(message = "{role.not.null.msg}", groups = One.class)
    private UserRole selectedRole;

    /**
     *
     */
    @Input(name = INPUT_LOGIN)
    @NotBlank(message = "{login.not.blank.msg}", groups = One.class)
    private String login;

    /**
     *
     */
    @Input(name = INPUT_MAIL)
    @NotBlank(message = "{mail.not.blank.msg}", groups = One.class)
    @Size(message = "{mail.size.msg}", groups = Two.class)
    @Email(message = "{mail.pattern.msg}", regexp = "{mail.pattern.regexp}", groups = Three.class)
    private String mail;

    /**
     *
     */
    @Input(name = INPUT_ACTUAL_PASSWORD)
    @NotBlank(message = "{password.not.blank.msg}", groups = One.class)
    private String actualPassword;

    /**
     *
     */
    @Input(name = INPUT_NEW_PASSWORD)
    @NotBlank(message = "{password.not.blank.msg}", groups = One.class)
    private String newPassword;

    /**
     *
     */
    @Input(name = INPUT_CONFIRM_PASSWORD)
    @NotBlank(message = "{password.not.blank.msg}", groups = One.class)
    private String confirmPassword;


    /**
     *
     */
    @Input(name = INPUT_SEXE)
    private UserSexe sexe;

    /**
     *
     */
    @Input(name = INPUT_BIRTH_YEAR)
    private String birthYear;

    /**
     *
     */
    @Input(name = INPUT_SELECTED_PCS)
    private String selectedPCS;

    /**
     *
     */
    @Input(name = INPUT_SELECTED_NAF)
    private String selectedNAF;

    /**
     *
     */
    @Input(name = INPUT_COMMENTAIRE)
    private String commentaire;

    /**
     *
     */
    @Input(name = INPUT_SELECTED_REGION)
    private String selectedRegion;

    /**
     *
     */
    @Input(name = INPUT_SELECTED_DEPARTEMENT)
    private String selectedDepartement;

    /**
     * @return the roles
     */
    public UserRole[] getRoles() {
        return this.roles;
    }

    /**
     * @param roles the roles to set
     */
    public void setRoles(final UserRole[] roles) {
        this.roles = roles;
    }

    /**
     * @return the selectedRole
     */
    public UserRole getSelectedRole() {
        return this.selectedRole;
    }

    /**
     * @param selectedRole the selectedRole to set
     */
    public void setSelectedRole(final UserRole selectedRole) {
        this.selectedRole = selectedRole;
    }

    /**
     * @return the login
     */
    public String getLogin() {
        return this.login;
    }

    /**
     * @param login the login to set
     */
    public void setLogin(final String login) {
        this.login = login;
    }

    /**
     * @return the mail
     */
    public String getMail() {
        return this.mail;
    }

    /**
     * @param mail the mail to set
     */
    public void setMail(final String mail) {
        this.mail = mail;
    }

    /**
     * @return the actualPassword
     */
    public String getActualPassword() {
        return this.actualPassword;
    }

    /**
     * @param actualPassword the actualPassword to set
     */
    public void setActualPassword(final String actualPassword) {
        this.actualPassword = actualPassword;
    }

    /**
     * @return the newPassword
     */
    public String getNewPassword() {
        return this.newPassword;
    }

    /**
     * @param newPassword the newPassword to set
     */
    public void setNewPassword(final String newPassword) {
        this.newPassword = newPassword;
    }

    /**
     * @return the confirmPassword
     */
    public String getConfirmPassword() {
        return this.confirmPassword;
    }

    /**
     * @param confirmPassword the confirmPassword to set
     */
    public void setConfirmPassword(final String confirmPassword) {
        this.confirmPassword = confirmPassword;
    }


    /**
     * @return the sexe
     */
    public UserSexe getSexe() {
        return this.sexe;
    }

    /**
     * @param sexe the sexe to set
     */
    public void setSexe(final UserSexe sexe) {
        this.sexe = sexe;
    }

    /**
     * @return the birthYear
     */
    public String getBirthYear() {
        return this.birthYear;
    }

    /**
     * @param birthYear the birthYear to set
     */
    public void setBirthYear(final String birthYear) {
        this.birthYear = birthYear;
    }

    /**
     * @return the selectedPCS
     */
    public String getSelectedPCS() {
        return this.selectedPCS;
    }

    /**
     * @param selectedPCS the selectedPCS to set
     */
    public void setSelectedPCS(final String selectedPCS) {
        this.selectedPCS = selectedPCS;
    }

    /**
     * @return the selectedNAF
     */
    public String getSelectedNAF() {
        return this.selectedNAF;
    }

    /**
     * @param selectedNAF the selectedNAF to set
     */
    public void setSelectedNAF(final String selectedNAF) {
        this.selectedNAF = selectedNAF;
    }

    /**
     * @return the commentaire
     */
    public String getCommentaire() {
        return this.commentaire;
    }

    /**
     * @param commentaire the commentaire to set
     */
    public void setCommentaire(final String commentaire) {
        this.commentaire = commentaire;
    }

    /**
     * @return the selectedRegion
     */
    public String getSelectedRegion() {
        return this.selectedRegion;
    }

    /**
     * @param selectedRegion the selectedRegion to set
     */
    public void setSelectedRegion(final String selectedRegion) {
        this.selectedRegion = selectedRegion;
    }

    /**
     * @return the selectedDepartement
     */
    public String getSelectedDepartement() {
        return this.selectedDepartement;
    }

    /**
     * @param selectedDepartement the selectedDepartement to set
     */
    public void setSelectedDepartement(final String selectedDepartement) {
        this.selectedDepartement = selectedDepartement;
    }
}

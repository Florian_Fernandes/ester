# Projet ESTER


## _Développement d’une plateforme informatique pour l’analyse et l’évaluation des risques professionnels_


## **Chefs de projet**					

Florian FERNANDES					

Quentin MAIGNAN		

## **Encadrants**

Laurent GARCIA

Vincent BARICHARD
								


# Installer le projet

Pré requis :



* Installer tomcat 9 : [https://tomcat.apache.org/download-90.cgi](https://tomcat.apache.org/download-90.cgi)
* Installer MongoDB : [https://www.mongodb.com/try/download/community](https://www.mongodb.com/try/download/community)
* Installer Maven : [https://maven.apache.org/download.cgi](https://maven.apache.org/download.cgi)
* Installer le jdk java 11 : [https://www.oracle.com/fr/java/technologies/javase/jdk11-archive-downloads.html](https://www.oracle.com/fr/java/technologies/javase/jdk11-archive-downloads.html)

Outils conseillé :



* Intellij : IDE JAVA ([https://www.jetbrains.com/fr-fr/idea/download/#section=linux](https://www.jetbrains.com/fr-fr/idea/download/#section=linux))
* Compass : outils de gestion de base de données ([https://www.mongodb.com/try/download/compass](https://www.mongodb.com/try/download/compass))
* Git : gestionnaire de version ([https://git-scm.com/downloads](https://git-scm.com/downloads))
* ZSH + plugin git : bash avancé + aliases git ([https://opensource.com/article/19/9/adding-plugins-zsh](https://opensource.com/article/19/9/adding-plugins-zsh))

Build du projet :



* Lancer le script ester-build à la racine du projet. L’option -v permet dans lancé les commandes sans l’option sudo. Le site sera alors disponible en localhost à l’adresse suivant  : [http://127.0.0.1:8080/ester-web/accueil](http://127.0.0.1:8080/ester-web/accueil)

Log JAVA :



* Lancer le script ester-log à la racine du projet. -h pour lister toutes les options disponibles

Log Tomcat :



* Les fichiers se trouve dans **_/etc/lib/tomcat9/logs_**


Deployer sur starwars ! :


* Installer les mêmes paquets que sur votre version local.
* sauf mongo (car cpu trop vieux sur les serveurs du leria) https://www.itzgeek.com/post/how-to-install-mongodb-4-2-4-0-on-debian-10/ (choisir la version 4.2)
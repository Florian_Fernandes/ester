package fr.univangers.masterinfo.ester.api.impl;

import com.sun.jersey.core.header.FormDataContentDisposition;
import com.sun.jersey.multipart.FormDataParam;
import fr.univangers.masterinfo.ester.api.contract.VideoApiContract;
import fr.univangers.masterinfo.ester.api.dto.VideoDto;
import fr.univangers.masterinfo.ester.api.exception.EsterApiException;
import fr.univangers.masterinfo.ester.service.contract.DocumentServiceContract;
import fr.univangers.masterinfo.ester.service.exception.EsterServiceException;
import fr.univangers.masterinfo.ester.service.pojo.VideoPojo;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.StreamingOutput;
import java.io.IOException;
import java.io.InputStream;

/**
 * Implémentation du CRUD générique
 *
 * @version 1.0
 * @date 04/10/2020
 */
@Component
@Path("/video")
public class VideoApiImpl extends AbstractApiImpl<VideoDto> implements VideoApiContract {

    /**
     * Log de la classe
     */
    private static final Logger LOG = LogManager.getLogger(VideoApiImpl.class);
    /**
     * Service document
     */
    @Autowired
    private DocumentServiceContract documentService;

    /**
     * @param code
     * @param fileInputStream
     * @param fileMetaData
     * @return succès ou erreur
     * @throws EsterApiException
     */
    @POST
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    public Response create(@FormDataParam("code") final String code,
                           @FormDataParam("file") final InputStream fileInputStream,
                           @FormDataParam("file") final FormDataContentDisposition fileMetaData)
            throws EsterApiException {

        final VideoDto video = new VideoDto();
        video.setFileInputStream(fileInputStream);
        video.setCode(code);

        return this.create(video);
    }

    @Override
    public Response create(final VideoDto videoDto) throws EsterApiException {
        final VideoPojo videoPojo = new VideoPojo();
        videoPojo.setFileInputStream(videoDto.getFileInputStream());
        videoPojo.setCode(videoDto.getCode());

        try {
            this.documentService.uploadVideo(videoPojo);
        } catch (final EsterServiceException e) {
            throw new EsterApiException(e.getMessage());
        }

        return Response.ok("Video uploaded successfully").build();
    }

    @Override
    @GET
    @Path("/{id}")
    @Produces("video/mp4")
    public Response read(@PathParam("id") final String id) throws EsterApiException {

        try {
            final VideoPojo video = this.documentService.downloadVideo(id);

            final StreamingOutput fileStream = new StreamingOutput() {
                @Override
                public void write(final java.io.OutputStream output) throws IOException {
                    final byte[] data = video.getFileInputStream().readAllBytes();
                    output.write(data);
                    output.flush();
                }
            };

            return Response.ok(fileStream, MediaType.APPLICATION_OCTET_STREAM)
                    .header("content-disposition", "attachment; filename = " + id).build();
        } catch (final EsterServiceException e) {
            throw new EsterApiException(e.getMessage());
        }
    }

    @Override
    @GET
    @Produces("video/mp4")
    public Response read() throws EsterApiException {
        return Response.ok().build();
    }

    @Override
    @PUT
    public Response update(final VideoDto video) throws EsterApiException {
        return Response.ok().build();
    }

    @Override
    public Response delete(final VideoDto video) throws EsterApiException {
        return Response.ok().build();
    }

    @Override
    @DELETE
    @Path("/{id}")
    public Response delete(@PathParam("id") final String id) throws EsterApiException {
        return Response.ok().build();
    }

    @Override
    @DELETE
    public Response delete() throws EsterApiException {
        return Response.ok().build();
    }
}

package fr.univangers.masterinfo.ester.api.dto;

import java.io.Serializable;
import java.util.Objects;

/**
 * DTO abstrait qui a pour identifiant un UUID
 *
 * @version 1.0
 * @date 04/10/2020
 */
public abstract class AbstractDto implements Serializable {

    /**
     * Serial Version UID
     */
    private static final long serialVersionUID = 1L;

    /**
     * Identifiant unique UUID de l'entité
     */
    protected String id;

    /**
     * @return the id
     */
    public String getId() {
        return this.id;
    }

    /**
     * @param id the id to set
     */
    public void setId(final String id) {
        this.id = id;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = (prime * result) + ((this.id == null) ? 0 : this.id.hashCode());
        return result;
    }

    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (this.getClass() != obj.getClass()) {
            return false;
        }
        final AbstractDto other = (AbstractDto) obj;

        // On ne prend pas en compte les id null
        if ((this.id == null) || (other.id == null)) {
            return false;
        }

        if (!Objects.equals(this.id, other.id)) {
            return false;
        }
        return true;
    }
}

package fr.univangers.masterinfo.ester.api.impl;

import fr.univangers.masterinfo.ester.api.contract.AbstractApiContract;
import fr.univangers.masterinfo.ester.api.dto.AbstractDto;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;

/**
 * Implémentation du CRUD générique
 *
 * @param <T> l'entité
 * @version 1.0
 * @date 04/10/2020
 */

public abstract class AbstractApiImpl<T extends AbstractDto> implements AbstractApiContract<T> {

    /**
     * Des infos sur l'URI
     */
    @Context
    protected UriInfo uriInfo;

    /**
     * La requête soumise
     */
    @Context
    protected HttpServletRequest request;

    /**
     * La réponse envoyé au client
     */
    @Context
    protected HttpServletResponse response;

    /**
     * Le contexte d'application
     */
    @Context
    private ServletContext context;
}

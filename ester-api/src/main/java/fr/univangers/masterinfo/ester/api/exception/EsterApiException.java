package fr.univangers.masterinfo.ester.api.exception;

/**
 * Exception de la couche service
 *
 * @version 1.0
 * @date 04/10/2020
 */
public class EsterApiException extends Exception {

    /**
     * Serial Version UID
     */
    private static final long serialVersionUID = 1L;

    /**
     * @param message
     */
    public EsterApiException(final String message) {
        super(message);
    }
}

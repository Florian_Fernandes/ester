package fr.univangers.masterinfo.ester.api.dto;

import java.io.InputStream;

/**
 * @author etudiant
 */
public class PictureDto extends AbstractDto {

    /**
     * Serial version UID
     */
    private static final long serialVersionUID = 1L;

    /**
     * Code unique de l'image afin de la retrouver
     */
    private String code;

    /**
     * Le flux image
     */
    private InputStream fileInputStream;

    /**
     * @return the code
     */
    public String getCode() {
        return this.code;
    }

    /**
     * @param code the code to set
     */
    public void setCode(final String code) {
        this.code = code;
    }

    /**
     * @return the fileInputStream
     */
    public InputStream getFileInputStream() {
        return this.fileInputStream;
    }

    /**
     * @param fileInputStream the fileInputStream to set
     */
    public void setFileInputStream(final InputStream fileInputStream) {
        this.fileInputStream = fileInputStream;
    }
}

package fr.univangers.masterinfo.ester.api.contract;

import fr.univangers.masterinfo.ester.api.dto.AbstractDto;
import fr.univangers.masterinfo.ester.api.exception.EsterApiException;

import javax.ws.rs.core.Response;

/**
 * Le contrat CRUD d'une DAO générique
 *
 * @param <T>
 * @version 1.0
 * @date 04/10/2020
 */
public interface AbstractApiContract<T extends AbstractDto> {

    /**
     * Sauvegarder une entité
     *
     * @param entity
     * @return l'entité crée
     * @throws EsterApiException
     */
    Response create(final T entity) throws EsterApiException;

    /**
     * Récupérer une entité selon un identifiant
     *
     * @param id
     * @return l'entité correpond au critère
     * @throws EsterApiException
     */
    Response read(final String id) throws EsterApiException;

    /**
     * Récupérer toutes les entités
     *
     * @return une liste d'entités
     * @throws EsterApiException
     */
    Response read() throws EsterApiException;

    /**
     * Mettre à jour une entité
     *
     * @param entity
     * @return l'entité maj
     * @throws EsterApiException
     */
    Response update(final T entity) throws EsterApiException;

    /**
     * Supprimer une entité par son id
     *
     * @param id
     * @return l'entité supprimé
     * @throws EsterApiException
     */
    Response delete(String id) throws EsterApiException;

    /**
     * Supprimer une entité
     *
     * @param entity
     * @return l'entité supprimé
     * @throws EsterApiException
     */
    Response delete(T entity) throws EsterApiException;

    /**
     * Supprimer toutes les entités
     *
     * @return toutes les entités supprimé
     * @throws EsterApiException
     */
    Response delete() throws EsterApiException;
}
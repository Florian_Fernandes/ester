package fr.univangers.masterinfo.ester.api.contract;

import fr.univangers.masterinfo.ester.api.dto.VideoDto;

/**
 * Le contrat CRUD d'une DAO générique
 *
 * @version 1.0
 * @date 04/10/2020
 */
public interface VideoApiContract extends AbstractApiContract<VideoDto> {

}
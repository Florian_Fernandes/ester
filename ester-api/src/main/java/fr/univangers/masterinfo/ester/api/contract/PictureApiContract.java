package fr.univangers.masterinfo.ester.api.contract;

import fr.univangers.masterinfo.ester.api.dto.PictureDto;

/**
 * Le contrat CRUD d'une DAO générique
 *
 * @version 1.0
 * @date 04/10/2020
 */
public interface PictureApiContract extends AbstractApiContract<PictureDto> {

}
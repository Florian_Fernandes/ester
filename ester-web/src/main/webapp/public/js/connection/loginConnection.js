function showDiv(name) {
    document.getElementById('divSalarie').style.display = "none";
    document.getElementById('divUtilisateur').style.display = "none";
    document.getElementById(name).style.display = "block";
}

function redirect(lien) {
    window.setTimeout(function () {
        document.location.href = lien;
    }, 1000);
}
var pcs_json = [{"Code": "100x", "Libelle": "Agriculteurs et éleveurs, salariés de leur exploitation"}, {
    "Code": "210x",
    "Libelle": "Artisans salariés de leur entreprise"
}, {"Code": "220x", "Libelle": "Commerçants et assimilés, salariés de leur entreprise"}, {
    "Code": "231a",
    "Libelle": "Chefs de grande entreprise de 500 salariés et plus"
}, {"Code": "232a", "Libelle": "Chefs de moyenne entreprise, de 50 à 499 salariés"}, {
    "Code": "233a",
    "Libelle": "Chefs d'entreprise du bâtiment et des travaux publics, de 10 à 49 salariés"
}, {
    "Code": "233b",
    "Libelle": "Chefs d'entreprise de l'industrie ou des transports, de 10 à 49 salariés"
}, {"Code": "233c", "Libelle": "Chefs d'entreprise commerciale, de 10 à 49 salariés"}, {
    "Code": "233d",
    "Libelle": "Chefs d'entreprise de services, de 10 à 49 salariés"
}, {"Code": "311c", "Libelle": "Chirurgiens dentistes"}, {
    "Code": "311d",
    "Libelle": "Psychologues, psychanalystes, psychothérapeutes (non médecins)"
}, {"Code": "311e", "Libelle": "Vétérinaires"}, {"Code": "312a", "Libelle": "Avocats"}, {
    "Code": "331a",
    "Libelle": "Personnels de direction de la fonction publique (Etat, collectivités locales, hôpitaux)"
}, {"Code": "332a", "Libelle": "Ingénieurs de l'Etat et assimilés"}, {
    "Code": "332b",
    "Libelle": "Ingénieurs des collectivités locales et des hôpitaux"
}, {"Code": "333c", "Libelle": "Cadres de la Poste"}, {
    "Code": "333d",
    "Libelle": "Cadres administratifs de France Télécom (statut public)"
}, {
    "Code": "333e",
    "Libelle": "Autres personnels administratifs de catégorie A de l'Etat et assimilés (hors Enseignement, Patrimoine)"
}, {
    "Code": "333f",
    "Libelle": "Personnel administratif de catégorie A des collectivités locales et hôpitaux publics (hors Enseignement, Patrimoine)"
}, {"Code": "341a", "Libelle": "Professeurs agrégés et certifiés de l'enseignement secondaire"}, {
    "Code": "341b",
    "Libelle": "Chefs d'établissement de l'enseignement secondaire et inspecteurs"
}, {"Code": "342b", "Libelle": "Professeurs et maîtres de conférences"}, {
    "Code": "342c",
    "Libelle": "Professeurs agrégés et certifiés en fonction dans l'enseignement supérieur"
}, {"Code": "342d", "Libelle": "Personnel enseignant temporaire de l'enseignement supérieur"}, {
    "Code": "342f",
    "Libelle": "Directeurs et chargés de recherche de la recherche publique"
}, {"Code": "342g", "Libelle": "Ingénieurs d'étude et de recherche de la recherche publique"}, {
    "Code": "342h",
    "Libelle": "Allocataires de la recherche publique"
}, {
    "Code": "343a",
    "Libelle": "Psychologues spécialistes de l'orientation scolaire et professionnelle"
}, {"Code": "344a", "Libelle": "Médecins hospitaliers sans activité libérale"}, {
    "Code": "344b",
    "Libelle": "Médecins salariés non hospitaliers"
}, {"Code": "344c", "Libelle": "Internes en médecine, odontologie et pharmacie"}, {
    "Code": "344d",
    "Libelle": "Pharmaciens salariés"
}, {
    "Code": "351a",
    "Libelle": "Bibliothécaires, archivistes, conservateurs et autres cadres du patrimoine"
}, {"Code": "352a", "Libelle": "Journalistes (y c. rédacteurs en chef)"}, {
    "Code": "352b",
    "Libelle": "Auteurs littéraires, scénaristes, dialoguistes"
}, {
    "Code": "353a",
    "Libelle": "Directeurs de journaux, administrateurs de presse, directeurs d'éditions (littéraire, musicale, audiovisuelle et multimédia)"
}, {
    "Code": "353b",
    "Libelle": "Directeurs, responsables de programmation et de production de l'audiovisuel et des spectacles"
}, {
    "Code": "353c",
    "Libelle": "Cadres artistiques et technico-artistiques de la réalisation de l'audiovisuel et des spectacles"
}, {"Code": "354a", "Libelle": "Artistes plasticiens"}, {
    "Code": "354b",
    "Libelle": "Artistes de la musique et du chant"
}, {"Code": "354c", "Libelle": "Artistes dramatiques"}, {
    "Code": "354e",
    "Libelle": "Artistes de la danse"
}, {"Code": "354f", "Libelle": "Artistes du cirque et des spectacles divers"}, {
    "Code": "354g",
    "Libelle": "Professeurs d'art (hors établissements scolaires)"
}, {
    "Code": "371a",
    "Libelle": "Cadres d'état-major administratifs, financiers, commerciaux des grandes entreprises"
}, {"Code": "372a", "Libelle": "Cadres chargés d'études économiques, financières, commerciales"}, {
    "Code": "372b",
    "Libelle": "Cadres de l'organisation ou du contrôle des services administratifs et financiers"
}, {"Code": "372c", "Libelle": "Cadres spécialistes des ressources humaines et du recrutement"}, {
    "Code": "372d",
    "Libelle": "Cadres spécialistes de la formation"
}, {"Code": "372e", "Libelle": "Juristes"}, {
    "Code": "372f",
    "Libelle": "Cadres de la documentation, de l'archivage (hors fonction publique)"
}, {"Code": "373a", "Libelle": "Cadres des services financiers ou comptables des grandes entreprises"}, {
    "Code": "373b",
    "Libelle": "Cadres des autres services administratifs des grandes entreprises"
}, {
    "Code": "373c",
    "Libelle": "Cadres des services financiers ou comptables des petites et moyennes entreprises"
}, {
    "Code": "373d",
    "Libelle": "Cadres des autres services administratifs des petites et moyennes entreprises"
}, {"Code": "374a", "Libelle": "Cadres de l'exploitation des magasins de vente du commerce de détail"}, {
    "Code": "374b",
    "Libelle": "Chefs de produits, acheteurs du commerce et autres cadres de la mercatique"
}, {"Code": "374c", "Libelle": "Cadres commerciaux des grandes entreprises (hors commerce de détail)"}, {
    "Code": "374d",
    "Libelle": "Cadres commerciaux des petites et moyennes entreprises (hors commerce de détail)"
}, {"Code": "375a", "Libelle": "Cadres de la publicité"}, {
    "Code": "375b",
    "Libelle": "Cadres des relations publiques et de la communication"
}, {"Code": "376a", "Libelle": "Cadres des marchés financiers"}, {
    "Code": "376b",
    "Libelle": "Cadres des opérations bancaires"
}, {"Code": "376c", "Libelle": "Cadres commerciaux de la banque"}, {
    "Code": "376d",
    "Libelle": "Chefs d'établissements et responsables de l'exploitation bancaire"
}, {"Code": "376e", "Libelle": "Cadres des services techniques des assurances"}, {
    "Code": "376f",
    "Libelle": "Cadres des services techniques des organismes de sécurité sociale et assimilés"
}, {"Code": "376g", "Libelle": "Cadres de l'immobilier"}, {
    "Code": "377a",
    "Libelle": "Cadres de l'hôtellerie et de la restauration"
}, {"Code": "380a", "Libelle": "Directeurs techniques des grandes entreprises"}, {
    "Code": "381b",
    "Libelle": "Ingénieurs et cadres d'étude et développement de l'agriculture, la pêche, les eaux et forêts"
}, {
    "Code": "381c",
    "Libelle": "Ingénieurs et cadres de production et d'exploitation de l'agriculture, la pêche, les eaux et forêts"
}, {"Code": "382a", "Libelle": "Ingénieurs et cadres d'étude du bâtiment et des travaux publics"}, {
    "Code": "382b",
    "Libelle": "Architectes salariés"
}, {
    "Code": "382c",
    "Libelle": "Ingénieurs, cadres de chantier et conducteurs de travaux (cadres) du bâtiment et des travaux publics"
}, {
    "Code": "382d",
    "Libelle": "Ingénieurs et cadres technico-commerciaux en bâtiment, travaux publics"
}, {
    "Code": "383a",
    "Libelle": "Ingénieurs et cadres d'étude, recherche et développement en électricité, électronique"
}, {
    "Code": "383b",
    "Libelle": "Ingénieurs et cadres de fabrication en matériel électrique, électronique"
}, {
    "Code": "383c",
    "Libelle": "Ingénieurs et cadres technico-commerciaux en matériel électrique ou électronique professionnel"
}, {
    "Code": "384a",
    "Libelle": "Ingénieurs et cadres d'étude, recherche et développement en mécanique et travail des métaux"
}, {
    "Code": "384b",
    "Libelle": "Ingénieurs et cadres de fabrication en mécanique et travail des métaux"
}, {
    "Code": "384c",
    "Libelle": "Ingénieurs et cadres technico-commerciaux en matériel mécanique professionnel"
}, {
    "Code": "385a",
    "Libelle": "Ingénieurs et cadres d'étude, recherche et développement des industries de transformation (agroalimentaire, chimie, métallurgie, matériaux lourds)"
}, {
    "Code": "385b",
    "Libelle": "Ingénieurs et cadres de fabrication des industries de transformation (agroalimentaire, chimie, métallurgie, matériaux lourds)"
}, {
    "Code": "385c",
    "Libelle": "Ingénieurs et cadres technico-commerciaux des industries de transformations (biens intermédiaires)"
}, {
    "Code": "386b",
    "Libelle": "Ingénieurs et cadres d'étude, recherche et développement de la distribution d'énergie, eau"
}, {
    "Code": "386c",
    "Libelle": "Ingénieurs et cadres d'étude, recherche et développement des autres industries (imprimerie, matériaux souples, ameublement et bois)"
}, {
    "Code": "386d",
    "Libelle": "Ingénieurs et cadres de la production et de la distribution d'énergie, eau"
}, {
    "Code": "386e",
    "Libelle": "Ingénieurs et cadres de fabrication des autres industries (imprimerie, matériaux souples, ameublement et bois)"
}, {"Code": "387a", "Libelle": "Ingénieurs et cadres des achats et approvisionnements industriels"}, {
    "Code": "387b",
    "Libelle": "Ingénieurs et cadres de la logistique, du planning et de l'ordonnancement"
}, {"Code": "387c", "Libelle": "Ingénieurs et cadres des méthodes de production"}, {
    "Code": "387d",
    "Libelle": "Ingénieurs et cadres du contrôle-qualité"
}, {
    "Code": "387e",
    "Libelle": "Ingénieurs et cadres de la maintenance, de l'entretien et des travaux neufs"
}, {"Code": "387f", "Libelle": "Ingénieurs et cadres techniques de l'environnement"}, {
    "Code": "388a",
    "Libelle": "Ingénieurs et cadres d'étude, recherche et développement en informatique"
}, {
    "Code": "388b",
    "Libelle": "Ingénieurs et cadres d'administration, maintenance, support et services aux utilisateurs en informatique"
}, {"Code": "388c", "Libelle": "Chefs de projets informatiques, responsables informatiques"}, {
    "Code": "388d",
    "Libelle": "Ingénieurs et cadres technico-commerciaux en informatique et télécommunications"
}, {"Code": "388e", "Libelle": "Ingénieurs et cadres spécialistes des télécommunications"}, {
    "Code": "389a",
    "Libelle": "Ingénieurs et cadres techniques de l'exploitation des transports"
}, {
    "Code": "389b",
    "Libelle": "Officiers et cadres navigants techniques et commerciaux de l'aviation civile"
}, {"Code": "389c", "Libelle": "Officiers et cadres navigants techniques de la marine marchande"}, {
    "Code": "421a",
    "Libelle": "Instituteurs"
}, {"Code": "421b", "Libelle": "Professeurs des écoles"}, {
    "Code": "422a",
    "Libelle": "Professeurs d'enseignement général des collèges"
}, {"Code": "422b", "Libelle": "Professeurs de lycée professionnel"}, {
    "Code": "422c",
    "Libelle": "Maîtres auxiliaires et professeurs contractuels de l'enseignement secondaire"
}, {"Code": "422d", "Libelle": "Conseillers principaux d'éducation"}, {
    "Code": "422e",
    "Libelle": "Surveillants et aides-éducateurs des établissements d'enseignement"
}, {"Code": "423a", "Libelle": "Moniteurs d'école de conduite"}, {
    "Code": "423b",
    "Libelle": "Formateurs et animateurs de formation continue"
}, {"Code": "424a", "Libelle": "Moniteurs et éducateurs sportifs, sportifs professionnels"}, {
    "Code": "425a",
    "Libelle": "Sous-bibliothécaires, cadres intermédiaires du patrimoine"
}, {"Code": "431a", "Libelle": "Cadres infirmiers et assimilés"}, {
    "Code": "431b",
    "Libelle": "Infirmiers psychiatriques"
}, {"Code": "431c", "Libelle": "Puéricultrices"}, {
    "Code": "431d",
    "Libelle": "Infirmiers spécialisés (autres qu'infirmiers psychiatriques et puéricultrices)"
}, {"Code": "431e", "Libelle": "Sages-femmes"}, {
    "Code": "431f",
    "Libelle": "Infirmiers en soins généraux"
}, {"Code": "432b", "Libelle": "Masseurs-kinésithérapeutes rééducateurs"}, {
    "Code": "432d",
    "Libelle": "Autres spécialistes de la rééducation"
}, {"Code": "433a", "Libelle": "Techniciens médicaux"}, {
    "Code": "433b",
    "Libelle": "Opticiens lunetiers et audioprothésistes"
}, {"Code": "433c", "Libelle": "Autres spécialistes de l'appareillage médical"}, {
    "Code": "433d",
    "Libelle": "Préparateurs en pharmacie"
}, {"Code": "434a", "Libelle": "Cadres de l'intervention socio-éducative"}, {
    "Code": "434b",
    "Libelle": "Assistants de service social"
}, {"Code": "434c", "Libelle": "Conseillers en économie sociale familiale"}, {
    "Code": "434d",
    "Libelle": "Educateurs spécialisés"
}, {"Code": "434e", "Libelle": "Moniteurs éducateurs"}, {
    "Code": "434f",
    "Libelle": "Educateurs techniques spécialisés, moniteurs d'atelier"
}, {"Code": "434g", "Libelle": "Educateurs de jeunes enfants"}, {
    "Code": "435a",
    "Libelle": "Directeurs de centres socioculturels et de loisirs"
}, {"Code": "435b", "Libelle": "Animateurs socioculturels et de loisirs"}, {
    "Code": "441a",
    "Libelle": "Clergé séculier"
}, {"Code": "441b", "Libelle": "Clergé régulier"}, {
    "Code": "451a",
    "Libelle": "Professions intermédiaires de la Poste"
}, {
    "Code": "451b",
    "Libelle": "Professions intermédiaires administratives de France Télécom (statut public)"
}, {
    "Code": "451e",
    "Libelle": "Autres personnels administratifs de catégorie B de l'Etat et assimilés (hors Enseignement, Patrimoine)"
}, {"Code": "451g", "Libelle": "Professions intermédiaires administratives des collectivités locales"}, {
    "Code": "451h",
    "Libelle": "Professions intermédiaires administratives des hôpitaux"
}, {"Code": "461b", "Libelle": "Secrétaires de direction, assistants de direction (non cadres)"}, {
    "Code": "461c",
    "Libelle": "Secrétaires de niveau supérieur (non cadres, hors secrétaires de direction)"
}, {"Code": "461d", "Libelle": "Maîtrise et techniciens des services financiers ou comptables"}, {
    "Code": "461e",
    "Libelle": "Maîtrise et techniciens administratifs des services juridiques ou du personnel"
}, {
    "Code": "461f",
    "Libelle": "Maîtrise et techniciens administratifs des autres services administratifs"
}, {"Code": "462a", "Libelle": "Chefs de petites surfaces de vente"}, {
    "Code": "462b",
    "Libelle": "Maîtrise de l'exploitation des magasins de vente"
}, {"Code": "462c", "Libelle": "Acheteurs non classés cadres, aides-acheteurs"}, {
    "Code": "462d",
    "Libelle": "Animateurs commerciaux des magasins de vente, marchandiseurs (non cadres)"
}, {
    "Code": "462e",
    "Libelle": "Autres professions intermédiaires commerciales (sauf techniciens des forces de vente)"
}, {
    "Code": "463a",
    "Libelle": "Techniciens commerciaux et technico-commerciaux, représentants en informatique"
}, {
    "Code": "463b",
    "Libelle": "Techniciens commerciaux et technico-commerciaux, représentants en biens d'équipement, en biens intermédiaires, commerce interindustriel (hors informatique)"
}, {
    "Code": "463c",
    "Libelle": "Techniciens commerciaux et technico-commerciaux, représentants en biens de consommation auprès d'entreprises"
}, {
    "Code": "463d",
    "Libelle": "Techniciens commerciaux et technico-commerciaux, représentants en services auprès d'entreprises ou de professionnels (hors banque, assurance, informatique)"
}, {
    "Code": "463e",
    "Libelle": "Techniciens commerciaux et technico-commerciaux, représentants auprès de particuliers (hors banque, assurance, informatique)"
}, {"Code": "464a", "Libelle": "Assistants de la publicité, des relations publiques"}, {
    "Code": "464b",
    "Libelle": "Interprètes, traducteurs"
}, {
    "Code": "465a",
    "Libelle": "Concepteurs et assistants techniques des arts graphiques, de la mode et de la décoration"
}, {
    "Code": "465b",
    "Libelle": "Assistants techniques de la réalisation des spectacles vivants et audiovisuels"
}, {"Code": "465c", "Libelle": "Photographes"}, {
    "Code": "466a",
    "Libelle": "Responsables commerciaux et administratifs des transports de voyageurs et du tourisme (non cadres)"
}, {
    "Code": "466b",
    "Libelle": "Responsables commerciaux et administratifs des transports de marchandises (non cadres)"
}, {
    "Code": "466c",
    "Libelle": "Responsables d'exploitation des transports de voyageurs et de marchandises (non cadres)"
}, {"Code": "467a", "Libelle": "Chargés de clientèle bancaire"}, {
    "Code": "467b",
    "Libelle": "Techniciens des opérations bancaires"
}, {"Code": "467c", "Libelle": "Professions intermédiaires techniques et commerciales des assurances"}, {
    "Code": "467d",
    "Libelle": "Professions intermédiaires techniques des organismes de sécurité sociale"
}, {"Code": "468a", "Libelle": "Maîtrise de restauration : salle et service"}, {
    "Code": "468b",
    "Libelle": "Maîtrise de l'hébergement : hall et étages"
}, {"Code": "471a", "Libelle": "Techniciens d'étude et de conseil en agriculture, eaux et forêts"}, {
    "Code": "471b",
    "Libelle": "Techniciens d'exploitation et de contrôle de la production en agriculture, eaux et forêts"
}, {"Code": "472a", "Libelle": "Dessinateurs en bâtiment, travaux publics"}, {
    "Code": "472b",
    "Libelle": "Géomètres, topographes"
}, {"Code": "472c", "Libelle": "Métreurs et techniciens divers du bâtiment et des travaux publics"}, {
    "Code": "472d",
    "Libelle": "Techniciens des travaux publics de l'Etat et des collectivités locales"
}, {"Code": "473a", "Libelle": "Dessinateurs en électricité, électromécanique et électronique"}, {
    "Code": "473b",
    "Libelle": "Techniciens de recherche-développement et des méthodes de fabrication en électricité, électromécanique et électronique"
}, {
    "Code": "473c",
    "Libelle": "Techniciens de fabrication et de contrôle-qualité en électricité, électromécanique et électronique"
}, {"Code": "474a", "Libelle": "Dessinateurs en construction mécanique et travail des métaux"}, {
    "Code": "474b",
    "Libelle": "Techniciens de recherche-développement et des méthodes de fabrication en construction mécanique et travail des métaux"
}, {
    "Code": "474c",
    "Libelle": "Techniciens de fabrication et de contrôle-qualité en construction mécanique et travail des métaux"
}, {
    "Code": "475a",
    "Libelle": "Techniciens de recherche-développement et des méthodes de production des industries de transformation"
}, {
    "Code": "475b",
    "Libelle": "Techniciens de production et de contrôle-qualité des industries de transformation"
}, {"Code": "476a", "Libelle": "Assistants techniques, techniciens de l'imprimerie et de l'édition"}, {
    "Code": "476b",
    "Libelle": "Techniciens de l'industrie des matériaux souples, de l'ameublement et du bois"
}, {"Code": "477a", "Libelle": "Techniciens de la logistique, du planning et de l'ordonnancement"}, {
    "Code": "477b",
    "Libelle": "Techniciens d'installation et de maintenance des équipements industriels (électriques, électromécaniques, mécaniques, hors informatique)"
}, {
    "Code": "477c",
    "Libelle": "Techniciens d'installation et de maintenance des équipements non industriels (hors informatique et télécommunications)"
}, {"Code": "477d", "Libelle": "Techniciens de l'environnement et du traitement des pollutions"}, {
    "Code": "478a",
    "Libelle": "Techniciens d'étude et de développement en informatique"
}, {"Code": "478b", "Libelle": "Techniciens de production, d'exploitation en informatique"}, {
    "Code": "478c",
    "Libelle": "Techniciens d'installation, de maintenance, support et services aux utilisateurs en informatique"
}, {"Code": "478d", "Libelle": "Techniciens des télécommunications et de l'informatique des réseaux"}, {
    "Code": "479a",
    "Libelle": "Techniciens des laboratoires de recherche publique ou de l'enseignement"
}, {"Code": "479b", "Libelle": "Experts de niveau technicien, techniciens divers"}, {
    "Code": "480a",
    "Libelle": "Contremaîtres et agents d'encadrement (non cadres) en agriculture, sylviculture"
}, {"Code": "480b", "Libelle": "Maîtres d'équipage de la marine marchande et de la pêche"}, {
    "Code": "481a",
    "Libelle": "Conducteurs de travaux (non cadres)"
}, {"Code": "481b", "Libelle": "Chefs de chantier (non cadres)"}, {
    "Code": "482a",
    "Libelle": "Agents de maîtrise en fabrication de matériel électrique, électronique"
}, {"Code": "483a", "Libelle": "Agents de maîtrise en construction mécanique, travail des métaux"}, {
    "Code": "484a",
    "Libelle": "Agents de maîtrise en fabrication : agroalimentaire, chimie, plasturgie, pharmacie."
}, {
    "Code": "484b",
    "Libelle": "Agents de maîtrise en fabrication : métallurgie, matériaux lourds et autres industries de transformation"
}, {
    "Code": "485a",
    "Libelle": "Agents de maîtrise et techniciens en production et distribution d'énergie, eau, chauffage"
}, {
    "Code": "485b",
    "Libelle": "Agents de maîtrise en fabrication des autres industries (imprimerie, matériaux souples, ameublement et bois)"
}, {
    "Code": "486b",
    "Libelle": "Agents de maîtrise en maintenance, installation en électricité et électronique"
}, {"Code": "486c", "Libelle": "Agents de maîtrise en maintenance, installation en électromécanique"}, {
    "Code": "486d",
    "Libelle": "Agents de maîtrise en maintenance, installation en mécanique"
}, {
    "Code": "486e",
    "Libelle": "Agents de maîtrise en entretien général, installation, travaux neufs (hors mécanique, électromécanique, électronique)"
}, {"Code": "487a", "Libelle": "Responsables d'entrepôt, de magasinage"}, {
    "Code": "487b",
    "Libelle": "Responsables du tri, de l'emballage, de l'expédition et autres responsables de la manutention"
}, {"Code": "488a", "Libelle": "Maîtrise de restauration  : cuisine/production"}, {
    "Code": "488b",
    "Libelle": "Maîtrise de restauration  : gestion d'établissement"
}, {"Code": "521a", "Libelle": "Employés de la Poste"}, {
    "Code": "521b",
    "Libelle": "Employés de France Télécom (statut public)"
}, {
    "Code": "523b",
    "Libelle": "Adjoints administratifs de l'Etat et assimilés (sauf Poste, France Télécom)"
}, {"Code": "523c", "Libelle": "Adjoints administratifs des collectivités locales"}, {
    "Code": "523d",
    "Libelle": "Adjoints administratifs des hôpitaux publics"
}, {
    "Code": "524b",
    "Libelle": "Agents administratifs de l'Etat et assimilés (sauf Poste, France Télécom)"
}, {"Code": "524c", "Libelle": "Agents administratifs des collectivités locales"}, {
    "Code": "524d",
    "Libelle": "Agents administratifs des hôpitaux publics"
}, {"Code": "525a", "Libelle": "Agents de service des établissements primaires"}, {
    "Code": "525b",
    "Libelle": "Agents de service des autres établissements d'enseignement"
}, {"Code": "525c", "Libelle": "Agents de service de la fonction publique (sauf écoles, hôpitaux)"}, {
    "Code": "525d",
    "Libelle": "Agents de service hospitaliers"
}, {"Code": "526a", "Libelle": "Aides-soignants"}, {
    "Code": "526b",
    "Libelle": "Assistants dentaires, médicaux et vétérinaires, aides de techniciens médicaux"
}, {"Code": "526c", "Libelle": "Auxiliaires de puériculture"}, {
    "Code": "526d",
    "Libelle": "Aides médico-psychologiques"
}, {"Code": "526e", "Libelle": "Ambulanciers"}, {"Code": "533a", "Libelle": "Pompiers"}, {
    "Code": "533b",
    "Libelle": "Agents techniques forestiers, gardes des espaces naturels"
}, {"Code": "533c", "Libelle": "Agents de surveillance du patrimoine et des administrations"}, {
    "Code": "534a",
    "Libelle": "Agents civils de sécurité et de surveillance"
}, {
    "Code": "534b",
    "Libelle": "Convoyeurs de fonds, gardes du corps, enquêteurs privés et métiers assimilés"
}, {"Code": "541b", "Libelle": "Agents d'accueil qualifiés, hôtesses d'accueil et d'information"}, {
    "Code": "541c",
    "Libelle": "Agents d'accueil non qualifiés"
}, {"Code": "541d", "Libelle": "Standardistes, téléphonistes"}, {
    "Code": "542a",
    "Libelle": "Secrétaires"
}, {
    "Code": "542b",
    "Libelle": "Dactylos, sténodactylos (sans secrétariat), opérateurs de traitement de texte"
}, {"Code": "543b", "Libelle": "Employés qualifiés des services comptables ou financiers"}, {
    "Code": "543c",
    "Libelle": "Employés non qualifiés des services comptables ou financiers"
}, {
    "Code": "543e",
    "Libelle": "Employés qualifiés des services du personnel et des services juridiques"
}, {
    "Code": "543f",
    "Libelle": "Employés qualifiés des services commerciaux des entreprises (hors vente)"
}, {
    "Code": "543g",
    "Libelle": "Employés administratifs qualifiés des autres services des entreprises"
}, {"Code": "543h", "Libelle": "Employés administratifs non qualifiés"}, {
    "Code": "544a",
    "Libelle": "Employés et opérateurs d'exploitation en informatique"
}, {"Code": "545a", "Libelle": "Employés administratifs des services techniques de la banque"}, {
    "Code": "545b",
    "Libelle": "Employés des services commerciaux de la banque"
}, {"Code": "545c", "Libelle": "Employés des services techniques des assurances"}, {
    "Code": "545d",
    "Libelle": "Employés des services techniques des organismes de sécurité sociale et assimilés"
}, {"Code": "546a", "Libelle": "Contrôleurs des transports (personnels roulants)"}, {
    "Code": "546b",
    "Libelle": "Agents des services commerciaux des transports de voyageurs et du tourisme"
}, {
    "Code": "546c",
    "Libelle": "Employés administratifs d'exploitation des transports de marchandises"
}, {"Code": "546d", "Libelle": "Hôtesses de l'air et stewards"}, {
    "Code": "546e",
    "Libelle": "Autres agents et hôtesses d'accompagnement (transports, tourisme)"
}, {"Code": "551a", "Libelle": "Employés de libre service du commerce et magasiniers"}, {
    "Code": "552a",
    "Libelle": "Caissiers de magasin"
}, {"Code": "553b", "Libelle": "Vendeurs polyvalents des grands magasins"}, {
    "Code": "553c",
    "Libelle": " Autres vendeurs non spécialisés"
}, {"Code": "554a", "Libelle": "Vendeurs en alimentation"}, {
    "Code": "554b",
    "Libelle": "Vendeurs en ameublement, décor, équipement du foyer"
}, {"Code": "554c", "Libelle": "Vendeurs en droguerie, bazar, quincaillerie, bricolage"}, {
    "Code": "554d",
    "Libelle": "Vendeurs du commerce de fleurs"
}, {"Code": "554e", "Libelle": "Vendeurs en habillement et articles de sport"}, {
    "Code": "554f",
    "Libelle": "Vendeurs en produits de beauté, de luxe (hors biens culturels) et optique"
}, {
    "Code": "554g",
    "Libelle": "Vendeurs de biens culturels (livres, disques, multimédia, objets d'art)"
}, {"Code": "554h", "Libelle": "Vendeurs de tabac, presse et articles divers"}, {
    "Code": "554j",
    "Libelle": "Pompistes et gérants de station-service (salariés ou mandataires)"
}, {"Code": "555a", "Libelle": "Vendeurs par correspondance, télévendeurs"}, {
    "Code": "556a",
    "Libelle": "Vendeurs en gros de biens d'équipement, biens intermédiaires"
}, {"Code": "561b", "Libelle": "Serveurs, commis de restaurant, garçons qualifiés"}, {
    "Code": "561c",
    "Libelle": "Serveurs, commis de restaurant, garçons non qualifiés"
}, {
    "Code": "561d",
    "Libelle": "Aides de cuisine, apprentis de cuisine et employés polyvalents de la restauration"
}, {"Code": "561e", "Libelle": "Employés de l'hôtellerie : réception et hall"}, {
    "Code": "561f",
    "Libelle": "Employés d'étage et employés polyvalents de l'hôtellerie"
}, {"Code": "562a", "Libelle": "Manucures, esthéticiens"}, {"Code": "562b", "Libelle": "Coiffeurs"}, {
    "Code": "563a",
    "Libelle": "Assistantes maternelles, gardiennes d'enfants, familles d'accueil"
}, {"Code": "563b", "Libelle": "Aides à domicile, aides ménagères, travailleuses familiales"}, {
    "Code": "563c",
    "Libelle": "Employés de maison et personnels de ménage chez des particuliers"
}, {"Code": "564a", "Libelle": "Concierges, gardiens d'immeubles"}, {
    "Code": "564b",
    "Libelle": "Employés des services divers"
}, {"Code": "621a", "Libelle": "Chefs d'équipe du gros oeuvre et des travaux publics"}, {
    "Code": "621b",
    "Libelle": "Ouvriers qualifiés du travail du béton"
}, {
    "Code": "621c",
    "Libelle": "Conducteurs qualifiés d'engins de chantiers du bâtiment et des travaux publics"
}, {
    "Code": "621d",
    "Libelle": "Ouvriers des travaux publics en installations électriques et de télécommunications"
}, {"Code": "621e", "Libelle": "Autres ouvriers qualifiés des travaux publics"}, {
    "Code": "621f",
    "Libelle": "Ouvriers qualifiés des travaux publics (salariés de l'Etat et des collectivités locales)"
}, {
    "Code": "621g",
    "Libelle": "Mineurs de fond qualifiés et autres ouvriers qualifiés des industries d'extraction (carrières, pétrole, gaz...)"
}, {
    "Code": "622a",
    "Libelle": "Opérateurs qualifiés sur machines automatiques en production électrique ou électronique"
}, {"Code": "622c", "Libelle": "Monteurs câbleurs qualifiés en électricité"}, {
    "Code": "622d",
    "Libelle": "Câbleurs qualifiés en électronique (prototype, unité, petite série)"
}, {"Code": "622e", "Libelle": "Autres monteurs câbleurs en électronique"}, {
    "Code": "622f",
    "Libelle": "Bobiniers qualifiés"
}, {
    "Code": "622g",
    "Libelle": "Plate-formistes, contrôleurs qualifiés de matériel électrique ou électronique"
}, {
    "Code": "623a",
    "Libelle": "Chaudronniers-tôliers industriels, opérateurs qualifiés du travail en forge, conducteurs qualifiés d'équipement de formage, traceurs qualifiés"
}, {"Code": "623b", "Libelle": "Tuyauteurs industriels qualifiés"}, {
    "Code": "623d",
    "Libelle": "Opérateurs qualifiés sur machine de soudage"
}, {"Code": "623e", "Libelle": "Soudeurs manuels"}, {
    "Code": "623f",
    "Libelle": "Opérateurs qualifiés d'usinage des métaux travaillant à l'unité ou en petite série, moulistes qualifiés"
}, {
    "Code": "623g",
    "Libelle": "Opérateurs qualifiés d'usinage des métaux sur autres machines (sauf moulistes)"
}, {
    "Code": "624b",
    "Libelle": "Monteurs, metteurs au point très qualifiés d'ensembles mécaniques travaillant à l'unité ou en petite série"
}, {
    "Code": "624c",
    "Libelle": "Monteurs qualifiés d'ensembles mécaniques travaillant en moyenne ou en grande série"
}, {"Code": "624d", "Libelle": "Monteurs qualifiés en structures métalliques"}, {
    "Code": "624e",
    "Libelle": "Ouvriers qualifiés de contrôle et d'essais en mécanique"
}, {
    "Code": "624f",
    "Libelle": "Ouvriers qualifiés des traitements thermiques et de surface sur métaux"
}, {
    "Code": "624g",
    "Libelle": "Autres mécaniciens ou ajusteurs qualifiés (ou spécialité non reconnue)"
}, {
    "Code": "625a",
    "Libelle": "Pilotes d'installation lourde des industries de transformation : agroalimentaire, chimie, plasturgie, énergie"
}, {
    "Code": "625b",
    "Libelle": "Ouvriers qualifiés et agents qualifiés de laboratoire : agroalimentaire, chimie, biologie, pharmacie"
}, {
    "Code": "625c",
    "Libelle": "Autres opérateurs et ouvriers qualifiés de la chimie (y.c. pharmacie) et de la plasturgie"
}, {"Code": "625d", "Libelle": "Opérateurs de la transformation des viandes"}, {
    "Code": "625f",
    "Libelle": "Autres opérateurs travaillant sur installations ou machines : industrie agroalimentaire (hors transformation des viandes)"
}, {
    "Code": "625g",
    "Libelle": "Autres ouvriers de production qualifiés ne travaillant pas sur machine : industrie agroalimentaire (hors transformation des viandes)"
}, {
    "Code": "625h",
    "Libelle": "Ouvriers qualifiés des autres industries (eau, gaz, énergie, chauffage)"
}, {
    "Code": "626a",
    "Libelle": "Pilotes d'installation lourde des industries de transformation : métallurgie, production verrière, matériaux de construction"
}, {
    "Code": "626b",
    "Libelle": "Autres opérateurs et ouvriers qualifiés : métallurgie, production verrière, matériaux de construction"
}, {
    "Code": "626c",
    "Libelle": "Opérateurs et ouvriers qualifiés des industries lourdes du bois et de la fabrication du papier-carton"
}, {"Code": "627a", "Libelle": "Opérateurs qualifiés du textile et de la mégisserie"}, {
    "Code": "627b",
    "Libelle": "Ouvriers qualifiés de la coupe des vêtements et de l'habillement, autres opérateurs de confection qualifiés"
}, {"Code": "627c", "Libelle": "Ouvriers qualifiés du travail industriel du cuir"}, {
    "Code": "627d",
    "Libelle": "Ouvriers qualifiés de scierie, de la menuiserie industrielle et de l'ameublement"
}, {
    "Code": "627e",
    "Libelle": "Ouvriers de la photogravure et des laboratoires photographiques et cinématographiques"
}, {
    "Code": "627f",
    "Libelle": "Ouvriers de la composition et de l'impression, ouvriers qualifiés de la brochure, de la reliure et du façonnage du papier-carton"
}, {
    "Code": "628a",
    "Libelle": "Mécaniciens qualifiés de maintenance, entretien : équipements industriels"
}, {
    "Code": "628b",
    "Libelle": "Electromécaniciens, électriciens qualifiés d'entretien : équipements industriels"
}, {
    "Code": "628c",
    "Libelle": "Régleurs qualifiés d'équipement de fabrication (travail des métaux, mécanique)"
}, {
    "Code": "628d",
    "Libelle": "Régleurs qualifiés d'équipement de fabrication (hors travail des métaux et mécanique)"
}, {"Code": "628e", "Libelle": "Ouvriers qualifiés de l'assainissement et du traitement des déchets"}, {
    "Code": "628f",
    "Libelle": "Agents qualifiés de laboratoire (sauf chimie, santé)"
}, {"Code": "628g", "Libelle": "Ouvriers qualifiés divers de type industriel"}, {
    "Code": "631a",
    "Libelle": "Jardiniers"
}, {"Code": "632a", "Libelle": "Maçons qualifiés"}, {
    "Code": "632b",
    "Libelle": "Ouvriers qualifiés du travail de la pierre"
}, {"Code": "632c", "Libelle": "Charpentiers en bois qualifiés"}, {
    "Code": "632d",
    "Libelle": "Menuisiers qualifiés du bâtiment"
}, {"Code": "632e", "Libelle": "Couvreurs qualifiés"}, {
    "Code": "632f",
    "Libelle": "Plombiers et chauffagistes qualifiés"
}, {
    "Code": "632g",
    "Libelle": "Peintres et ouvriers qualifiés de pose de revêtements sur supports verticaux"
}, {
    "Code": "632h",
    "Libelle": "Soliers moquetteurs et ouvriers qualifiés de pose de revêtements souples sur supports horizontaux"
}, {"Code": "632j", "Libelle": "Monteurs qualifiés en agencement, isolation"}, {
    "Code": "632k",
    "Libelle": "Ouvriers qualifiés d'entretien général des bâtiments"
}, {"Code": "633a", "Libelle": "Electriciens qualifiés de type artisanal (y c. bâtiment)"}, {
    "Code": "633b",
    "Libelle": "Dépanneurs qualifiés en radiotélévision, électroménager, matériel électronique"
}, {
    "Code": "633c",
    "Libelle": "Electriciens, électroniciens qualifiés en maintenance entretien, réparation : automobile"
}, {
    "Code": "633d",
    "Libelle": "Electriciens, électroniciens qualifiés en maintenance, entretien : équipements non industriels"
}, {"Code": "634a", "Libelle": "Carrossiers d'automobiles qualifiés"}, {
    "Code": "634b",
    "Libelle": "Métalliers, serruriers qualifiés"
}, {
    "Code": "634c",
    "Libelle": "Mécaniciens qualifiés en maintenance, entretien, réparation : automobile"
}, {
    "Code": "634d",
    "Libelle": "Mécaniciens qualifiés de maintenance, entretien : équipements non industriels"
}, {
    "Code": "635a",
    "Libelle": "Tailleurs et couturières qualifiés, ouvriers qualifiés du travail des étoffes (sauf fabrication de vêtements), ouvriers qualifiés de type artisanal du travail du cuir"
}, {"Code": "636a", "Libelle": "Bouchers (sauf industrie de la viande)"}, {
    "Code": "636b",
    "Libelle": "Charcutiers (sauf industrie de la viande)"
}, {"Code": "636c", "Libelle": "Boulangers, pâtissiers (sauf activité industrielle)"}, {
    "Code": "636d",
    "Libelle": "Cuisiniers et commis de cuisine"
}, {
    "Code": "637a",
    "Libelle": "Modeleurs (sauf modeleurs de métal), mouleurs-noyauteurs à la main, ouvriers qualifiés du travail du verre ou de la céramique à la main"
}, {"Code": "637b", "Libelle": "Ouvriers d'art"}, {
    "Code": "637c",
    "Libelle": "Ouvriers et techniciens des spectacles vivants et audiovisuels"
}, {"Code": "637d", "Libelle": "Ouvriers qualifiés divers de type artisanal"}, {
    "Code": "641a",
    "Libelle": "Conducteurs routiers et grands routiers"
}, {"Code": "641b", "Libelle": "Conducteurs de véhicule routier de transport en commun"}, {
    "Code": "642a",
    "Libelle": "Conducteurs de taxi"
}, {"Code": "642b", "Libelle": "Conducteurs de voiture particulière"}, {
    "Code": "643a",
    "Libelle": "Conducteurs livreurs, coursiers"
}, {"Code": "644a", "Libelle": "Conducteurs de véhicule de ramassage des ordures ménagères"}, {
    "Code": "651a",
    "Libelle": "Conducteurs d'engin lourd de levage"
}, {"Code": "651b", "Libelle": "Conducteurs d'engin lourd de manoeuvre"}, {
    "Code": "652a",
    "Libelle": "Ouvriers qualifiés de la manutention, conducteurs de chariots élévateurs, caristes"
}, {"Code": "652b", "Libelle": "Dockers"}, {"Code": "653a", "Libelle": "Magasiniers qualifiés"}, {
    "Code": "654b",
    "Libelle": "Conducteurs qualifiés d'engins de transport guidés (sauf remontées mécaniques)"
}, {"Code": "654c", "Libelle": "Conducteurs qualifiés de systèmes de remontées mécaniques"}, {
    "Code": "655a",
    "Libelle": "Autres agents et ouvriers qualifiés (sédentaires) des services d'exploitation des transports"
}, {"Code": "656b", "Libelle": "Matelots de la marine marchande"}, {
    "Code": "656c",
    "Libelle": "Capitaines et matelots timoniers de la navigation fluviale"
}, {
    "Code": "671a",
    "Libelle": "Ouvriers non qualifiés des travaux publics de l'Etat et des collectivités locales"
}, {"Code": "671c", "Libelle": "Ouvriers non qualifiés des travaux publics et du travail du béton"}, {
    "Code": "671d",
    "Libelle": "Aides-mineurs, ouvriers non qualifiés de l'extraction"
}, {"Code": "672a", "Libelle": "Ouvriers non qualifiés de l'électricité et de l'électronique"}, {
    "Code": "673a",
    "Libelle": "Ouvriers de production non qualifiés travaillant par enlèvement de métal"
}, {
    "Code": "673b",
    "Libelle": "Ouvriers de production non qualifiés travaillant par formage de métal"
}, {
    "Code": "673c",
    "Libelle": "Ouvriers non qualifiés de montage, contrôle en mécanique et travail des métaux"
}, {"Code": "674a", "Libelle": "Ouvriers de production non qualifiés : chimie, pharmacie, plasturgie"}, {
    "Code": "674b",
    "Libelle": "Ouvriers de production non qualifiés de la transformation des viandes"
}, {
    "Code": "674c",
    "Libelle": "Autres ouvriers de production non qualifiés : industrie agroalimentaire"
}, {
    "Code": "674d",
    "Libelle": "Ouvriers de production non qualifiés : métallurgie, production verrière, céramique, matériaux de construction"
}, {
    "Code": "674e",
    "Libelle": "Ouvriers de production non qualifiés : industrie lourde du bois, fabrication des papiers et cartons"
}, {
    "Code": "675a",
    "Libelle": "Ouvriers de production non qualifiés du textile et de la confection, de la tannerie-mégisserie et du travail du cuir"
}, {
    "Code": "675b",
    "Libelle": "Ouvriers de production non qualifiés du travail du bois et de l'ameublement"
}, {
    "Code": "675c",
    "Libelle": "Ouvriers de production non qualifiés de l'imprimerie, presse, édition"
}, {"Code": "676a", "Libelle": "Manutentionnaires non qualifiés"}, {
    "Code": "676b",
    "Libelle": "Déménageurs (hors chauffeurs-déménageurs), non qualifiés"
}, {"Code": "676c", "Libelle": "Ouvriers du tri, de l'emballage, de l'expédition, non qualifiés"}, {
    "Code": "676d",
    "Libelle": "Agents non qualifiés des services d'exploitation des transports"
}, {"Code": "676e", "Libelle": "Ouvriers non qualifiés divers de type industriel"}, {
    "Code": "681a",
    "Libelle": "Ouvriers non qualifiés du gros oeuvre du bâtiment"
}, {"Code": "681b", "Libelle": "Ouvriers non qualifiés du second oeuvre du bâtiment"}, {
    "Code": "682a",
    "Libelle": "Métalliers, serruriers, réparateurs en mécanique non qualifiés"
}, {"Code": "683a", "Libelle": "Apprentis boulangers, bouchers, charcutiers"}, {
    "Code": "684a",
    "Libelle": "Nettoyeurs"
}, {
    "Code": "684b",
    "Libelle": "Ouvriers non qualifiés de l'assainissement et du traitement des déchets"
}, {"Code": "685a", "Libelle": "Ouvriers non qualifiés divers de type artisanal"}, {
    "Code": "691a",
    "Libelle": "Conducteurs d'engin agricole ou forestier"
}, {"Code": "691b", "Libelle": "Ouvriers de l'élevage"}, {
    "Code": "691c",
    "Libelle": "Ouvriers du maraîchage ou de l'horticulture"
}, {"Code": "691d", "Libelle": "Ouvriers de la viticulture ou de l'arboriculture fruitière"}, {
    "Code": "691e",
    "Libelle": "Ouvriers agricoles sans spécialisation particulière"
}, {"Code": "691f", "Libelle": "Ouvriers de l'exploitation forestière ou de la sylviculture"}, {
    "Code": "692a",
    "Libelle": "Marins-pêcheurs et ouvriers de l'aquaculture"
}]

var naf_json = [{"Code": "01", "Libelle": "Culture et production animale, chasse et services annexes"}, {
    "Code": "02",
    "Libelle": "Sylviculture et exploitation forestière"
}, {"Code": "03", "Libelle": "Pêche et aquaculture"}, {
    "Code": "05",
    "Libelle": "Extraction de houille et de lignite"
}, {"Code": "06", "Libelle": "Extraction d'hydrocarbures"}, {
    "Code": "07",
    "Libelle": "Extraction de minerais métalliques"
}, {"Code": "08", "Libelle": "Autres industries extractives"}, {
    "Code": "09",
    "Libelle": "Services de soutien aux industries extractives"
}, {"Code": 10, "Libelle": "Industries alimentaires"}, {"Code": 11, "Libelle": "Fabrication de boissons"}, {
    "Code": 12,
    "Libelle": "Fabrication de produits à base de tabac"
}, {"Code": 13, "Libelle": "Fabrication de textiles"}, {
    "Code": 14,
    "Libelle": "Industrie de l'habillement"
}, {"Code": 15, "Libelle": "Industrie du cuir et de la chaussure"}, {
    "Code": 16,
    "Libelle": "Travail du bois et fabrication d'articles en bois et en liège, à l'exception des meubles ; fabrication d'articles en vannerie et sparterie"
}, {"Code": 17, "Libelle": "Industrie du papier et du carton"}, {
    "Code": 18,
    "Libelle": "Imprimerie et reproduction d'enregistrements"
}, {"Code": 19, "Libelle": "Cokéfaction et raffinage"}, {"Code": 20, "Libelle": "Industrie chimique"}, {
    "Code": 21,
    "Libelle": "Industrie pharmaceutique"
}, {"Code": 22, "Libelle": "Fabrication de produits en caoutchouc et en plastique"}, {
    "Code": 23,
    "Libelle": "Fabrication d'autres produits minéraux non métalliques"
}, {"Code": 24, "Libelle": "Métallurgie"}, {
    "Code": 25,
    "Libelle": "Fabrication de produits métalliques, à l'exception des machines et des équipements"
}, {"Code": 26, "Libelle": "Fabrication de produits informatiques, électroniques et optiques"}, {
    "Code": 27,
    "Libelle": "Fabrication d'équipements électriques"
}, {"Code": 28, "Libelle": "Fabrication de machines et équipements n.c.a."}, {
    "Code": 29,
    "Libelle": "Industrie automobile"
}, {"Code": 30, "Libelle": "Fabrication d'autres matériels de transport"}, {
    "Code": 31,
    "Libelle": "Fabrication de meubles"
}, {"Code": 32, "Libelle": "Autres industries manufacturières"}, {
    "Code": 33,
    "Libelle": "Réparation et installation de machines et d'équipements"
}, {
    "Code": 35,
    "Libelle": "Production et distribution d'électricité, de gaz, de vapeur et d'air conditionné"
}, {"Code": 36, "Libelle": "Captage, traitement et distribution d'eau"}, {
    "Code": 37,
    "Libelle": "Collecte et traitement des eaux usées"
}, {"Code": 38, "Libelle": "Collecte, traitement et élimination des déchets ; récupération"}, {
    "Code": 39,
    "Libelle": "Dépollution et autres services de gestion des déchets"
}, {"Code": 41, "Libelle": "Construction de bâtiments"}, {"Code": 42, "Libelle": "Génie civil"}, {
    "Code": 43,
    "Libelle": "Travaux de construction spécialisés"
}, {"Code": 45, "Libelle": "Commerce et réparation d'automobiles et de motocycles"}, {
    "Code": 46,
    "Libelle": "Commerce de gros, à l'exception des automobiles et des motocycles"
}, {"Code": 47, "Libelle": "Commerce de détail, à l'exception des automobiles et des motocycles"}, {
    "Code": 49,
    "Libelle": "Transports terrestres et transport par conduites"
}, {"Code": 50, "Libelle": "Transports par eau"}, {"Code": 51, "Libelle": "Transports aériens"}, {
    "Code": 52,
    "Libelle": "Entreposage et services auxiliaires des transports"
}, {"Code": 53, "Libelle": "Activités de poste et de courrier"}, {"Code": 55, "Libelle": "Hébergement"}, {
    "Code": 56,
    "Libelle": "Restauration"
}, {"Code": 58, "Libelle": "Édition"}, {
    "Code": 59,
    "Libelle": "Production de films cinématographiques, de vidéo et de programmes de télévision ; enregistrement sonore et édition musicale"
}, {"Code": 60, "Libelle": "Programmation et diffusion"}, {"Code": 61, "Libelle": "Télécommunications"}, {
    "Code": 62,
    "Libelle": "Programmation, conseil et autres activités informatiques"
}, {"Code": 63, "Libelle": "Services d'information"}, {
    "Code": 64,
    "Libelle": "Activités des services financiers, hors assurance et caisses de retraite"
}, {"Code": 65, "Libelle": "Assurance"}, {
    "Code": 66,
    "Libelle": "Activités auxiliaires de services financiers et d'assurance"
}, {"Code": 68, "Libelle": "Activités immobilières"}, {
    "Code": 69,
    "Libelle": "Activités juridiques et comptables"
}, {"Code": 70, "Libelle": "Activités des sièges sociaux ; conseil de gestion"}, {
    "Code": 71,
    "Libelle": "Activités d'architecture et d'ingénierie ; activités de contrôle et analyses techniques"
}, {"Code": 72, "Libelle": "Recherche-développement scientifique"}, {
    "Code": 73,
    "Libelle": "Publicité et études de marché"
}, {"Code": 74, "Libelle": "Autres activités spécialisées, scientifiques et techniques"}, {
    "Code": 75,
    "Libelle": "Activités vétérinaires"
}, {"Code": 77, "Libelle": "Activités de location et location-bail"}, {
    "Code": 78,
    "Libelle": "Activités liées à l'emploi"
}, {
    "Code": 79,
    "Libelle": "Activités des agences de voyage, voyagistes, services de réservation et activités connexes"
}, {"Code": 80, "Libelle": "Enquêtes et sécurité"}, {
    "Code": 81,
    "Libelle": "Services relatifs aux bâtiments et aménagement paysager"
}, {"Code": 82, "Libelle": "Activités administratives et autres activités de soutien aux entreprises"}, {
    "Code": 84,
    "Libelle": "Administration publique et défense ; sécurité sociale obligatoire"
}, {"Code": 85, "Libelle": "Enseignement"}, {"Code": 86, "Libelle": "Activités pour la santé humaine"}, {
    "Code": 87,
    "Libelle": "Hébergement médico-social et social"
}, {"Code": 88, "Libelle": "Action sociale sans hébergement"}, {
    "Code": 90,
    "Libelle": "Activités créatives, artistiques et de spectacle"
}, {"Code": 91, "Libelle": "Bibliothèques, archives, musées et autres activités culturelles"}, {
    "Code": 92,
    "Libelle": "Organisation de jeux de hasard et d'argent"
}, {"Code": 93, "Libelle": "Activités sportives, récréatives et de loisirs"}, {
    "Code": 94,
    "Libelle": "Activités des organisations associatives"
}, {"Code": 95, "Libelle": "Réparation d'ordinateurs et de biens personnels et domestiques"}, {
    "Code": 96,
    "Libelle": "Autres services personnels"
}, {"Code": 97, "Libelle": "Activités des ménages en tant qu'employeurs de personnel domestique"}, {
    "Code": 98,
    "Libelle": "Activités indifférenciées des ménages en tant que producteurs de biens et services pour usage propre"
}, {"Code": 99, "Libelle": "Activités des organisations et organismes extraterritoriaux"}]

document.addEventListener('readystatechange', event => {
    if (event.target.readyState === "complete") {

        // On complète le select annee avec les 100 dernières années en cours

        let annee = document.getElementById("annee_naissance")

        let annee_en_cours = new Date().getFullYear();

        for (let i = annee_en_cours; i >= annee_en_cours - 100; i--) {
            annee.innerHTML += "<option value=\"" + i + "\">" + i + "</option>";
        }

        let pcs = document.getElementById("PCS_metier")

        pcs_json.forEach(function (value) {
            pcs.innerHTML += "<option value=\"" + value.Libelle + "\">" + value.Libelle + "</option>";
        })

        let naf = document.getElementById("NAF_metier")

        naf_json.forEach(function (value) {
            naf.innerHTML += "<option value=\"" + value.Libelle + "\">" + value.Libelle + "</option>";
        })

        let salarie = document.getElementById("salarie").innerHTML
        dynamiqueRoles(salarie)
    }
});

function dynamiqueRoles(roleSalarie) {
    let email = document.getElementById("emailDiv")
    let nom = document.getElementById("familyNameDiv")
    let prenom = document.getElementById("nameDiv")
    let divArray = [email, nom, prenom]

    let emailValue = document.getElementById("Compte_Email")
    let nomValue = document.getElementById("Compte_name")
    let prenomValue = document.getElementById("Compte_familyName")
    let inputArray = [emailValue, nomValue, prenomValue]

    let selectedRole = document.getElementById("Compte_Select").value

    // Div pour les salariés

    let sexe = document.getElementById("sexeDiv")
    let annee = document.getElementById("anneeDiv")
    let pcs = document.getElementById("PCSDiv")
    let naf = document.getElementById("NAFDiv")
    let commentaire = document.getElementById("CommentaireDiv")
    let divArraySalarie = [sexe, annee, pcs, naf, commentaire]

    // Required pour les salariés

    let anneeValue = document.getElementById("annee_naissance")
    let pcsValue = document.getElementById("PCS_metier")
    let nafValue = document.getElementById("NAF_metier")
    let inputArraySalarie = [anneeValue, pcsValue, nafValue]

    if (selectedRole === roleSalarie) {
        setDisplay(divArray, "none")
        setEmptyValue(inputArray)
        setRequire(inputArray, false)

        setDisplay(divArraySalarie, "")
        setRequire(inputArraySalarie, false)
    } else {
        setDisplay(divArray, "")
        setRequire(inputArray, true)

        setDisplay(divArraySalarie, "none")
        setRequire(inputArraySalarie, false)
    }
}

function setDisplay(arrayDiv, optionDisplay) {
    arrayDiv.forEach(div => div.style.display = optionDisplay)
}

function setEmptyValue(arrayInput) {
    arrayInput.forEach(input => input.value = "")
}

function setRequire(arrayInput, optionRequire) {
    arrayInput.forEach(input => input.required = optionRequire)
}
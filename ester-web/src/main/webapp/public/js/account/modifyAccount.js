/**
 * function qui permet de verifier des mot de passe et colorer les alentours  des champs de saisies
 * et afficher un message dans le cas d'un mot de passe erroné
 * et desactiver l'envoie du formulaire
 */
function checkSamePassword() {
    if ($('#NewPassword').val() === $('#NewPasswordConfirm').val()) {
        $('#message').empty()
        $('#NewPasswordConfirm').removeClass("shadow-danger")
        $('#NewPasswordConfirm').addClass("shadow-success")
        $('#submit').removeAttr("disabled")
        return true
    } else {
        $('#NewPasswordConfirm').removeClass("shadow-success")
        $('#NewPasswordConfirm').addClass("shadow-danger")
        $('#message').text(" X Mot de passe non identique")
        $('#submit').attr("disabled", "true")
        return false
    }
}

function focusOutPasswordShowError() {
    if (checkSamePassword()) {
        $('#not_same_password').css("display", "none")
    } else {
        $('#not_same_password').css("display", "block")
    }
}
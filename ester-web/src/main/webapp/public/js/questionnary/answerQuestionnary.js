var numQuestion = 0;

function affiche(objQuestionnaire) {
    console.log(objQuestionnaire);

    // Affichage du titre du questionnaire
    var titre = document.createElement("h3");
    titre.setAttribute("class", "font-weight-normal text-center");
    var txtTitre = document.createTextNode(objQuestionnaire.name);
    titre.appendChild(txtTitre);
    document.getElementById("titreQuestionnaire").appendChild(titre);

    var container = document.getElementById("containerQuestionnaire");

    /* Types de questions :
        shortAnswer
        longAnswer
        checkbox
        select
        radio
    */

    var tabQuestions = objQuestionnaire.questions;

    // On parcourt la collection de questions pour les trainer une à une
    // Toutes les questions sont en "required", puisque logiquement la personne doit
    for (var i = 0; i < tabQuestions.length; i++) {

        // Chaque question est dans un <div class="from-group">
        var form_grp = document.createElement("div");
        form_grp.setAttribute("class", "form-group container-fluid col-md-12 mb-4 d-flex justify-content-center row");

        var question = tabQuestions[i];

        // On récupère le type et on place l'intitule de la question
        var type = question.type;

        numQuestion++;

        var labelRow = document.createElement("div");
        labelRow.setAttribute("class", "row");
        var label = document.createElement("label");
        label.setAttribute("class", "col-md-12");
        var underline = document.createElement("u");

        var intitule = document.createTextNode(question.name);

        underline.appendChild(intitule);
        label.appendChild(document.createTextNode(numQuestion + ") "));
        label.appendChild(underline);

        labelRow.appendChild(label);

        form_grp.appendChild(label);

        // S'il s'agit d'un type input_text, le tableau de réponses est vide
        if (type === "SHORTANSWER") {

            var row1 = document.createElement("div");
            row1.setAttribute("class", "col-md-10 justify-content-center d-flex");

            var input_txt = document.createElement("input");
            input_txt.setAttribute("class", "form-control questionWritten");
            input_txt.setAttribute("type", "text");
            $(input_txt).prop("required", "required");

            var txtId1 = "q_" + numQuestion;
            input_txt.setAttribute("id", txtId1);
            input_txt.setAttribute("name", question.id);

            row1.appendChild(input_txt);
            form_grp.appendChild(row1);
        }

        // Même chose pour un textarea
        if (type === "LONGANSWER") {

            var row2 = document.createElement("div");
            row2.setAttribute("class", "col-md-10 justify-content-center d-flex");

            var textarea = document.createElement("textarea");
            textarea.setAttribute("class", "form-control questionWritten");
            $(textarea).prop("required", "required");

            textarea.setAttribute("rows", "3");
            var txtId2 = "q_" + numQuestion;
            textarea.setAttribute("id", txtId2);
            textarea.setAttribute("name", question.id);

            row2.appendChild(textarea);
            form_grp.appendChild(row2);
        }

        // S'il s'agit d'un type select, les réponses s'affichent dans un menu déroulant
        if (type === "SELECT") {

            var col = document.createElement("div");
            col.setAttribute("class", "col-md-10");

            var select = document.createElement("select");
            select.setAttribute("class", "custom-select questionSelected");
            $(select).prop("required", "required");
            select.setAttribute("name", question.id);

            var option_vide = document.createElement("option");
            option_vide.setAttribute("selected", "true");
            option_vide.setAttribute("class", "emptyOption");
            select.appendChild(option_vide);

            var num_option = 0;

            // On affiche les réponses une par une sous forme de bouton radio
            for (var j = 0; j < question.questionOptions.length; j++) {

                var opt = question.questionOptions[j];

                num_option++;
                var select_option = document.createElement("option");
                var txtId3 = "q_" + numQuestion + "_r_" + num_option;
                select_option.setAttribute("id", txtId3);
                select_option.setAttribute("value", opt.id);

                var option1 = document.createTextNode(opt.value);

                select_option.appendChild(option1);
                select.appendChild(select_option);
            }

            col.appendChild(select);
            form_grp.appendChild(col);
        }

        // S'il s'agit d'un type radio, on doit choisir une seule réponse parmi plusieurs
        if (type === "RADIO") {

            var num_radio = 0;

            // On affiche les réponses une par une sous forme de bouton radio
            for (var k = 0; k < question.questionOptions.length; k++) {

                var optRadio = question.questionOptions[k];
                num_radio++;

                // Chaque option est dans un <div class="form-check">

                var col_rad = document.createElement("div");
                col_rad.setAttribute('class', "col-md-10");

                var form_chk1 = document.createElement("div");
                form_chk1.setAttribute("class", "form-check");

                var input_radio = document.createElement("input");
                input_radio.setAttribute("class", "form-check-input reponseQCM");
                input_radio.setAttribute("type", "radio");

                $(input_radio).prop("required", "required");
                input_radio.setAttribute("name", question.id);
                input_radio.setAttribute("value", optRadio.id);

                var txtId4 = "q_" + numQuestion + "_r_" + num_radio;
                input_radio.setAttribute("id", txtId4);

                label = document.createElement("label");
                label.setAttribute("class", "form-check-label pt-1 ml-4");
                label.setAttribute("for", txtId4);
                var option2 = document.createTextNode(optRadio.value);
                label.appendChild(option2);

                form_chk1.appendChild(input_radio);
                form_chk1.appendChild(label);
                col_rad.appendChild(form_chk1);

                form_grp.appendChild(col_rad);
            }
        }

        // S'il s'agit d'un type checkbox, on peux choisir plusieurs réponses parmi celles proposées
        if (type === "CHECKBOX") {

            // On affiche les réponses une par une sous forme de bouton checkbox
            for (var l = 0; l < question.questionOptions.length; l++) {

                // Chaque option est dans un <div class="form-check">
                var col_chk = document.createElement("div");
                col_chk.setAttribute('class', "col-md-10");

                var form_chk = document.createElement("div");
                form_chk.setAttribute("class", "form-check");


                var checkbox = document.createElement("input");
                checkbox.setAttribute("class", "form-check-input reponseQCM");
                checkbox.setAttribute("type", "checkbox");
                checkbox.setAttribute("name", question.id + "_q" + l);
                checkbox.setAttribute("value", question.questionOptions[l].id);
                var txtId = "q_" + numQuestion + "_r_" + (l + 1);
                checkbox.setAttribute("id", txtId);

                label = document.createElement("label");
                label.setAttribute("class", "form-check-label pt-1 ml-4");
                label.setAttribute("for", txtId);
                var option = document.createTextNode(question.questionOptions[l].value);
                label.appendChild(option);

                form_chk.appendChild(checkbox);
                form_chk.appendChild(label);

                col_chk.appendChild(form_chk);

                form_grp.appendChild(col_chk);
            }
        }

        container.appendChild(form_grp);
    } //fin question
}

$(document).on("click", "#validateAction", function () {

    console.log("click");

    let questionChecked = [];
    $(".reponseQCM").each(function () {
        // for checkbox we can have more than requiredOk Answer (
        if ($(this).is(":checked")) {
            let exist = false
            let id = $(this).attr("id").split('_')
            id = id[0] + id[1]
            questionChecked.indexOf(id) === -1 ? questionChecked.push(id) : exist = true
            if(!exist){
                $(this).addClass("requiredOK");
            }
        }
    });

    console.log("reponseQCM");

    $(".questionWritten").each(function () {
        if ($(this).val() != "" && $(this).val() != undefined) {
            $(this).addClass("requiredOK");
        }
    });

    console.log("questionWritten");

    $(".questionSelected").each(function () {
        var currentSelect = $(this);
        currentSelect.children().each(function () {
            if ($(this).is(":selected") && !$(this).hasClass("emptyOption")) {
                currentSelect.addClass("requiredOK");
            }
        })
    });

    console.log("questionSelected");

    var numberOfQuestionsNotEmpty = 0;
    $(document).find('.requiredOK').each(function () {
        numberOfQuestionsNotEmpty += 1;
        $(this).removeClass("requiredOK");
    });

    console.log("numberOfQuestionsNotEmpty");

    if (numberOfQuestionsNotEmpty === numQuestion) {
        $('#ModalQuestionnaire').modal('show');
        $('#ModalQuestionnaireOK').on('click', function () {
            $("#validateAction").prop('type', 'submit');
            $("#validateAction").click();
        });
    } else {
        $('#MsgErreur').html("Navré mais il vous reste des <u>champs vides</u>. Remplisser les balises vides avant de le valider.");
        $('#ErreurSaisie').modal('show');
    }

    console.log("ModalQuestionnaire");
});
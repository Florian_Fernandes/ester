/****************************************************
 *                                                  *
 *           DEFINITIONS DES VARIABLES              *
 *                                                  *
 ****************************************************/

// Liste déroulante pour choisir quel type de question ajouté
var liste_question = document.getElementById("choix_question")

// Bouton pour ajouter des questions
var ajout_question = document.getElementById("nouvelle_question")

// Div dans lequel seront ajoutées les questions
var questions = document.getElementById("questions")

// Nombre de questions créées
var num_questions = document.getElementById("nb_questions").getAttribute("value")


/****************************************************
 *                                                  *
 *           FONCTIONS POUR LES BOUTONS             *
 *                                                  *
 ****************************************************/

// Listener sur le bouton des questions qui va ajouter une nouvelle question en fonction du choix dans la liste déroulante
ajout_question.addEventListener("click", function () {
    switch (liste_question.value) {
        // Question courte

        case 'question_courte':
            num_questions++ // on augmente le nombre de questions de 1
            questions.insertAdjacentHTML("beforeend", question_courte(num_questions)) // on ajoute une question courte dans le div
            MaJ_nombre_question(num_questions) // on met à jour le compteur de questions à renvoyer à la servlet
            break;

        // Question longue

        case 'question_longue':
            num_questions++ // on augmente le nombre de questions de 1
            questions.insertAdjacentHTML("beforeend", question_longue(num_questions)) // on ajoute une question longue dans le div
            MaJ_nombre_question(num_questions) // on met à jour le compteur de questions à renvoyer à la servlet
            break;

        // Question à choix multiples avec boutons radio

        case 'question_radio':
            num_questions++ // on augmente le nombre de questions de 1
            questions.insertAdjacentHTML("beforeend", question_choix(num_questions, "radio")) // on ajoute une question radio dans le div
            MaJ_nombre_question(num_questions) // on met à jour le compteur de questions à renvoyer à la servlet
            break;

        // Question à choix multiples avec liste déroulante

        case 'question_liste':
            num_questions++ // on augmente le nombre de questions de 1
            questions.insertAdjacentHTML("beforeend", question_choix(num_questions, "liste")) // on ajoute une question avec liste déroulante dans le div
            MaJ_nombre_question(num_questions) // on met à jour le compteur de questions à renvoyer à la servlet
            break;

        // Question à choix multiples avec cases à cocher

        case 'question_case':
            num_questions++ // on augmente le nombre de questions de 1
            questions.insertAdjacentHTML("beforeend", question_choix(num_questions, "case")) // on ajoute une question avec cases à cocher dans le div
            MaJ_nombre_question(num_questions) // on met à jour le compteur de questions à renvoyer à la servlet
            break;
    }
})

// met à jour le hidden pour renvoyer à la servlet le bon nombre de questions une fois le formulaire validé
function MaJ_nombre_question(nbr_question) {
    document.getElementById("nb_questions").setAttribute("value", nbr_question)
}

// supprime la question n° "num_question" passé en paramètre
function delete_button(num_question) {
    document.getElementById("question_" + num_question).remove() // on retire la question sélectionnée

    for (var i = num_question + 1; i <= num_questions; i++) // on modifie toutes les questions supérieures à la question supprimée
    {
        // on modifie le div

        document.getElementById("question_" + i).setAttribute("id", "question_" + (i - 1))

        // on modifie les boutons

        // bouton UP

        document.getElementById("up_" + i).setAttribute("onclick", "move_button(" + (i - 1) + ",-1)")
        document.getElementById("up_" + i).setAttribute("id", "up_" + (i - 1))

        // bouton DOWN

        document.getElementById("down_" + i).setAttribute("onclick", "move_button(" + (i - 1) + ",1)")
        document.getElementById("down_" + i).setAttribute("id", "down_" + (i - 1))

        // bouton DELETE

        document.getElementById("delete_" + i).setAttribute("onclick", "delete_button(" + (i - 1) + ")")
        document.getElementById("delete_" + i).setAttribute("id", "delete_" + (i - 1))

        // on modifie les champs communs à toutes les questions

        document.getElementsByName("question_" + i)[0].setAttribute("name", "question_" + (i - 1))
        document.getElementsByName("type_question_" + i)[0].setAttribute("name", "type_question_" + (i - 1))
        document.getElementById("body_question_" + i).setAttribute("id", "body_question_" + (i - 1))

        // on modifie les champs et les boutons spécifiques aux questions à choix multiples

        // le bouton pour ajouter des sous-questions

        if (document.getElementById("button_choix_question_" + i)) {
            switch (document.getElementsByName("type_question_" + (i - 1))[0].getAttribute("value")) {
                case 'radio':
                    document.getElementById("button_choix_question_" + i).setAttribute("onclick", "ajouter_question(" + (i - 1) + ",'radio')");
                    break;
                case 'liste':
                    document.getElementById("button_choix_question_" + i).setAttribute("onclick", "ajouter_question(" + (i - 1) + ",'liste')");
                    break;
                case 'case':
                    document.getElementById("button_choix_question_" + i).setAttribute("onclick", "ajouter_question(" + (i - 1) + ",'case')");
                    break;
            }

            document.getElementById("button_choix_question_" + i).setAttribute("id", "button_choix_question_" + (i - 1))

            // le seuil de la question

            document.getElementsByName("seuil_question_" + i)[0].setAttribute("name", "seuil_question_" + (i - 1))

            // la variable de référence de la question

            document.getElementsByName("variable_question_" + i)[0].setAttribute("name", "variable_question_" + (i - 1))

            // le bouton pour retirer des sous-questions

            document.getElementById("button_retirer_question_" + i).setAttribute("onclick", "retirer_question(" + (i - 1) + ")")
            document.getElementById("button_retirer_question_" + i).setAttribute("id", "button_retirer_question_" + (i - 1))

            // et les sous_questions

            for (var j = 1; j <= parseInt(document.getElementsByName("nb_sous_questions_" + i)[0].getAttribute("value")); j++) {
                document.getElementsByName("sous_question_" + j + "_" + i)[0].setAttribute("name", "sous_question_" + j + "_" + (i - 1))

                document.getElementsByName("score_sous_question_" + j + "_" + i)[0].setAttribute("name", "score_sous_question_" + j + "_" + (i - 1))

                document.getElementsByName("coefficient_sous_question_" + j + "_" + i)[0].setAttribute("name", "coefficient_sous_question_" + j + "_" + (i - 1))

                document.getElementsByName("valeur_variable_sous_question_" + j + "_" + i)[0].setAttribute("name", "valeur_variable_sous_question_" + j + "_" + (i - 1))
            }
        }

        // le nombre de sous-questions

        if (document.getElementsByName("nb_sous_questions_" + i)[0]) {
            document.getElementsByName("nb_sous_questions_" + i)[0].setAttribute("name", "nb_sous_questions_" + (i - 1))
        }
    }

    num_questions--
    MaJ_nombre_question(num_questions)
}

// remonte (pas == -1) ou descend (pas == 1) d'un emplacement la question n° "num_question" passé en paramètre
function move_button(num_question, pas) {
    if ((pas == -1 && num_question != 1) || (pas == 1 && num_question < num_questions)) {
        div_actuel = document.getElementById("question_" + num_question) // on récupère la question

        div_precedent = document.getElementById("question_" + (num_question + pas)) // celle d'avant

        if (pas == -1) {
            document.getElementById("questions").insertBefore(div_actuel, div_precedent) // on switch leur place si on monte
        } else {
            document.getElementById("questions").insertBefore(div_precedent, div_actuel) // on switch leur place si on descend
        }

        // on modifie les div

        div_actuel.setAttribute("id", "question_" + (num_question + pas))
        div_precedent.setAttribute("id", "question_" + num_question)

        // on modifie les boutons

        // bouton UP

        bouton_up_actuel = document.getElementById("up_" + num_question)
        bouton_up_precedent = document.getElementById("up_" + (num_question + pas))

        bouton_up_actuel.setAttribute("onclick", "move_button(" + (num_question + pas) + ",-1)")
        bouton_up_actuel.setAttribute("id", "up_" + (num_question + pas))

        bouton_up_precedent.setAttribute("onclick", "move_button(" + (num_question) + ",-1)")
        bouton_up_precedent.setAttribute("id", "up_" + (num_question))

        // bouton DOWN

        bouton_down_actuel = document.getElementById("down_" + num_question)
        bouton_down_precedent = document.getElementById("down_" + (num_question + pas))

        bouton_down_actuel.setAttribute("onclick", "move_button(" + (num_question + pas) + ",1)")
        bouton_down_actuel.setAttribute("id", "down_" + (num_question + pas))

        bouton_down_precedent.setAttribute("onclick", "move_button(" + (num_question) + ",1)")
        bouton_down_precedent.setAttribute("id", "down_" + (num_question))

        // bouton DELETE

        bouton_delete_actuel = document.getElementById("delete_" + num_question)
        bouton_delete_precedent = document.getElementById("delete_" + (num_question + pas))

        bouton_delete_actuel.setAttribute("onclick", "delete_button(" + (num_question + pas) + ")")
        bouton_delete_actuel.setAttribute("id", "delete_" + (num_question + pas))

        bouton_delete_precedent.setAttribute("onclick", "delete_button(" + num_question + ")")
        bouton_delete_precedent.setAttribute("id", "delete_" + num_question)

        // bouton AJOUT

        bouton_ajout_actuel = document.getElementById("button_choix_question_" + num_question)
        bouton_ajout_precedent = document.getElementById("button_choix_question_" + (num_question + pas))

        if (bouton_ajout_actuel) {
            switch (document.getElementsByName("type_question_" + num_question)[0].getAttribute("value")) {
                case 'radio':
                    bouton_ajout_actuel.setAttribute("onclick", "ajouter_question(" + (num_question + pas) + ",'radio')")
                    break;
                case 'liste':
                    bouton_ajout_actuel.setAttribute("onclick", "ajouter_question(" + (num_question + pas) + ",'liste')")
                    break;
                case 'case':
                    bouton_ajout_actuel.setAttribute("onclick", "ajouter_question(" + (num_question + pas) + ",'case')")
                    break;
            }

            bouton_ajout_actuel.setAttribute("id", "button_choix_question_" + (num_question + pas))
        }

        if (bouton_ajout_precedent) {
            switch (document.getElementsByName("type_question_" + (num_question + pas))[0].getAttribute("value")) {
                case 'radio':
                    bouton_ajout_precedent.setAttribute("onclick", "ajouter_question(" + (num_question) + ",'radio')")
                    break;
                case 'liste':
                    bouton_ajout_precedent.setAttribute("onclick", "ajouter_question(" + (num_question) + ",'liste')")
                    break;
                case 'case':
                    bouton_ajout_precedent.setAttribute("onclick", "ajouter_question(" + (num_question) + ",'case')")
                    break;
            }

            bouton_ajout_precedent.setAttribute("id", "button_choix_question_" + num_question)
        }

        // bouton RETIRER

        bouton_retirer_actuel = document.getElementById("button_retirer_question_" + num_question)
        bouton_retirer_precedent = document.getElementById("button_retirer_question_" + (num_question + pas))

        if (bouton_retirer_actuel) {
            bouton_retirer_actuel.setAttribute("onclick", "retirer_question(" + (num_question + pas) + ")")
            bouton_retirer_actuel.setAttribute("id", "button_retirer_question_" + (num_question + pas))
        }

        if (bouton_retirer_precedent) {
            bouton_retirer_precedent.setAttribute("onclick", "retirer_question(" + num_question + ")")
            bouton_retirer_precedent.setAttribute("id", "button_retirer_question_" + num_question)
        }

        // on modifie les champs

        // champ question

        champ_question_actuel = document.getElementsByName("question_" + num_question)[0]
        champ_question_precedent = document.getElementsByName("question_" + (num_question + pas))[0]

        champ_question_actuel.setAttribute("name", "question_" + (num_question + pas))
        champ_question_precedent.setAttribute("name", "question_" + num_question)

        // champ type question

        champ_type_actuel = document.getElementsByName("type_question_" + num_question)[0]
        champ_type_precedent = document.getElementsByName("type_question_" + (num_question + pas))[0]

        champ_type_actuel.setAttribute("name", "type_question_" + (num_question + pas))
        champ_type_precedent.setAttribute("name", "type_question_" + num_question)

        // champ body

        champ_body_actuel = document.getElementById("body_question_" + num_question)
        champ_body_precedent = document.getElementById("body_question_" + (num_question + pas))

        champ_body_actuel.setAttribute("id", "body_question_" + (num_question + pas))
        champ_body_precedent.setAttribute("id", "body_question_" + num_question)

        // champ seuil

        champ_seuil_actuel = document.getElementsByName("seuil_question_" + num_question)[0]
        champ_seuil_precedent = document.getElementsByName("seuil_question_" + (num_question + pas))[0]

        if (champ_seuil_actuel) {
            champ_seuil_actuel.setAttribute("name", "seuil_question_" + (num_question + pas))
        }

        if (champ_seuil_precedent) {
            champ_seuil_precedent.setAttribute("name", "seuil_question_" + num_question)
        }

        // champ variable de référence

        champ_variable_actuel = document.getElementsByName("variable_question_" + num_question)[0]
        champ_variable_precedent = document.getElementsByName("variable_question_" + (num_question + pas))[0]

        if (champ_variable_actuel) {
            champ_variable_actuel.setAttribute("name", "variable_question_" + (num_question + pas))
        }

        if (champ_variable_precedent) {
            champ_variable_precedent.setAttribute("name", "variable_question_" + num_question)
        }

        // on modifie les sous-questions

        // on récupère le nombre de sous-questions pour chaque question

        nb_sous_questions_actuel = document.getElementsByName("nb_sous_questions_" + (num_question))[0]
        nb_sous_questions_precedent = document.getElementsByName("nb_sous_questions_" + (num_question + pas))[0]

        // et on va stocker chaque champ dans des listes spécifiques
        // (obligatoire car si on modifie au fur et à mesure sans les stocker auparavant, ce sont les questions modifiées qui seront prises à la place des questions précédentes)
        // (ex : si la sous_question_1_2 devient la sous_question_1_1, c'est celle-ci qui sera ensuite reprise lorsque l'on voudra modifier la sous_question_1_1 en sous_question_1_2)

        liste_sous_questions_actuel = []
        liste_sous_questions_precedente = []

        liste_score_sous_questions_actuel = []
        liste_score_sous_questions_precedente = []

        liste_coefficient_sous_questions_actuel = []
        liste_coefficient_sous_questions_precedente = []

        liste_valeur_sous_questions_actuel = []
        liste_valeur_sous_questions_precedente = []

        // on récupère les champs des sous-questions, on les stocke et on modifie le champ pour le nombre de sous-questions

        if (nb_sous_questions_actuel) {
            for (var i = 1; i <= parseInt(nb_sous_questions_actuel.getAttribute("value")); i++) {
                liste_sous_questions_actuel.push(document.getElementsByName("sous_question_" + i + "_" + (num_question))[0])
                liste_score_sous_questions_actuel.push(document.getElementsByName("score_sous_question_" + i + "_" + (num_question))[0])
                liste_coefficient_sous_questions_actuel.push(document.getElementsByName("coefficient_sous_question_" + i + "_" + (num_question))[0])
                liste_valeur_sous_questions_actuel.push(document.getElementsByName("valeur_variable_sous_question_" + i + "_" + (num_question))[0])
            }

            nb_sous_questions_actuel.setAttribute("name", "nb_sous_questions_" + (num_question + pas))
        }

        if (nb_sous_questions_precedent) {
            for (var i = 1; i <= parseInt(nb_sous_questions_precedent.getAttribute("value")); i++) {
                liste_sous_questions_precedente.push(document.getElementsByName("sous_question_" + i + "_" + (num_question + pas))[0])
                liste_score_sous_questions_precedente.push(document.getElementsByName("score_sous_question_" + i + "_" + (num_question + pas))[0])
                liste_coefficient_sous_questions_precedente.push(document.getElementsByName("coefficient_sous_question_" + i + "_" + (num_question + pas))[0])
                liste_valeur_sous_questions_precedente.push(document.getElementsByName("valeur_variable_sous_question_" + i + "_" + (num_question + pas))[0])
            }

            nb_sous_questions_precedent.setAttribute("name", "nb_sous_questions_" + (num_question))
        }

        // on modifie les champs des sous-questions

        for (var i = 0; i < liste_sous_questions_actuel.length; i++) {
            liste_sous_questions_actuel[i].setAttribute("name", "sous_question_" + (i + 1) + "_" + (num_question + pas))
            liste_score_sous_questions_actuel[i].setAttribute("name", "score_sous_question_" + (i + 1) + "_" + (num_question + pas))
            liste_coefficient_sous_questions_actuel[i].setAttribute("name", "coefficient_sous_question_" + (i + 1) + "_" + (num_question + pas))
            liste_valeur_sous_questions_actuel[i].setAttribute("name", "valeur_variable_sous_question_" + (i + 1) + "_" + (num_question + pas))
        }

        for (var i = 0; i < liste_sous_questions_precedente.length; i++) {
            liste_sous_questions_precedente[i].setAttribute("name", "sous_question_" + (i + 1) + "_" + (num_question))
            liste_score_sous_questions_precedente[i].setAttribute("name", "score_sous_question_" + (i + 1) + "_" + (num_question))
            liste_coefficient_sous_questions_precedente[i].setAttribute("name", "coefficient_sous_question_" + (i + 1) + "_" + (num_question))
            liste_valeur_sous_questions_precedente[i].setAttribute("name", "valeur_variable_sous_question_" + (i + 1) + "_" + (num_question))
        }
    }
}

// ajoute une sous-question pour une question à choix multiples avec boutons radio
function ajouter_question(num_question, type) {
    // on ajoute la nouvelle sous-question dans la question avec boutons radio, liste ou case

    num_sous_question = parseInt(document.getElementsByName("nb_sous_questions_" + num_question)[0].getAttribute("value"))

    symbole = ""

    switch (type) {
        case "radio":
            symbole = "       <div class=\"input-group mb-3\">\n" +
                "           <div class=\"input-group-prepend\">\n" +
                "               <span class=\"input-group-text\" id=\"radio-aria\">\n" +
                "                   <input type=\"radio\" disabled/>\n" +
                "               </span>\n" +
                "           </div>\n" +
                "           <input type=\"text\" name=\"sous_question_" + (num_sous_question + 1) + "_" + num_question + "\" aria-label=\"radio-aria\" aria-describedby=\"radio-aria\" placeholder=\"Intitulé du choix\" class=\"form-control\" required/>\n" +
                "       </div>\n"
            break
        case "liste":
            symbole = "       <div class=\"input-group mb-3\">\n" +
                "           <div class=\"input-group-prepend\">\n" +
                "               <span class=\"input-group-text\" id=\"select-aria\">\n" +
                "                   -" +
                "               </span>\n" +
                "           </div>\n" +
                "           <input type=\"text\" name=\"sous_question_" + (num_sous_question + 1) + "_" + num_question + "\" aria-label=\"select-aria\" aria-describedby=\"select-aria\" placeholder=\"Intitulé du choix\" class=\"form-control\" required/>\n" +
                "       </div>\n"
            break
        case "case":
            symbole = "       <div class=\"input-group mb-3\">\n" +
                "           <div class=\"input-group-prepend\">\n" +
                "               <span class=\"input-group-text\" id=\"checkbox-aria\">\n" +
                "                   <input type=\"checkbox\" disabled/>\n" +
                "               </span>\n" +
                "           </div>\n" +
                "           <input type=\"text\" name=\"sous_question_" + (num_sous_question + 1) + "_" + num_question + "\" aria-label=\"checkbox-aria\" aria-describedby=\"checkbox-aria\" placeholder=\"Intitulé du choix\" class=\"form-control\" required/>\n" +
                "       </div>\n"
    }

    sous_question = "<tr>\n" +
        "   <td colspan='3'>\n" +
        symbole +
        "   </td>\n" +
        "</tr>\n" +
        "<tr>\n" +
        "   <td>\n" +
        "       <div class=\"input-group mb-3\">\n" +
        "           <div class=\"input-group-prepend\">\n" +
        "               <span class=\"input-group-text\" id=\"score-aria\">Score :</span>\n" +
        "           </div>\n" +
        "           <input type=\"number\" name=\"score_sous_question_" + (num_sous_question + 1) + "_" + num_question + "\" size=\"3\" value=\"1\" aria-label=\"score-aria\" aria-describedby=\"score-aria\" class=\"form-control\" required/>\n" +
        "       </div>\n" +
        "   </td>\n" +
        "   <td>\n" +
        "       <div class=\"input-group mb-3\">\n" +
        "           <div class=\"input-group-prepend\">\n" +
        "               <span class=\"input-group-text\" id=\"coeff-aria\">Coefficient :</span>\n" +
        "           </div>\n" +
        "           <input type=\"number\" name=\"coefficient_sous_question_" + (num_sous_question + 1) + "_" + num_question + "\" size=\"3\" value=\"1\" aria-label=\"coeff-aria\" aria-describedby=\"coeff-aria\" class=\"form-control\" required/>\n" +
        "       </div>\n" +
        "   </td>\n" +
        "   <td>\n" +
        "       <div class=\"input-group mb-3\">\n" +
        "           <div class=\"input-group-prepend\">\n" +
        "               <span class=\"input-group-text\" id=\"valeur-aria\">Valeur de la variable :</span>\n" +
        "           </div>\n" +
        "           <input type=\"number\" name=\"valeur_variable_sous_question_" + (num_sous_question + 1) + "_" + num_question + "\" size=\"3\" value=\"" + (num_sous_question + 1) + "\"aria-label=\"valeur-aria\" aria-describedby=\"valeur-aria\" class=\"form-control\"/>\n" +
        "       </div>\n" +
        "   </td>\n" +
        "</tr>"

    document.getElementById("body_question_" + num_question).insertAdjacentHTML("beforeend", sous_question)

    // et on incrémente de 1 son nombre de sous-questions

    document.getElementsByName("nb_sous_questions_" + num_question)[0].setAttribute("value", parseInt(document.getElementsByName("nb_sous_questions_" + num_question)[0].getAttribute("value")) + 1)
}

// retire une sous-question pour une question à choix multiples
function retirer_question(num_question) {
    if (parseInt(document.getElementsByName("nb_sous_questions_" + num_question)[0].getAttribute("value")) > 0) // si son nombre de sous-questions est supérieur à 0
    {
        document.getElementById("body_question_" + num_question).removeChild(document.getElementById("body_question_" + num_question).lastElementChild) // on retire le score et le coefficient

        document.getElementById("body_question_" + num_question).removeChild(document.getElementById("body_question_" + num_question).lastElementChild) // on retire la sous-question

        document.getElementsByName("nb_sous_questions_" + num_question)[0].setAttribute("value", parseInt(document.getElementsByName("nb_sous_questions_" + num_question)[0].getAttribute("value")) - 1) // on décrémente le nombre de sous-questions de 1
    }
}

/****************************************************
 *                                                  *
 *  FONCTIONS GENERANT LES TEMPLATE DES QUESTIONS   *
 *                                                  *
 ****************************************************/

// renvoie le template d'une question courte avec pour numéro de question "num_question"
function question_courte(num_question) {
    return "<div id=\"question_" + num_question + "\" class=\"questionCard\">\n" +
        "   <input type=\"hidden\" name=\"type_question_" + num_question + "\" value=\"courte\"/>\n" +
        "       <table>\n" +
        "           <thead>\n" +
        "               <tr>\n" +
        "                   <td colspan=\"2\">\n" +
        "                       Question à réponse courte\n" +
        "                   </td>\n" +
        "                   <td class=\"td_right\">\n" +
        "                       <button type=\"button\" id=\"up_" + num_question + "\" onclick=\"move_button(" + num_question + ",-1)\" class=\"btn btn-md btn-primary\">\n" +
        "                           <i class=\"material-icons\">keyboard_arrow_up</i>\n" +
        "                       </button>\n" +
        "                       <button type=\"button\" id=\"down_" + num_question + "\" onclick=\"move_button(" + num_question + ",1)\" class=\"btn btn-md btn-primary\">\n" +
        "                           <i class=\"material-icons\">keyboard_arrow_down</i>\n" +
        "                       </button>\n" +
        "                       <button type=\"button\" id=\"delete_" + num_question + "\" onclick=\"delete_button(" + num_question + ")\" class=\"btn btn-danger\">\n" +
        "                           <i class=\"material-icons\">delete</i>\n" +
        "                       </button>\n" +
        "                   </td>\n" +
        "               </tr>\n" +
        "           </thead>\n" +

        "           <tbody id=\"body_question_" + num_question + "\">\n" +
        "               <tr>\n" +
        "                   <td colspan=\"3\">\n" +
        "                       <div class=\"input-group mb-3\">\n" +
        "                           <div class=\"input-group-prepend\">\n" +
        "                               <span class=\"input-group-text\" id=\"intitule-courte-aria\">Intitulé de la question :</span>\n" +
        "                           </div>\n" +
        "                           <input type=\"text\" name=\"question_" + num_question + "\" aria-label=\"intitule-courte-aria\" aria-describedby=\"intitule-courte-aria\" placeholder=\"Question courte\" class=\"form-control\" required/>\n" +
        "                       </div>\n" +
        "                   </td>\n" +
        "               </tr>\n" +
        "           </tbody>\n" +
        "       </table>\n" +
        "</div>"
}

// renvoie le template d'une question longue avec pour numéro de question "num_question"
function question_longue(num_question) {
    return "<div id=\"question_" + num_question + "\" class=\"questionCard\">\n" +
        "   <input type=\"hidden\" name=\"type_question_" + num_question + "\" value=\"longue\"/>\n" +
        "       <table>\n" +
        "           <thead>\n" +
        "               <tr>\n" +
        "                   <td colspan=\"2\">\n" +
        "                       Question à réponse longue\n" +
        "                   </td>\n" +
        "                   <td class=\"td_right\">\n" +
        "                       <button type=\"button\" id=\"up_" + num_question + "\" onclick=\"move_button(" + num_question + ",-1)\" class=\"btn btn-md btn-primary\">\n" +
        "                           <i class=\"material-icons\">keyboard_arrow_up</i>\n" +
        "                       </button>\n" +
        "                       <button type=\"button\" id=\"down_" + num_question + "\" onclick=\"move_button(" + num_question + ",1)\" class=\"btn btn-md btn-primary\">\n" +
        "                           <i class=\"material-icons\">keyboard_arrow_down</i>\n" +
        "                       </button>\n" +
        "                       <button type=\"button\" id=\"delete_" + num_question + "\" onclick=\"delete_button(" + num_question + ")\" class=\"btn btn-danger\">\n" +
        "                           <i class=\"material-icons\">delete</i>\n" +
        "                       </button>\n" +
        "                   </td>\n" +
        "               </tr>\n" +
        "           </thead>\n" +

        "           <tbody id=\"body_question_" + num_question + "\">\n" +
        "               <tr>\n" +
        "                   <td colspan=\"3\">\n" +
        "                       <div class=\"input-group mb-3\">\n" +
        "                           <div class=\"input-group-prepend\">\n" +
        "                               <span class=\"input-group-text\" id=\"intitule-longue-aria\">Intitulé de la question :</span>\n" +
        "                           </div>\n" +
        "                           <textarea cols=\"50\" name=\"question_" + num_question + "\" aria-label=\"intitule-longue-aria\" aria-describedby=\"intitule-longue-aria\" placeholder=\"Question longue\" className=\"form-control\" required></textarea>\n" +
        "                       </div>\n" +
        "                   </td>\n" +
        "               </tr>\n" +
        "           </tbody>\n" +
        "       </table>\n" +
        "</div>"
}

// renvoie le template d'une question à choix multiples avec boutons radio, liste ou cases à cocher selon le type renseigné et ayant pour numéro de question "num_question"
function question_choix(num_question, type) {

    intitule = ""

    switch (type) {
        case "radio":
            intitule = "Question à choix multiples avec boutons radio (une réponse)\n"
            break
        case "liste":
            intitule = "Question à choix multiples avec liste déroulante (plusieurs réponses)\n"
            break
        case "case":
            intitule = "Question à choix multiples avec cases à cocher (plusieurs réponses)\n"
    }

    return "<div id=\"question_" + num_question + "\" class=\"questionCard\">\n" +
        "       <input type='hidden' name=\"type_question_" + num_question + "\" value=\"" + type + "\">\n" +
        "       <input type='hidden' name=\"nb_sous_questions_" + num_question + "\" value=\"0\">\n" +
        "       <table>\n" +
        "           <thead>\n" +
        "              <tr>\n" +
        "                <td colspan=\"2\">\n" +
        "                   " + intitule + "\n" +
        "                </td>\n" +
        "                <td class=\"td_right\">\n" +
        "                   <button type=\"button\" id=\"up_" + num_question + "\" onclick=\"move_button(" + num_question + ",-1)\" class=\"btn btn-md btn-primary\">\n" +
        "                       <i class=\"material-icons\">keyboard_arrow_up</i>\n" +
        "                   </button>\n" +
        "                   <button type=\"button\" id=\"down_" + num_question + "\" onclick=\"move_button(" + num_question + ",1)\" class=\"btn btn-md btn-primary\">\n" +
        "                       <i class=\"material-icons\">keyboard_arrow_down</i>\n" +
        "                   </button>\n" +
        "                   <button type=\"button\" id=\"delete_" + num_question + "\" onclick=\"delete_button(" + num_question + ")\" class=\"btn btn-danger\">\n" +
        "                       <i class=\"material-icons\">delete</i>\n" +
        "                   </button>\n" +
        "                 </td>\n" +
        "               </tr>\n" +
        "           </thead>\n" +
        "           <tbody id=\"body_question_" + num_question + "\">\n" +
        "               <tr>\n" +
        "                   <td colspan=\"3\">\n" +
        "                       <div class=\"input-group mb-3\">\n" +
        "                           <div class=\"input-group-prepend\">\n" +
        "                               <span class=\"input-group-text\" id=\"intitule-QCM-aria\">Intitulé de la question :</span>\n" +
        "                           </div>\n" +
        "                           <input type=\"text\" name=\"question_" + num_question + "\" aria-label=\"intitule-QCM-aria\" aria-describedby=\"intitule-QCM-aria\" placeholder=\"Question à choix multiples\" class=\"form-control\" required/>\n" +
        "                       </div>\n" +
        "                    </td>\n" +
        "               </tr>\n" +
        "               <tr>\n" +
        "                   <td>\n" +
        "                       <div class=\"input-group mb-3\">\n" +
        "                           <div class=\"input-group-prepend\">\n" +
        "                               <span class=\"input-group-text\" id=\"seuil-aria\">Seuil de risque :</span>\n" +
        "                           </div>\n" +
        "                           <input type=\"number\" name=\"seuil_question_" + num_question + "\" size=\"3\" value=\"1\" aria-label=\"seuil-aria\" aria-describedby=\"seuil-aria\" class=\"form-control\" required/>\n" +
        "                       </div>\n" +
        "                   </td>\n" +
        "                   <td colspan=\"2\">\n" +
        "                       <div class=\"input-group mb-3\">\n" +
        "                           <div class=\"input-group-prepend\">\n" +
        "                               <span class=\"input-group-text\" id=\"variable-aria\">Variable de référence :</span>\n" +
        "                           </div>\n" +
        "                           <input type=\"text\" name=\"variable_question_" + num_question + "\" aria-label=\"variable-aria\" aria-describedby=\"variable-aria\" placeholder=\"Variable de référence\" class=\"form-control\"/>\n" +
        "                       </div>\n" +
        "                   </td>\n" +
        "               </tr>\n" +
        "               <tr>\n" +
        "                   <td colspan='3'>\n" +
        "                       <div class=\"input-group mb-3\">\n" +
        "                           <input type=\"button\" id=\"button_choix_question_" + num_question + "\" value=\"Ajouter un choix\" onclick=\"ajouter_question(" + num_question + ",'" + type + "')\" class=\"btn btn-success \"/>\n" +
        "                           <input type=\"button\" id=\"button_retirer_question_" + num_question + "\" value=\"Retirer un choix\" onclick=\"retirer_question(" + num_question + ")\" class=\"btn btn-danger\"/>\n" +
        "                       </div>\n" +
        "                   </td>\n" +
        "               </tr>\n" +
        "           </tbody>\n" +
        "       </table>\n" +
        "</div>"
}

//fonction pour arrondir a n chiffre apres la virgule
function roundDecimal(nombre, precision) {
    precision = precision || 2;
    let tmp = Math.pow(10, precision);
    return Math.round(nombre * tmp) / tmp;
}

function drawHighcharts() {
    let min = tab[100];
    let max = tab[101];
    let nbLignes = tab[102];
    let nbColonne = tab[103];
    let resultats = new Array();
    let tabXAxis = new Array();

    //puis on parcours le tableau
    for (let i = min; i < max + 1; i++) {
        tabXAxis.push(i);
        resultats.push(roundDecimal(tab[i] / (nbLignes*nbColonne) * 100, 2));
    }

    $('#graphique').highcharts({
        chart: {
            type: 'column'
        },
        title: {
            text: 'Situation du score de l\'histogramme de la population de référence'
        },
        xAxis: {
            categories: tabXAxis

        },
        yAxis: {
            min: 0,
            max: 35,

            title: {
                text: 'Pourcentage'
            },
            labels: {
                format: "{value} %"
            }

        },
        legend: {
            enabled: false
        },
        plotOptions: {
            series: {
                stacking: 'normal',
                dataLabels: {
                    color: '#000',
                    style: {fontWeight: 'bolder'},
                    enabled: true,
                    inside: true
                }
            }
        },
        credits: {
            enabled: false
        },
        tooltip: {
            shared: true
        },

        series: [{
            name: 'pourcentage',
            data: resultats
        }],


    })
}

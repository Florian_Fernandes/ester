let question

//fonction pour arrondir a n chiffre apres la virgule
function roundDecimal(nombre, precision) {
    precision = precision || 2;
    let tmp = Math.pow(10, precision);
    return Math.round(nombre * tmp) / tmp;
}

function drawHighcharts(scoreEmployee) {
    let min = tab[100];
    let max = tab[101];
    let nbColonnes = tab[102];
    let nbLignes = tab[103];
    let resultats = new Array();
    let tabXAxis = new Array();
    let index = undefined;
    let position = 0;

    for (let i = min; i < max + 1; i++) {
        if(i === scoreEmployee) {
            index = position
        }
        position++;
        tabXAxis.push(i);
        resultats.push(roundDecimal(tab[i] / (nbLignes*nbColonnes) * 100, 2));
    }

    $('#graphique').highcharts({
        colors: ['#0275d8'],
        chart: {
            type: 'column',
            events: {
                load: function() {
                    Highcharts.each(this.series[0].points, function(p) {
                        if (index != undefined && p.x == index) {
                            p.update({
                                color: 'green'
                            });
                        }
                    });
                }
            }
        },
        title: {
            text: 'Situation du score de l\'histogramme de la population de référence'
        },
        xAxis: {
            categories: tabXAxis

        },
        yAxis: {
            min: 0,
            max: 35,

            title: {
                text: 'Pourcentage'
            },
            labels: {
                format: "{value} %"
            }

        },
        legend: {
            enabled: false
        },
        plotOptions: {
            series: {
                stacking: 'normal',
                dataLabels: {
                    color: '#000',
                    style: {fontWeight: 'bolder'},
                    enabled: true,
                    inside: true
                }
            }
        },
        credits: {
            enabled: false
        },
        tooltip: {
            shared: true
        },

        series: [{
            name: 'pourcentage',
            data: resultats
        }]
    })
}

function drawChartForQuestion(positionQuestion) {
    if(question.variableRef !== "") {
        let chart = Highcharts.chart("graphQuestion_"+positionQuestion, {
            chart: {
                type: 'bar',
                height: 200
            },
            title: {
                text: "Comparaison des réponses à la population de références (variable utilisée : " + question.variableRef + ")"
            },
            yAxis: {
                min: 0,
                title: {
                    text: 'Pourcentage'
                },
                labels: {
                    format: "{value} %"
                }
            },
            plotOptions: {
                series: {
                    stacking: 'normal',
                    dataLabels: {
                        color: '#0275d8',
                        style: {
                            fontSize: '1.8em'
                        },
                        formatter: function() {
                            return "✔";
                        },
                        inside: true
                    }
                }
            },
            legend: {
                reversed: true
            }
        })

        let colors = tabColor(question.options.length)
        for(let i = question.options.length; i >= 1; i--) {
            let option = question.options.find(opt => opt.position === i)

            if(option.valeurVariableRef === question.seuil-1) {
                chart.addSeries({
                    name: "Seuil de risque",
                    data: [0.5],
                    color: 'black'
                })
            }

            chart.addSeries({
                name: option.intitule,
                data: [option.pourcentageVariableRef],
                dataLabels: {
                    enabled: (question.valeurVariableRefRespEmpl.includes(option.valeurVariableRef))
                },
                color: colors.pop()
            })
        }
    }
}

function tabColor(numberOfColors) {
    let maxColor = 15
    let allColors = [
        '#00fa00',
        '#59ef00',
        '#7be400',
        '#93d900',
        '#a7cc00',
        '#b8c000',
        '#c7b200',
        '#d4a400',
        '#df9600',
        '#e98600',
        '#f17500',
        '#f76300',
        '#fb4e00',
        '#fe3400',
        '#ff0000',
    ]
    let colors = []
    if (numberOfColors == 1) {
        colors.push(allColors[0])
        return colors
    }

    for(let i = 0; i < numberOfColors -1; i++){
        let index = Math.ceil((maxColor / numberOfColors) * i)
        colors.push(allColors[index])
    }

    colors.push(allColors[maxColor - 1])

    return colors
}
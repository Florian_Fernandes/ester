/**
 * fonction js (JQuery) qui permet d'afficher le nom du fichier lors de son select
 * */
$(document).ready(function () {
    $('input[type="file"]').change(function (e) {
        let fileName = e.target.files[0].name;
        console.log(fileName)
        $("#label_csv_file").empty().append(fileName);
    });
});
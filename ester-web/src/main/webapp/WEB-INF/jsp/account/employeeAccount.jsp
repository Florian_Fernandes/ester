<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ page import="fr.univangers.masterinfo.ester.web.servlet.account.EmployeeAccountServlet" %>
<%@ page import="fr.univangers.masterinfo.ester.service.form.account.EmployeeAccountForm" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib tagdir="/WEB-INF/tags" prefix="ester" %>
<!DOCTYPE html>
<html lang="fr-FR" class="min-vh-100">
<head>
    <title>ESTER - Information sur le salarié</title>
    <c:import url="/WEB-INF/jsp/librairies/header.jsp"/>
    <link rel="stylesheet" href="<c:url value="/public/css/account/employeeAccount.css"/>">
    <script src="<c:url value="/public/js/account/employeeAccount.js"/>"></script>
</head>
<body class="d-flex flex-column min-vh-100">
<c:import url="/WEB-INF/jsp/librairies/navbar.jsp"/>
<main>
    <div class="container-fluid min-vh-100">
        <div class="row menu_gauche">
            <div class="col-md-8 container mt-3 m_shadow" style="background-color: white" id="container">
                <div class="card-body center_block">

                    <div class="row col-md-12 d-flex justify-content-center mt-xl-2">
                        <h2 style="border-bottom: solid 2px #127ba3" classQuestionnaryServiceImpl.="mb-lg-5">
                            Informations du compte salarié <c:out value="${employeeAccountForm.login}"/>
                        </h2>
                    </div>

                    <form id="parametresEmploye"  method="post" class="container-fluid" align="left"
                          action="<c:url value="${EmployeeAccountServlet.EMPLOYEE_ACCOUNT_URL}"/>">

                        <span id="sexe" hidden><c:out value="${employeeAccountForm.sexe.getName()}"/></span>
                        <span id="annee" hidden><c:out value="${employeeAccountForm.birthYear}"/></span>
                        <span id="pcs" hidden><c:out value="${employeeAccountForm.selectedPCS}"/></span>
                        <span id="naf" hidden><c:out value="${employeeAccountForm.selectedNAF}"/></span>


                        <input type="text"
                               name="${EmployeeAccountForm.INPUT_LOGIN}"
                               value="${employeeAccountForm.login}"
                               class="form-control" hidden/>

                        <!-- Radio group pour le sexe -->
                        <div class="row" id="sexeDiv" style="display: block">
                            <div class="custom-control mr-2 custom-radio">
                                <input class="custom-control-input"
                                       type="radio"
                                       id="homme"
                                       value="HOMME"
                                       name="sexe"
                                       disabled/>
                                <label class="custom-control-label"
                                       for="homme">
                                    Homme
                                </label>
                            </div>

                            <div class="custom-control mr-2 custom-radio">
                                <input class="custom-control-input"
                                       type="radio"
                                       id="femme"
                                       value="FEMME"
                                       name="sexe"
                                       disabled/>
                                <label class="custom-control-label"
                                       for="femme">
                                    Femme
                                </label>
                            </div>

                            <div class="custom-control mr-2 custom-radio">
                                <input class="custom-control-input"
                                       type="radio"
                                       id="autre"
                                       value="AUTRE"
                                       name="sexe"
                                       disabled/>
                                <label class="custom-control-label"
                                       for="autre">
                                    Autre
                                </label>
                            </div>
                        </div>

                        <!-- année de naissance -->
                        <div class="row" id="anneeDiv" style="display: block">
                            <div class="input-group mb-3">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" id="year-aria">Année de naissance</span>
                                </div>
                                <select id="annee_naissance"
                                        aria-label="year-aria"
                                        aria-describedby="year-aria"
                                        name="${EmployeeAccountForm.INPUT_BIRTH_YEAR}"
                                        class="custom-select"
                                        disabled>
                                </select>
                            </div>
                        </div>


                        <!-- PCS -->
                        <div class="row" id="PCSDiv" style="display: block">
                            <div class="input-group mb-3">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" id="pcs-aria">PCS</span>
                                </div>
                                <select id="PCS_metier"
                                        aria-label="pcs-aria"
                                        aria-describedby="pcs-aria"
                                        name="${EmployeeAccountForm.INPUT_SELECTED_PCS}"
                                        class="custom-select"
                                        disabled>
                                </select>
                            </div>
                        </div>


                        <!-- NAF -->
                        <div class="row" id="NAFDiv" style="display: block">
                            <div class="input-group mb-3">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" id="naf-aria">NAF</span>
                                </div>
                                <select id="NAF_metier"
                                        aria-label="naf-aria"
                                        aria-describedby="naf-aria"
                                        name="${EmployeeAccountForm.INPUT_SELECTED_NAF}"
                                        class="custom-select"
                                        disabled>
                                </select>
                            </div>
                        </div>


                        <!-- commentaire -->
                        <div class="row" id="CommentairesDiv" style="display: block">
                            <div class="input-group mb-3">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" id="comment-aria">Commentaire</span>
                                </div>

                                <textarea
                                        cols = "50"
                                        aria-label="comment-aria"
                                        aria-describedby="comment-aria"
                                        name="${EmployeeAccountForm.INPUT_COMMENTAIRE}"
                                        id="commentaire"
                                        class="form-control"
                                        disabled><c:out value="${employeeAccountForm.commentaire}"/></textarea>
                            </div>
                        </div>


                        <div class="row">
                            <input type="button"
                                   class="btn btn-md btn-primary"
                                   value="Modifier"
                                   id="modify"
                                   onclick="modifyAccount()">
                        </div>

                        <div class="row">
                            <input type="button"
                                   class="btn btn-success"
                                   value="Confirmer"
                                   id="submitBtn"
                                   data-toggle="modal"
                                   data-target="#confirm-submit"
                                   hidden>
                        </div>

                        <div class="row">
                            <input type="button"
                                   class="btn btn-danger"
                                   value="Annuler"
                                   id="cancel"
                                   hidden
                                   onclick="cancelModifications()">
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
</main>

<div class="modal fade" id="confirm-submit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <p>
                    Etes-vous-sûr de vouloir modifier le compte <c:out value="${employeeAccountForm.login}"/> ?
                </p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Annuler</button>
                <a id="submit-modal" class="btn btn-success" onclick="submitModal()">Modifier</a>
            </div>
        </div>
    </div>
</div>

<c:import url="/WEB-INF/jsp/librairies/footer.jsp"/>
</body>
</html>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>

<%@ page import="fr.univangers.masterinfo.ester.service.form.data.EmployeeDataForm" %>
<%@ page import="fr.univangers.masterinfo.ester.web.servlet.account.EmployeeDataServlet" %>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib tagdir="/WEB-INF/tags" prefix="ester" %>

<!DOCTYPE html>
<html lang="fr-FR" class="min-vh-100">
<head>

    <title>ESTER - Page de visualisation des données salariées</title>

    <c:import url="/WEB-INF/jsp/librairies/header.jsp"/>
</head>

<body class="d-flex flex-column min-vh-100">

<c:import url="/WEB-INF/jsp/librairies/navbar.jsp"/>

<main>
    <div class="container-fluid min-vh-100">
        <div class="row menu_gauche">
            <div class="col-md-8 container mt-3 m_shadow"
                 style="background-color: white" id="container">
                <div class="card-body center_block ">
                    <div class="row col-md-12 d-flex justify-content-center mt-xl-2">
                        <h2 style="border-bottom: solid 2px #127ba3" class="mb-lg-5">
                            Liste des salariés
                        </h2>
                    </div>
                    <table class="table table-striped table-hover ">

                        <c:forEach items="${employees}" var="e">
                            <tr>
                                <td>
                                    <p>${e.login}</p>
                                </td>
                                <td>
                                    <form method="get" action="compte-employe">
                                        <input type="hidden" value="${e.login}" name="login"/>
                                        <input class="float-right btn btn-primary btn" type="submit"
                                               value="Afficher le profil"/>
                                    </form>
                                </td>
                            </tr>
                        </c:forEach>
                    </table>
                    </select>
                </div>
            </div>
        </div>
    </div>
</main>

<c:import url="/WEB-INF/jsp/librairies/footer.jsp"/>

</body>
</html>
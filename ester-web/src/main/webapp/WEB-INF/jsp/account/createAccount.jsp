<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ page import="fr.univangers.masterinfo.ester.web.servlet.account.CreateAccountServlet" %>
<%@ page import="fr.univangers.masterinfo.ester.service.form.account.CreateAccountForm" %>
<%@ page import="fr.univangers.masterinfo.ester.dao.bean.UserRole" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib tagdir="/WEB-INF/tags" prefix="ester" %>
<!DOCTYPE html>
<html lang="fr-FR" class="min-vh-100">
<head>
    <title>ESTER - Créer un compte</title>
    <c:import url="/WEB-INF/jsp/librairies/header.jsp"/>
    <style><c:import url="/public/css/account/createAccount.css"/></style>
    <script src="<c:url value="/public/js/account/createAccount.js"/>"></script>
</head>
<body class="d-flex flex-column min-vh-100">
<c:import url="/WEB-INF/jsp/librairies/navbar.jsp"/>
<main>
    <div class="container-fluid min-vh-100">
        <div class="row menu_gauche">
            <div class="col-md-8 container mt-3 m_shadow" style="background-color: white" id="container">
                <ester:notifications messages="${createAccountForm.notifications}"/>
                <div class="card-body center_block">

                    <div class="row col-md-12 d-flex justify-content-center mt-xl-2">
                        <h2 style="border-bottom: solid 2px #127ba3" class="mb-lg-5">
                            Création d'un compte
                        </h2>
                    </div>

                    <form action="<c:url value="${CreateAccountServlet.CREATE_ACCOUNT_URL}"/>"
                          class="container-fluid"
                          method="post">

                        <!-- Selection du compte -->
                        <div class="row">
                            <div class="input-group mb-3">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" id="type-aria">Type de compte </span>
                                </div>
                                <select id="Compte_Select"
                                        aria-label="type-aria"
                                        aria-describedby="type-aria"
                                        name="${CreateAccountForm.SELECT_ROLE_ACCOUNT}"
                                        class="custom-select"
                                        onchange="dynamiqueRoles('${UserRole.SALARIE}')">

                                    <c:forEach items="${createAccountForm.roles}" var="role">
                                        <option value="${role.name()}">${role.name}</option>
                                    </c:forEach>
                                </select>
                            </div>
                        </div>

                        <p id="salarie" disabled hidden><c:out value="${UserRole.SALARIE}"/></p>

                        <!-- Email -->
                        <div class="row" id="emailDiv">
                            <div class="input-group mb-3">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" id="email-aria">Email</span>
                                </div>

                                <input type="text"
                                       aria-label="email-aria"
                                       aria-describedby="email-aria"
                                       name="${CreateAccountForm.INPUT_MAIL_ACCOUNT}"
                                       id="Compte_Email"
                                       placeholder="nom@exemple.com"
                                       class="form-control" required/>
                            </div>
                        </div>


                        <!-- nom -->
                        <div class="row" id="familyNameDiv">
                            <div class="input-group mb-3">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" id="family-name-aria">Nom</span>
                                </div>
                                <input type="text"
                                       aria-label="family-name-aria"
                                       aria-describedby="family-name-aria"
                                       name="${CreateAccountForm.INPUT_FAMILY_NAME_ACCOUNT}"
                                       id="Compte_familyName"
                                       placeholder="Nom"
                                       class="form-control" required/>
                            </div>
                        </div>


                        <!-- prenom -->
                        <div class="row" id="nameDiv">
                            <div class="input-group mb-3">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" id="name-aria">Prénom</span>
                                </div>
                                <input type="text"
                                       aria-label="name-aria"
                                       aria-describedby="name-aria"
                                       name="${CreateAccountForm.INPUT_NAME_ACCOUNT}"
                                       id="Compte_name"
                                       placeholder="Prénom"
                                       class="form-control" required/>
                            </div>
                        </div>


                        <!-- partie du formulaire ne concernant que les salariés display = none par default-->

                        <!-- Radio group pour le choix du sexe -->
                        <div class="row" id="sexeDiv" style="display: none">
                            <div class="custom-control mr-2 custom-radio">
                                <input class="custom-control-input"
                                       type="radio"
                                       id="homme"
                                       value="homme"
                                       name="${CreateAccountForm.INPUT_SEXE_ACCOUNT}"/>
                                <label class="custom-control-label"
                                       for="homme">
                                    Homme
                                </label>
                            </div>

                            <div class="custom-control mr-2 custom-radio">
                                <input class="custom-control-input"
                                       type="radio"
                                       id="femme"
                                       value="femme"
                                       name="${CreateAccountForm.INPUT_SEXE_ACCOUNT}"/>
                                <label class="custom-control-label"
                                       for="femme">
                                    Femme
                                </label>
                            </div>

                            <div class="custom-control mr-2 custom-radio">
                                <input class="custom-control-input"
                                       type="radio"
                                       id="autre"
                                       value="autre"
                                       name="${CreateAccountForm.INPUT_SEXE_ACCOUNT}" *
                                       checked/>
                                <label class="custom-control-label"
                                       for="autre">
                                    Autre
                                </label>
                            </div>
                        </div>

                        <!-- choix de l'année de naissance -->
                        <div class="row" id="anneeDiv" style="display: none">
                            <div class="input-group mb-3">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" id="year-aria">Année de naissance</span>
                                </div>
                                <select id="annee_naissance"
                                        aria-label="year-aria"
                                        aria-describedby="year-aria"
                                        name="${CreateAccountForm.INPUT_BIRTH_ACCOUNT}"
                                        class="custom-select">

                                    <option value="">Choisissez une année de naissance</option>
                                </select>
                            </div>
                        </div>


                        <!-- choix du PCS -->
                        <div class="row" id="PCSDiv" style="display: none">
                            <div class="input-group mb-3">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" id="pcs-aria">PCS</span>
                                </div>
                                <select id="PCS_metier"
                                        aria-label="pcs-aria"
                                        aria-describedby="pcs-aria"
                                        name="${CreateAccountForm.INPUT_PCS_ACCOUNT}"
                                        class="custom-select">

                                    <option value="">--Choisissez une PCS--</option>
                                </select>
                            </div>
                        </div>


                        <!-- choix du NAF -->
                        <div class="row" id="NAFDiv" style="display: none">
                            <div class="input-group mb-3">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" id="naf-aria">NAF</span>
                                </div>
                                <select id="NAF_metier"
                                        aria-label="naf-aria"
                                        aria-describedby="naf-aria"
                                        name="${CreateAccountForm.INPUT_NAF_ACCOUNT}"
                                        class="custom-select">

                                    <option value="">--Choisissez une NAF--</option>
                                </select>
                            </div>
                        </div>


                        <!-- commentaire -->
                        <div class="row" id="CommentaireDiv" style="display: none">
                            <div class="input-group mb-3">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" id="comment-aria">Commentaire</span>
                                </div>

                                <textarea
                                       cols = "50"
                                       aria-label="comment-aria"
                                       aria-describedby="comment-aria"
                                       name="${CreateAccountForm.INPUT_COMMENTAIRE_ACCOUNT}"
                                       id="commentaire"
                                       class="form-control"></textarea>
                            </div>
                        </div>

                        <!-- fin de la partie du formulaire pour les salariés -->

                        <div class="row mt-lg-5">
                            <input type="submit"
                                   class="btn btn-md btn-primary"
                                   value="Valider"
                                   id="submit">
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</main>
<c:import url="/WEB-INF/jsp/librairies/footer.jsp"/>
</body>
</html>
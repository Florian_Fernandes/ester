<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ page import="fr.univangers.masterinfo.ester.web.servlet.account.ModifyAccountServlet" %>
<%@ page import="fr.univangers.masterinfo.ester.service.form.account.ModifyAccountForm" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib tagdir="/WEB-INF/tags" prefix="ester" %>
<!DOCTYPE html>
<html lang="fr-FR" class="min-vh-100">
<head>
    <title>ESTER - Modifier un compte</title>
    <c:import url="/WEB-INF/jsp/librairies/header.jsp"/>
    <link rel="stylesheet" href="<c:url value="/public/css/account/modifyAccount.css"/>">
    <script src="<c:url value="/public/js/account/modifyAccount.js"/>"></script>
</head>
<body class="d-flex flex-column min-vh-100">
<c:import url="/WEB-INF/jsp/librairies/navbar.jsp"/>


<%-- code qui affiche un message d'erreur : une notification rouge quand le mot de passe n'est pas identique à celui de la BD et vert sinon  --%>

<c:choose>
    <c:when test="${MessageError == true }">
        <div class="alert alert-success alert-dismissible fade show" id="id_alert_succes" role="alert">
            <strong>Success </strong> données modifiées avec succes
            <button type="button" class="btn close" data-bs-dismiss="alert" aria-label="close"></button>
        </div>
        <script> $("#id_alert_succes").fadeOut(3000)</script>
    </c:when>
    <c:when test="${MessageError == false}">
        <div class="alert alert-danger alert-dismissible fade show" id="id_alert_error" role="alert">
            <strong>Error!</strong> pas le meme mot de passe
            <button type="button" class="btn close" data-bs-dismiss="alert" aria-label="close"></button>
        </div>
        <script> $("#id_alert_error").fadeOut(3000)</script>
    </c:when>
</c:choose>

<div class="alert alert-danger alert-dismissible show" id="not_same_password" role="alert" style="display: none">
    <strong>Error!</strong> mots de passes non identiques
    <button type="button" class="btn close" data-bs-dismiss="alert" aria-label="close"></button>
</div>

<main>
    <div class="container-fluid min-vh-100">
        <div class="row menu_gauche">
            <div class="col-md-8 container mt-3 m_shadow" style="background-color: white" id="container">
                <div class="card-body center_block">

                    <div class="row col-md-12 d-flex justify-content-center mt-xl-2">
                        <h2 style="border-bottom: solid 2px #127ba3" class="mb-lg-5">
                            Modification des paramètres de compte
                        </h2>
                    </div>
                    <form id="parametresConnexion" class="container-fluid" method="post">
                        <div class="row">
                            <div class="input-group mb-3">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" id="Identifiant-aria">Identifiant</span>
                                </div>
                                <input
                                        type="text"
                                        id="Identifiant"
                                        aria-label="Identifiant-aria"
                                        aria-describedby="Identifiant-aria"
                                        name="${ModifyAccountForm.INPUT_LOGIN}"
                                        value="${modifyAccountForm.login}"
                                        class="form-control" required>
                            </div>
                        </div>

                        <div class="row">
                            <div class="input-group mb-3">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" id="Email-aria">Email</span>
                                </div>
                                <input
                                        type="email"
                                        id="Email"
                                        aria-label="Email-aria"
                                        aria-describedby="Email-aria"
                                        name="${ModifyAccountForm.INPUT_MAIL}"
                                        value="${modifyAccountForm.mail}"
                                        class="form-control" required>
                            </div>
                        </div>

                        <div class="row">
                            <div class="input-group mb-3">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" id="Mdp-aria">Mot de passe actuel</span>
                                </div>
                                <input
                                        type="password"
                                        id="Mdp"
                                        aria-label="Mdp-aria"
                                        aria-describedby="Mdp-aria"
                                        name="${ModifyAccountForm.INPUT_ACTUAL_PASSWORD}"
                                        value="${modifyAccountForm.actualPassword}"
                                        class="form-control" required>
                            </div>
                        </div>

                        <div class="row">
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" id="NewPassword-aria">Nouveau mot de passe</span>
                                </div>
                                <input
                                        type="password"
                                        id="NewPassword"
                                        aria-label="NewPassword-aria"
                                        aria-describedby="NewPassword-aria"
                                        name="${ModifyAccountForm.INPUT_NEW_PASSWORD}"
                                        value="${modifyAccountForm.newPassword}"
                                        class="form-control" required>

                                <input
                                        type="password"
                                        id="NewPasswordConfirm"
                                        aria-label="NewPassword-aria"
                                        aria-describedby="NewPassword-aria"
                                        name="${ModifyAccountForm.INPUT_CONFIRM_PASSWORD}"
                                        value="${modifyAccountForm.confirmPassword}"
                                        class="form-control shadow-danger"
                                        onkeyup="checkSamePassword()"
                                        onfocusout="focusOutPasswordShowError()"
                                        required>
                            </div>
                        </div>

                        <div class="row mt-lg-5">
                            <input type="submit"
                                   class="btn btn-md btn-primary"
                                   value="Valider"
                                   id="submit" required>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</main>
<c:import url="/WEB-INF/jsp/librairies/footer.jsp"/>
</body>
</html>
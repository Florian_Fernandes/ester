<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib tagdir="/WEB-INF/tags" prefix="ester" %>

<!DOCTYPE html>
<html lang="fr-FR" class="min-vh-100">
<head>

    <title>ESTER - Modifier un autre compte</title>

    <c:import url="/WEB-INF/jsp/librairies/header.jsp"/>
    <link rel="stylesheet" href="<c:url value="/public/css/account/modifyOthersAccount.css"/>">
    <script src="<c:url value="/public/js/account/modifyOthersAccount.js"/>"></script>

</head>

<body class="d-flex flex-column min-vh-100">

<c:import url="/WEB-INF/jsp/librairies/navbar.jsp"/>

<main>
    <div class="container-fluid min-vh-100">
        <div class="row menu_gauche">
            <div class="col-md-8 container mt-3 m_shadow" style="background-color: white" id="container">
                <div class="card-body center_block">

                    <p>Modification d'un autre compte</p>

                    <!-- TODO -->

                </div>
            </div>
        </div>
    </div>
</main>

<c:import url="/WEB-INF/jsp/librairies/footer.jsp"/>

</body>
</html>
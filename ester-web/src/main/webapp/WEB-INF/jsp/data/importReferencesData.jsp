<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ page import="fr.univangers.masterinfo.ester.web.servlet.data.ImportReferencesDataServlet" %>
<%@ page import="fr.univangers.masterinfo.ester.service.form.data.ImportReferencesDataForm" %>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib tagdir="/WEB-INF/tags" prefix="ester" %>

<!DOCTYPE html>
<html lang="fr-FR" class="min-vh-100">
<head>

    <title>ESTER - Import des données de références</title>

    <c:import url="/WEB-INF/jsp/librairies/header.jsp"/>
    <link rel="stylesheet" href="<c:url value="/public/css/data/importReferencesData.css"/>">
    <script src="<c:url value="/public/js/data/importReferencesData.js"/>"></script>

</head>

<body class="d-flex flex-column min-vh-100">

<c:import url="/WEB-INF/jsp/librairies/navbar.jsp"/>

<main>
    <div class="container-fluid min-vh-100">
        <div class="row menu_gauche">
            <div class="col-md-8 container mt-3 m_shadow" style="background-color: white">
                <div class="card-body center_block">
                    <ester:notifications messages="${importReferencesDataForm.notifications}"/>

                    <div class="row col-md-12 d-flex justify-content-center mt-xl-2">
                        <h2 style="border-bottom: solid 2px #127ba3" class="mb-lg-5">
                            Importer des données de références
                        </h2>
                    </div>

                    <div class="row m-2" id="container">
                        <div class="col justify-content-center">
                            <div class="row">
                                <div class="col">
                                    <form action="<c:url value="${ImportReferencesDataServlet.IMPORT_REFERENCES_DATA_URL}"/>"
                                          method="post" enctype="multipart/form-data">

                                        <div class="row">
                                            <div class="input-group mb-3">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text"
                                                          id="base-name-aria">Nom de la base</span>
                                                </div>

                                                <input type="text"
                                                       aria-label="base-name-aria"
                                                       aria-describedby="base-name-aria"
                                                       name="${ImportReferencesDataForm.INPUT_DATABASE_NAME}"
                                                       id="Nom_BDD"
                                                       class="form-control"/>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="input-group mb-3">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text"
                                                          id="base-varibles-aria">Variables du score séparées par des points virgules</span>
                                                </div>

                                                <input type="text"
                                                       aria-label="base-varibles-aria"
                                                       aria-describedby="base-varibles-aria"
                                                       name="${ImportReferencesDataForm.INPUT_DATABASE_VARIABLES}"
                                                       class="form-control"
                                                       placeholder="R3AQ48;R3AQ49"/>
                                            </div>
                                        </div>


                                        <div class="form-group row" id="fileImportDiv">
                                            <div class="custom-file">

                                                <input id="fileImportInput"
                                                       name="${ImportReferencesDataForm.INPUT_FILE}" type="file"
                                                       class="custom-file-input" lang="fr" accept=".csv">
                                                <%--  Rajout d'un ID pour l'utilisation du JS dans importReferenceData.js --%>
                                                <label for="fileImportInput" class="custom-file-label"
                                                       id="label_csv_file">
                                                    Sélectionner un fichier à importer
                                                </label>
                                            </div>
                                        </div>

                                        <div class="row mt-lg-5">
                                            <input type="submit"
                                                   class="btn btn-md btn-primary"
                                                   value="Importer"
                                                   id="submit">
                                        </div>

                                    </form>
                                </div>
                            </div>
                        </div>

                    </div>

                </div>
            </div>
        </div>
    </div>
</main>

<c:import url="/WEB-INF/jsp/librairies/footer.jsp"/>

</body>
</html>

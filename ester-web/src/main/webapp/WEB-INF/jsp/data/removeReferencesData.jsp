<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ page import="fr.univangers.masterinfo.ester.web.servlet.data.RemoveReferencesDataServlet" %>
<%@ page import="fr.univangers.masterinfo.ester.service.form.data.RemoveReferencesDataForm" %>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib tagdir="/WEB-INF/tags" prefix="ester" %>

<!DOCTYPE html>
<html lang="fr-FR" class="min-vh-100">
<head>

    <title>ESTER - Suppression des données de références</title>

    <c:import url="/WEB-INF/jsp/librairies/header.jsp"/>
    <link rel="stylesheet" href="<c:url value="/public/css/data/removeReferencesData.css"/>">
    <script src="<c:url value="/public/js/data/removeReferencesData.js"/>"></script>

</head>

<body class="d-flex flex-column min-vh-100">

<c:import url="/WEB-INF/jsp/librairies/navbar.jsp"/>

<main>
    <div class="container-fluid min-vh-100">
        <div class="row menu_gauche">
            <div class="col-md-8 container mt-3 m_shadow" style="background-color: white" id="container">
                <div class="card-body center_block">
                    <ester:notifications messages="${removeReferencesDataForm.notifications}"/>

                    <div class="row col-md-12 d-flex justify-content-center mt-xl-2">
                        <h2 style="border-bottom: solid 2px #127ba3" class="mb-lg-5">
                            Suppression des données de références
                        </h2>
                    </div>

                    <form action="<c:url value="${RemoveReferencesDataServlet.REMOVE_REFERENCES_DATA_URL}"/>"
                          method="post" enctype="multipart/form-data">


                        <div class="row">
                            <div class="input-group mb-3">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" id="delete-aria">Base de référence à supprimer</span>
                                </div>
                                <select id="selectReference"
                                        aria-label="delete-aria"
                                        aria-describedby="delete-aria"
                                        name="${RemoveReferencesDataForm.SELECT_REFERENCE}"
                                        class="custom-select">

                                    <c:forEach items="${removeReferencesDataForm.references}" var="r">
                                        <option value="${r.id}">${r.name}</option>
                                    </c:forEach>
                                </select>
                            </div>
                        </div>

                        <div class="row mt-lg-5">
                            <input type="submit"
                                   class="btn btn-md btn-primary"
                                   value="Supprimer"
                                   id="submit">
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
</main>

<c:import url="/WEB-INF/jsp/librairies/footer.jsp"/>

</body>
</html>

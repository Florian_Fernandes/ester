<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ page import="fr.univangers.masterinfo.ester.service.form.questionnary.ShareQuestionnaryForm" %>
<%@ page import="fr.univangers.masterinfo.ester.web.servlet.questionnary.ShareQuestionnaryServlet" %>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib tagdir="/WEB-INF/tags" prefix="ester" %>

<!DOCTYPE html>
<html lang="fr-FR" class="min-vh-100">
<head>

    <title>ESTER - Administration questionnaires</title>

    <c:import url="/WEB-INF/jsp/librairies/header.jsp"/>
    <link rel="stylesheet" href="<c:url value="/public/css/questionnary/shareQuestionnary.css"/>">
    <script src="<c:url value="/public/js/questionnary/shareQuestionnary.js"/>"></script>

</head>

<body class="d-flex flex-column min-vh-100">

<c:import url="/WEB-INF/jsp/librairies/navbar.jsp"/>

<main>
    <div class="container-fluid min-vh-100">
        <div class="row menu_gauche">
            <div class="col-md-8 container mt-3 m_shadow" style="background-color: white">
                <div class="card-body center_block">

                    <div class="row col-md-12 d-flex justify-content-center mt-xl-2">
                        <h2 style="border-bottom: solid 2px #127ba3" class="mb-lg-5">
                            Affecter des droits d'administration aux membres Ester
                        </h2>
                    </div>

                    <form action="<c:url value="${ShareQuestionnaryServlet.SHARE_QUESTIONNARY_URL}"/>"
                          class="container-fluid"
                          method="post">

                        <div class="row">
                            <div class="input-group mb-3">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" id="member-aria">Membre Ester</span>
                                </div>
                                <select id="NAF_metier"
                                        aria-label="member-aria"
                                        aria-describedby="member-aria"
                                        name="${ShareQuestionnaryForm.MEMBER_ESTER_SELECT}"
                                        class="custom-select">

                                    <c:forEach items="${shareQuestionnaryForm.membersEster}" var="memberEster">
                                        <option value="${memberEster.id}">${memberEster.login}</option>
                                    </c:forEach>
                                </select>
                            </div>
                        </div>

                        <div class="row">
                            <div class="input-group mb-3">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" id="Questionnaire-aria">Questionnaire</span>
                                </div>
                                <select id="PCS_metier"
                                        aria-label="Questionnaire-aria"
                                        aria-describedby="Questionnaire-aria"
                                        name="${ShareQuestionnaryForm.QUESTIONNARY_SELECT}"
                                        class="custom-select">

                                    <c:forEach items="${shareQuestionnaryForm.questionnaries}" var="questionnary">
                                        <option value="${questionnary.id}">${questionnary.name}</option>
                                    </c:forEach>
                                </select>
                            </div>
                        </div>

                        <div class="row mt-lg-5">
                            <input type="submit"
                                   class="btn btn-md btn-primary"
                                   value="Partager"
                                   id="load">
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</main>

<c:import url="/WEB-INF/jsp/librairies/footer.jsp"/>

</body>
</html>

<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib tagdir="/WEB-INF/tags" prefix="ester" %>


<!DOCTYPE html>
<html lang="fr-FR" class="min-vh-100">
<head>

    <title>ESTER - Création de questionnaires</title>

    <c:import url="/WEB-INF/jsp/librairies/header.jsp"/>
    <link rel="stylesheet" href="<c:url value="/public/css/questionnary/createQuestionnary.css"/>">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <script src="<c:url value="/public/js/questionnary/createQuestionnary.js"/>" defer></script>

</head>

<body class="d-flex flex-column min-vh-100">
<c:import url="/WEB-INF/jsp/librairies/navbar.jsp"/>
<main>
    <div class="container-fluid min-vh-100">
        <div class="row menu_gauche">
            <div class="col-md-8 container mt-3 m_shadow"
                 style="background-color: white" id="container">
                <div class="card-body center_block">

                    <div class="row col-md-12 d-flex justify-content-center mt-xl-2">
                        <h2 style="border-bottom: solid 2px #127ba3" class="mb-lg-5">
                            Créer vos questionnaires
                        </h2>
                    </div>

                    <form action="configuration-questionnaires" method="post">

                        <div class="questionnaryTitle">

                            <!-- Champ texte pour le nom du questionnaire -->

                            <div class="input-group mb-3">

                                <input type="hidden" name="action" value="${action}"/>

                                <div class="input-group-prepend">
                                    <span class="input-group-text" id="name-aria">Nom du questionnaire :</span>
                                </div>

                                <c:choose>
                                    <c:when test="${questionnaire.name != null && action == \"modify\"}">
                                        <input type="text" id="nom_questionnaire"
                                               value="<c:out value="${questionnaire.getName()}"/>"
                                               aria-label="name-aria" aria-describedby="name-aria"
                                               placeholder="Nom du questionnaire" class="form-control" disabled/>
                                        <input type="hidden" name="nom_questionnaire"
                                               value="<c:out value="${questionnaire.getName()}"/>"/>
                                    </c:when>

                                    <c:when test="${questionnaire.name != null && action == \"duplicate\"}">
                                        <input type="text" id="nom_questionnaire" name="nom_questionnaire"
                                               value="<c:out value="${questionnaire.getName()}"/>_Copie"
                                               aria-label="name-aria" aria-describedby="name-aria"
                                               placeholder="Nom du questionnaire" class="form-control" required/>
                                    </c:when>

                                    <c:otherwise>
                                        <input type="text" id="nom_questionnaire" name="nom_questionnaire"
                                               aria-label="name-aria" aria-describedby="name-aria"
                                               placeholder="Nom du questionnaire" class="form-control" required/>
                                    </c:otherwise>
                                </c:choose>


                            </div>

                            <div class="input-group mb-3">

                                <select name="choix_reference" id="choix_reference" aria-label="type-aria"
                                        aria-describedby="type-aria" class="custom-select">
                                    <option value="">--Choisissez une base de référence--</option>

                                    <c:forEach items="${ references }" var="reference" varStatus="status">

                                        <c:choose>

                                            <c:when test="${reference.getId() == reference_questionnaire}">
                                                <option value="<c:out value="${ reference.getId() }" />" selected><c:out
                                                        value="${ reference.getName() }"/></option>
                                            </c:when>

                                            <c:otherwise>
                                                <option value="<c:out value="${ reference.getId() }" />"><c:out
                                                        value="${ reference.getName() }"/></option>
                                            </c:otherwise>

                                        </c:choose>

                                    </c:forEach>

                                </select>

                                <c:out value="${test}"/>

                            </div>

                        </div>

                        <!-- Liste déroulante pour ajouter une nouvelle question -->

                        <div class="questionCard">

                            <div class="input-group mb-3">

                                <select name="choix_question" id="choix_question" aria-label="type-aria"
                                        aria-describedby="type-aria" class="custom-select">
                                    <option value="">--Ajouter une nouvelle question--</option>
                                    <option value="question_courte">Question à réponse courte</option>
                                    <option value="question_longue">Question à réponse longue</option>
                                    <option value="question_radio">Question à choix multiples avec bouton radio (une
                                        réponse)
                                    </option>
                                    <option value="question_liste">Question à choix multiples avec liste déroulante (une
                                        réponse)
                                    </option>
                                    <option value="question_case">Question à choix multiples avec cases à cocher
                                        (plusieurs réponses)
                                    </option>
                                </select>

                                <input type="button" id="nouvelle_question" value="Ajouter"
                                       class="btn btn-md btn-primary"/>

                            </div>

                        </div>

                        <div id="questions">

                            <c:choose>

                                <c:when test="${questionnaire.getQuestions() != null}">
                                    <input id="nb_questions" type="hidden" name="nb_questions"
                                           value="<c:out value="${questionnaire.getQuestions().size()}"/>"/>
                                </c:when>

                                <c:otherwise>
                                    <input id="nb_questions" type="hidden" name="nb_questions" value="0"/>
                                </c:otherwise>

                            </c:choose>

                            <!-- Contient toutes les questions du questionnaire -->

                            <c:forEach var="i" begin="1" end="${questionnaire.getQuestions().size()}" step="1">

                                <c:set var="question" value="${questionnaire.getQuestion(i)}" scope="page"/>

                                <c:choose>

                                    <c:when test="${question.getType() == 'SHORTANSWER'}">

                                        <div id="question_<c:out value="${question.getPosition()}"/>"
                                             class="questionCard">

                                            <input type='hidden'
                                                   name="type_question_<c:out value="${question.getPosition()}"/>"
                                                   value="courte"/>

                                            <table>
                                                <thead>
                                                <tr>
                                                    <td colspan="2">
                                                        Question à réponse courte
                                                    </td>
                                                    <td class="td_right">
                                                        <button type="button"
                                                                id="up_<c:out value="${question.getPosition()}"/>"
                                                                onclick="move_button(<c:out
                                                                        value="${question.getPosition()}"/>,-1)"
                                                                class="btn btn-md btn-primary">
                                                            <i class="material-icons">keyboard_arrow_up</i>
                                                        </button>
                                                        <button type="button"
                                                                id="down_<c:out value="${question.getPosition()}"/>"
                                                                onclick="move_button(<c:out
                                                                        value="${question.getPosition()}"/>,1)"
                                                                class="btn btn-md btn-primary">
                                                            <i class="material-icons">keyboard_arrow_down</i>
                                                        </button>
                                                        <button type="button"
                                                                id="delete_<c:out value="${question.getPosition()}"/>"
                                                                onclick="delete_button(<c:out
                                                                        value="${question.getPosition()}"/>)"
                                                                class="btn btn-danger">
                                                            <i class="material-icons">delete</i>
                                                        </button>
                                                    </td>
                                                </tr>
                                                </thead>

                                                <tbody id="body_question_<c:out value="${question.getPosition()}"/>">
                                                <tr>
                                                    <td colspan="3">
                                                        <div class="input-group mb-3">
                                                            <div class="input-group-prepend">
                                                                <span class="input-group-text"
                                                                      id="intitule-courte-aria">Intitulé de la question :</span>
                                                            </div>
                                                            <input type="text"
                                                                   name="question_<c:out value="${question.getPosition()}"/>"
                                                                   value="<c:out value="${question.getName()}"/>"
                                                                   aria-label="intitule-courte-aria"
                                                                   aria-describedby="intitule-courte-aria"
                                                                   placeholder="Question courte" class="form-control"
                                                                   required/>
                                                        </div>
                                                    </td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </div>

                                    </c:when>

                                    <c:when test="${question.getType() == 'LONGANSWER'}">

                                        <div id="question_<c:out value="${question.getPosition()}"/>"
                                             class="questionCard">

                                            <input type="hidden"
                                                   name="type_question_<c:out value="${question.getPosition()}"/>"
                                                   value="longue"/>

                                            <table>
                                                <thead>
                                                <tr>
                                                    <td colspan="2">
                                                        Question à réponse longue
                                                    </td>
                                                    <td class="td_right">
                                                        <button type="button"
                                                                id="up_<c:out value="${question.getPosition()}"/>"
                                                                onclick="move_button(<c:out
                                                                        value="${question.getPosition()}"/>,-1)"
                                                                class="btn btn-md btn-primary">
                                                            <i class="material-icons">keyboard_arrow_up</i>
                                                        </button>
                                                        <button type="button"
                                                                id="down_<c:out value="${question.getPosition()}"/>"
                                                                onclick="move_button(<c:out
                                                                        value="${question.getPosition()}"/>,1)"
                                                                class="btn btn-md btn-primary">
                                                            <i class="material-icons">keyboard_arrow_down</i>
                                                        </button>
                                                        <button type="button"
                                                                id="delete_<c:out value="${question.getPosition()}"/>"
                                                                onclick="delete_button(<c:out
                                                                        value="${question.getPosition()}"/>)"
                                                                class="btn btn-danger">
                                                            <i class="material-icons">delete</i>
                                                        </button>
                                                    </td>
                                                </tr>
                                                </thead>

                                                <tbody id="body_question_<c:out value="${question.getPosition()}"/>">
                                                <tr>
                                                    <td colspan="3">
                                                        <div class="input-group mb-3">
                                                            <div class="input-group-prepend">
                                                                <span class="input-group-text"
                                                                      id="intitule-longue-aria">Intitulé de la question :</span>
                                                            </div>
                                                            <textarea
                                                                    name="question_<c:out value="${question.getPosition()}"/>"
                                                                    aria-label="intitule-longue-aria"
                                                                    aria-describedby="intitule-longue-aria"
                                                                    placeholder="Question longue" class="form-control"
                                                                    required><c:out
                                                                    value="${question.getName()}"/></textarea>
                                                        </div>
                                                    </td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </div>

                                    </c:when>

                                    <c:when test="${question.getType() == 'RADIO' || question.getType() == 'SELECT' || question.getType() == 'CHECKBOX'}">

                                        <div id="question_<c:out value="${question.getPosition()}"/>"
                                             class="questionCard">

                                            <input type='hidden'
                                                   name="type_question_<c:out value="${question.getPosition()}"/>"
                                                   value="<c:choose><c:when test="${question.getType() == 'RADIO'}">radio</c:when><c:when test="${question.getType() == 'SELECT'}">liste</c:when><c:when test="${question.getType() == 'CHECKBOX'}">case</c:when></c:choose>"/>
                                            <input type='hidden'
                                                   name="nb_sous_questions_<c:out value="${question.getPosition()}"/>"
                                                   value="<c:out value="${question.getQuestionOptions().size()}"/>"/>

                                            <table>
                                                <thead>
                                                <tr>
                                                    <td colspan="2">
                                                        <c:choose>
                                                            <c:when test="${question.getType() == 'RADIO'}">Question à choix multiples avec boutons radio</c:when>
                                                            <c:when test="${question.getType() == 'SELECT'}">Question à choix multiples avec liste déroulante</c:when>
                                                            <c:when test="${question.getType() == 'CHECKBOX'}">Question à choix multiples avec cases à cocher</c:when>
                                                        </c:choose>
                                                    </td>
                                                    <td class="td_right">
                                                        <button type="button"
                                                                id="up_<c:out value="${question.getPosition()}"/>"
                                                                onclick="move_button(<c:out
                                                                        value="${question.getPosition()}"/>,-1)"
                                                                class="btn btn-md btn-primary">
                                                            <i class="material-icons">keyboard_arrow_up</i>
                                                        </button>
                                                        <button type="button"
                                                                id="down_<c:out value="${question.getPosition()}"/>"
                                                                onclick="move_button(<c:out
                                                                        value="${question.getPosition()}"/>,1)"
                                                                class="btn btn-md btn-primary">
                                                            <i class="material-icons">keyboard_arrow_down</i>
                                                        </button>
                                                        <button type="button"
                                                                id="delete_<c:out value="${question.getPosition()}"/>"
                                                                onclick="delete_button(<c:out
                                                                        value="${question.getPosition()}"/>)"
                                                                class="btn btn-danger">
                                                            <i class="material-icons">delete</i>
                                                        </button>
                                                    </td>
                                                </tr>
                                                </thead>

                                                <tbody id="body_question_<c:out value="${question.getPosition()}"/>">
                                                <tr>
                                                    <td colspan="3">
                                                        <div class="input-group mb-3">
                                                            <div class="input-group-prepend">
                                                                <span class="input-group-text" id="intitule-QCM-aria">Intitulé de la question :</span>
                                                            </div>
                                                            <input type="text"
                                                                   name="question_<c:out value="${question.getPosition()}"/>"
                                                                   value="${question.getName()}"
                                                                   aria-label="intitule-QCM-aria"
                                                                   aria-describedby="intitule-QCM-aria"
                                                                   placeholder="Question à choix multiples"
                                                                   class="form-control" required/>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <div class="input-group mb-3">
                                                            <div class="input-group-prepend">
                                                                <span class="input-group-text" id="seuil-aria">Seuil de risque :</span>
                                                            </div>
                                                            <input type="number"
                                                                   name="seuil_question_<c:out value="${question.getPosition()}"/>"
                                                                   size="3" value="${question.getSeuil()}"
                                                                   aria-label="seuil-aria" aria-describedby="seuil-aria"
                                                                   class="form-control" required/>
                                                        </div>
                                                    </td>
                                                    <td colspan="2">
                                                        <div class="input-group mb-3">
                                                            <div class="input-group-prepend">
                                                                <span class="input-group-text" id="variable-aria">Variable de référence :</span>
                                                            </div>
                                                            <input type="text"
                                                                   name="variable_question_<c:out value="${question.getPosition()}"/>"
                                                                   value="${question.getVariable()}"
                                                                   aria-label="variable-aria"
                                                                   aria-describedby="variable-aria"
                                                                   class="form-control"/>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan='3'>
                                                        <div class="input-group mb-3">
                                                            <input type="button"
                                                                   id="button_choix_question_<c:out value="${question.getPosition()}"/>"
                                                                   value="Ajouter un choix"
                                                                   onclick="ajouter_question(<c:out
                                                                           value="${question.getPosition()}"/>,
                                                                   <c:choose>
                                                                   <c:when test="${question.getType() == 'RADIO'}">'radio'</c:when>
                                                                   <c:when test="${question.getType() == 'SELECT'}">'liste'</c:when>
                                                                   <c:when test="${question.getType() == 'CHECKBOX'}">'case'</c:when>
                                                                   </c:choose>)" class="btn btn-success "/>
                                                            <input type="button"
                                                                   id="button_retirer_question_<c:out value="${question.getPosition()}"/>"
                                                                   value="Retirer un choix"
                                                                   onclick="retirer_question(<c:out
                                                                           value="${question.getPosition()}"/>)"
                                                                   class="btn btn-danger"/>
                                                        </div>
                                                    </td>
                                                </tr>

                                                <c:forEach var="j" begin="1"
                                                           end="${question.getQuestionOptions().size()}" step="1">

                                                    <c:set var="sous_question" value="${question.getQuestionOption(j)}"
                                                           scope="page"/>

                                                    <tr>
                                                        <td colspan='3'>
                                                            <c:choose>
                                                                <c:when test="${question.getType() == 'RADIO'}">
                                                                    <div class="input-group mb-3">
                                                                        <div class="input-group-prepend">
                                                                            <span class="input-group-text"
                                                                                  id="radio-aria">
                                                                                <input type="radio" disabled/>
                                                                            </span>
                                                                        </div>
                                                                        <input type="text"
                                                                               name="sous_question_<c:out value="${sous_question.getPosition()}"/>_<c:out value="${question.getPosition()}"/>"
                                                                               size='50'
                                                                               value="${sous_question.getValue()}"
                                                                               aria-label="radio-aria"
                                                                               aria-describedby="radio-aria"
                                                                               class="form-control" required/>
                                                                    </div>
                                                                </c:when>
                                                                <c:when test="${question.getType() == 'SELECT'}">
                                                                    <div class="input-group mb-3">
                                                                        <div class="input-group-prepend">
                                                                            <span class="input-group-text"
                                                                                  id="select-aria">-</span>
                                                                        </div>
                                                                        <input type="text"
                                                                               name="sous_question_<c:out value="${sous_question.getPosition()}"/>_<c:out value="${question.getPosition()}"/>"
                                                                               size='50'
                                                                               value="${sous_question.getValue()}"
                                                                               aria-label="select-aria"
                                                                               aria-describedby="select-aria"
                                                                               class="form-control" required/>
                                                                    </div>
                                                                </c:when>
                                                                <c:when test="${question.getType() == 'CHECKBOX'}">
                                                                    <div class="input-group mb-3">
                                                                        <div class="input-group-prepend">
                                                                            <span class="input-group-text"
                                                                                  id="checkbox-aria">
                                                                                <input type="checkbox" disabled/>
                                                                            </span>
                                                                        </div>
                                                                        <input type="text"
                                                                               name="sous_question_<c:out value="${sous_question.getPosition()}"/>_<c:out value="${question.getPosition()}"/>"
                                                                               size='50'
                                                                               value="${sous_question.getValue()}"
                                                                               aria-label="checkbox-aria"
                                                                               aria-describedby="checkbox-aria"
                                                                               class="form-control" required/>
                                                                    </div>
                                                                </c:when>
                                                            </c:choose>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <div class="input-group mb-3">
                                                                <div class="input-group-prepend">
                                                                    <span class="input-group-text" id="score-aria">Score :</span>
                                                                </div>
                                                                <input type="number"
                                                                       name="score_sous_question_<c:out value="${sous_question.getPosition()}"/>_<c:out value="${question.getPosition()}"/>"
                                                                       size="3" value="${sous_question.getScore()}"
                                                                       aria-label="score-aria"
                                                                       aria-describedby="score-aria"
                                                                       class="form-control" required/>
                                                            </div>
                                                        </td>
                                                        <td>
                                                            <div class="input-group mb-3">
                                                                <div class="input-group-prepend">
                                                                    <span class="input-group-text" id="coeff-aria">Coefficient :</span>
                                                                </div>
                                                                <input type="number"
                                                                       name="coefficient_sous_question_<c:out value="${sous_question.getPosition()}"/>_<c:out value="${question.getPosition()}"/>"
                                                                       size="3" value="${sous_question.getCoeff()}"
                                                                       aria-label="coeff-aria"
                                                                       aria-describedby="coeff-aria"
                                                                       class="form-control" required/>
                                                            </div>
                                                        </td>
                                                        <td>
                                                            <div class="input-group mb-3">
                                                                <div class="input-group-prepend">
                                                                    <span class="input-group-text" id="valeur-aria">Valeur de la variable :</span>
                                                                </div>
                                                                <input type="number"
                                                                       name="valeur_variable_sous_question_<c:out value="${sous_question.getPosition()}"/>_<c:out value="${question.getPosition()}"/>"
                                                                       size="3" value="${sous_question.getValeur()}"
                                                                       aria-label="valeur-aria"
                                                                       aria-describedby="valeur-aria"
                                                                       class="form-control"/>
                                                            </div>
                                                        </td>
                                                    </tr>

                                                </c:forEach>
                                                </tbody>
                                            </table>
                                        </div>

                                    </c:when>

                                    <c:otherwise>
                                        <p>Type non trouvé : <c:out value="${question.getType()}"/></p>
                                    </c:otherwise>

                                </c:choose>
                            </c:forEach>

                        </div>

                        <div class="input-group mb-3">

                            <input type="submit" value="Confirmer" class="btn btn-md btn-primary"/>

                        </div>

                    </form>

                </div>
            </div>
        </div>
    </div>
</main>

<c:import url="/WEB-INF/jsp/librairies/footer.jsp"/>

</body>
</html>
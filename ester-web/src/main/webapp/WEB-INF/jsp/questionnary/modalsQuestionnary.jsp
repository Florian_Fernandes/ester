<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<link rel="stylesheet" href="<c:url value="/public/css/questionnary/modalsQuestionnary.css"/>">

<!-- Modal d'erreurs standard -->

<div class="modal" role="dialog" id="ErreurSaisie">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Erreur</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="bg-danger rounded text-light p-2" id="MsgErreur"></div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Fermer</button>
            </div>
        </div>
    </div>
</div>


<div class="modal" role="dialog" id="BDDSuccess">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Demande effectuée avec succès</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="bg-success rounded text-light p-2" id="MsgSuccess"></div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Fermer</button>
            </div>
        </div>
    </div>
</div>

<!-- Modal d'attente standard -->

<div class="modal" role="dialog" id="WaitedDemand">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Traitement de votre demande en cours</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p id="typeDemand"></p>
                <div class="row col-md-auto d-flex justify-content-center">
                    <div class="inProgressGIF">
                        <div class="loadingio-spinner-ellipsis-6vb3rsze10t">
                            <div class="ldio-j7oudfb56o">
                                <div></div>
                                <div></div>
                                <div></div>
                                <div></div>
                                <div></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Toutes les modals du générateur de questionnaires -->

<div class="modal" role="dialog" id="questionModal">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Formulaire d'ajout de questions</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="container-fluid">
                    <div id="questionShortAnswer" class="row pb-1">
                        <div class="col-md-12 questionnaryCard canBeSelected">
                            <span class="questionnaryTitle"> Question à réponse courte </span>
                        </div>
                    </div>
                    <div id="questionLongAnswer" class="row pb-1">
                        <div class="col-md-12 questionnaryCard canBeSelected">
                            <span class="questionnaryTitle"> Question à réponse longue </span>
                        </div>
                    </div>
                    <div id="questionRadio" class="row pb-1">
                        <div class="col-md-12 questionnaryCard canBeSelected">
                            <span class="questionnaryTitle"> Question à choix multiples avec radio bouton </span>
                        </div>
                    </div>
                    <div id="questionSelect" class="row pb-1">
                        <div class="col-md-12 questionnaryCard canBeSelected">
                            <span class="questionnaryTitle"> Question à choix multiples avec liste déroulante </span>
                        </div>
                    </div>
                    <div id="questionCheckbox" class="row pb-1">
                        <div class="col-md-12 questionnaryCard canBeSelected">
                            <span class="questionnaryTitle"> Question à choix multiples avec case à cocher </span>
                        </div>
                    </div>
                    <div id="questionPicture" class="row pb-1">
                        <div class="col-md-12 questionnaryCard canBeSelected">
                            <span class="questionnaryTitle"> Question avec image </span>
                        </div>
                    </div>
                    <div id="questionVideo" class="row pb-1">
                        <div class="col-md-12 questionnaryCard canBeSelected">
                            <span class="questionnaryTitle"> Question avec vidéo </span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Fermer</button>
            </div>
        </div>
    </div>
</div>

<div class="modal" role="dialog" id="questionnaryListModal">
    <div class="modal-dialog modal-xl" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Liste de questionnaires</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body fixedMaxSize">
                <h5>Questionnaires enregistrés en local :</h5>
                <div class="container-fluid overflow-auto mb-1" id="questionnaryListContainerSession"
                     style="max-height: 200px"></div>
                <h5>Questionnaires enregistrés sur le serveur :</h5>
                <div class="container-fluid overflow-auto mb-1" id="questionnaryListContainerDataBase"
                     style="max-height: 200px"></div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Fermer</button>
            </div>
        </div>
    </div>
</div>

<div class="modal" role="dialog" id="DeletingQuestionnary">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Êtes-vous vraiment sûr ?</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p>Êtes-vous vraiment sûr de vouloir supprimer ce questionnaire ?</p>
            </div>
            <div class="modal-footer">
                <button type="submit" id='ValidateDeleting' class="btn btn-primary" data-dismiss="modal">Valider
                </button>
                <button type="button" class="btn btn-danger" data-dismiss="modal">Annuler</button>
            </div>
        </div>
    </div>
</div>

<!-- Modals réponses aux questionnaires du Salarié -->
<!--Modal pour confirmer ses réponses avant envoi -->

<div class="modal" role="dialog" id="ModalQuestionnaire">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Envoi des réponses</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p>
                    Êtes-vous sûr(e) de vouloir envoyer vos réponses ? <br>
                    <em>Attention :</em> Vous ne pourrez plus modifier vos réponses après validation de ce
                    formulaire.
                </p>
            </div>
            <div class="modal-footer">
                <button id="ModalQuestionnaireOK" type="button" class="btn btn-primary" data-dismiss="modal">Valider
                </button>
                <button type="button" class="btn btn-danger" data-dismiss="modal">Annuler</button>
            </div>
        </div>
    </div>
</div>
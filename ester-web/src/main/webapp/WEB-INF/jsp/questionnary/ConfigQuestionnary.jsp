<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>

<%@ page import="fr.univangers.masterinfo.ester.service.form.questionnary.ConfigQuestionnaryForm" %>
<%@ page import="fr.univangers.masterinfo.ester.web.servlet.questionnary.ConfigQuestionnaryServlet" %>


<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib tagdir="/WEB-INF/tags" prefix="ester" %>

<!DOCTYPE html>
<html lang="fr-FR" class="min-vh-100">
<head>

    <title>ESTER - Page de configuration des questionnaires</title>

    <c:import url="/WEB-INF/jsp/librairies/header.jsp"/>
    <script src="<c:url value="/public/js/questionnary/configQuestionnary.js"/>"></script>
</head>

<body class="d-flex flex-column min-vh-100">

<c:import url="/WEB-INF/jsp/librairies/navbar.jsp"/>

<main>
    <div class="container-fluid min-vh-100">
        <div class="row menu_gauche">
            <div class="col-md-8 container mt-3 m_shadow"
                 style="background-color: white" id="container">
                <div class="card-body center_block ">
                    <div class="row col-md-12 d-flex justify-content-center mt-xl-2">
                        <h2 style="border-bottom: solid 2px #127ba3" class="mb-lg-5">
                            Liste des questionnaires
                        </h2>
                    </div>

                    <form method="get" action="creer-questionnaires">
                        <input class="float-right btn btn-success btn-primary mb-3 mr-2" type="submit"
                               value="Créer un questionnaire"/>
                    </form>
                    <table class="table table-striped table-hover" style="table-layout: fixed;width: 100%;">
                        <c:forEach items="${configQuestionnaryForm.questionnaries}" var="q" varStatus="loop">
                        <tr>
                            <td colspan="3"><h2
                                    style="white-space: nowrap;overflow: hidden;text-overflow: ellipsis; ">${q.name}</h2>
                </div>
                </td>
                <td>
                    <form method="get" action="visualiserQuestionnary" class="col-1 offset-md-1">
                        <input type="hidden" value="${q.id}" name="idQuestionnary"/>
                        <div><input class="btn btn-success " type="submit" value="Visualiser"/></div>
                    </form>
                </td>
                <td>
                    <form id="${'supprimer'.concat(loop.index)}" method="post" action="configuration-questionnaires" class="col-1 offset-md-1">
                        <input type="hidden" value="${q.id}" name="idQuestionnary"/>
                        <input type="hidden" value="remove" name="action"/>
                        <input
                                type="button"
                                name="btn"
                                value="Supprimer"
                                data-toggle="modal"
                                data-target="#confirm-submit"
                                class="btn btn-danger"
                                onclick="openModal('supprimer', ${'supprimer'.concat(loop.index)})"
                        />
                    </form>
                </td>

                <c:choose>
                    <c:when test="${q.published eq true}">
                        <td colspan="2">
                            <button
                                    style="background-color: transparent;border-color: transparent;"
                                    class="btn btn-light"
                                    disabled>Publié
                            </button>
                        </td>
                    </c:when>
                    <c:otherwise>
                        <td>
                            <form id="${'publier'.concat(loop.index)}" method="post" action="configuration-questionnaires">
                                <input type="hidden" value="publier" name="action"/>
                                <input type="hidden" value="${q.id}" name="idQuestionnary"/>
                                <input type="button"
                                       name="btn"
                                       value="Publier"
                                       data-toggle="modal"
                                       data-target="#confirm-submit"
                                       class="btn btn-primary"
                                       onclick="openModal('publier', ${'publier'.concat(loop.index)})"/>
                            </form>
                        </td>

                        <td>
                            <form method="get" action="creer-questionnaires">
                                <input type="hidden" value="${q.id}" name="questionnaire"/>
                                <input type="hidden" value="modify" name="action"/>
                                <input class="btn btn-primary" type="submit" value="Modifier"/>
                            </form>
                        </td>
                    </c:otherwise>
                </c:choose>

                <td>
                    <form method="get" action="creer-questionnaires">
                        <input type="hidden" value="${q.id}" name="questionnaire"/>
                        <input type="hidden" value="duplicate" name="action"/>
                        <input class="btn btn-primary " type="submit" value="Dupliquer"/>
                    </form>
                </td>
                </tr>
                </c:forEach>
                </table>
            </div>
        </div>
    </div>
    </div>
</main>

<div class="modal fade" id="confirm-submit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <p>
                    Etes-vous-sûr de vouloir <span id="action"></span> ?
                    <input type="hidden" id="formId" value="">
                </p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Non</button>
                <a id="submit-modal" class="btn btn-success" onclick="submitModal()">Oui</a>
            </div>
        </div>
    </div>
</div>

<c:import url="/WEB-INF/jsp/librairies/footer.jsp"/>

</body>
</html>

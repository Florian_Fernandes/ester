<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ page import="fr.univangers.masterinfo.ester.web.servlet.questionnary.CreateOrModifQuestionnaryServlet" %>
<%@ page import="fr.univangers.masterinfo.ester.service.form.questionnary.CreateOrModifQuestionnaryForm" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib tagdir="/WEB-INF/tags" prefix="ester" %>
<!DOCTYPE html>
<html lang="fr-FR" class="min-vh-100">
<head>
    <title>ESTER - Mise a jour ou création d'un questionnaire</title>
    <c:import url="/WEB-INF/jsp/librairies/header.jsp"/>
</head>
<body class="d-flex flex-column min-vh-100">
<c:import url="/WEB-INF/jsp/librairies/navbar.jsp"/>
<main>
    <div class="container-fluid min-vh-100">
        <div class="row menu_gauche">
            <div class="col-md-8 container mt-3 m_shadow" style="background-color: white" id="container">
                <div class="card-body center_block">
                    <h1>${sessionUser.role.name}</h1>
                    <p>Bienvenue sur la partie ${sessionUser.role.name} du Projet ESTER</p>
                    <h3 class="text-center"><c:out value="${createOrModifQuestionnaryForm.getQuestionnaryName()}"/></h3>
                    <form action="<c:url value="${CreateOrModifQuestionnaryServlet.CREATE_OR_MODIF_QUESTIONNARY_URL}"/>"
                          method="post" enctype="multipart/form-data">
                        <table style="width : 100%;">
                            <tr>
                                <td><label for="titreQuestionnaire">Nom du questionnaire : </label>
                                    <input id="titreQuestionnaire" type="text"
                                           name="${CreateOrModifQuestionnaryForm.INPUT_QUESTIONNARY_NAME}"
                                           placeholder="${CreateOrModifQuestionnaryForm.INPUT_QUESTIONNARY_NAME}"></td>
                                <td><label for="refQuestionnaire">Données de réferences par défaut : </label>
                                    <select id="refQuestionnaire"
                                            name="${CreateOrModifQuestionnaryForm.SELECT_REFERENCE}"
                                            class="custom-select">
                                        <option value="" selected></option>
                                        <c:forEach items="${createOrModifQuestionnaryForm.references}" var="r">
                                            <option value="${r.id}">${r.name}</option>
                                        </c:forEach>
                                    </select></td>
                                <td>
                                    <button type="submit" class="btn btn-success" id="valider">Valider</button>
                                </td>
                            </tr>
                        </table>
                    </form>
                </div>
            </div>
        </div>
    </div>
</main>
<c:import url="/WEB-INF/jsp/librairies/footer.jsp"/>
</body>
</html>

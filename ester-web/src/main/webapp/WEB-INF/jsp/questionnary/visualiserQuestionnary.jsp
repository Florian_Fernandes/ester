<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ page import="fr.univangers.masterinfo.ester.web.servlet.questionnary.ViewQuestionnaryServlet" %>
<%@ page import="fr.univangers.masterinfo.ester.service.form.questionnary.visualiserQuestionnaryForm" %>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib tagdir="/WEB-INF/tags" prefix="ester" %>
<!DOCTYPE html>
<html lang="fr-FR" class="min-vh-100">
<head>
    <title>ESTER - <c:out value="${visualiserQuestionnaryForm.getQuestionnaryName()}"/></title>
    <c:import url="/WEB-INF/jsp/librairies/header.jsp"/>
    <link rel="stylesheet" href="<c:url value="/public/css/questionnary/visualiserQuestionnary.css"/>">
</head>
<body class="d-flex flex-column min-vh-100">
<c:import url="/WEB-INF/jsp/librairies/navbar.jsp"/>
<main>
    <div class="container-fluid min-vh-100">
        <div class="row menu_gauche">
            <div class="col-md-8 container mt-3 m_shadow" style="background-color: white" id="container">
                <div class="card-body">

                    <div class="questionnaryTitle">
                        <span><c:out value="${visualiserQuestionnaryForm.getQuestionnaryName()}"/></span>
                    </div>
                    <div class="container-fluid  m-2">
                        <c:forEach items="${visualiserQuestionnaryForm.getQuestions()}" var="question">
                            <c:choose>
                                <c:when test="${question.type ==\"SHORTANSWER\"}">
                                    <div class="questionCard mb-1">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <span class="questionTitle">${question.name}</span>
                                            </div>
                                            <div class="col-md-6">
                                                <span class="questionType">Question à réponse courte</span>
                                            </div>
                                        </div>
                                    </div>
                                </c:when>
                                <c:when test="${question.type ==\"LONGANSWER\"}">
                                    <div class="questionCard mb-1">
                                        <div class="row question">
                                            <div class="col-md-6">
                                                <span class="questionTitle">${question.name}</span>
                                            </div>
                                            <div class="col-md-6">
                                                <span class="questionType">Question à réponse longue</span>
                                            </div>
                                        </div>
                                    </div>
                                </c:when>
                                <c:when test="${question.type ==\"CHECKBOX\"}">
                                    <div class="questionCard mb-1">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <span class="questionTitle">${question.name}</span>
                                            </div>
                                            <div class="col-md-6">
                                                <span class="questionType">Question à choix multiple avec bouton checkbox</span>
                                            </div>
                                        </div>
                                        <div class="row answerCard m-2">
                                            <c:forEach items="${question.questionOptions}" var="qOptions">
                                                <div class="col-md-5">
                                                    <span>Intitulé de réponse : ${qOptions.value}</span>
                                                </div>
                                                <div class="col-md-3">
                                                    <span>Point : ${qOptions.score}</span>
                                                </div>
                                            </c:forEach>
                                        </div>
                                    </div>
                                </c:when>
                                <c:when test="${question.type ==\"RADIO\"}">
                                    <div class="questionCard mb-1">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <span class="questionTitle">${question.name}</span>
                                            </div>
                                            <div class="col-md-6">
                                                <span class="questionType">Question à choix multiple avec bouton radio</span>
                                            </div>
                                        </div>
                                        <div class="row answerCard m-2">
                                            <c:forEach items="${question.questionOptions}" var="qOptions">
                                                <div class="col-md-5">
                                                    <span>Intitulé de réponse : ${qOptions.value}</span>
                                                </div>
                                                <div class="col-md-3">
                                                    <span>Point : ${qOptions.score}</span>
                                                </div>
                                            </c:forEach>
                                        </div>
                                    </div>
                                </c:when>
                                <c:when test="${question.type ==\"SELECT\"}">
                                    <div class="questionCard mb-1">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <span class="questionTitle">${question.name}</span>
                                            </div>
                                            <div class="col-md-6">
                                                <span class="questionType">Question à choix multiple avec liste déroulante</span>
                                            </div>
                                        </div>
                                        <div class="row answerCard m-2">
                                            <c:forEach items="${question.questionOptions}" var="qOptions">
                                                <div class="col-md-5">
                                                    <span>Intitulé de réponse : ${qOptions.value}</span>
                                                </div>
                                                <div class="col-md-3">
                                                    <span>Point : ${qOptions.score}</span>
                                                </div>
                                            </c:forEach>
                                        </div>
                                    </div>
                                </c:when>
                            </c:choose>
                        </c:forEach>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
<c:import url="/WEB-INF/jsp/librairies/footer.jsp"/>
</body>
</html>
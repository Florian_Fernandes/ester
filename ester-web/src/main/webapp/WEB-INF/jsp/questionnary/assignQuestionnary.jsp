<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ page import="fr.univangers.masterinfo.ester.service.form.questionnary.AssignQuestionnaryForm" %>
<%@ page import="fr.univangers.masterinfo.ester.web.servlet.questionnary.AssignQuestionnaryServlet" %>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib tagdir="/WEB-INF/tags" prefix="ester" %>

<!DOCTYPE html>
<html lang="fr-FR" class="min-vh-100">
<head>
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
            integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
            crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js"
            integrity="sha384-b/U6ypiBEHpOf/4+1nzFpr53nxSS+GLCkfwBdFNTxtclqqenISfwAzpKaMNFNmj4"
            crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/js/bootstrap.min.js"
            integrity="sha384-h0AbiXch4ZDo7tp9hKZ4TsHbi047NrKGLO3SEJAg45jXxnGIfYzk4Si90RDIqNm1"
            crossorigin="anonymous"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" rel="stylesheet"/>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.18/js/bootstrap-select.min.js"></script>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.18/css/bootstrap-select.min.css"
          rel="stylesheet"/>

    <title>ESTER - Affecter des questionnaires</title>

    <c:import url="/WEB-INF/jsp/librairies/header.jsp"/>
    <link rel="stylesheet" href="<c:url value="/public/css/questionnary/assignQuestionnary.css"/>">

</head>

<body class="d-flex flex-column min-vh-100">

<c:import url="/WEB-INF/jsp/librairies/navbar.jsp"/>

<main>
    <div class="container-fluid min-vh-100">
        <div class="row menu_gauche">
            <div class="col-md-8 container mt-3 m_shadow" style="background-color: white">
                <div class="card-body center_block">

                    <div class="row col-md-12 d-flex justify-content-center mt-xl-2">
                        <h2 style="border-bottom: solid 2px #127ba3" class="mb-lg-5">
                            Affecter un questionnaire à un salarié
                        </h2>
                    </div>

                    <form action="<c:url value="${AssignQuestionnaryServlet.ASSIGN_QUESTIONNARY_URL}"/>"
                          class="col-md-12" method="post">
                        <!-- Selection du compte -->
                        <div class="row justify-content-between mb-3">
                            <div class="col-3 text-left">
                                <label for="selectEmployee">Salarié : </label>
                            </div>
                            <div>
                                <select id="selectEmployee"
                                        name="${AssignQuestionnaryForm.EMPLOYEE_SELECT}"
                                        class="selectpicker"
                                        data-live-search="true">
                                    <c:forEach items="${assignQuestionnaryForm.employees}" var="employee">
                                        <option data-tokens="${employee.login}"
                                                value="${employee.id}">${employee.login}</option>
                                    </c:forEach>
                                </select>
                            </div>
                        </div>

                        <!-- Selection du Questionnaire -->
                        <div class="row justify-content-between">
                            <div class="text-left">
                                <label for="selectQuestionnary">Questionnaire : </label>
                            </div>
                            <div>
                                <select id="selectQuestionnary"
                                        name="${AssignQuestionnaryForm.QUESTIONNARY_SELECT}"
                                        class="selectpicker"
                                        data-live-search="true">
                                    <c:forEach items="${assignQuestionnaryForm.questionnaries}" var="questionnaire">
                                        <option data-tokens="${questionnaire.name}"
                                                value="${questionnaire.id}">${questionnaire.name}</option>
                                    </c:forEach>
                                </select>
                            </div>
                            </select>
                        </div>


                        <div class="row mt-lg-5 mb-5">
                            <input type="submit"
                                   id="load"
                                   class="btn btn-md btn-primary"
                                   value="Attribuer le questionnaire"
                                   id="submit">
                        </div>

                    </form>


                    <div class="row col-md-12 d-flex justify-content-center mt-xl-2 mt-5">
                        <h2 style="border-bottom: solid 2px #127ba3" class="mb-lg-5">
                            Liste des salariés et leurs questionnaires associés
                        </h2>
                    </div>

                    <!-- Filtre des Salariés -->
                    <div class="row justify-content-between mb-3">
                        <button style="min-width: 200px" type="button" class="btn btn-primary"
                                onclick="filter('filterEmployee')">filter par salarié
                        </button>

                        <!-- Selection du salarié -->
                        <div class="row justify-content-center" style="min-width: 500px">
                            <div class="col-3 text-left">
                                <label for="filterEmployee">Salariés</label>
                            </div>
                            <div class="col-9">
                                <select id="filterEmployee"
                                        class="selectpicker"
                                        data-live-search="true">
                                    <c:forEach items="${assignQuestionnaryForm.employees}" var="employees">
                                        <option data-tokens="${employees.login}"
                                                value="${employees.login}">${employees.login}</option>
                                    </c:forEach>
                                </select>
                            </div>
                            </select>
                        </div>
                    </div>

                    <!-- Filtre des questionnaire -->
                    <div class="row justify-content-between mb-5">
                        <button style="min-width: 200px" type="button" class="btn btn-primary"
                                onclick="filter('filterQuestionnaries')">filter par questionnaire
                        </button>

                        <!-- Selection du Questionnaire -->
                        <div class="row justify-content-center" style="min-width: 500px">
                            <div class="col-3 text-left">
                                <label for="filterQuestionnaries">Questionnaire</label>
                            </div>
                            <div class="col-9">
                                <select id="filterQuestionnaries"
                                        class="selectpicker"
                                        data-live-search="true">
                                    <c:forEach items="${assignQuestionnaryForm.questionnaries}" var="questionnaire">
                                        <option data-tokens="${questionnaire.name}"
                                                value="${questionnaire.name}">${questionnaire.name}</option>
                                    </c:forEach>
                                </select>
                            </div>
                            </select>
                        </div>
                    </div>


                    <div class="row col-md-12 d-flex justify-content-center mt-xl-2 mt-5">
                        <table class="table" id="employeeTable">
                            <thead>
                            <tr>
                                <th scope="col">Salarié</th>
                                <th scope="col">Questionnaires répondus</th>
                                <th scope="col">Questionnaires en attente</th>
                            </tr>
                            </thead>
                            <tbody>
                            <c:forEach items="${employeeMapping}" var="employeeMapping">
                            <tr scope="row">
                                <td>${employeeMapping.employeeBean.login}</td>
                                <td>
                                    <c:forEach items="${employeeMapping.questionnaireAnswered}"
                                               var="questionnaireAnswered">
                                        ${questionnaireAnswered.name} <br>
                                    </c:forEach>
                                </td>
                                <td>
                                    <c:forEach items="${employeeMapping.questionnaireNotAnswered}"
                                               var="questionnaireNotAnswered">
                                        ${questionnaireNotAnswered.name} <br>
                                    </c:forEach>
                                </td>
                            <tr>
                                </c:forEach>
                            </tbody>
                        </table>
                    </div>

                    <script>
                        function filter(id) {
                            let filerSelectValue = document.getElementById(id).value
                            let employeeTable = document.getElementById("employeeTable")


                            for (let i = 1, row; row = employeeTable.rows[i]; i++) {
                                let rowContainsQuestionnaries = false
                                for (let j = 0, col; col = row.cells[j]; j++) {
                                    if (col.innerText.includes(filerSelectValue)) {
                                        rowContainsQuestionnaries = true
                                    }
                                }
                                row.hidden = !rowContainsQuestionnaries
                            }
                        }
                    </script>

                </div>
            </div>
        </div>
    </div>
</main>

<c:import url="/WEB-INF/jsp/librairies/footer.jsp"/>

</body>
</html>

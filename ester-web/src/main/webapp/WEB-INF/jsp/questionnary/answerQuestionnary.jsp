<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ page import="fr.univangers.masterinfo.ester.service.form.questionnary.AnswerQuestionnaryForm" %>
<%@ page import="fr.univangers.masterinfo.ester.web.servlet.questionnary.AnswerQuestionnaryServlet" %>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib tagdir="/WEB-INF/tags" prefix="ester" %>

<!DOCTYPE html>
<html lang="fr-FR" class="min-vh-100">
<head>

    <title>ESTER - Répondre aux questionnaires</title>

    <c:import url="/WEB-INF/jsp/librairies/header.jsp"/>
    <link rel="stylesheet" href="<c:url value="/public/css/questionnary/answerQuestionnary.css"/>">
    <script src="<c:url value="/public/js/questionnary/answerQuestionnary.js"/>"></script>

</head>

<body class="d-flex flex-column min-vh-100">

<c:import url="/WEB-INF/jsp/librairies/navbar.jsp"/>

<main>
    <div class="container-fluid min-vh-100">
        <div class="row menu_gauche">
            <div class="col-md-8 container mt-3 m_shadow" style="background-color: white" id="container">
                <div class="card-body center_block">

                    <ester:notifications messages="${answerQuestionnaryForm.notifications}"/>

                    <h1>Répondre aux questionnaires</h1>

                    <!-- Tous les questionnaires non répondus du salarié -->
                    <div style="text-align: initial;">

                        <c:if test="${not empty sessionUser.questionnariesNoAnswered}">
                            <div id="listQuestionnaire">
                                <form action="<c:url value="${AnswerQuestionnaryServlet.ANSWER_QUESTIONNARY_URL}"/>"
                                      method="post">
                                    <div class="form-group row">
                                        <div class="col-md-auto justify-content-center d-flex align-content-center pt-2">
                                            <label for="questionnaires">Liste des Questionnaires : </label>
                                        </div>
                                        <div class="col-md-auto justify-content-center d-flex align-content-center pt-2">
                                            <select id="questionnaires"
                                                    name="${AnswerQuestionnaryForm.INPUT_SELECTED_QUESTIONNARY}"
                                                    class="custom-select">
                                                <c:forEach items="${sessionUser.questionnariesNoAnswered}"
                                                           var="questionnaire">
                                                    <c:choose>
                                                        <c:when test="${questionnaire.id == answerQuestionnaryForm.selectedQuestionnary}">
                                                            <option value="${questionnaire.id}"
                                                                    selected>${questionnaire.name}</option>
                                                        </c:when>
                                                        <c:otherwise>
                                                            <option value="${questionnaire.id}">${questionnaire.name}</option>
                                                        </c:otherwise>
                                                    </c:choose>
                                                </c:forEach>
                                            </select>
                                        </div>

                                        <div class="col-md-auto justify-content-center d-flex align-content-center pt-2">
                                            <button class="btn btn-primary btn-md" type="submit">Choisir</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </c:if>

                        <c:if test="${empty sessionUser.questionnariesNoAnswered}">
                            <div class="col-md-auto justify-content-center d-flex align-content-center pt-2">
                                <p>Vous n'avez pas de questionnaires disponible pour le moment !</p>
                            </div>
                        </c:if>

                        <c:if test="${not empty answerQuestionnaryForm.jsonQuestionnary}">

                            <!-- Questionnaire sélectionné -->
                            <div class="row m-2 justify-content-center">
                                <div class="col justify-content-center">
                                    <div class="row justify-content-center">
                                        <div class="col-md-auto my-3" id="titreQuestionnaire">
                                        </div>
                                    </div>

                                    <div class="row justify-content-center">
                                        <div class="col md-auto justify-content-center">
                                            <form method="post" id="FormQuestionnaire">

                                                <input type="hidden"
                                                       name="${AnswerQuestionnaryForm.INPUT_QUESTIONNARY_ANSWERED}"
                                                       value="true"/>
                                                <input type="hidden"
                                                       name="${AnswerQuestionnaryForm.INPUT_SELECTED_QUESTIONNARY}"
                                                       value="${answerQuestionnaryForm.selectedQuestionnary}"/>

                                                <!-- Questions -->
                                                <div id="containerQuestionnaire"
                                                     class="container-fluid row d-flex justify-content-center">
                                                </div>

                                                <!-- Boutons -->
                                                <div class="from-group">
                                                    <div class="col-md-auto">
                                                        <div class="row mt-2 d-flex justify-content-center">
                                                            <button class="col-md-2 btn btn-md btn-primary d-flex mt-1 mr-1 ml-2 justify-content-center"
                                                                    id="validateAction" type="button">Valider
                                                            </button>
                                                            <button class="col-md-2 btn btn-md btn-warning d-flex mt-1 mr-1 ml-2 justify-content-center"
                                                                    type="reset" id="resetAction">Reset
                                                            </button>
                                                            <a class="col-md-2 btn btn-md btn-danger d-flex mt-1 mr-1 ml-2 justify-content-center"
                                                               href="" id="cancelAction">Annuler</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </form>

                                        </div>
                                    </div>
                                </div>
                            </div>

                            <script>
                                affiche(${answerQuestionnaryForm.jsonQuestionnary});
                            </script>

                        </c:if>

                    </div>
                </div>
            </div>
        </div>
    </div>
</main>

<c:import url="/WEB-INF/jsp/questionnary/modalsQuestionnary.jsp"/>
<c:import url="/WEB-INF/jsp/librairies/footer.jsp"/>

</body>
</html>
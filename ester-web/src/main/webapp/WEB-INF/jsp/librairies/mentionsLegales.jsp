<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib tagdir="/WEB-INF/tags" prefix="ester" %>

<!DOCTYPE html>
<html lang="fr-FR" class="min-vh-100">
<head>

    <title>ESTER - Mentions légales</title>

    <c:import url="/WEB-INF/jsp/librairies/header.jsp"/>
    <script src="<c:url value="/public/js/login/login.js"/>"></script>

</head>
<body class="d-flex flex-column min-vh-100">

<c:import url="/WEB-INF/jsp/librairies/navbar.jsp"/>

<main>
    <div class="container p-2 m_shadow" style="background-color: white;margin-top: 10px;">
        <div class="card-body center_block">

            <div class="row justify-content-center align-items-center mb-2">
                <div class="col col-lg-3" style="text-align:center;"><img src="<c:url value="/public/img/irset.png"/>"
                                                                          alt="Logo-ister"></div>
                <div class="col col-lg-3" style="text-align:center;"><img src="<c:url value="/public/img/ua_h.png"/>"
                                                                          alt="Logo-Université-Angers"></div>
            </div>

            <div class="row p-2">
                <div class="col-md-12 container-fluid">
                    <h2><strong>Mentions Légales</strong></h2>

                    <h3>Données enregistrées</h3>

                    <c:import url="/WEB-INF/jsp/librairies/dataAccordeon.jsp"/>
                </div>
            </div>
        </div>
    </div>
</main>

<c:import url="/WEB-INF/jsp/librairies/footer.jsp"/>

</body>

<c:import url="/WEB-INF/jsp/librairies/modalFirstConnexion.jsp"/>

</html>
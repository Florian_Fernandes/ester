<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ page import="fr.univangers.masterinfo.ester.dao.bean.UserRole" %>
<%@ page import="fr.univangers.masterinfo.ester.web.servlet.connection.LoginConnectionServlet" %>
<%@ page import="fr.univangers.masterinfo.ester.web.servlet.connection.LogoutConnectionServlet" %>
<%@ page import="fr.univangers.masterinfo.ester.web.servlet.account.CreateAccountServlet" %>
<%@ page import="fr.univangers.masterinfo.ester.web.servlet.account.ModifyAccountServlet" %>
<%@ page import="fr.univangers.masterinfo.ester.web.servlet.account.EmployeeDataServlet" %>
<%@ page import="fr.univangers.masterinfo.ester.web.servlet.data.ImportReferencesDataServlet" %>
<%@ page import="fr.univangers.masterinfo.ester.web.servlet.data.RemoveReferencesDataServlet" %>
<%@ page import="fr.univangers.masterinfo.ester.web.servlet.questionnary.AssignQuestionnaryServlet" %>
<%@ page import="fr.univangers.masterinfo.ester.web.servlet.questionnary.ShareQuestionnaryServlet" %>
<%@ page import="fr.univangers.masterinfo.ester.web.servlet.questionnary.AnswerQuestionnaryServlet" %>
<%@ page import="fr.univangers.masterinfo.ester.web.servlet.analysis.CompareDataReferencesAnalysisServlet" %>
<%@ page import="fr.univangers.masterinfo.ester.web.servlet.analysis.AnalysisScoreServlet" %>
<%@ page import="fr.univangers.masterinfo.ester.web.servlet.questionnary.ConfigQuestionnaryServlet" %>


<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib tagdir="/WEB-INF/tags" prefix="ester" %>

<style>
    <%@include file="../../../public/css/librairies/navbar.css" %>
</style>
<header class="m_shadow">
    <nav class="navbar fixed-top navbar-expand-lg navbar-dark bg-primary m_border">
        <nav class="menu">
            <input id="menu__toggle" type="checkbox" class='menu__toggle'/>
            <label for="menu__toggle" class="menu__toggle-label">
                <svg class="svgNavbar" preserveAspectRatio='xMinYMin' viewBox='0 0 24 24'>
                    <path d='M3,6H21V8H3V6M3,11H21V13H3V11M3,16H21V18H3V16Z'/>
                </svg>
                <svg class="svgNavbar" preserveAspectRatio='xMinYMin' viewBox='0 0 24 24'>
                    <path d="M19,6.41L17.59,5L12,10.59L6.41,5L5,6.41L10.59,12L5,17.59L6.41,19L12,13.41L17.59,19L19,17.59L13.41,12L19,6.41Z"/>
                </svg>
            </label>


            <ol class='menu__content'>
                <li class="menu-item"><a class="link" href="<c:url value="/tableau-de-bord"/>">Accueil</a></li>
                <c:if test="${sessionUser.role == UserRole.ADMIN
                || sessionUser.role == UserRole.MEDECIN
                || sessionUser.role == UserRole.PLURIDISCIPLINAIRE}">
                    <li class="menu-item">
                        <a class="link" href="" style="pointer-events: none;">Comptes :</a>
                        <ol class="sub-menu">
                            <li class="menu-item"><a class="link"
                                                     href="<c:url value="${CreateAccountServlet.CREATE_ACCOUNT_URL}"/>">Créer</a>
                            </li>
                            <li class="menu-item"><a class="link"
                                                     href="<c:url value="${ModifyAccountServlet.MODIFY_ACCOUNT_URL}?id=${sessionUser.id}"/>">Modifier</a>
                            </li>
                            <li class="menu-item"><a class="link"
                                                     href="<c:url value="${EmployeeDataServlet.EMPLOYEE_DATA_URL}"/>">Liste
                                Salariés</a></li>
                        </ol>
                    </li>
                    <c:if test="${sessionUser.role == UserRole.ADMIN}">
                        <li class="menu-item">
                            <a class="link" href="" style="pointer-events: none;">Données :</a>
                            <ol class="sub-menu">
                                <li class="menu-item"><a class="link"
                                                         href="<c:url value="${ImportReferencesDataServlet.IMPORT_REFERENCES_DATA_URL}"/>">Importer</a>
                                </li>
                                <li class="menu-item"><a class="link"
                                                         href="<c:url value="${RemoveReferencesDataServlet.REMOVE_REFERENCES_DATA_URL}"/>">Supprimer</a>
                                </li>
                            </ol>
                        </li>
                    </c:if>
                    <li class="menu-item">
                        <a class="link" href="" style="pointer-events: none;">Questionnaires :</a>
                        <ol class="sub-menu">
                            <li class="menu-item"><a class="link"
                                                     href="<c:url value="${ConfigQuestionnaryServlet.CONFIG_QUESTIONNARY_URL}"/>">Configurer</a>
                            </li>
                            <li class="menu-item"><a class="link"
                                                     href="<c:url value="${AssignQuestionnaryServlet.ASSIGN_QUESTIONNARY_URL}"/>">Affecter
                                aux salariés</a></li>
                            <li class="menu-item"><a class="link"
                                                     href="<c:url value="${ShareQuestionnaryServlet.SHARE_QUESTIONNARY_URL}"/>">Affecter
                                droits</a></li>
                        </ol>
                    </li>
                    <li class="menu-item">
                        <a class="link" href="" style="pointer-events: none;">Analyses :</a>
                        <ol class="sub-menu">
                            <li class="menu-item"><a class="link"
                                                     href="<c:url value="${AnalysisScoreServlet.ACCESS_WEB_SITE_ANALYSIS_URL}"/>">Score
                                références</a></li>
                            <li class="menu-item"><a class="link"
                                                     href="<c:url value="${CompareDataReferencesAnalysisServlet.COMPARE_DATA_REFERENCES_ANALYSIS_URL}"/>">Données
                                références</a></li>
                        </ol>
                    </li>
                </c:if>

                <c:if test="${sessionUser.role == UserRole.SALARIE}">
                    <li class="menu-item"><a class="link"
                                             href="<c:url value="${AnswerQuestionnaryServlet.ANSWER_QUESTIONNARY_URL}"/>">Répondre
                        aux questionnaires</a></li>
                </c:if>
            </ol>

        </nav>
        <span class="menu-text">Menu</span>
        <div class="topcorner">
            <form class="form-inline my-2 my-lg-0">
                <c:if test="${empty sessionUser}">
                    <div class="btn-group" role="group">
                        <a class="btn btn-light" href="<c:url value="${LoginConnectionServlet.LOGIN_CONNECTION_URL}"/>">SE
                            CONNECTER</a>
                    </div>
                </c:if>
                <c:if test="${not empty sessionUser}">
                    <div class="btn-group" role="group">
                        <button id="btnGroupDrop1" type="button" class="btn btn-danger dropdown-toggle"
                                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <c:out value="${sessionUser.login}"/>
                        </button>
                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="btnGroupDrop1">
                            <a class="dropdown-item"
                               href="<c:url value="${LogoutConnectionServlet.LOGOUT_CONNECTION_URL}"/>">Déconnexion</a>
                        </div>
                    </div>

                </c:if>
            </form>
        </div>
    </nav>
    <c:import url="/WEB-INF/jsp/librairies/modalFirstConnexion.jsp"/>
</header>
<div style="min-height: 50px"></div>

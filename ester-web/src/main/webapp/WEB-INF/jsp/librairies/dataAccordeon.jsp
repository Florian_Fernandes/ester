<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib tagdir="/WEB-INF/tags" prefix="ester" %>

<p>Afin de faciliter votre navigation sur ce site, nous avons besoin d'utiliser certaines données.</p>
<div id="accordion">
    <div class="card">
        <div class="card-header" id="headingSalarie">
            <h3 class="mb-0">
                <button class="btn btn-link" data-toggle="collapse" data-target="#collapseSalarie" aria-expanded="true"
                        aria-controls="collapseSalarie">
                    Données correspondant aux salariés
                </button>
            </h3>
        </div>

        <div id="collapseSalarie" class="collapse" aria-labelledby="headingSalarie" data-parent="#accordion">
            <div class="card-body">
                Afin d'obtenir des résultats les plus intéressants possible et de regrouper des données statistiques les
                plus précises,
                nous aurons besoin de vous demander :
                <ul>
                    <li>Le sexe</li>
                    <li>Un âge quinquennal (exemple : 25-29 ans)</li>
                    <li>La région et le département d'habitat</li>
                    <li>Le domaine d'activité et le nom du poste</li>
                </ul>
                De plus, pour chaque questionnaire, nous enregistrerons les réponses que vous aurez fourni afin de
                regrouper les résultats
                et les comparer avec des réponses obtenues d'enquêtes préalables.
                <br>
                Enfin, un cookie permet d'enregistrer que vous avez déjà eu cette petite fenêtre d'avertissement.
            </div>
        </div>
    </div>
<%--    <div class="card">--%>
<%--        <div class="card-header" id="headingEntreprise">--%>
<%--            <h3 class="mb-0">--%>
<%--                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseEntreprise"--%>
<%--                        aria-expanded="false" aria-controls="collapseEntreprise">--%>
<%--                    Données correspondant aux entreprises--%>
<%--                </button>--%>
<%--            </h3>--%>
<%--        </div>--%>
<%--        <div id="collapseEntreprise" class="collapse" aria-labelledby="headingEntreprise" data-parent="#accordion">--%>
<%--            <div class="card-body">--%>
<%--                Afin d'obtenir de faciliter votre connexion et la navigation sur l'ensemble du site,--%>
<%--                nous avons besoin d'enregistrer sur notre base de données :--%>
<%--                <ul>--%>
<%--                    <li>Une adresse email</li>--%>
<%--                    <li>Un Identifiant</li>--%>
<%--                    <li>Ainsi qu'un mot de passe</li>--%>
<%--                </ul>--%>
<%--                Enfin, un cookie permet d'enregistrer que vous avez déjà eu cette petite fenêtre d'avertissement.--%>
<%--            </div>--%>
<%--        </div>--%>
<%--    </div>--%>
    <div class="card">
        <div class="card-header" id="headingESTER">
            <h3 class="mb-0">
                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseESTER"
                        aria-expanded="false" aria-controls="collapseESTER">
                    Données correspondant aux membres ESTER
                </button>
            </h3>
        </div>
        <div id="collapseESTER" class="collapse" aria-labelledby="headingESTER" data-parent="#accordion">
            <div class="card-body">
                Afin d'obtenir de faciliter votre connexion et la navigation sur l'ensemble du site,
                nous avons besoin d'enregistrer sur notre base de données :
                <ul>
                    <li>Une adresse email</li>
                    <li>Un Identifiant</li>
                    <li>Ainsi qu'un mot de passe</li>
                    <li>Votre rôle en tant que membre ESTER (i.e. Administrateur, Médecin, Assistant, Infirmier,
                        Préventeur)
                    </li>
                    <li>Une liste de salarié créés</li>
                    <li><em>Uniquement possible pour les Administrateurs, Médecins et Préventeurs :</em> Les
                        questionnaires que vous aurez créé ou auxquels vous avez les droits de modification.
                        De plus, afin de faciliter la création des questionnaires, les derniers éléments créés seront
                        enregistrés sur votre navigateur
                        jusqu'à votre validation et l'envoi vers la base de données. Toutes les données enregistrées en
                        local seront effacées au bout de 24H.
                    </li>
                </ul>
                Enfin, un cookie permet d'enregistrer que vous avez déjà eu cette petite fenêtre d'avertissement.
            </div>
        </div>
    </div>
</div>
<p class="pb-3">* Toutes les données indiqués dans le tableau ci-dessus correspondent strictement à un statut
    d'utilisateur et aucune donnée supplémentaire, hormis celles requises pour son statut, ne seront exigées ou
    utilisées.<br>
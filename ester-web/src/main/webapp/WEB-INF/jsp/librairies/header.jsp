<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib tagdir="/WEB-INF/tags" prefix="ester" %>

<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">

<link rel="icon" href="<c:url value="/public/img/ua.png"/>">
<link rel="stylesheet" href="<c:url value="/public/css/librairies/global_style.css"/>">
<link rel="stylesheet" href="<c:url value="/public/css/librairies/menu_login.css"/>">
<link rel="stylesheet" href="<c:url value="/public/css/librairies/menu.css"/>">
<link rel="stylesheet" href="<c:url value="/public/css/librairies/bootstrap.min.css"/>">

<script src="<c:url value="/public/js/librairies/jquery.min.js"/>"></script>
<script src="<c:url value="/public/js/librairies/jquery.session.js "/>"></script>
<script src="<c:url value="/public/js/librairies/popper.min.js"/>"></script>
<script src="<c:url value="/public/js/librairies/bootstrap.min.js"/>"></script>
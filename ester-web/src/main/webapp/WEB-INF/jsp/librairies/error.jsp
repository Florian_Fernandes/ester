<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib tagdir="/WEB-INF/tags" prefix="ester" %>

<!DOCTYPE html>
<html lang="fr-FR" class="h-100">
<head>

    <title>ESTER - Erreur</title>

    <c:import url="/WEB-INF/jsp/librairies/header.jsp"/>

</head>

<body class="d-flex flex-column h-100">

<c:import url="/WEB-INF/jsp/librairies/navbar.jsp"/>

<div class="card">
    <div class="card-header">
        <h5 class="card-title" style='font-weight:bold'>Erreur</h5>
    </div>
    <div id="histo-div" class="card-body">
        <div class="row justify-content-center">
            <div class="col-md-4 justify-content-center">
                <p style="font-size: 120%;">Désolé, une erreur s'est produite côté serveur. Veuillez réessayer plus
                    tard</p>
                <p><c:out value="${errorMsg}"></c:out></p>
            </div>
        </div>
        <div class="row justify-content-center">
            <div class="col-md-2 justify-content-center">
                <img src="<c:url value="/public/img/worker.png"/>" alt="Women Worker" width="250">
            </div>
        </div>
    </div>

</div>

<c:import url="/WEB-INF/jsp/librairies/footer.jsp"/>

</body>
</html>
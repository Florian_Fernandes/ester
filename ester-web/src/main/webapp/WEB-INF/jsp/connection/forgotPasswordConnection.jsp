<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ page
        import="fr.univangers.masterinfo.ester.web.servlet.connection.ForgotPasswordConnectionServlet" %>
<%@ page
        import="fr.univangers.masterinfo.ester.service.form.connection.ForgotPasswordConnectionForm" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib tagdir="/WEB-INF/tags" prefix="ester" %>
<!DOCTYPE html>
<html lang="fr-FR" class="h-100">
<head>
    <title>ESTER - Mot de passe oublié</title>
    <c:import url="/WEB-INF/jsp/librairies/header.jsp"/>
    <link rel="stylesheet" href="<c:url value="/public/css/connection/forgotPasswordConnection.css"/>">
    <script src="<c:url value="/public/js/connection/forgotPasswordConnection.js"/>"></script>
</head>
<body class="d-flex flex-column h-100">
<c:import url="/WEB-INF/jsp/librairies/navbar.jsp"/>
<main>
    <div class="container p-2" id="container">
        <ester:notifications messages="${forgotPasswordConnectionForm.notifications}"/>
        <div class="row justify-content-center p-2">
            <div class="col-md-auto">
                <h1 class="font-weight-normal text-center">Mot de passe oublié</h1>
            </div>
        </div>
        <div class="row justify-content-center">
            <div class="col-md-auto my-1 mx-3">
                <form class="form-signin m_shadow"
                      action="<c:url value="${ForgotPasswordConnectionServlet.FORGOT_PASSWORD_CONNECTION_URL}"/>"
                      method="post">
                    <div class="form-group">
                        <label for="Compte_Email">Veuillez saisir votre email</label> <input type="email"
                                                                                             name="${ForgotPasswordConnectionForm.INPUT_EMAIL}"
                                                                                             id="Compte_Email"
                                                                                             placeholder="nom@exemple.com"
                                                                                             class="form-control"
                                                                                             required>
                    </div>
                    <div class="row justify-content-md-center">
                        <div class="col-md-auto">
                            <button class="btn btn-md-auto btn-primary btn-block" type="submit">VALIDER</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</main>
<c:import url="/WEB-INF/jsp/librairies/footer.jsp"/>
</body>
</html>
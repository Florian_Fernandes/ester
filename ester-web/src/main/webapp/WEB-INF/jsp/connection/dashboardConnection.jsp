<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib tagdir="/WEB-INF/tags" prefix="ester" %>

<!DOCTYPE html>
<html lang="fr-FR" class="min-vh-100">
<head>

    <title>ESTER - Tableau de bord</title>

    <c:import url="/WEB-INF/jsp/librairies/header.jsp"/>
    <link rel="stylesheet" href="<c:url value="/public/css/connection/dashboardConnection.css"/>">
    <script src="<c:url value="/public/js/connection/dashboardConnection.js"/>"></script>

</head>

<body class="d-flex flex-column min-vh-100">

<c:import url="/WEB-INF/jsp/librairies/navbar.jsp"/>

<main>
    <div class="container-fluid min-vh-100">
        <div class="row menu_gauche">
            <div class="col-md-8 container mt-3 m_shadow" style="background-color: white" id="container">
                <div class="card-body center_block">

                    <div class="row col-md-12 d-flex justify-content-center mt-xl-2">
                        <h2 style="border-bottom: solid 2px #127ba3" class="mb-lg-5">
                            Accueil
                        </h2>
                    </div>

                    <div class="row flex-column justify-content-start mt-xl-2 mb-5">
                        <h3 class="mb-3">Gestions des comptes</h3>
                        <ul class="list-group list-group-flush">
                            <li class="list-group-item">
                                <h7>Créer</h7>
                                <p>
                                    Sur cette page vous allez pouvoir créer des comptes pour les salariés et tous les
                                    utilisateurs du site
                                </p>
                            </li>
                            <li class="list-group-item">
                                <h7>Modifier</h7>
                                <p>
                                    Sur cette page vous allez pouvoir modifier votre compte
                                </p>
                            </li>
                            <li class="list-group-item">
                                <h7>Liste Salariés</h7>
                                <p>
                                    Sur cette page vous allez pouvoir afficher la liste de tous les salariés inscrit sur
                                    le site. De plus vous allez pouvoir modifier les informations des salariés
                                </p>
                            </li>
                        </ul>
                    </div>

                    <div class="row flex-column justify-content-center mt-xl-2 mb-5">
                        <h3 class="mb-3">Configurez les bases de données de références</h3>
                        <ul class="list-group list-group-flush">
                            <li class="list-group-item">
                                <h7>importer</h7>
                                <p>
                                    Sur cette page vous allez pouvoir importer les bases de données de références. C'est
                                    sur cette page que vous allez préciser les colonnes à utiliser pour le calcul du
                                    score
                                </p>
                            </li>
                            <li class="list-group-item">
                                <h7>Supprimer</h7>
                                <p>
                                    Sur cette page vous allez pouvoir supprimer des bases de données de références
                                </p>
                            </li>
                        </ul>
                    </div>

                    <div class="row flex-column justify-content-center mt-xl-2 mb-5">
                        <h3 class="mb-3">Gérer les questionnaires</h3>
                        <ul class="list-group list-group-flush">
                            <li class="list-group-item">
                                <h7>Configurer</h7>
                                <p>
                                    Sur cette page vous allez pouvoir créer des questionnaires, les supprimer, les
                                    modifier ansi que les dupliquer. Il sera également possible de les publier, un
                                    questionnaire publié sera alors prêt à être attribué à un salarié
                                </p>
                            </li>
                            <li class="list-group-item">
                                <h7>Affecter aux salariés</h7>
                                <p>
                                    Sur cette page vous allez pouvoir attribuer aux salariés des questionnaires publiés
                                </p>
                            </li>
                            <li class="list-group-item">
                                <h7>Affecter droits</h7>
                                <p>
                                    Sur cette page vous allez pouvoir données aux autres membres de votre équipe des
                                    droits sur les questionnaires. Un fois que les droits seront transmit, la personne
                                    pourra effectuer les meme opérations que vous sur un questionnaire
                                </p>
                            </li>
                        </ul>
                    </div>

                    <div class="row flex-column justify-content-center mt-xl-2 mb-5">
                        <h3 class="mb-3">Analyser les résultats</h3>
                        <ul class="list-group list-group-flush">
                            <li class="list-group-item">
                                <h7>Score références</h7>
                                <p>
                                    Sur cette page vous allez pouvoir voir la distribution du score sur les bases de
                                    données de références importées
                                </p>
                            </li>
                            <li class="list-group-item">
                                <h7>Données références</h7>
                                <p>
                                    Sur cette page vous allez pouvoir analyser les réponses d'un salarié pour un
                                    questionnaire
                                </p>
                            </li>
                        </ul>
                    </div>

                </div>
            </div>
        </div>
    </div>
</main>

<c:import url="/WEB-INF/jsp/librairies/footer.jsp"/>

</body>
</html>
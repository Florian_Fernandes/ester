<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib tagdir="/WEB-INF/tags" prefix="ester" %>

<!DOCTYPE html>
<html lang="fr-FR" class="min-vh-100">
<head>

    <title>ESTER - Accueil</title>

    <c:import url="/WEB-INF/jsp/librairies/header.jsp"/>
    <link rel="stylesheet" href="<c:url value="/public/css/connection/welcomeConnection.css"/>">
    <script src="<c:url value="/public/js/connection/welcomeConnection.js"/>"></script>

</head>

<body class="d-flex flex-column min-vh-100">

<c:import url="/WEB-INF/jsp/librairies/navbar.jsp"/>

<main>

    <div class="container p-2" style="margin-top: 7%;">

        <div class="row justify-content-center align-items-center mb-2">
            <div class="col col-lg-3" style="text-align:center;"><img src="<c:url value="/public/img/irset.png"/>"
                                                                      alt="Logo-Ester"></div>
            <div class="col col-lg-3" style="text-align:center;"><img src="<c:url value="/public/img/ua_h.png"/>"
                                                                      alt="Logo-Université-Angers"></div>
        </div>

        <div class="row p-2">
            <div class="col">
                <div class="card m_shadow">
                    <div class="card-body center_block">
                        <h5 class="card-title">Bienvenue</h5>
                        <p class="card-text">Vous êtes sur le projet Ester</p>
                    </div>
                </div>
            </div>
        </div>

    </div>

</main>

<c:import url="/WEB-INF/jsp/librairies/footer.jsp"/>

</body>

</html>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ page import="fr.univangers.masterinfo.ester.dao.bean.UserRole" %>
<%@ page import="fr.univangers.masterinfo.ester.dao.bean.UserGroup" %>
<%@ page import="fr.univangers.masterinfo.ester.service.form.connection.LoginConnectionForm" %>
<%@ page import="fr.univangers.masterinfo.ester.web.servlet.connection.LoginConnectionServlet" %>
<%@ page import="fr.univangers.masterinfo.ester.web.servlet.connection.DashboardConnectionServlet" %>
<%@ page import="fr.univangers.masterinfo.ester.web.servlet.connection.ForgotPasswordConnectionServlet" %>
<%@ page import="fr.univangers.masterinfo.ester.web.servlet.connection.FirstConnectionServlet" %>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib tagdir="/WEB-INF/tags" prefix="ester" %>

<!DOCTYPE html>
<html lang="fr-FR" class="h-100">
<head>

    <title>ESTER - Connexion</title>

    <c:import url="/WEB-INF/jsp/librairies/header.jsp"/>
    <link rel="stylesheet" href="<c:url value="/public/css/connection/loginConnection.css"/>">
    <script src="<c:url value="/public/js/connection/loginConnection.js"/>"></script>

</head>

<body class="d-flex flex-column h-100">

<c:import url="/WEB-INF/jsp/librairies/navbar.jsp"/>

<main>
    <div class="container p-2" id="container">

        <c:if test="${not empty sessionUser}">
            <script>
                redirect("<c:url value="${DashboardConnectionServlet.DASHBOARD_CONNECTION_URL}"/>");
            </script>
        </c:if>

        <ester:notifications messages="${loginConnectionForm.notifications}"/>

        <div class="row justify-content-center p-2">
            <div class="col-md-auto">
                <h1 class="font-weight-normal text-center">Connexion</h1>
            </div>
        </div>
        <div class="row justify-content-center">
            <button class="btnDiv" id="btnSalarie" onclick="showDiv('divSalarie');">Salarié</button>
            <button class="btnDiv" id="btnUtilisateur" onclick="showDiv('divUtilisateur');">Personnel Soignant</button>
        </div>
        <div class="row justify-content-center">
            <div class="col-md-auto my-1 mx-3">
                <form action="<c:url value="${LoginConnectionServlet.LOGIN_CONNECTION_URL}"/>" id="divSalarie"
                      style="display: none;" class="form-signin m_shadow" method="post">
                    <div class="form-group">
                        <label for="login">Identifiant</label>
                        <input type="text" id="login" name="${LoginConnectionForm.INPUT_LOGIN_OR_EMAIL}"
                               class="form-control" value="${loginConnectionForm.loginOrEmail}" required>
                    </div>
                    <div class="row justify-content-md-center">
                        <div class="col-md-auto">
                            <input type="hidden" name="${LoginConnectionForm.INPUT_GROUP}"
                                   value="${UserGroup.SALARIE.name()}"/>
                            <button class="btn btn-block btn-primary" type="submit">SE CONNECTER</button>
                        </div>
                    </div>
                </form>

                <form action="<c:url value="${LoginConnectionServlet.LOGIN_CONNECTION_URL}"/>" id="divUtilisateur"
                      style="display:none;" class="form-signin m_shadow " method="post">
                    <div class="form-group">
                        <label for="login">Adresse email ou identifiant</label>
                        <input type="text" id="login" name="${LoginConnectionForm.INPUT_LOGIN_OR_EMAIL}"
                               class="form-control" value="${loginConnectionForm.loginOrEmail}" required>
                    </div>
                    <div class="form-group">
                        <label for="PasswordInputUtil">Mot de passe</label>
                        <input type="password" id="PasswordInputUtil" name="${LoginConnectionForm.INPUT_PASSWORD}"
                               class="form-control" value="${loginConnectionForm.password}" required>
                        <div class="row justify-content-center">
                            <a href="<c:url value='${ForgotPasswordConnectionServlet.FORGOT_PASSWORD_CONNECTION_URL}'/>">Mot
                                de passe oublié ?</a>
                        </div>
                    </div>

                    <div class="row justify-content-md-center">
                        <div class="col-md-auto">
                            <input type="hidden" name="${LoginConnectionForm.INPUT_GROUP}"
                                   value="${UserGroup.MEMBRE_ESTER.name()}"/>
                            <button id="submitUtilisateur" class="btn btn-block btn-primary" type="submit">SE
                                CONNECTER
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>

    </div>
</main>

<c:import url="/WEB-INF/jsp/librairies/footer.jsp"/>

</body>
</html>

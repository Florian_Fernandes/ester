<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page import="fr.univangers.masterinfo.ester.web.servlet.connection.FirstConnectionServlet" %>
<%@ page import="fr.univangers.masterinfo.ester.service.form.connection.FirstConnectionForm" %>
<%@ taglib tagdir="/WEB-INF/tags" prefix="ester" %>
<!DOCTYPE html>
<html lang="fr-FR" class="h-100">
<head>
    <title>ESTER - Première connexion</title>
    <c:import url="/WEB-INF/jsp/librairies/header.jsp"/>
    <link rel="stylesheet" href="<c:url value="/public/css/connection/firstConnection.css"/>">
    <script src="<c:url value="/public/js/connection/firstConnection.js"/>"></script>
</head>
<body class="d-flex flex-column h-100">
<c:import url="/WEB-INF/jsp/librairies/navbar.jsp"/>
<main>
    <div class="container p-2" id="container">
        <ester:notifications messages="${firstConnectionForm.notifications}"/>
        <div class="row justify-content-center p-2">
            <div class="col-md-auto">
                <h1 class="font-weight-normal text-center">Première connexion</h1>
            </div>
        </div>
        <div class="row justify-content-center">
            <div class="col-md-auto my-1 mx-3">
                <form class="form-signin m_shadow"
                      action="<c:url value="${FirstConnectionServlet.FIRST_CONNECTION_URL}"/>"
                      method="post">
                    <div class="form-group">
                        <label for="Password">Saisir un mot de passe</label>
                        <input type="password"
                               name="${FIRST_CONNECTION_FORM.INPUT_PASSWORD}" id="Password" class="form-control"
                               required>
                    </div>
                    <div class="form-group">
                        <label for="Password_confirmation">Confirmer le mot de passe</label>
                        <input type="password"
                               name="${FIRST_CONNECTION_FORM.INPUT_CONFIRM_PASSWORD}" id="Password_confirmation"
                               class="form-control" required>
                    </div>
                    <div class="row justify-content-md-center">
                        <div class="col-md-auto">
                            <button class="btn btn-block btn-primary" type="submit">VALIDER</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</main>
<c:import url="/WEB-INF/jsp/librairies/footer.jsp"/>
</body>
</html>
<%@page import="java.util.ArrayList" %>
<%@ page import="java.util.List" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<!DOCTYPE html>
<html lang="fr-FR" class="h-100">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" href="<c:url value="/public/img/ua.png"/>">

    <title>Connexion - ESTER</title>

    <link rel="stylesheet" href="<c:url value="/public/css/libraries/bootstrap.min.css"/>">
    <link rel="stylesheet" href="<c:url value="/public/css/Accueil.css"/>">

    <script src="<c:url value="/dwr/engine.js"/>"></script>
    <script src="<c:url value="/dwr/interface/Resultat.js"/>"></script>
    <script src="<c:url value="/dwr/util.js"/>"></script>
    <script src="https://code.highcharts.com/highcharts.js"></script>
    <script src="https://code.highcharts.com/modules/data.js"></script>
    <script src="https://code.highcharts.com/modules/exporting.js"></script>
    <script src="https://code.highcharts.com/modules/export-data.js"></script>
    <script src="<c:url value="/public/js/libraries/jquery.min.js"/>"></script>
    <script src="<c:url value="/public/js/libraries/popper.min.js"/>"></script>
    <script src="<c:url value="/public/js/libraries/bootstrap.min.js"/>"></script>
</head>
<body class="d-flex flex-column h-100">
<main>
    <br> <br>
    <div class="row">
        <div class="col-md-12 pl-12" style="margin-bottom: 5%">
            <div id="chart-A" class="chart"></div>
        </div>
        <div class="col-md-12 pl-12" style="margin-bottom: 5%">
            <div id="chart-B" class="chart"></div>
        </div>
        <div class="col-md-6 pl-6" style="margin-bottom: 5%">
            <div id="chart-C" class="chart"></div>
        </div>
        <div class="col-md-6 pl-6" style="margin-bottom: 5%">
            <div id="chart-D" class="chart"></div>
        </div>
    </div>
</main>

</body>
<script type="text/javascript">
    var score = parseInt('${scoreEmployee}')

</script>
<script type="text/javascript" src="<c:url value="/public/js/analysis/compareDataReferencesAnalysis.js"/>">

</script>
</html>

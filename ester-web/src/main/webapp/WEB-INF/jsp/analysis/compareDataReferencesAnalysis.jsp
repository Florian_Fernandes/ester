<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ page import="fr.univangers.masterinfo.ester.web.servlet.analysis.CompareDataReferencesAnalysisServlet" %>
<%@ page import="fr.univangers.masterinfo.ester.service.form.analysis.CompareDataReferencesAnalysisForm" %>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib tagdir="/WEB-INF/tags" prefix="ester" %>

<!DOCTYPE html>
<html lang="fr-FR" class="min-vh-100">
<head>
    <title>ESTER - Analyses données de références</title>
    <c:import url="/WEB-INF/jsp/librairies/header.jsp"/>
    <link rel="stylesheet" href="<c:url value="/public/css/analysis/compareDataReferencesAnalysis.css"/>">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
    <script src="https://code.highcharts.com/highcharts.js"></script>
    <script src="<c:url value="/public/js/analysis/compareDataReferencesAnalysis.js"/>"></script>
</head>
<body class="d-flex flex-column min-vh-100">
<c:import url="/WEB-INF/jsp/librairies/navbar.jsp"/>
<main>
    <div class="container-fluid min-vh-100">
        <div class="row menu_gauche">
            <div class="col-md-8 container mt-3 m_shadow" style="background-color: white" id="container">
                <div class="card-body center_block">

                    <div class="row col-md-12 d-flex justify-content-center mt-xl-2">
                        <h2 style="border-bottom: solid 2px #127ba3" class="mb-lg-5">
                            Analyse sur les données de références
                        </h2>
                    </div>

                    <div class="row col-md-12 d-flex">
                        <form
                                id="saisi"
                                action="<c:url value="${CompareDataReferencesAnalysisServlet.COMPARE_DATA_REFERENCES_ANALYSIS_URL}"/>"
                                method="post"
                                class="w-100">
                            <div class="row pt-2 pb-8">

                                <c:choose>
                                    <c:when test="${!employee_selected}">

                                        <div class="col-md-4">
                                            <label for="listeSalaries">Choisissez un Salarié :</label>
                                            <select id="listeSalaries" class="form-control"
                                                    name="${CompareDataReferencesAnalysisForm.INPUT_ID_EMPLOYEE}">
                                                <c:forEach items="${listEmployees}" var="item">
                                                    <option value="${item.id}"><c:out
                                                            value="${item.login}"></c:out></option>
                                                </c:forEach>
                                            </select>
                                        </div>
                                    </c:when>
                                    <c:when test="${employee_selected && !questionnary_selected}">
                                        <div class="col-md-4">
                                            <input type="hidden"
                                                   name="${CompareDataReferencesAnalysisForm.INPUT_ID_EMPLOYEE}"
                                                   value="${employee_id}"/>
                                            Salarié sélectionné : <c:out value="${employee_name}"/>
                                        </div>
                                        <div class="col-md-4">
                                            <label for="listeQuestionnaires">Choisissez un Questionnaire :</label>
                                            <select id="listeQuestionnaires" class="form-control"
                                                    name="${CompareDataReferencesAnalysisForm.INPUT_ID_QUESTIONNARY}">
                                                <c:forEach items="${listQuestionnaries}" var="item">
                                                    <option value="${item.id}"><c:out
                                                            value="${item.name}"></c:out></option>
                                                </c:forEach>
                                            </select>
                                        </div>
                                    </c:when>
                                    <c:otherwise>
                                        <h5 style="text-align: left" class="mb-xl-5">
                                            Salarié sélectionné : <c:out value="${employee_name}"/> <br>
                                            Questionnaire sélectionné : <c:out value="${questionnary_name}"/>
                                        </h5>
                                        <div class="col-md-12">
                                            <div>
                                                <c:if test="${isDisplayDiagrammes}">
                                                    <h2>Score de risque</h2>
                                                    <div class="alert alert-info" role="alert">
                                                        Score du salarié est :
                                                        <c:out value="${scoreEmployee}"></c:out>
                                                    </div>
                                                    <div id="graphique" style="margin: auto; margin-top: 35px;">
                                                        <c:if test="${questionnary_reference_exists}">
                                                            <script>
                                                                var tab = new Array();
                                                            </script>
                                                            <c:forEach items="${liste_score}" var="val">
                                                                <script>
                                                                    tab.push(${val});
                                                                </script>
                                                            </c:forEach>
                                                            <script>drawHighcharts(${scoreEmployee})</script>
                                                        </c:if>
                                                    </div>
                                                    <br/>
                                                </c:if>
                                            </div>

                                            <c:forEach var="question" items="${questionsList}">
                                                <div class="questionCard mb-1">
                                                    <div class="container-fluid">
                                                        <div class="row">
                                                            <span class="questionType">Question : ${question.name}</span>
                                                        </div>
                                                        <div class="row">
                                                            <c:choose>
                                                                <c:when test="${(question.type ==\"CHECKBOX\") || (question.type ==\"RADIO\") || (question.type ==\"SELECT\")}">
                                                                    <c:forEach var="responseOptionEntry"
                                                                               items="${responsesOptionMap}">
                                                                        <c:if test="${responseOptionEntry.key == question.position}">
                                                                            <span class="questionType"> Réponse :
                                                                                <c:forEach var="responseOption"
                                                                                           items="${responseOptionEntry.value}">
                                                                                    ${responseOption.value}
                                                                                </c:forEach>
                                                                            </span>
                                                                        </c:if>
                                                                    </c:forEach>
                                                                </c:when>
                                                                <c:otherwise>
                                                                    <c:forEach var="responseString"
                                                                               items="${responsesStringMap}">
                                                                        <c:if test="${responseString.key == question.position}">
                                                                            <span class="questionType"> Réponse : ${responseString.value}</span>
                                                                        </c:if>
                                                                    </c:forEach>
                                                                </c:otherwise>
                                                            </c:choose>
                                                        </div>
                                                    </div>

                                                    <c:if test="${(question.type ==\"CHECKBOX\") || (question.type ==\"RADIO\") || (question.type ==\"SELECT\")}">
                                                        <c:forEach var="responseOption" items="${responsesOptionMap}">
                                                            <c:if test="${responseOption.key == question.position}">
                                                                <script>
                                                                    question = {
                                                                        name: "${question.name}",
                                                                        position: parseInt("${question.position}"),
                                                                        variableRef: "${question.variable}",
                                                                        seuil: parseInt("${question.seuil}"),
                                                                        valeurVariableRefRespEmpl: [
                                                                            <c:forEach var="value" items="${responseOption.value}">
                                                                                parseInt("${value.valeur}"),
                                                                            </c:forEach>
                                                                        ],
                                                                        options: [
                                                                            <c:forEach var="option" items="${question.questionOptions}">
                                                                            {
                                                                                intitule: "${option.value}",
                                                                                position: parseInt("${option.position}"),
                                                                                valeurVariableRef: parseInt("${option.valeur}"),
                                                                                <c:forEach var="percentage" items="${optionsPercentages}">
                                                                                    <c:if test="${percentage.key == option.id}">
                                                                                        pourcentageVariableRef: parseInt("${percentage.value}")
                                                                                    </c:if>
                                                                                </c:forEach>
                                                                            },
                                                                            </c:forEach>
                                                                        ]
                                                                    }
                                                                </script>
                                                                <div id="graphQuestion_${question.position}">
                                                                    <script>drawChartForQuestion(${question.position})</script>
                                                                </div>
                                                            </c:if>
                                                        </c:forEach>
                                                    </c:if>

                                                </div>
                                            </c:forEach>
                                        </div>
                                    </c:otherwise>
                                </c:choose>

                                <c:if test="${!isDisplayDiagrammes}">

                                    <div class="col-md-4" style="padding-top: 8px;">
                                        <br> <input type="submit" class="btn btn-info" value="Valider"
                                                    style="float: left;">
                                    </div>
                                </c:if>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
<div id="test"></div>
<c:import url="/WEB-INF/jsp/librairies/footer.jsp"/>
</body>
</html>

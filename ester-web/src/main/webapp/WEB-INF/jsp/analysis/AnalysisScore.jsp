<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ page import="fr.univangers.masterinfo.ester.web.servlet.analysis.AnalysisScoreServlet" %>
<%@ page import="fr.univangers.masterinfo.ester.service.form.analysis.AccessWebSiteAnalysisForm" %>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib tagdir="/WEB-INF/tags" prefix="ester" %>
<!DOCTYPE html>
<html lang="fr-FR" class="min-vh-100">
<head>
    <title>ESTER - Analyses des résultats</title>
    <c:import url="/WEB-INF/jsp/librairies/header.jsp"/>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
    <script src="https://code.highcharts.com/highcharts.js"></script>
    <script src="<c:url value="/public/js/analysis/AnalysisScore.js"/>"></script>
</head>
<body class="d-flex flex-column min-vh-100">
<c:import url="/WEB-INF/jsp/librairies/navbar.jsp"/>
<main>
    <div class="container-fluid min-vh-100">
        <div class="row menu_gauche">
            <div class="col-md-8 container mt-3 m_shadow" style="background-color: white" id="container">
                <div class="card-body center_block">

                    <div class="row col-md-12 d-flex justify-content-center mt-xl-2">
                        <h2 style="border-bottom: solid 2px #127ba3" class="mb-lg-5">
                            Analyse sur le score des données de références
                        </h2>
                    </div>
                    <div class="col-md-6" style="margin: auto; margin-top: 35px;">
                        <form action="<c:url value="${AnalysisScoreServlet.ACCESS_WEB_SITE_ANALYSIS_URL}"/>"
                              method="post">


                            <div class="row">
                                <label for="Reference_Select" style="text-align: center;">
                                    <b>selectionnez la base de référence</b>
                                </label>
                                <select id="Reference_Select"
                                        name="${AccessWebSiteAnalysisForm.SELECT_REFERENCE}"
                                        class="custom-select">
                                    <c:forEach items="${accessWebSiteAnalysisForm.references}" var="r">
                                        <option value="${r.id}">${r.name}</option>
                                    </c:forEach>
                                </select>
                                <input type="submit" class="btn btn-md btn-primary" value="valider" id="submit">
                            </div>

                        </form>
                    </div>
                    <div id="graphique" style="margin: auto; margin-top: 35px;">
                        <c:if test="${accessWebSiteAnalysisForm.selectReference !=null}">
                            <script>
                                var tab = new Array();
                            </script>
                            <c:forEach items="${liste}" var="val">
                                <script>
                                    tab.push(${val});
                                </script>
                            </c:forEach>
                            <script>drawHighcharts()</script>
                        </c:if>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
<c:import url="/WEB-INF/jsp/librairies/footer.jsp"/>
</body>
</html>

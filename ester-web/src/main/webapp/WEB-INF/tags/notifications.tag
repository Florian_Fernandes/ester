<%@ tag language="java" pageEncoding="UTF-8" %>
<%@ tag import="fr.univangers.masterinfo.ester.service.form.notification.NotificationLevel" %>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<%@ attribute name="messages" required="true" rtexprvalue="true" type="java.util.List" %>

<c:if test="${not empty messages}">

    <c:forEach items="${messages}" var="notify">
        <c:if test="${notify.level == NotificationLevel.SUCCESS}">
            <div class="alert alert-success alert-dismissible" role="alert">
                <strong>Succès : </strong> <c:out value="${notify.message}"/>
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        </c:if>

        <c:if test="${notify.level == NotificationLevel.WARNING}">
            <div class="alert alert-warning alert-dismissible" role="alert">
                <strong>Attention : </strong> <c:out value="${notify.message}"/>
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        </c:if>

        <c:if test="${notify.level == NotificationLevel.ERROR}">
            <div class="alert alert-danger alert-dismissible" role="alert">
                <strong>Erreur : </strong> <c:out value="${notify.message}"/>
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        </c:if>
    </c:forEach>

</c:if>
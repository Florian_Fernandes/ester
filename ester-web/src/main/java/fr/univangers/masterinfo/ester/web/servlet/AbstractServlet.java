package fr.univangers.masterinfo.ester.web.servlet;

import fr.univangers.masterinfo.ester.dao.bean.UserBean;
import fr.univangers.masterinfo.ester.dao.bean.UserRole;
import fr.univangers.masterinfo.ester.dao.exception.EsterDaoException;
import fr.univangers.masterinfo.ester.service.contract.AccountServiceContract;
import fr.univangers.masterinfo.ester.service.contract.ValidatorServiceContract;
import fr.univangers.masterinfo.ester.service.exception.EsterServiceException;
import fr.univangers.masterinfo.ester.service.form.AbstractForm;
import fr.univangers.masterinfo.ester.utils.ReflectUtils;
import fr.univangers.masterinfo.ester.web.exception.EsterWebException;
import fr.univangers.masterinfo.ester.web.utils.FormUtils;
import fr.univangers.masterinfo.ester.web.utils.SessionUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Exception pour la couche WEB
 *
 * @param <T> le formulaire associé à la servlet
 * @version 1.0
 * @date 04/10/2020
 */
public abstract class AbstractServlet<T extends AbstractForm> extends HttpServlet {

    /**
     * Serial Version UID
     */
    private static final long serialVersionUID = 1L;

    /**
     * Le chemin de la JSP si une erreur s'est produite dans le serveur
     */
    private static final String ERROR_JSP = "/WEB-INF/jsp/librairies/error.jsp";


    /**
     * Log de la classe
     */
    private static final Logger LOG = LogManager.getLogger(AbstractServlet.class);

    /**
     * Le classe qui permet de charger un formulaire
     */
    protected Class<T> classForm;

    /**
     * Service de validation (formulaire, etc.)
     */
    @Autowired
    protected ValidatorServiceContract validatorService;
    /**
     * Service gestion utilisateur
     */
    @Autowired
    private AccountServiceContract accountService;

    /**
     * @param classForm
     */
    public AbstractServlet(final Class<T> classForm) {
        this.classForm = classForm;
    }

    /**
     * Initialisation de la page
     *
     * @param form
     * @param request
     * @param response
     * @return la JSP à rediriger
     * @throws EsterWebException
     */
    protected abstract String doGet(T form, HttpServletRequest request,
                                    HttpServletResponse response) throws EsterWebException, EsterDaoException;

    /**
     * Traitement de la page
     *
     * @param form
     * @param request
     * @param response
     * @return la JSP à rediriger
     * @throws EsterWebException
     */
    protected abstract String doPost(T form, HttpServletRequest request,
                                     HttpServletResponse response) throws EsterWebException, EsterDaoException, EsterServiceException;

    /**
     * On injecte tous les composants nécessaires pour répondre aux besoins grâce à Spring
     */
    @Override
    public void init() throws ServletException {
        final WebApplicationContext context = WebApplicationContextUtils
                .getWebApplicationContext(this.getServletContext());
        context.getAutowireCapableBeanFactory().autowireBean(this);
    }

    @Override
    protected void doGet(final HttpServletRequest request, final HttpServletResponse response)
            throws ServletException, IOException {
        try {
            final T form = ReflectUtils.newInstance(this.classForm);

            form.setSessionUser(SessionUtils.getSessionUser(request));
            FormUtils.requestToForm(request, form);

            final String dispatch = this.doGet(form, request, response);

            if (dispatch != null) {
                SessionUtils.setSessionUser(request, form.getSessionUser());
                FormUtils.formToRequest(request, form);

                request.getRequestDispatcher(dispatch).forward(request, response);
            }
        } catch (final EsterWebException e) {
            LOG.error(e.getMessage(), e);
            try {
                if (e.hasHelperMessage()) {
                    request.setAttribute("errorMsg", e.getHelperMessage());
                }
                request.getRequestDispatcher(ERROR_JSP).forward(request, response);
            } catch (final Exception e2) {
                LOG.error(e.getMessage(), e2);
            }
        } catch (final Exception e) {
            LOG.error(e.getMessage(), e);
            try {
                request.getRequestDispatcher(ERROR_JSP).forward(request, response);
            } catch (final Exception e2) {
                LOG.error(e.getMessage(), e2);
            }
        }
    }

    @Override
    protected void doPost(final HttpServletRequest request, final HttpServletResponse response)
            throws ServletException, IOException {
        try {
            final T form = ReflectUtils.newInstance(this.classForm);

            form.setSessionUser(SessionUtils.getSessionUser(request));
            FormUtils.requestToForm(request, form);

            final String dispatch = this.doPost(form, request, response);

            if (dispatch != null) {
                SessionUtils.setSessionUser(request, form.getSessionUser());
                FormUtils.formToRequest(request, form);

                request.getRequestDispatcher(dispatch).forward(request, response);
            }
        } catch (final EsterWebException e) {
            LOG.error(e.getMessage(), e);
            try {
                if (e.hasHelperMessage()) {
                    request.setAttribute("errorMsg", e.getHelperMessage());
                }
                request.getRequestDispatcher(ERROR_JSP).forward(request, response);
            } catch (final Exception e2) {
                LOG.error(e.getMessage(), e2);
            }
        } catch (final Exception e) {
            LOG.error(e.getMessage(), e);
            try {
                request.getRequestDispatcher(ERROR_JSP).forward(request, response);
            } catch (final Exception e2) {
                LOG.error(e.getMessage(), e2);
            }
        }
    }

    public void handleAuthUser(final HttpServletRequest request, final HttpServletResponse response, UserRole minRole) throws EsterWebException {
        UserBean userBean = (UserBean) request.getSession().getAttribute("sessionUser");
        String checkIfUserCanAccess = accountService.checkIfUserCanAccess(userBean, minRole);
        if (checkIfUserCanAccess != null) {
            LOG.info("checkIfUserCanAccess{}", checkIfUserCanAccess);
            try {
                request.getRequestDispatcher(checkIfUserCanAccess).forward(request, response);
            } catch (Exception e) {
                throw new EsterWebException(e.getMessage());
            }
        }
    }


}

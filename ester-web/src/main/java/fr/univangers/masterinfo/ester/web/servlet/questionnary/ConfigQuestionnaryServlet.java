package fr.univangers.masterinfo.ester.web.servlet.questionnary;


import fr.univangers.masterinfo.ester.dao.bean.MemberEsterBean;
import fr.univangers.masterinfo.ester.dao.bean.QuestionnaryBean;
import fr.univangers.masterinfo.ester.dao.bean.UserRole;
import fr.univangers.masterinfo.ester.dao.contract.QuestionnaryDaoContract;
import fr.univangers.masterinfo.ester.service.contract.QuestionnaryServiceContract;
import fr.univangers.masterinfo.ester.service.exception.EsterServiceException;
import fr.univangers.masterinfo.ester.service.form.questionnary.ConfigQuestionnaryForm;
import fr.univangers.masterinfo.ester.web.exception.EsterWebException;
import fr.univangers.masterinfo.ester.web.servlet.AbstractServlet;
import fr.univangers.masterinfo.ester.web.servlet.connection.DashboardConnectionServlet;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

@WebServlet(loadOnStartup = 1, urlPatterns = {fr.univangers.masterinfo.ester.web.servlet.questionnary.ConfigQuestionnaryServlet.CONFIG_QUESTIONNARY_URL})
public class ConfigQuestionnaryServlet extends AbstractServlet<ConfigQuestionnaryForm> {

    /**
     * Le chemin de la JSP pour afficher la page de configuration des questionnaires
     */
    public static final String CONFIG_QUESTIONNARY_JSP = "/WEB-INF/jsp/questionnary/ConfigQuestionnary.jsp";
    /**
     * L'URL de la configuration des questionnaires
     */
    public static final String CONFIG_QUESTIONNARY_URL = DashboardConnectionServlet.DASHBOARD_CONNECTION_URL + "/configuration-questionnaires";
    /**
     * Serial Version UID
     */
    private static final long serialVersionUID = 1L;
    /**
     * LOG de la classe
     */
    private static final Logger LOG = LogManager.getLogger(ConfigQuestionnaryServlet.class);

    /**
     * Service pour récuperer les questionnaires
     */
    @Autowired
    private QuestionnaryServiceContract questionnaryService;
    /**
     * Service pour trouver et supprimer
     */
    private QuestionnaryDaoContract questionnaryDao;

    /**
     * Constructeur
     */
    public ConfigQuestionnaryServlet() {
        super(ConfigQuestionnaryForm.class);
    }

    @Override
    protected String doGet(ConfigQuestionnaryForm form, HttpServletRequest request, HttpServletResponse response) throws EsterWebException {

        this.handleAuthUser(request, response, UserRole.PLURIDISCIPLINAIRE);

        try {

            final List<QuestionnaryBean> questionnaire;
            if (form.getSessionUser().getRole() == UserRole.ADMIN) {
                questionnaire = this.questionnaryService.findAll();
            } else {
                questionnaire = this.questionnaryService.findJustMember((MemberEsterBean) form.getSessionUser());
            }
            form.setQuestionnaries(questionnaire);
        } catch (final EsterServiceException e) {
            throw new EsterWebException(e.getMessage(), e.getHelperMessage());
        }

        return CONFIG_QUESTIONNARY_JSP;
    }

    @Override
    protected String doPost(ConfigQuestionnaryForm form, HttpServletRequest request, HttpServletResponse response) throws EsterWebException {

        this.handleAuthUser(request, response, UserRole.PLURIDISCIPLINAIRE);

        final String action = request.getParameter("action");
        final String idQuestionnary = request.getParameter("idQuestionnary");

        try {
            if (idQuestionnary != null) {
                if (action.equals("publier")) {
                    questionnaryService.publishQuestionnaryById(idQuestionnary);

                }
                if (action.equals("remove")) {
                    questionnaryService.deleteQuestionnaryById(idQuestionnary);

                }
                //gestion de cas ou un member ester voit que les questionnaire que il a droit
                final List<QuestionnaryBean> questionnaire;
                if (form.getSessionUser().getRole() == UserRole.ADMIN)
                    questionnaire = this.questionnaryService.findAll();
                else
                    questionnaire = this.questionnaryService.findJustMember((MemberEsterBean) form.getSessionUser());
                form.setQuestionnaries(questionnaire);
                return CONFIG_QUESTIONNARY_JSP;
            } else {
                QuestionnaryBean questionnaryForShar = questionnaryService.saveOrUpdateQuestionnary(request);
                questionnaryService.shareQuestionnaryToMemberEsterOnCreation(questionnaryForShar.getId(), form.getSessionUser().getId());
                //gestion de cas ou un member ester voit que les questionnaire que il a droit
                final List<QuestionnaryBean> questionnaires;
                if (form.getSessionUser().getRole() == UserRole.ADMIN) {
                    questionnaires = this.questionnaryService.findAll();
                } else {
                    questionnaires = this.questionnaryService.findJustMember((MemberEsterBean) form.getSessionUser());
                }

                form.setQuestionnaries(questionnaires);
            }
        } catch (EsterServiceException e) {
            throw new EsterWebException(e.getMessage(), e.getHelperMessage());
        }
        return CONFIG_QUESTIONNARY_JSP;
    }
}
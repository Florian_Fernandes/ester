package fr.univangers.masterinfo.ester.web.servlet.account;

import fr.univangers.masterinfo.ester.dao.bean.UserRole;
import fr.univangers.masterinfo.ester.service.contract.AccountServiceContract;
import fr.univangers.masterinfo.ester.service.contract.MailServiceContract;
import fr.univangers.masterinfo.ester.service.exception.EsterServiceException;
import fr.univangers.masterinfo.ester.service.form.account.CreateAccountForm;
import fr.univangers.masterinfo.ester.web.exception.EsterWebException;
import fr.univangers.masterinfo.ester.web.servlet.AbstractServlet;
import fr.univangers.masterinfo.ester.web.servlet.connection.DashboardConnectionServlet;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @author etudiant
 */
@WebServlet(loadOnStartup = 1, urlPatterns = {CreateAccountServlet.CREATE_ACCOUNT_URL})
public class CreateAccountServlet extends AbstractServlet<CreateAccountForm> {

    /**
     * L'URL de création d'un compte
     */
    public static final String CREATE_ACCOUNT_URL = DashboardConnectionServlet.DASHBOARD_CONNECTION_URL
            + "/creation-compte";
    /**
     * Serial Version UID
     */
    private static final long serialVersionUID = 1L;
    /**
     * Log de la classe
     */
    private static final Logger LOG = LogManager.getLogger(CreateAccountServlet.class);
    /**
     * Le chemin de la JSP pour afficher la page de création d'un compte
     */
    private static final String CREATE_ACCOUNT_JSP = "/WEB-INF/jsp/account/createAccount.jsp";

    /**
     * Service gestion utilisateur
     */
    @Autowired
    private AccountServiceContract accountService;

    /**
     * Service gestion des mails
     */
    @Autowired
    private MailServiceContract mailSender;

    /**
     * Constructeur
     */
    public CreateAccountServlet() {
        super(CreateAccountForm.class);
    }

    @Override
    protected String doGet(final CreateAccountForm form, final HttpServletRequest request,
                           final HttpServletResponse response) throws EsterWebException {
        try {

            this.handleAuthUser(request, response, UserRole.PLURIDISCIPLINAIRE);

            final UserRole[] roles = this.accountService.getRoles(form);
            form.setRoles(roles);

            return CREATE_ACCOUNT_JSP;
        } catch (final EsterServiceException e) {
            throw new EsterWebException(e.getMessage(), e.getHelperMessage());
        }
    }

    @Override
    protected String doPost(final CreateAccountForm form, final HttpServletRequest request,
                            final HttpServletResponse response) throws EsterWebException {
        try {

            this.handleAuthUser(request, response, UserRole.PLURIDISCIPLINAIRE);

            this.doGet(form, request, response);

            if (form.getSelectedRole() == UserRole.SALARIE) {
                this.accountService.registerUser(form);
            } else if (!this.accountService.checkEmailExist(form)) {
                this.accountService.registerUser(form);
                this.mailSender.sendCreateAccountAsync(form);
            }

            return CREATE_ACCOUNT_JSP;
        } catch (final EsterServiceException e) {
            throw new EsterWebException(e.getMessage(), e.getHelperMessage());
        }
    }
}

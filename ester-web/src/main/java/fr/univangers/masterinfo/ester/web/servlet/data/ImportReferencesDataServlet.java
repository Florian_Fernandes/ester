package fr.univangers.masterinfo.ester.web.servlet.data;

import fr.univangers.masterinfo.ester.dao.bean.QuestionnaryBean;
import fr.univangers.masterinfo.ester.dao.bean.ReferenceBean;
import fr.univangers.masterinfo.ester.dao.bean.UserRole;
import fr.univangers.masterinfo.ester.service.contract.DataServiceContract;
import fr.univangers.masterinfo.ester.service.contract.QuestionnaryServiceContract;
import fr.univangers.masterinfo.ester.service.exception.EsterServiceException;
import fr.univangers.masterinfo.ester.service.form.data.ImportReferencesDataForm;
import fr.univangers.masterinfo.ester.service.form.notification.NotificationLevel;
import fr.univangers.masterinfo.ester.web.exception.EsterWebException;
import fr.univangers.masterinfo.ester.web.servlet.AbstractServlet;
import fr.univangers.masterinfo.ester.web.servlet.connection.DashboardConnectionServlet;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * @author etudiant
 */
@WebServlet(loadOnStartup = 1, urlPatterns = {
        ImportReferencesDataServlet.IMPORT_REFERENCES_DATA_URL})
@MultipartConfig
public class ImportReferencesDataServlet extends AbstractServlet<ImportReferencesDataForm> {

    /**
     * L'URL d'import des données de références
     */
    public static final String IMPORT_REFERENCES_DATA_URL = DashboardConnectionServlet.DASHBOARD_CONNECTION_URL
            + "/import-donnees-references";
    /**
     * Serial Version UID
     */
    private static final long serialVersionUID = 1L;
    /**
     * Log de la classe
     */
    private static final Logger LOG = LogManager.getLogger(ImportReferencesDataServlet.class);
    /**
     * Le chemin de la JSP pour afficher la page d'import des données de références
     */
    private static final String IMPORT_REFERENCES_DATA_JSP = "/WEB-INF/jsp/data/importReferencesData.jsp";

    /**
     * Le service questionnaire
     */
    @Autowired
    private QuestionnaryServiceContract questionnaryService;

    @Autowired
    private DataServiceContract referenceService;

    /**
     * Constructeur
     */
    public ImportReferencesDataServlet() {
        super(ImportReferencesDataForm.class);
    }

    @Override
    protected String doGet(final ImportReferencesDataForm form, final HttpServletRequest request,
                           final HttpServletResponse response) throws EsterWebException {

        this.handleAuthUser(request, response, UserRole.ADMIN);

        try {
            final List<QuestionnaryBean> questionnariesObject = this.questionnaryService.findAll();


            form.setQuestionnaries(questionnariesObject);

            final List<ReferenceBean> referencesObject = this.referenceService.findAll();


            for (final ReferenceBean b : referencesObject) {

            }
            form.setReferences(referencesObject);

            return IMPORT_REFERENCES_DATA_JSP;
        } catch (final EsterServiceException e) {
            throw new EsterWebException(e.getMessage(), e.getHelperMessage());
        }
    }

    @Override
    protected String doPost(final ImportReferencesDataForm form, final HttpServletRequest request,
                            final HttpServletResponse response) throws EsterWebException {

        this.handleAuthUser(request, response, UserRole.ADMIN);

        final String routePathServer = request.getServletContext().getRealPath("/");
        form.setRoutePathServer(routePathServer);

        // On récupère toutes les références de la base de données
        List<ReferenceBean> liste_references;

        try {
            liste_references = this.referenceService.findAll();
        } catch (final EsterServiceException e) {
            throw new EsterWebException(e.getMessage(), e.getHelperMessage());
        }

        // On regarde si le nom de la BDD rentrée est déjà utilisé ou non
        boolean exist_reference = false;

        for (ReferenceBean reference : liste_references) {
            if ((form.getNomBDD().toLowerCase()).equals(reference.getName())) {
                exist_reference = true;
                break;
            }
        }

        if (!exist_reference && (form.isValid())) {
            try {
                this.referenceService.addReference(form);
                final String msg = "Le fichier " + form.getNomBDD() + " a bien été importé";
                form.notify(msg, NotificationLevel.SUCCESS);
            } catch (final EsterServiceException e) {
                throw new EsterWebException(e.getMessage(), e.getHelperMessage());
            }
        } else {
            final String msg = "Le fichier " + form.getNomBDD() + " existe déjà";
            form.notify(msg, NotificationLevel.ERROR);
        }

        this.doGet(form, request, response);

        return IMPORT_REFERENCES_DATA_JSP;
    }
}

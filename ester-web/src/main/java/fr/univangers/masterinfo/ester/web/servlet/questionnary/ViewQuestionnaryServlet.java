package fr.univangers.masterinfo.ester.web.servlet.questionnary;

import fr.univangers.masterinfo.ester.dao.bean.QuestionnaryBean;
import fr.univangers.masterinfo.ester.dao.bean.UserRole;
import fr.univangers.masterinfo.ester.dao.contract.QuestionnaryDaoContract;
import fr.univangers.masterinfo.ester.dao.exception.EsterDaoException;
import fr.univangers.masterinfo.ester.service.form.questionnary.visualiserQuestionnaryForm;
import fr.univangers.masterinfo.ester.web.exception.EsterWebException;
import fr.univangers.masterinfo.ester.web.servlet.AbstractServlet;
import fr.univangers.masterinfo.ester.web.servlet.connection.DashboardConnectionServlet;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * servlet pour la page de visualisation d'un questionaire
 */
@WebServlet(loadOnStartup = 1, urlPatterns = {ViewQuestionnaryServlet.VISUALISER_QUESTIONNARY_URL})
public class ViewQuestionnaryServlet extends AbstractServlet<visualiserQuestionnaryForm> {

    /**
     * L'URL de la page de visualisation d'un questionaire
     */
    public static final String VISUALISER_QUESTIONNARY_URL = DashboardConnectionServlet.DASHBOARD_CONNECTION_URL
            + "/visualiserQuestionnary";
    /**
     * Serial Version UID
     */
    private static final long serialVersionUID = 1L;
    /**
     * Log de la classe
     */
    private static final Logger LOG = LogManager.getLogger(ViewQuestionnaryServlet.class);
    /**
     * Le chemin de la JSP pour afficher la page de visualisation d'un questionaire
     */
    private static final String VISUALISER_QUESTIONNARY_JSP = "/WEB-INF/jsp/questionnary/visualiserQuestionnary.jsp";

    @Autowired
    private QuestionnaryDaoContract questionnaryDao;

    /**
     * Constructeur
     */
    public ViewQuestionnaryServlet() {
        super(visualiserQuestionnaryForm.class);
    }

    @Override
    protected String doGet(final visualiserQuestionnaryForm form, final HttpServletRequest request,
                           final HttpServletResponse response) throws EsterWebException {

        this.handleAuthUser(request, response, UserRole.PLURIDISCIPLINAIRE);

        String idQuestionnary = request.getParameter("idQuestionnary");

        try {
            QuestionnaryBean visualiserQuestionnary = questionnaryDao.read(idQuestionnary);
            form.setQuestionnaryName(visualiserQuestionnary.getName());
            form.setQuestions(visualiserQuestionnary.getQuestions());
            return VISUALISER_QUESTIONNARY_JSP;
        } catch (final EsterDaoException e) {
            throw new EsterWebException(e.getMessage(), e.getHelperMessage());
        } catch (final Exception e) {
            throw new EsterWebException(e.getMessage());
        }
    }

    @Override
    protected String doPost(final visualiserQuestionnaryForm form, final HttpServletRequest request,
                            final HttpServletResponse response) throws EsterWebException {

        this.handleAuthUser(request, response, UserRole.PLURIDISCIPLINAIRE);

        return null;
    }
}


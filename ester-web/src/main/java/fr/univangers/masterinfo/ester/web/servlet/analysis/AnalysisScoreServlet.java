package fr.univangers.masterinfo.ester.web.servlet.analysis;

import fr.univangers.masterinfo.ester.dao.bean.ReferenceBean;
import fr.univangers.masterinfo.ester.dao.bean.UserRole;
import fr.univangers.masterinfo.ester.service.contract.AnalysisServiceContract;
import fr.univangers.masterinfo.ester.service.contract.DataServiceContract;
import fr.univangers.masterinfo.ester.service.exception.EsterServiceException;
import fr.univangers.masterinfo.ester.service.form.analysis.AccessWebSiteAnalysisForm;
import fr.univangers.masterinfo.ester.web.exception.EsterWebException;
import fr.univangers.masterinfo.ester.web.servlet.AbstractServlet;
import fr.univangers.masterinfo.ester.web.servlet.connection.DashboardConnectionServlet;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * @author etudiant
 */
@WebServlet(loadOnStartup = 1, urlPatterns = {
        AnalysisScoreServlet.ACCESS_WEB_SITE_ANALYSIS_URL})
public class AnalysisScoreServlet extends AbstractServlet<AccessWebSiteAnalysisForm> {


    /**
     * L'URL permettant d'accéder aux statistiques simples de la plateforme (nombre
     * de connexions, durée, fréquence de connexion, etc.)
     */
    public static final String ACCESS_WEB_SITE_ANALYSIS_URL = DashboardConnectionServlet.DASHBOARD_CONNECTION_URL
            + "/analyses-scores";
    /**
     * Serial Version UID
     */
    private static final long serialVersionUID = 1L;
    /**
     * Log de la classe
     */
    private static final Logger LOG = LogManager.getLogger(AnalysisScoreServlet.class);
    /**
     * Le chemin de la JSP permettant d'accéder aux statistiques simples de la
     * plateforme (nombre de connexions, durée, fréquence de connexion, etc.)
     */
    private static final String ACCESS_WEB_SITE_ANALYSIS_JSP = "/WEB-INF/jsp/analysis/AnalysisScore.jsp";
    /**
     * Service pour récuperer les scores
     */
    @Autowired
    private AnalysisServiceContract analysisService;
    /**
     * Service Reference
     */
    @Autowired
    private DataServiceContract referenceService;


    /**
     * Constructeur
     */
    public AnalysisScoreServlet() {
        super(AccessWebSiteAnalysisForm.class);
    }

    @Override
    protected String doGet(final AccessWebSiteAnalysisForm form, final HttpServletRequest request,
                           final HttpServletResponse response) throws EsterWebException {

        this.handleAuthUser(request, response, UserRole.PLURIDISCIPLINAIRE);

        try {

            final List<ReferenceBean> referencesObject = this.referenceService.findAll();
            form.setReferences(referencesObject);

            return ACCESS_WEB_SITE_ANALYSIS_JSP;

        } catch (final EsterServiceException e) {
            throw new EsterWebException(e.getMessage());
        }
    }

    @Override
    protected String doPost(final AccessWebSiteAnalysisForm form, final HttpServletRequest request,
                            final HttpServletResponse response) throws EsterWebException {

        this.handleAuthUser(request, response, UserRole.PLURIDISCIPLINAIRE);

        this.doGet(form, request, response);
        try {

            String referenceId = request.getParameter("selectReference");
            ReferenceBean referenceBean = referenceService.findReferenceById(referenceId);
            List<Long> listScores = analysisService.getlistScoreResults(referenceBean.getName(), referenceBean.getVariables());
            request.setAttribute("liste", listScores);

            return ACCESS_WEB_SITE_ANALYSIS_JSP;

        } catch (final EsterServiceException e) {
            throw new EsterWebException(e.getMessage(), e.getHelperMessage());
        }
    }
}

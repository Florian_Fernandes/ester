package fr.univangers.masterinfo.ester.web.servlet.questionnary;

import fr.univangers.masterinfo.ester.dao.bean.EmployeeBean;
import fr.univangers.masterinfo.ester.dao.bean.MemberEsterBean;
import fr.univangers.masterinfo.ester.dao.bean.QuestionnaryBean;
import fr.univangers.masterinfo.ester.dao.bean.UserRole;
import fr.univangers.masterinfo.ester.service.contract.AccountServiceContract;
import fr.univangers.masterinfo.ester.service.contract.QuestionnaryServiceContract;
import fr.univangers.masterinfo.ester.service.exception.EsterServiceException;
import fr.univangers.masterinfo.ester.service.form.questionnary.AssignQuestionnaryForm;
import fr.univangers.masterinfo.ester.web.exception.EsterWebException;
import fr.univangers.masterinfo.ester.web.servlet.AbstractServlet;
import fr.univangers.masterinfo.ester.web.servlet.connection.DashboardConnectionServlet;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.*;

/**
 * @author etudiant
 */
@WebServlet(loadOnStartup = 1, urlPatterns = {AssignQuestionnaryServlet.ASSIGN_QUESTIONNARY_URL})
public class AssignQuestionnaryServlet extends AbstractServlet<AssignQuestionnaryForm> {

    /**
     * L'URL pour affecter des questionnaires aux salariés
     */
    public static final String ASSIGN_QUESTIONNARY_URL = DashboardConnectionServlet.DASHBOARD_CONNECTION_URL
            + "/affecter-questionnaires";
    /**
     * Serial Version UID
     */
    private static final long serialVersionUID = 1L;
    /**
     * Log de la classe
     */
    private static final Logger LOG = LogManager.getLogger(AssignQuestionnaryServlet.class);
    /**
     * Le chemin de la JSP pour affecter des questionnaires aux salariés
     */
    private static final String ASSIGN_QUESTIONNARY_JSP = "/WEB-INF/jsp/questionnary/assignQuestionnary.jsp";

    /**
     * Service pour récuperer les questionnaires
     */
    @Autowired
    private QuestionnaryServiceContract questionnaryService;

    /**
     * Service pour récuperer les salariés
     */
    @Autowired
    private AccountServiceContract accountService;

    /**
     * Constructeur
     */
    public AssignQuestionnaryServlet() {
        super(AssignQuestionnaryForm.class);
    }

    @Override
    protected String doGet(final AssignQuestionnaryForm form, final HttpServletRequest request,
                           final HttpServletResponse response) throws EsterWebException {


        this.handleAuthUser(request, response, UserRole.PLURIDISCIPLINAIRE);

        try {

            final List<QuestionnaryBean> questionnaries;

            if (form.getSessionUser().getRole() == UserRole.ADMIN) {
                questionnaries = this.questionnaryService.findJustPublished();
            } else {
                questionnaries = this.questionnaryService.findJustPublishedandMember((MemberEsterBean) form.getSessionUser());
            }

            final List<EmployeeBean> employees = this.accountService.findAll();
            Collections.sort(employees);


            request.setAttribute("employeeMapping", this.accountService.getMappingEmployee(employees));

            form.setQuestionnaries(questionnaries);
            form.setEmployees(employees);

            return ASSIGN_QUESTIONNARY_JSP;

        } catch (final EsterServiceException e) {
            throw new EsterWebException(e.getMessage(), e.getHelperMessage());
        }
    }

    @Override
    protected String doPost(final AssignQuestionnaryForm form, final HttpServletRequest request,
                            final HttpServletResponse response) throws EsterWebException, EsterServiceException {

        this.handleAuthUser(request, response, UserRole.PLURIDISCIPLINAIRE);

        this.doGet(form, request, response);

        try {
            questionnaryService.assignQuestionnaryToEmployee(form);

            final List<EmployeeBean> employees = this.accountService.findAll();
            Collections.sort(employees);
            request.setAttribute("employeeMapping", this.accountService.getMappingEmployee(employees));

            return ASSIGN_QUESTIONNARY_JSP;
        } catch (final EsterServiceException e) {
            throw new EsterWebException(e.getMessage(), e.getHelperMessage());
        }
    }
}

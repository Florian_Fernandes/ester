package fr.univangers.masterinfo.ester.web.servlet.connection;

import fr.univangers.masterinfo.ester.dao.bean.MemberEsterBean;
import fr.univangers.masterinfo.ester.dao.contract.MemberEsterDaoContract;
import fr.univangers.masterinfo.ester.dao.criteria.MemberEsterCriteria;
import fr.univangers.masterinfo.ester.dao.exception.EsterDaoException;
import fr.univangers.masterinfo.ester.service.form.connection.ForgotPasswordConnectionForm;
import fr.univangers.masterinfo.ester.service.form.notification.NotificationLevel;
import fr.univangers.masterinfo.ester.web.exception.EsterWebException;
import fr.univangers.masterinfo.ester.web.servlet.AbstractServlet;
import freemarker.template.Configuration;
import freemarker.template.Template;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.ui.freemarker.FreeMarkerTemplateUtils;

import javax.mail.internet.MimeMessage;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;

/**
 * Page pour se connecter au site
 *
 * @version 1.0
 * @date 04/10/2020
 */
@WebServlet(loadOnStartup = 1, urlPatterns = {
        ForgotPasswordConnectionServlet.FORGOT_PASSWORD_CONNECTION_URL})
public class ForgotPasswordConnectionServlet extends AbstractServlet<ForgotPasswordConnectionForm> {

    /**
     * L'URL de connexion
     */
    public static final String FORGOT_PASSWORD_CONNECTION_URL = "/mot-passe-oublie";
    /**
     * Serial Version UID
     */
    private static final long serialVersionUID = 1L;
    /**
     * Le chemin de la JSP pour afficher la page de connexion
     */
    private static final String FORGOT_PASSWORD_CONNECTION_JSP = "/WEB-INF/jsp/connection/forgotPasswordConnection.jsp";
    /**
     * Template associé à la récupération d'un mot de passe
     */
    private static final String TEMPLATE_FORGOT_PASSWORD = "mail-forgot-password-account.html";
    /**
     * Sujet du mail de récupération de mot de passe
     */
    private static final String SUBJECT_FORGOT_PASSWORD = "[ESTER] - Récupération de mot de passe";
    /**
     * Objet permettant de récupérer le compte Ester lié à l'adresse mail
     */
    @Autowired
    private MemberEsterDaoContract memberEsterDao;
    /**
     * Objet qui permet de charger un fichier template
     */
    @Autowired
    private Configuration freemarker;
    /**
     * Objet qui permet d'envoyer des messages
     */
    @Autowired
    private JavaMailSender mailSender;

    /**
     * Constructeur
     */
    public ForgotPasswordConnectionServlet() {
        super(ForgotPasswordConnectionForm.class);
    }

    @Override
    protected String doGet(final ForgotPasswordConnectionForm form,
                           final HttpServletRequest request, final HttpServletResponse response) throws EsterWebException {
        return FORGOT_PASSWORD_CONNECTION_JSP;
    }

    @Override
    protected String doPost(final ForgotPasswordConnectionForm form,
                            final HttpServletRequest request, final HttpServletResponse response) throws EsterWebException {

        try {
            final MemberEsterCriteria memberEsterCriteria = new MemberEsterCriteria();
            memberEsterCriteria.setLoginOrEmail(form.getEmail());

            if (memberEsterDao.existMemberEsterByLoginOrEmail(memberEsterCriteria)) {
                MemberEsterBean membre = memberEsterDao.findMemberEsterByLoginOrEmail(memberEsterCriteria);

                final Map<String, Object> model = new HashMap<>();
                model.put("account", membre.getRole());
                model.put("identifiant", membre.getLogin());
                model.put("password", membre.getPassword());

                final Template template = this.freemarker.getTemplate(TEMPLATE_FORGOT_PASSWORD);
                final String content = FreeMarkerTemplateUtils.processTemplateIntoString(template,
                        model);

                final MimeMessage mimeMessage = this.mailSender.createMimeMessage();
                final MimeMessageHelper message = new MimeMessageHelper(mimeMessage,
                        StandardCharsets.UTF_8.name());
                message.setSubject(SUBJECT_FORGOT_PASSWORD);
                message.setFrom("ester.chu.angers@gmail.com");
                message.setTo(membre.getEmail());
                message.setText(content, true);

                this.mailSender.send(mimeMessage);
            }

            form.notify("Un mail a été envoyé avec les identifiants à l'adresse : " + form.getEmail(), NotificationLevel.SUCCESS);
        } catch (final EsterDaoException e) {
            throw new EsterWebException(e.getMessage(), e.getHelperMessage());
        } catch (final Exception e) {
            throw new EsterWebException(e.getMessage());
        }

        return FORGOT_PASSWORD_CONNECTION_JSP;
    }
}

package fr.univangers.masterinfo.ester.web.servlet.connection;

import fr.univangers.masterinfo.ester.service.form.connection.LogoutConnectionForm;
import fr.univangers.masterinfo.ester.web.exception.EsterWebException;
import fr.univangers.masterinfo.ester.web.servlet.AbstractServlet;
import fr.univangers.masterinfo.ester.web.utils.SessionUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Page pour se déconnecter du site
 *
 * @version 1.0
 * @date 04/10/2020
 */
@WebServlet(loadOnStartup = 1, urlPatterns = {LogoutConnectionServlet.LOGOUT_CONNECTION_URL})
public class LogoutConnectionServlet extends AbstractServlet<LogoutConnectionForm> {

    /**
     * L'URL de deconnexion
     */
    public static final String LOGOUT_CONNECTION_URL = "/deconnexion";
    /**
     * Serial Version UID
     */
    private static final long serialVersionUID = 1L;
    /**
     * Log de la classe
     */
    private static final Logger LOG = LogManager.getLogger(LogoutConnectionServlet.class);

    /**
     * Constructeur
     */
    public LogoutConnectionServlet() {
        super(LogoutConnectionForm.class);
    }

    @Override
    protected String doGet(final LogoutConnectionForm form, final HttpServletRequest request,
                           final HttpServletResponse response) throws EsterWebException {

        SessionUtils.invalidate(request);
        form.setSessionUser(null);
        try {
            response.sendRedirect(
                    request.getContextPath() + WelcomeConnectionServlet.WELCOME_CONNECTION_URL);
            return null;
        } catch (final IOException e) {
            throw new EsterWebException(e.getMessage());
        }
    }

    @Override
    protected String doPost(final LogoutConnectionForm form, final HttpServletRequest request,
                            final HttpServletResponse response) throws EsterWebException {
        return this.doGet(form, request, response);
    }
}

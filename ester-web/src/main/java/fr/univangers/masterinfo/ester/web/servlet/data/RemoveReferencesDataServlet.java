package fr.univangers.masterinfo.ester.web.servlet.data;

import fr.univangers.masterinfo.ester.dao.bean.ReferenceBean;
import fr.univangers.masterinfo.ester.dao.bean.UserRole;
import fr.univangers.masterinfo.ester.service.contract.DataServiceContract;
import fr.univangers.masterinfo.ester.service.exception.EsterServiceException;
import fr.univangers.masterinfo.ester.service.form.data.RemoveReferencesDataForm;
import fr.univangers.masterinfo.ester.service.form.notification.NotificationLevel;
import fr.univangers.masterinfo.ester.web.exception.EsterWebException;
import fr.univangers.masterinfo.ester.web.servlet.AbstractServlet;
import fr.univangers.masterinfo.ester.web.servlet.connection.DashboardConnectionServlet;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * @author etudiant
 */
@WebServlet(loadOnStartup = 1, urlPatterns = {
        RemoveReferencesDataServlet.REMOVE_REFERENCES_DATA_URL})
@MultipartConfig
public class RemoveReferencesDataServlet extends AbstractServlet<RemoveReferencesDataForm> {

    /**
     * L'URL pour supprimer les données de références
     */
    public static final String REMOVE_REFERENCES_DATA_URL = DashboardConnectionServlet.DASHBOARD_CONNECTION_URL
            + "/modifier-donnees-references";
    /**
     * Serial Version UID
     */
    private static final long serialVersionUID = 1L;
    /**
     * Log de la classe
     */
    private static final Logger LOG = LogManager.getLogger(RemoveReferencesDataServlet.class);
    /**
     * Le chemin de la JSP pour afficher la page de suppression des données de
     * références
     */
    private static final String REMOVE_REFERENCES_DATA_JSP = "/WEB-INF/jsp/data/removeReferencesData.jsp";

    /**
     * Service Reference
     */
    @Autowired
    private DataServiceContract referenceService;

    /**
     * Constructeur
     */
    public RemoveReferencesDataServlet() {
        super(RemoveReferencesDataForm.class);
    }

    @Override
    protected String doGet(final RemoveReferencesDataForm form, final HttpServletRequest request,
                           final HttpServletResponse response) throws EsterWebException {

        this.handleAuthUser(request, response, UserRole.ADMIN);

        try {

            final List<ReferenceBean> referencesObject = this.referenceService.findAll();
            form.setReferences(referencesObject);

            return REMOVE_REFERENCES_DATA_JSP;
        } catch (final EsterServiceException e) {
            throw new EsterWebException(e.getMessage(), e.getHelperMessage());
        }
    }

    @Override
    protected String doPost(final RemoveReferencesDataForm form, final HttpServletRequest request,
                            final HttpServletResponse response) throws EsterWebException {

        this.handleAuthUser(request, response, UserRole.ADMIN);

        try {
            referenceService.removeReference(form);
            final String msg = "Les données de références ont bien été supprimé";
            form.notify(msg, NotificationLevel.SUCCESS);
        } catch (EsterServiceException e) {
            e.printStackTrace();
        }

        this.doGet(form, request, response);
        return REMOVE_REFERENCES_DATA_JSP;
    }
}

package fr.univangers.masterinfo.ester.web.utils;

import fr.univangers.masterinfo.ester.dao.bean.UserBean;

import javax.servlet.http.HttpServletRequest;

/**
 * Classe utilitaire sur la session
 *
 * @version 1.0
 * @date 04/10/2020
 */
public class SessionUtils {

    /**
     * La clé permettant d'accèder à l'utilisateur courant
     */
    private static final String SESSION_USER = "sessionUser";

    /**
     * Constructeur privé
     */
    private SessionUtils() {

    }

    /**
     * Modifier l'utilisateur courant dans la session à partir de la requête
     *
     * @param request
     * @param user
     */
    public static void setSessionUser(final HttpServletRequest request, final UserBean user) {
        if (request.getSession(false) == null) {
            request.getSession(true);
        }
        request.getSession(false).setAttribute(SESSION_USER, user);
    }

    /**
     * Récupérer l'utilisateur courant dans la session à partir de la requête
     *
     * @param request
     * @return l'utilisateur courant
     */
    public static UserBean getSessionUser(final HttpServletRequest request) {
        if (request.getSession(false) == null) {
            request.getSession(true);
        }
        return (UserBean) request.getSession(false).getAttribute(SESSION_USER);
    }

    /**
     * Détruire la session
     *
     * @param request
     */
    public static void invalidate(final HttpServletRequest request) {
        setSessionUser(request, null);
        request.getSession(false).invalidate();
    }
}

package fr.univangers.masterinfo.ester.web.servlet.questionnary;

import fr.univangers.masterinfo.ester.dao.bean.QuestionBean;
import fr.univangers.masterinfo.ester.dao.bean.QuestionOptionBean;
import fr.univangers.masterinfo.ester.dao.bean.QuestionType;
import fr.univangers.masterinfo.ester.dao.bean.QuestionnaryBean;
import fr.univangers.masterinfo.ester.dao.contract.QuestionDaoContract;
import fr.univangers.masterinfo.ester.dao.contract.QuestionOptionDaoContract;
import fr.univangers.masterinfo.ester.dao.exception.EsterDaoException;
import fr.univangers.masterinfo.ester.service.contract.QuestionnaryServiceContract;
import fr.univangers.masterinfo.ester.service.exception.EsterServiceException;
import fr.univangers.masterinfo.ester.service.form.questionnary.AnswerQuestionnaryForm;
import fr.univangers.masterinfo.ester.web.exception.EsterWebException;
import fr.univangers.masterinfo.ester.web.servlet.AbstractServlet;
import fr.univangers.masterinfo.ester.web.servlet.connection.DashboardConnectionServlet;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * @author etudiant
 */
@WebServlet(loadOnStartup = 1, urlPatterns = {AnswerQuestionnaryServlet.ANSWER_QUESTIONNARY_URL})
public class AnswerQuestionnaryServlet extends AbstractServlet<AnswerQuestionnaryForm> {

    /**
     * L'URL pour répondre aux questionnaires
     */
    public static final String ANSWER_QUESTIONNARY_URL = DashboardConnectionServlet.DASHBOARD_CONNECTION_URL
            + "/repondre-questionnaires";
    /**
     * Serial Version UID
     */
    private static final long serialVersionUID = 1L;
    /**
     * Log de la classe
     */
    private static final Logger LOG = LogManager.getLogger(AnswerQuestionnaryServlet.class);
    /**
     * Le chemin de la JSP pour afficher les droits administrations des questionnaires
     */
    private static final String ANSWER_QUESTIONNARY_JSP = "/WEB-INF/jsp/questionnary/answerQuestionnary.jsp";

    /**
     * Le service questionnaire
     */
    @Autowired
    private QuestionnaryServiceContract questionnaryService;

    /**
     * le contracte dao  de Question
     */
    @Autowired
    private QuestionDaoContract questionDaoContract;

    /**
     * le contracte dao  de Question
     */
    @Autowired
    private QuestionOptionDaoContract questionOptionDaoContract;

    /**
     * Constructeur
     */
    public AnswerQuestionnaryServlet() {
        super(AnswerQuestionnaryForm.class);
    }

    @Override
    protected String doGet(final AnswerQuestionnaryForm form, final HttpServletRequest request,
                           final HttpServletResponse response) throws EsterWebException {

        try {
            final Set<QuestionnaryBean> questionnaries = this.questionnaryService.getQuestionnariesNoAnswered(form);
            form.setQuestionnariesNoAnswered(questionnaries);
        } catch (final EsterServiceException e) {
            throw new EsterWebException(e.getMessage(), e.getHelperMessage());
        }

        return ANSWER_QUESTIONNARY_JSP;
    }

    @Override
    protected String doPost(final AnswerQuestionnaryForm form, final HttpServletRequest request,
                            final HttpServletResponse response) throws EsterWebException {

        try {
            // Si on ne répond pas au questionnaire dans ce cas on essaye de le récupérer
            if (!form.isQuestionnaryAnswered()) {
                final String jsonQuestionnary = this.questionnaryService
                        .getQuestionnaryAsJsonById(form);
                form.setJsonQuestionnary(jsonQuestionnary);
            }
            // Si on veut enregistrer les réponses du questionnaire
            else {
                final QuestionnaryBean objQuestionnary = this.questionnaryService.getQuestionnaryAsObjectById(form);
                form.setQuestionnary(objQuestionnary);

                AtomicInteger scoreQuestionnary = new AtomicInteger();
                // Pour chaque question
                for (final QuestionBean question : objQuestionnary.getQuestions()) {
                    LOG.info("question type {}", question.getType());
                    // On récupère la réponse donné pour cette question
                    final String questionOptionId = request.getParameter(question.getId());

                    if (question.getType() == QuestionType.SHORTANSWER || question.getType() == QuestionType.LONGANSWER) {
                        QuestionBean bean = questionDaoContract.read(question.getId());
                        form.getQuestionsSelected().put(bean, questionOptionId);

                    } else if (question.getType() == QuestionType.CHECKBOX) {
                        request.getParameterMap().forEach((key, value) -> {

                            if (key.split("_q")[0].equals(question.getId())) {

                                try {
                                    QuestionOptionBean bean = questionOptionDaoContract.read(value[0]);
                                    form.getQuestionsOptionSelected().add(bean);
                                    scoreQuestionnary.addAndGet(bean.getScore() * bean.getCoeff());

                                } catch (EsterDaoException e) {
                                    e.printStackTrace();
                                }
                            }
                        });

                    } else {
                        QuestionOptionBean bean = questionOptionDaoContract.read(questionOptionId);
                        form.getQuestionsOptionSelected().add(bean);
                        scoreQuestionnary.addAndGet(bean.getScore() * bean.getCoeff());
                    }
                }

                this.questionnaryService.saveResultQuestionnary(form, scoreQuestionnary.get());
            }

        } catch (final EsterServiceException e) {
            throw new EsterWebException(e.getMessage(), e.getHelperMessage());
        } catch (final EsterDaoException e) {
            throw new EsterWebException(e.getMessage(), e.getHelperMessage());
        }

        return ANSWER_QUESTIONNARY_JSP;
    }
}

package fr.univangers.masterinfo.ester.web.servlet.analysis;

import fr.univangers.masterinfo.ester.dao.bean.*;
import fr.univangers.masterinfo.ester.dao.contract.AnswerDaoContract;
import fr.univangers.masterinfo.ester.dao.contract.QuestionDaoContract;
import fr.univangers.masterinfo.ester.dao.contract.QuestionOptionDaoContract;
import fr.univangers.masterinfo.ester.dao.contract.ResultDaoContract;
import fr.univangers.masterinfo.ester.dao.criteria.AnswerCriteria;
import fr.univangers.masterinfo.ester.dao.criteria.ResultCriteria;
import fr.univangers.masterinfo.ester.service.contract.AccountServiceContract;
import fr.univangers.masterinfo.ester.service.contract.AnalysisServiceContract;
import fr.univangers.masterinfo.ester.service.contract.DataServiceContract;
import fr.univangers.masterinfo.ester.service.contract.QuestionnaryServiceContract;
import fr.univangers.masterinfo.ester.service.exception.EsterServiceException;
import fr.univangers.masterinfo.ester.service.form.analysis.CompareDataReferencesAnalysisForm;
import fr.univangers.masterinfo.ester.web.exception.EsterWebException;
import fr.univangers.masterinfo.ester.web.servlet.AbstractServlet;
import fr.univangers.masterinfo.ester.web.servlet.connection.DashboardConnectionServlet;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.*;

/**
 * @author etudiant
 */
@WebServlet(loadOnStartup = 1, urlPatterns = {
        CompareDataReferencesAnalysisServlet.COMPARE_DATA_REFERENCES_ANALYSIS_URL})
public class CompareDataReferencesAnalysisServlet
        extends AbstractServlet<CompareDataReferencesAnalysisForm> {

    /**
     * L'URL permettant de comparer les données résultats avec les données de
     * références
     */
    public static final String COMPARE_DATA_REFERENCES_ANALYSIS_URL = DashboardConnectionServlet.DASHBOARD_CONNECTION_URL
            + "/analyses-donnees-references";
    /**
     * Serial Version UID
     */
    private static final long serialVersionUID = 1L;
    /**
     * Log de la classe
     */
    private static final Logger LOG = LogManager
            .getLogger(CompareDataReferencesAnalysisServlet.class);
    /**
     * Le chemin de la JSP permettant de comparer les données résultats avec les
     * données de références
     */
    private static final String COMPARE_DATA_REFERENCES_ANALYSIS_JSP = "/WEB-INF/jsp/analysis/compareDataReferencesAnalysis.jsp";
    int scoreGlobal;
    /**
     * Service pour calculer le score
     */
    @Autowired
    private AnalysisServiceContract analysisService;
    /**
     * Service Reference
     */
    @Autowired
    private DataServiceContract referenceService;
    /**
     * Service pour récuperer les questionnaires
     */
    @Autowired
    private QuestionnaryServiceContract questionnaryService;
    /**
     * Service pour récuperer les Answers
     */
    @Autowired
    private ResultDaoContract resultDaoContract;
    /**
     * Service pour récuperer les questions
     */
    @Autowired
    private QuestionDaoContract questionService;
    /**
     * Service pour récuperer les salariés
     */
    @Autowired
    private AccountServiceContract accountService;
    @Autowired
    private  DataServiceContract dataService;
    @Autowired
    private QuestionOptionDaoContract questionOptionDaoContract;
    @Autowired
    private AnswerDaoContract answerDaoContract;
    /*
     * Liste pour stocker les questionnaires
     */
    private List<QuestionnaryBean> questionnaries = new ArrayList<>();
    /*
     * Liste des questions du questionniare selectionné
     */
    private List<QuestionBean> questions = new ArrayList<>();
    /*
     * Liste pour stocker les employées
     */
    private List<EmployeeBean> employees = new ArrayList<>();
    /*
     * utiliser pour afficher et masquer les statistiques
     */
    private boolean isDisplayDiagrammes = false;

    /**
     * Constructeur
     */
    public CompareDataReferencesAnalysisServlet() {
        super(CompareDataReferencesAnalysisForm.class);
    }

    @Override
    protected String doGet(final CompareDataReferencesAnalysisForm form,
                           final HttpServletRequest request, final HttpServletResponse response)
            throws EsterWebException {

        this.handleAuthUser(request, response, UserRole.PLURIDISCIPLINAIRE);

        try {
            this.employees = this.accountService.findAll(); // on récupère la liste de tous les employés

            request.setAttribute("employee_selected", false); // on indique qu'on n'a pas encore sélectionné d'employé
            request.setAttribute("listEmployees", this.employees); // on passe en attribut la liste des employés

            this.isDisplayDiagrammes = false;

            return COMPARE_DATA_REFERENCES_ANALYSIS_JSP;
        } catch (final EsterServiceException e) {
            throw new EsterWebException(e.getMessage(), e.getHelperMessage());
        }
    }

    @Override
    protected String doPost(final CompareDataReferencesAnalysisForm form, final HttpServletRequest request, final HttpServletResponse response)
            throws EsterWebException {

        this.handleAuthUser(request, response, UserRole.PLURIDISCIPLINAIRE);
        if (form.getIdQuestionnary() != null) // si on a sélectionné un questionnaire
        {
            try {
                EmployeeBean selected_employee = this.accountService.findEmployeeByID(form.getIdEmployee()); // on récupère l'employé sélectionné
                QuestionnaryBean selected_questionnary = this.questionnaryService.getQuestionnaryByID(form.getIdQuestionnary()); // on récupère le questionnaire envoyé

                ResultCriteria resultCriteria = new ResultCriteria();
                resultCriteria.setIdQuestionnary(selected_questionnary.getId());
                resultCriteria.setIdEmployee(selected_employee.getId());
                ResultBean resultBean = this.resultDaoContract.getResultByEmployeeAndQuestionnary(resultCriteria);

                AnswerCriteria answerCriteria = new AnswerCriteria();
                answerCriteria.setIdResultat(resultBean.getId());
                List<AnswerBean> answerBeanList = answerDaoContract.getListAnwserByResulat(answerCriteria);

                request.setAttribute("employee_selected", true); // on indique qu'on a sélectionné un employé
                request.setAttribute("questionnary_selected", true); // on indique qu'on a sélectionné un questionnaire
                request.setAttribute("employee_name", selected_employee.getLogin()); // on passe en attribut le nom du salarié sélectionné
                request.setAttribute("questionnary_name", selected_questionnary.getName()); // on passe en attribut le nom du questionnaire sélectionné
                request.setAttribute("default_reference", selected_questionnary.getReference());

                ReferenceBean referenceBean = null;

                if(selected_questionnary.getReference() != null && !selected_questionnary.getReference().isBlank()) {
                    request.setAttribute("questionnary_reference_exists", true);
                    referenceBean = dataService.findReferenceById(selected_questionnary.getReference());
                    List<Long> listScores = analysisService.getlistScoreResults(referenceBean.getName(), referenceBean.getVariables());
                    request.setAttribute("liste_score", listScores);
                }
                else {
                    request.setAttribute("questionnary_reference_exists", false);
                }

                List<QuestionBean> questionsList = new ArrayList<>();
                Map<Integer, String> responsesStringMap = new HashMap<>();
                Map<Integer, List<QuestionOptionBean>> responsesOptionMap = new HashMap<>();
                Map<String, Long> optionsPercentages = new HashMap<>();

                for (AnswerBean answerBean : answerBeanList) {
                    try {
                        // Question type QCM
                        if (answerBean.getResponse() != null && !answerBean.getResponse().isEmpty()) {

                            List<QuestionOptionBean> responses = new ArrayList<>();
                            QuestionBean questionBean = null;

                            for (QuestionOptionBean option : answerBean.getResponse()) {

                                questionBean = option.getQuestion();

                                for (QuestionOptionBean optionBean : questionBean.getQuestionOptions()) {
                                    if(referenceBean != null) {
                                        long percentage = analysisService.getPercentageValueVariable(referenceBean.getName(), questionBean.getVariable(), optionBean.getValeur());
                                        optionsPercentages.put(optionBean.getId(), percentage);
                                    }

                                }
                                questionsList.add(questionBean);
                                responses.add(option);

                            }

                            if(questionBean != null) {
                                responsesOptionMap.put(questionBean.getPosition(), sortOptionsList(responses));
                            }

                        // Question courte ou longue
                        } else {

                            QuestionBean questionBean = questionService.read(answerBean.getResponseID());
                            questionsList.add(questionBean);
                            responsesStringMap.put(questionBean.getPosition(), answerBean.getReponsesEmploye());
                        }
                    } catch (Exception e) {
                        e.getMessage();
                    }
                }

                request.setAttribute("questionsList", sortQuestionsList(questionsList));
                request.setAttribute("responsesStringMap", responsesStringMap);
                request.setAttribute("responsesOptionMap", responsesOptionMap);
                request.setAttribute("optionsPercentages", optionsPercentages);

                /**
                 * score global de l'employee sélectionné
                 */
                this.scoreGlobal = this.analysisService.getScoresGolablsByEmployeeAndQuestionnary(form);
                this.isDisplayDiagrammes = true;
                request.setAttribute("isDisplayDiagrammes", this.isDisplayDiagrammes);
                request.setAttribute("scoreEmployee", this.scoreGlobal);
                return COMPARE_DATA_REFERENCES_ANALYSIS_JSP;

            } catch (EsterServiceException e) {
                throw new EsterWebException(e.getMessage(), e.getHelperMessage());
            } catch (final Exception e) {
                throw new EsterWebException(e.getMessage());
            }
        } else {
            try {
                EmployeeBean selected_employee = this.accountService.findEmployeeByID(form.getIdEmployee()); // on récupère l'employé sélectionné
                request.setAttribute("employee_selected", true); // on indique qu'on a sélectionné un employé
                request.setAttribute("questionnary_selected", false); // mais pas encore de questionnaire
                request.setAttribute("employee_name", selected_employee.getLogin()); // on passe en attribut le nom du salarié sélectionné
                request.setAttribute("employee_id", selected_employee.getId()); // et son id
                request.setAttribute("listQuestionnaries", selected_employee.getQuestionnariesAnswered()); // et on passe en attribut uniquement les questionnaires auxquels il a répondu

                this.isDisplayDiagrammes = false;

                return COMPARE_DATA_REFERENCES_ANALYSIS_JSP;
            } catch (EsterServiceException e) {
                throw new EsterWebException(e.getMessage(), e.getHelperMessage());
            } catch (final Exception e) {
                throw new EsterWebException(e.getMessage());
            }
        }
    }

    private List<QuestionBean> sortQuestionsList(List<QuestionBean> questionsList) {
        List<QuestionBean> questionsListSorted = new ArrayList<>();
        for(int position = 1; position <= questionsList.size(); position++) {
            for (QuestionBean question : questionsList) {
                if(question.getPosition() == position) {
                    questionsListSorted.add(question);
                    break;
                }
            }
        }
        return questionsListSorted;
    }

    private List<QuestionOptionBean> sortOptionsList(List<QuestionOptionBean> optionsList) {
        List<QuestionOptionBean> optionsListSorted = new ArrayList<>();
        if(optionsList != null && !optionsList.isEmpty()) {
            QuestionBean question = optionsList.get(0).getQuestion();
            for(int position = 1; position <= question.getQuestionOptions().size(); position++) {
                for (QuestionOptionBean option : optionsList) {
                    if(option.getPosition() == position) {
                        optionsListSorted.add(option);
                    }
                }
            }
        }
        return optionsListSorted;
    }
}
package fr.univangers.masterinfo.ester.web.exception;

/**
 * Exception pour la couche WEB
 *
 * @version 1.0
 * @date 04/10/2020
 */
public class EsterWebException extends Exception {

    /**
     * Serial Version UID
     */
    private static final long serialVersionUID = 1L;

    /**
     * ce message nous servira à informer l'utilisateur du type d'erreur que le programme a rencontré
     */
    private String helperMessage;

    /**
     * Constructeur
     *
     * @param message
     */
    public EsterWebException(final String message) {
        super(message);
        helperMessage = "";
    }

    /**
     * Constructeur
     *
     * @param message
     * @param helperMessage
     */
    public EsterWebException(final String message, final String helperMessage) {
        super(message);
        this.helperMessage = helperMessage;
    }

    public String getHelperMessage() {
        return helperMessage;
    }

    public boolean hasHelperMessage() {
        return !(helperMessage.isEmpty());
    }
}

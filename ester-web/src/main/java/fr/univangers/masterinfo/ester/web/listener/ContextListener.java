package fr.univangers.masterinfo.ester.web.listener;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

/**
 * Peupler la BD
 *
 * @version 1.0
 * @date 04/10/2020
 */
@WebListener
public class ContextListener implements ServletContextListener {


    @Override
    public void contextInitialized(final ServletContextEvent sce) {
//		final WebApplicationContext context = WebApplicationContextUtils
//			.getWebApplicationContext(sce.getServletContext());
//		final ManagerData data = context.getBean(ManagerData.class);
//		try {
//			final String root = sce.getServletContext().getRealPath("/");
//			data.clearData();
//			data.populateData(root);
//		}
//		catch (final EsterDaoException e) {
//			e.printStackTrace();
//		}
    }

    @Override
    public void contextDestroyed(final ServletContextEvent servletContextEvent) {

    }
}
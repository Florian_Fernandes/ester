package fr.univangers.masterinfo.ester.web.utils;

import fr.univangers.masterinfo.ester.service.form.AbstractForm;
import fr.univangers.masterinfo.ester.service.form.Form;
import fr.univangers.masterinfo.ester.service.form.Input;
import fr.univangers.masterinfo.ester.utils.ReflectUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.Part;
import java.io.InputStream;
import java.lang.reflect.Field;
import java.util.List;

/**
 * Passer les données d'une requête vers un objet formulaire et inversement
 *
 * @version 1.0
 * @date 04/10/2020
 */
public class FormUtils {

    /**
     * Log de la classe
     */
    private static final Logger LOG = LogManager.getLogger(FormUtils.class);

    /**
     * Requête qui contient des données comme des fichiers
     */
    private static final String MULTIPART_FORM_DATA = "multipart/form-data";

    /**
     * Constructeur privé
     */
    private FormUtils() {

    }

    /**
     * Convertir les données de la requête dans un formulaire
     *
     * @param request
     * @param form
     */
    public static void requestToForm(final HttpServletRequest request, final AbstractForm form) {
        final List<Field> fields = ReflectUtils.getAllDeclaredField(form.getClass(), Input.class);

        for (final Field field : fields) {
            final Input input = field.getAnnotation(Input.class);

            final String name = input.name();


            final String value = request.getParameter(name);


            if (Enum.class.isAssignableFrom(field.getType())) {
                injectValueEnum(request, form, field, name, value);
            } else if (InputStream.class.isAssignableFrom(field.getType())) {
                injectValueInputStream(request, form, field, name, value);
            } else if (Boolean.class.isAssignableFrom(field.getType())
                    || boolean.class.isAssignableFrom(field.getType())) {
                injectValueBool(request, form, field, name, value);
            } else if (String.class.isAssignableFrom(field.getType())) {
                ReflectUtils.setValue(form, field, value);
            } else {
                ReflectUtils.setValue(form, field, null);
            }
        }
    }

    /**
     * Injecter une valeur de type énumération
     *
     * @param request
     * @param form
     * @param field
     * @param name
     * @param value
     */
    @SuppressWarnings({"unchecked", "rawtypes"})
    private static void injectValueEnum(final HttpServletRequest request, final AbstractForm form,
                                        final Field field, final String name, final String value) {
        Enum valueEnum = null;
        if (StringUtils.isNoneBlank(value)) {
            valueEnum = Enum.valueOf((Class<Enum>) field.getType(), value);
        }
        ReflectUtils.setValue(form, field, valueEnum);
    }

    /**
     * Injecter une booléenne
     *
     * @param request
     * @param form
     * @param field
     * @param name
     * @param value
     */
    private static void injectValueBool(final HttpServletRequest request, final AbstractForm form,
                                        final Field field, final String name, final String value) {
        final boolean bool = Boolean.parseBoolean(value);
        ReflectUtils.setValue(form, field, bool);
    }

    /**
     * Injecter une valeur de type input stream
     *
     * @param request
     * @param form
     * @param field
     * @param name
     * @param value
     */
    private static void injectValueInputStream(final HttpServletRequest request,
                                               final AbstractForm form, final Field field, final String name, final String value) {


        final boolean isMultipart = StringUtils.containsIgnoreCase(request.getContentType(),
                MULTIPART_FORM_DATA);


        // Si le formulaire n'a pas été soumis en multipart
        if (!isMultipart) {
            return;
        }

        try {
            final Part part = request.getPart(name);


            ReflectUtils.setValue(form, field, part == null ? null : part.getInputStream());
        } catch (final Exception e) {
            LOG.error(e.getMessage(), e);
        }
    }

    /**
     * Enregistrer le formulaire dans la requête
     *
     * @param request
     * @param form
     */
    public static void formToRequest(final HttpServletRequest request, final AbstractForm form) {
        final Form f = form.getClass().getAnnotation(Form.class);
        request.setAttribute(f.name(), form);
    }
}

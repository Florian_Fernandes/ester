package fr.univangers.masterinfo.ester.web.servlet.account;

import fr.univangers.masterinfo.ester.dao.bean.EmployeeBean;
import fr.univangers.masterinfo.ester.dao.bean.UserRole;
import fr.univangers.masterinfo.ester.service.contract.AccountServiceContract;
import fr.univangers.masterinfo.ester.service.exception.EsterServiceException;
import fr.univangers.masterinfo.ester.service.form.data.EmployeeDataForm;
import fr.univangers.masterinfo.ester.web.exception.EsterWebException;
import fr.univangers.masterinfo.ester.web.servlet.AbstractServlet;
import fr.univangers.masterinfo.ester.web.servlet.connection.DashboardConnectionServlet;
import org.springframework.beans.factory.annotation.Autowired;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Collections;
import java.util.List;

@WebServlet(loadOnStartup = 1, urlPatterns = {EmployeeDataServlet.EMPLOYEE_DATA_URL})
public class EmployeeDataServlet extends AbstractServlet<EmployeeDataForm> {

    /**
     * L'URL de la visualisation des employés
     */
    public static final String EMPLOYEE_DATA_URL = DashboardConnectionServlet.DASHBOARD_CONNECTION_URL + "/visualisation-employes";
    /**
     * Serial Version UID
     */
    private static final long serialVersionUID = 1L;
    /**
     * Le chemin de la JSP pour afficher la page de visualisation des employés
     */
    private static final String EMPLOYEE_DATA_JSP = "/WEB-INF/jsp/account/EmployeeList.jsp";
    /**
     * Service pour récuperer les salariés
     */
    @Autowired
    private AccountServiceContract accountService;

    /**
     * Constructeur
     */
    public EmployeeDataServlet() {
        super(EmployeeDataForm.class);
    }

    @Override
    protected String doGet(EmployeeDataForm form, HttpServletRequest request, HttpServletResponse response) throws EsterWebException {
        try {

            this.handleAuthUser(request, response, UserRole.PLURIDISCIPLINAIRE);

            final List<EmployeeBean> employees = this.accountService.findAll();

            // trie par login la liste des employées
            Collections.sort(employees);
            request.setAttribute("employees", employees);

        } catch (final EsterServiceException e) {
            throw new EsterWebException(e.getMessage(), e.getHelperMessage());
        }
        return EMPLOYEE_DATA_JSP;
    }

    @Override
    protected String doPost(EmployeeDataForm form, HttpServletRequest request, HttpServletResponse response) throws EsterWebException {
        return null;
    }
}

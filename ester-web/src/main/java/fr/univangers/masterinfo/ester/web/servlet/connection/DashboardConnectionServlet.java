package fr.univangers.masterinfo.ester.web.servlet.connection;

import fr.univangers.masterinfo.ester.service.contract.ConnectionServiceContract;
import fr.univangers.masterinfo.ester.service.form.connection.DashboardConnectionForm;
import fr.univangers.masterinfo.ester.web.exception.EsterWebException;
import fr.univangers.masterinfo.ester.web.servlet.AbstractServlet;
import org.springframework.beans.factory.annotation.Autowired;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Page tableau de bord de l'utilisateur
 *
 * @version 1.0
 * @date 04/10/2020
 */
@WebServlet(loadOnStartup = 1, urlPatterns = {
        DashboardConnectionServlet.DASHBOARD_CONNECTION_URL})
public class DashboardConnectionServlet extends AbstractServlet<DashboardConnectionForm> {

    /**
     * L'URL pour accéder au tableau de bord
     */
    public static final String DASHBOARD_CONNECTION_URL = "/tableau-de-bord";
    /**
     * Serial Version UID
     */
    private static final long serialVersionUID = 1L;
    /**
     * Le chemin de la JSP pour afficher la page tableau de bord
     */
    private static final String DASHBOARD_CONNECTION_JSP = "/WEB-INF/jsp/connection/dashboardConnection.jsp";

    /**
     * Service pour se connecter
     */
    @SuppressWarnings("unused")
    @Autowired
    private ConnectionServiceContract connectionService;

    /**
     * Constructeur
     */
    public DashboardConnectionServlet() {
        super(DashboardConnectionForm.class);
    }

    @Override
    protected String doGet(final DashboardConnectionForm form, final HttpServletRequest request,
                           final HttpServletResponse response) throws EsterWebException {
        return DASHBOARD_CONNECTION_JSP;
    }

    @Override
    protected String doPost(final DashboardConnectionForm form, final HttpServletRequest request,
                            final HttpServletResponse response) throws EsterWebException {
        return DASHBOARD_CONNECTION_JSP;
    }
}

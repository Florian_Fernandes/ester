package fr.univangers.masterinfo.ester.web.servlet.connection;

import fr.univangers.masterinfo.ester.dao.bean.UserBean;
import fr.univangers.masterinfo.ester.dao.bean.UserRole;
import fr.univangers.masterinfo.ester.service.contract.ConnectionServiceContract;
import fr.univangers.masterinfo.ester.service.exception.EsterServiceException;
import fr.univangers.masterinfo.ester.service.form.connection.LoginConnectionForm;
import fr.univangers.masterinfo.ester.web.exception.EsterWebException;
import fr.univangers.masterinfo.ester.web.servlet.AbstractServlet;
import fr.univangers.masterinfo.ester.web.utils.SessionUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Page pour se connecter au site
 *
 * @version 1.0
 * @date 04/10/2020
 */
@WebServlet(loadOnStartup = 1, urlPatterns = {LoginConnectionServlet.LOGIN_CONNECTION_URL})
public class LoginConnectionServlet extends AbstractServlet<LoginConnectionForm> {

    /**
     * L'URL de connexion
     */
    public static final String LOGIN_CONNECTION_URL = "/connexion";
    /**
     * Serial Version UID
     */
    private static final long serialVersionUID = 1L;
    /**
     * Log de la classe
     */
    private static final Logger LOG = LogManager.getLogger(LoginConnectionServlet.class);
    /**
     * Le chemin de la JSP pour afficher la page de connexion
     */
    private static final String LOGIN_CONNECTION_JSP = "/WEB-INF/jsp/connection/loginConnection.jsp";
    /**
     * Le chemin de la JSP pour arepondre au questionaire
     */
    private static final String ANSWER_QUESTIONNARY_JSP = "/WEB-INF/jsp/questionnary/answerQuestionnary.jsp";

    /**
     * Service pour se connecter
     */
    @Autowired
    private ConnectionServiceContract connectionService;

    /**
     * Constructeur
     */
    public LoginConnectionServlet() {
        super(LoginConnectionForm.class);
    }

    @Override
    protected String doGet(final LoginConnectionForm form, final HttpServletRequest request,
                           final HttpServletResponse response) throws EsterWebException {

        return LOGIN_CONNECTION_JSP;
    }

    @Override
    protected String doPost(final LoginConnectionForm form, final HttpServletRequest request,
                            final HttpServletResponse response) throws EsterWebException {
        try {
            final UserBean user = this.connectionService.findUser(form);


            SessionUtils.invalidate(request);
            form.setSessionUser(user);

            //si c'est un salairie il va se déreger directement vers la page pour répondre aux questionnaire
            if (user != null && user.getRole() == UserRole.SALARIE)
                return ANSWER_QUESTIONNARY_JSP;

            return LOGIN_CONNECTION_JSP;
        } catch (final EsterServiceException e) {
            throw new EsterWebException(e.getMessage(), e.getHelperMessage());
        }
    }
}

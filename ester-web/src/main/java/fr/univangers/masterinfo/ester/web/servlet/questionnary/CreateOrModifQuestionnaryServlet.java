package fr.univangers.masterinfo.ester.web.servlet.questionnary;

import fr.univangers.masterinfo.ester.dao.bean.QuestionnaryBean;
import fr.univangers.masterinfo.ester.dao.bean.ReferenceBean;
import fr.univangers.masterinfo.ester.dao.bean.UserRole;
import fr.univangers.masterinfo.ester.dao.contract.QuestionnaryDaoContract;
import fr.univangers.masterinfo.ester.dao.exception.EsterDaoException;
import fr.univangers.masterinfo.ester.service.contract.DataServiceContract;
import fr.univangers.masterinfo.ester.service.exception.EsterServiceException;
import fr.univangers.masterinfo.ester.service.form.questionnary.CreateOrModifQuestionnaryForm;
import fr.univangers.masterinfo.ester.web.exception.EsterWebException;
import fr.univangers.masterinfo.ester.web.servlet.AbstractServlet;
import fr.univangers.masterinfo.ester.web.servlet.connection.DashboardConnectionServlet;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * servlet pour la page de création ou de modification d'un questionnaire
 */
@WebServlet(loadOnStartup = 1, urlPatterns = {fr.univangers.masterinfo.ester.web.servlet.questionnary.CreateOrModifQuestionnaryServlet.CREATE_OR_MODIF_QUESTIONNARY_URL})
public class CreateOrModifQuestionnaryServlet extends AbstractServlet<CreateOrModifQuestionnaryForm> {

    /**
     * L'URL de la page de création ou de modification d'un questionnaire
     */
    public static final String CREATE_OR_MODIF_QUESTIONNARY_URL = DashboardConnectionServlet.DASHBOARD_CONNECTION_URL
            + "/creer-ou-modifier-questionnaire";
    /**
     * Serial Version UID
     */
    private static final long serialVersionUID = 1L;
    /**
     * Log de la classe
     */
    private static final Logger LOG = LogManager.getLogger(fr.univangers.masterinfo.ester.web.servlet.questionnary.CreateOrModifQuestionnaryServlet.class);
    /**
     * Le chemin de la JSP pour afficher la page de création ou de modification d'un questionnaire
     */
    private static final String CREATE_OR_MODIF_QUESTIONNARY_JSP = "/WEB-INF/jsp/questionnary/CreateOrModifQuestionnary.jsp";

    @Autowired
    private QuestionnaryDaoContract questionnaryDao;

    /**
     * Service Reference
     */
    @Autowired
    private DataServiceContract referenceService;

    /**
     * Constructeur
     */
    public CreateOrModifQuestionnaryServlet() {
        super(CreateOrModifQuestionnaryForm.class);
    }

    @Override
    protected String doGet(final CreateOrModifQuestionnaryForm form, final HttpServletRequest request,
                           final HttpServletResponse response) throws EsterWebException {

        this.handleAuthUser(request, response, UserRole.MEDECIN);

        String idQuestionnary = request.getParameter("idQuestionnary");

        try {
            final List<ReferenceBean> referencesObject = this.referenceService.findAll();
            form.setReferences(referencesObject);

            if (idQuestionnary != null) {
                QuestionnaryBean createOrModifQuestionnary = questionnaryDao.read(idQuestionnary);
                form.setQuestionnaryName(createOrModifQuestionnary.getName());
            } else {
                QuestionnaryBean createOrModifQuestionnary = new QuestionnaryBean();
                form.setQuestionnaryName("nouveau questionnaire");
            }

            return CREATE_OR_MODIF_QUESTIONNARY_JSP;

        } catch (final EsterServiceException e) {
            throw new EsterWebException(e.getMessage(), e.getHelperMessage());
        } catch (final EsterDaoException e) {
            throw new EsterWebException(e.getMessage(), e.getHelperMessage());
        } catch (final Exception e) {
            throw new EsterWebException(e.getMessage());
        }
    }

    @Override
    protected String doPost(final CreateOrModifQuestionnaryForm form, final HttpServletRequest request,
                            final HttpServletResponse response) throws EsterWebException {

        this.handleAuthUser(request, response, UserRole.MEDECIN);

        return CREATE_OR_MODIF_QUESTIONNARY_JSP;
    }
}


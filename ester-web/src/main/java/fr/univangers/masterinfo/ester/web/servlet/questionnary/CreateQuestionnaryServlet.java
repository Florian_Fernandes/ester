package fr.univangers.masterinfo.ester.web.servlet.questionnary;

import fr.univangers.masterinfo.ester.dao.bean.QuestionnaryBean;
import fr.univangers.masterinfo.ester.dao.bean.ReferenceBean;
import fr.univangers.masterinfo.ester.dao.bean.UserRole;
import fr.univangers.masterinfo.ester.service.contract.DataServiceContract;
import fr.univangers.masterinfo.ester.service.contract.QuestionnaryServiceContract;
import fr.univangers.masterinfo.ester.service.exception.EsterServiceException;
import fr.univangers.masterinfo.ester.service.form.questionnary.ManageQuestionnaryForm;
import fr.univangers.masterinfo.ester.web.exception.EsterWebException;
import fr.univangers.masterinfo.ester.web.servlet.AbstractServlet;
import fr.univangers.masterinfo.ester.web.servlet.connection.DashboardConnectionServlet;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * @author etudiant
 */
@WebServlet(loadOnStartup = 1, urlPatterns = {CreateQuestionnaryServlet.CREATE_QUESTIONNARY_URL})
public class CreateQuestionnaryServlet extends AbstractServlet<ManageQuestionnaryForm> {

    /**
     * L'URL pour afficher le générateur de questionnaire
     */
    public static final String CREATE_QUESTIONNARY_URL = DashboardConnectionServlet.DASHBOARD_CONNECTION_URL
            + "/creer-questionnaires";
    /**
     * Serial Version UID
     */
    private static final long serialVersionUID = 1L;
    /**
     * Log de la classe
     */
    private static final Logger LOG = LogManager.getLogger(CreateQuestionnaryServlet.class);
    /**
     * Le chemin de la JSP pour afficher le générateur de questionnaire
     */
    private static final String CREATE_QUESTIONNARY_JSP = "/WEB-INF/jsp/questionnary/createQuestionnary.jsp";

    /**
     * Le service pour les questionnaires
     */
    @Autowired
    private QuestionnaryServiceContract questionnaryService;

    /**
     * Le service pour les références
     */
    @Autowired
    private DataServiceContract referenceService;

    /**
     * Constructeur
     */
    public CreateQuestionnaryServlet() {
        super(ManageQuestionnaryForm.class);
    }

    @Override
    protected String doGet(final ManageQuestionnaryForm form, final HttpServletRequest request, final HttpServletResponse response) throws EsterWebException {

        this.handleAuthUser(request, response, UserRole.MEDECIN);

        try {
            String action = request.getParameter("action");
            String IDquestionnaire = request.getParameter("questionnaire");

            final QuestionnaryBean questionnaire = questionnaryService.getQuestionnaryByID(IDquestionnaire);

            final List<ReferenceBean> references = referenceService.findAll();

            if (questionnaire != null) {
                String reference_questionnaire = questionnaire.getReference();

                request.setAttribute("reference_questionnaire", reference_questionnaire);
            }

            request.setAttribute("action", action);
            request.setAttribute("questionnaire", questionnaire);
            request.setAttribute("references", references);

            return CREATE_QUESTIONNARY_JSP;
        } catch (final EsterServiceException e) {
            throw new EsterWebException(e.getMessage(), e.getHelperMessage());
        }


    }

    @Override
    protected String doPost(final ManageQuestionnaryForm form, final HttpServletRequest request, final HttpServletResponse response) throws EsterWebException {

        this.handleAuthUser(request, response, UserRole.MEDECIN);

        return CREATE_QUESTIONNARY_JSP;
    }
}

package fr.univangers.masterinfo.ester.web.servlet.questionnary;

import fr.univangers.masterinfo.ester.dao.bean.MemberEsterBean;
import fr.univangers.masterinfo.ester.dao.bean.QuestionnaryBean;
import fr.univangers.masterinfo.ester.dao.bean.UserRole;
import fr.univangers.masterinfo.ester.service.contract.AccountServiceContract;
import fr.univangers.masterinfo.ester.service.contract.QuestionnaryServiceContract;
import fr.univangers.masterinfo.ester.service.exception.EsterServiceException;
import fr.univangers.masterinfo.ester.service.form.questionnary.ShareQuestionnaryForm;
import fr.univangers.masterinfo.ester.web.exception.EsterWebException;
import fr.univangers.masterinfo.ester.web.servlet.AbstractServlet;
import fr.univangers.masterinfo.ester.web.servlet.connection.DashboardConnectionServlet;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * @author etudiant
 */
@WebServlet(loadOnStartup = 1, urlPatterns = {ShareQuestionnaryServlet.SHARE_QUESTIONNARY_URL})
public class ShareQuestionnaryServlet extends AbstractServlet<ShareQuestionnaryForm> {

    /**
     * L'URL pour afficher les droits administrations des questionnaires
     */
    public static final String SHARE_QUESTIONNARY_URL = DashboardConnectionServlet.DASHBOARD_CONNECTION_URL
            + "/administration-questionnaires";
    /**
     * Serial Version UID
     */
    private static final long serialVersionUID = 1L;
    /**
     * Log de la classe
     */
    private static final Logger LOG = LogManager.getLogger(ShareQuestionnaryServlet.class);
    /**
     * Le chemin de la JSP pour afficher les droits administrations des
     * questionnaires
     */
    private static final String SHARE_QUESTIONNARY_JSP = "/WEB-INF/jsp/questionnary/shareQuestionnary.jsp";

    /**
     * Service pour récuperer les questionnaires
     */
    @Autowired
    private QuestionnaryServiceContract questionnaryService;

    /**
     * Service pour récuperer les salariés
     */
    @Autowired
    private AccountServiceContract accountService;

    /**
     * Constructeur
     */
    public ShareQuestionnaryServlet() {
        super(ShareQuestionnaryForm.class);
    }

    @Override
    protected String doGet(final ShareQuestionnaryForm form, final HttpServletRequest request,
                           final HttpServletResponse response) throws EsterWebException {

        this.handleAuthUser(request, response, UserRole.PLURIDISCIPLINAIRE);

        try {
            final List<QuestionnaryBean> questionnaries;
            if (form.getSessionUser().getRole() == UserRole.ADMIN)
                questionnaries = this.questionnaryService.findAll();
            else
                questionnaries = this.questionnaryService.findJustMember((MemberEsterBean) form.getSessionUser());

            final List<MemberEsterBean> adminAndMedecins = this.accountService.findAllMemberEsterExceptCurrentUser(form.getSessionUser().getId());

            form.setQuestionnaries(questionnaries);
            form.setMembersEster(adminAndMedecins);

            return SHARE_QUESTIONNARY_JSP;
        } catch (final EsterServiceException e) {
            throw new EsterWebException(e.getMessage(), e.getHelperMessage());
        }
    }

    @Override
    protected String doPost(final ShareQuestionnaryForm form, final HttpServletRequest request,
                            final HttpServletResponse response) throws EsterWebException {

        this.handleAuthUser(request, response, UserRole.PLURIDISCIPLINAIRE);

        this.doGet(form, request, response);

        try {
            questionnaryService.shareQuestionnaryToMemberEster(form);
            return SHARE_QUESTIONNARY_JSP;

        } catch (final EsterServiceException e) {
            throw new EsterWebException(e.getMessage(), e.getHelperMessage());
        }
    }
}

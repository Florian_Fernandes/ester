package fr.univangers.masterinfo.ester.web.servlet.account;

import fr.univangers.masterinfo.ester.dao.bean.MemberEsterBean;
import fr.univangers.masterinfo.ester.dao.bean.UserRole;
import fr.univangers.masterinfo.ester.dao.contract.MemberEsterDaoContract;
import fr.univangers.masterinfo.ester.dao.criteria.MemberEsterCriteria;
import fr.univangers.masterinfo.ester.dao.exception.EsterDaoException;
import fr.univangers.masterinfo.ester.service.form.account.ModifyAccountForm;
import fr.univangers.masterinfo.ester.web.exception.EsterWebException;
import fr.univangers.masterinfo.ester.web.servlet.AbstractServlet;
import fr.univangers.masterinfo.ester.web.servlet.connection.DashboardConnectionServlet;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @author etudiant
 */
@WebServlet(loadOnStartup = 1, urlPatterns = {ModifyAccountServlet.MODIFY_ACCOUNT_URL})
public class ModifyAccountServlet extends AbstractServlet<ModifyAccountForm> {

    /**
     * L'URL de modification d'un compte
     */
    public static final String MODIFY_ACCOUNT_URL = DashboardConnectionServlet.DASHBOARD_CONNECTION_URL
            + "/modification-compte";
    /**
     * Serial Version UID
     */
    private static final long serialVersionUID = 1L;

    /**
     * Membre Ester Dao
     */
    /**
     * Log de la classe
     */
    private static final Logger LOG = LogManager.getLogger(ModifyAccountServlet.class);
    /**
     * Le chemin de la JSP pour afficher la page de modification d'un compte
     */
    private static final String MODIFY_ACCOUNT_JSP = "/WEB-INF/jsp/account/modifyAccount.jsp";
    /**
     * Rajout d'un memebreEsterDao un attribut qui permet de recuperer les elements  de la BD
     */
    @Autowired
    private MemberEsterDaoContract memberEsterDaoContract;

    /**
     * Constructeur
     */
    public ModifyAccountServlet() {
        super(ModifyAccountForm.class);
    }

    @Override
    protected String doGet(final ModifyAccountForm form, final HttpServletRequest request,
                           final HttpServletResponse response) throws EsterWebException, EsterDaoException {

        this.handleAuthUser(request, response, UserRole.PLURIDISCIPLINAIRE);

        final MemberEsterCriteria memberEsterCriteria = new MemberEsterCriteria();
        String id = request.getParameter("id");
        memberEsterCriteria.setId(id);

        try {
            if (memberEsterDaoContract.existMembreById(memberEsterCriteria)) {
                MemberEsterBean memberEsterBean = memberEsterDaoContract.findMembreEsterById(memberEsterCriteria);
                form.setLogin(memberEsterBean.getLogin());
                form.setMail(memberEsterBean.getEmail());
                form.setRoles(UserRole.values());
            }

        } catch (final EsterDaoException e) {
            throw new EsterWebException(e.getMessage(), e.getHelperMessage());
        } catch (final Exception e) {
            throw new EsterWebException(e.getMessage());
        }
        return MODIFY_ACCOUNT_JSP;
    }

    @Override
    protected String doPost(final ModifyAccountForm form, final HttpServletRequest request,
                            final HttpServletResponse response) throws EsterWebException, EsterDaoException {

        this.handleAuthUser(request, response, UserRole.PLURIDISCIPLINAIRE);

        final MemberEsterCriteria memberEsterCriteria = new MemberEsterCriteria();
        String id = request.getParameter("id");
        memberEsterCriteria.setId(id);
        boolean MessageError = true;

        try {
            if (memberEsterDaoContract.existMembreById(memberEsterCriteria)) {
                MemberEsterBean memberEsterBean = memberEsterDaoContract.findMembreEsterById(memberEsterCriteria);
                String passwordDB = memberEsterBean.getPassword();
                String newLogin = request.getParameter(ModifyAccountForm.INPUT_LOGIN);
                String newMail = request.getParameter(ModifyAccountForm.INPUT_MAIL);
                String actuelPasswordForm = request.getParameter(ModifyAccountForm.INPUT_ACTUAL_PASSWORD);
                String newConfirmPassword = request.getParameter(ModifyAccountForm.INPUT_CONFIRM_PASSWORD);

                if (passwordDB.equals(actuelPasswordForm)) {
                    memberEsterBean.setId(id);
                    memberEsterBean.setEmail(newMail);
                    memberEsterBean.setLogin(newLogin);
                    memberEsterBean.setPassword(newConfirmPassword);
                    LOG.info(memberEsterBean);
                } else {
                    MessageError = false;
                }

                memberEsterDaoContract.update(memberEsterBean);
            }
        } catch (EsterDaoException e) {
            throw new EsterWebException(e.getMessage(), e.getHelperMessage());
        } catch (Exception e) {
            throw new EsterWebException(e.getMessage());
        }

        request.setAttribute("MessageError", MessageError);

        return MODIFY_ACCOUNT_JSP;
    }

}


package fr.univangers.masterinfo.ester.web.servlet.connection;

import fr.univangers.masterinfo.ester.service.contract.ConnectionServiceContract;
import fr.univangers.masterinfo.ester.service.form.connection.FirstConnectionForm;
import fr.univangers.masterinfo.ester.web.exception.EsterWebException;
import fr.univangers.masterinfo.ester.web.servlet.AbstractServlet;
import org.springframework.beans.factory.annotation.Autowired;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Page pour se connecter au site
 *
 * @version 1.0
 * @date 04/10/2020
 */
@WebServlet(loadOnStartup = 1, urlPatterns = {FirstConnectionServlet.FIRST_CONNECTION_URL})
public class FirstConnectionServlet extends AbstractServlet<FirstConnectionForm> {

    /**
     * L'URL de connexion
     */
    public static final String FIRST_CONNECTION_URL = "/premiere-connexion";
    /**
     * Serial Version UID
     */
    private static final long serialVersionUID = 1L;
    /**
     * Le chemin de la JSP pour afficher la page de connexion
     */
    private static final String FIRST_CONNECTION_JSP = "/WEB-INF/jsp/connection/firstConnection.jsp";

    /**
     * Service pour se connecter
     */
    @SuppressWarnings("unused")
    @Autowired
    private ConnectionServiceContract connectionService;

    /**
     * Constructeur
     */
    public FirstConnectionServlet() {
        super(FirstConnectionForm.class);
    }

    @Override
    protected String doGet(final FirstConnectionForm form, final HttpServletRequest request,
                           final HttpServletResponse response) throws EsterWebException {
        return FIRST_CONNECTION_JSP;
    }

    @Override
    protected String doPost(final FirstConnectionForm form, final HttpServletRequest request,
                            final HttpServletResponse response) throws EsterWebException {
        return FIRST_CONNECTION_JSP;
    }
}

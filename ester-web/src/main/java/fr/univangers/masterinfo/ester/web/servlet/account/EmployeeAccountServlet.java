package fr.univangers.masterinfo.ester.web.servlet.account;

import fr.univangers.masterinfo.ester.dao.bean.EmployeeBean;
import fr.univangers.masterinfo.ester.dao.bean.UserRole;
import fr.univangers.masterinfo.ester.dao.contract.EmployeeDaoContract;
import fr.univangers.masterinfo.ester.dao.criteria.EmployeeCriteria;
import fr.univangers.masterinfo.ester.dao.exception.EsterDaoException;
import fr.univangers.masterinfo.ester.service.contract.AccountServiceContract;
import fr.univangers.masterinfo.ester.service.exception.EsterServiceException;
import fr.univangers.masterinfo.ester.service.form.account.EmployeeAccountForm;
import fr.univangers.masterinfo.ester.web.exception.EsterWebException;
import fr.univangers.masterinfo.ester.web.servlet.AbstractServlet;
import fr.univangers.masterinfo.ester.web.servlet.connection.DashboardConnectionServlet;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Date;

/**
 * servlet pour la page d'informations sur les employés
 */
@WebServlet(loadOnStartup = 1, urlPatterns = {EmployeeAccountServlet.EMPLOYEE_ACCOUNT_URL})
public class EmployeeAccountServlet extends AbstractServlet<EmployeeAccountForm> {

    /**
     * L'URL de la page d'informations pour un employé
     */
    public static final String EMPLOYEE_ACCOUNT_URL = DashboardConnectionServlet.DASHBOARD_CONNECTION_URL
            + "/compte-employe";
    /**
     * Serial Version UID
     */
    private static final long serialVersionUID = 1L;
    /**
     * Log de la classe
     */
    private static final Logger LOG = LogManager.getLogger(EmployeeAccountServlet.class);
    /**
     * Le chemin de la JSP pour afficher la page de modification d'un compte
     */
    private static final String EMPLOYEE_ACCOUNT_JSP = "/WEB-INF/jsp/account/employeeAccount.jsp";

    /**
     * Objet permettant de récupérer le compte Ester lié à l'adresse mail
     */
    @Autowired
    private EmployeeDaoContract employeeDao;

    /**
     * Objet permettant la mise à jour du salarié
     */
    @Autowired
    private AccountServiceContract accountServiceContract;

    /**
     * Constructeur
     */
    public EmployeeAccountServlet() {
        super(EmployeeAccountForm.class);
    }

    @Override
    protected String doGet(final EmployeeAccountForm form, final HttpServletRequest request,
                           final HttpServletResponse response) throws EsterWebException {

        this.handleAuthUser(request, response, UserRole.PLURIDISCIPLINAIRE);

        String login = request.getParameter("login");
        final EmployeeCriteria employeeCriteria = new EmployeeCriteria();
        employeeCriteria.setLogin(login);

        try {
            EmployeeBean employee = employeeDao.findEmployee(employeeCriteria);

            if (employee != null) {
                form.setId(employee.getId());
                form.setLogin(employee.getLogin());
                form.setSexe(employee.getSexe());

                Date birth = employee.getBirth();
                if(birth != null)
                    form.setBirthYear(String.valueOf(employee.getBirth().getYear() + 1900)); // problème avec la date, il faudrait changer ça en int dans la BDD au lieu d'utiliser un format déprécié de Java
                form.setSelectedPCS(employee.getPcs());
                form.setSelectedNAF(employee.getNaf());
                form.setCommentaire(employee.getCommentaires());
            }
        } catch (final EsterDaoException e) {
            throw new EsterWebException(e.getMessage(), e.getHelperMessage());
        } catch (final Exception e) {
            throw new EsterWebException(e.getMessage());
        }

        return EMPLOYEE_ACCOUNT_JSP;
    }

    @Override
    protected String doPost(final EmployeeAccountForm form, final HttpServletRequest request,
                            final HttpServletResponse response) throws EsterWebException {
        try {
            this.accountServiceContract.modifyEmployee(form);
        } catch (final EsterServiceException e) {
            throw new EsterWebException(e.getMessage(), e.getHelperMessage());
        } catch (final Exception e) {
            throw new EsterWebException(e.getMessage());
        }

        return EMPLOYEE_ACCOUNT_JSP;
    }
}

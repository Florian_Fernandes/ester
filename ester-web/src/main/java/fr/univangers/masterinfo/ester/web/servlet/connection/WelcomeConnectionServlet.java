package fr.univangers.masterinfo.ester.web.servlet.connection;

import fr.univangers.masterinfo.ester.service.contract.ConnectionServiceContract;
import fr.univangers.masterinfo.ester.service.form.connection.WelcomeConnectionForm;
import fr.univangers.masterinfo.ester.web.exception.EsterWebException;
import fr.univangers.masterinfo.ester.web.servlet.AbstractServlet;
import org.springframework.beans.factory.annotation.Autowired;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Page d'accueil
 *
 * @version 1.0
 * @date 04/10/2020
 */
@WebServlet(loadOnStartup = 1, urlPatterns = {WelcomeConnectionServlet.WELCOME_CONNECTION_URL})
public class WelcomeConnectionServlet extends AbstractServlet<WelcomeConnectionForm> {

    /**
     * l'URL d'accueil
     */
    public static final String WELCOME_CONNECTION_URL = "/accueil";
    /**
     * Serial Version UID
     */
    private static final long serialVersionUID = 1L;
    /**
     * La page JSP pour afficher l'accueil
     */
    private static final String WELCOME_CONNECTION_JSP = "/WEB-INF/jsp/connection/welcomeConnection.jsp";

    /**
     * Service pour se connecter
     */
    @SuppressWarnings("unused")
    @Autowired
    private ConnectionServiceContract connectionService;

    /**
     * Constructeur
     */
    public WelcomeConnectionServlet() {
        super(WelcomeConnectionForm.class);
    }

    @Override
    protected String doGet(final WelcomeConnectionForm form, final HttpServletRequest request,
                           final HttpServletResponse response) throws EsterWebException {
        return WELCOME_CONNECTION_JSP;
    }

    @Override
    protected String doPost(final WelcomeConnectionForm form, final HttpServletRequest request,
                            final HttpServletResponse response) throws EsterWebException {
        return WELCOME_CONNECTION_JSP;
    }
}

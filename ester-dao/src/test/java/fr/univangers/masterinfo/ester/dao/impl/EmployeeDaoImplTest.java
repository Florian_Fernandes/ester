package fr.univangers.masterinfo.ester.dao.impl;

import fr.univangers.masterinfo.ester.dao.bean.EmployeeBean;
import fr.univangers.masterinfo.ester.dao.contract.EmployeeDaoContract;
import fr.univangers.masterinfo.ester.dao.criteria.EmployeeCriteria;
import fr.univangers.masterinfo.ester.dao.exception.EsterDaoException;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.transaction.Transactional;

/**
 * Test sur la DAO salarié
 *
 * @version 1.0
 * @date 04/10/2020
 */
@ContextConfiguration(locations = "classpath:spring-dao-test.xml")
@RunWith(SpringJUnit4ClassRunner.class)
@Transactional
public class EmployeeDaoImplTest {

    /**
     * La DAO salarié à tester
     */
    @Autowired
    private EmployeeDaoContract employeeDao;

    /**
     * Test d'un critère null
     *
     * @throws EsterDaoException
     */
    @Test(expected = EsterDaoException.class)
    @Rollback(true)
    public void testExistEmployeeWithCriteriaIsNull() throws EsterDaoException {
        this.employeeDao.existEmployeeById(null);
    }

    /**
     * Test d'un critère avec champ null
     *
     * @throws EsterDaoException
     */
    @Test(expected = EsterDaoException.class)
    @Rollback(true)
    public void testExistEmployeeWithLoginOrEmailIsNull() throws EsterDaoException {
        final EmployeeCriteria employeeCriteria = new EmployeeCriteria();
        employeeCriteria.setLogin(null);

        this.employeeDao.existEmployeeById(employeeCriteria);
    }

    /**
     * Test d'un membre ESTER qui n'existe pas
     *
     * @throws EsterDaoException
     */
    @Test
    @Rollback(true)
    public void testExistEmployeeFalse() throws EsterDaoException {
        final EmployeeCriteria employeeCriteria = new EmployeeCriteria();
        employeeCriteria.setLogin("identifiantbidon");

        final boolean exist = this.employeeDao.existEmployeeById(employeeCriteria);

        Assert.assertFalse(exist);
    }

    /**
     * Test d'un membre ESTER avec son identifiant qui existe
     *
     * @throws EsterDaoException
     */
    @Test
    @Rollback(true)
    public void testExistEmployeeTrueWithIdentifiant() throws EsterDaoException {
        final EmployeeCriteria employeeCriteria = new EmployeeCriteria();
        employeeCriteria.setLogin("abcd1234");

        final boolean exist = this.employeeDao.existEmployeeById(employeeCriteria);

        Assert.assertTrue(exist);
    }

    /**
     * Test d'un critère null
     *
     * @throws EsterDaoException
     */
    @Test(expected = EsterDaoException.class)
    @Rollback(true)
    public void testFindEmployeeWithCriteriaIsNull() throws EsterDaoException {
        this.employeeDao.findEmployee(null);
    }

    /**
     * Test d'un critère avec champ null
     *
     * @throws EsterDaoException
     */
    @Test(expected = EsterDaoException.class)
    @Rollback(true)
    public void testFindEmployeeWithLoginOrEmailIsNull() throws EsterDaoException {
        final EmployeeCriteria employeeCriteria = new EmployeeCriteria();
        employeeCriteria.setLogin(null);

        this.employeeDao.findEmployee(employeeCriteria);
    }

    /**
     * Test d'un membre ESTER qui n'a pas été retrouvé
     *
     * @throws EsterDaoException
     */
    @Test
    @Rollback(true)
    public void testFindEmployeeNull() throws EsterDaoException {
        final EmployeeCriteria employeeCriteria = new EmployeeCriteria();
        employeeCriteria.setLogin("identifiantbidon");

        final EmployeeBean employee = this.employeeDao.findEmployee(employeeCriteria);

        Assert.assertNull(employee);
    }

    /**
     * Test d'un membre ESTER qui a été retrouvé par son identifiant
     *
     * @throws EsterDaoException
     */
    @Test
    @Rollback(true)
    public void testFindEmployeeNotNullWithIdentifiant() throws EsterDaoException {
        final EmployeeCriteria employeeCriteria = new EmployeeCriteria();
        employeeCriteria.setLogin("abcd1234");

        final EmployeeBean employee = this.employeeDao.findEmployee(employeeCriteria);

        Assert.assertNotNull(employee);
        Assert.assertEquals("abcd1234", employee.getLogin());
    }
}

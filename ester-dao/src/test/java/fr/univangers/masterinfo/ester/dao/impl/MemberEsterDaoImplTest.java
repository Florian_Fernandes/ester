package fr.univangers.masterinfo.ester.dao.impl;

import fr.univangers.masterinfo.ester.dao.bean.MemberEsterBean;
import fr.univangers.masterinfo.ester.dao.contract.MemberEsterDaoContract;
import fr.univangers.masterinfo.ester.dao.criteria.MemberEsterCriteria;
import fr.univangers.masterinfo.ester.dao.exception.EsterDaoException;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.transaction.Transactional;

/**
 * Test sur la DAO membre ESTER
 *
 * @version 1.0
 * @date 04/10/2020
 */
@ContextConfiguration(locations = "classpath:spring-dao-test.xml")
@RunWith(SpringJUnit4ClassRunner.class)
@Transactional
public class MemberEsterDaoImplTest {

    /**
     * La DAO membre ESTER à tester
     */
    @Autowired
    private MemberEsterDaoContract memberEsterDao;

    /**
     * Test d'un critère null
     *
     * @throws EsterDaoException
     */
    @Test(expected = EsterDaoException.class)
    @Rollback(true)
    public void testExistMemberEsterWithCriteriaIsNull() throws EsterDaoException {
        this.memberEsterDao.existMemberEsterByLoginOrEmail(null);
    }

    /**
     * Test d'un critère avec champ null
     *
     * @throws EsterDaoException
     */
    @Test(expected = EsterDaoException.class)
    @Rollback(true)
    public void testExistMemberEsterWithLoginOrEmailIsNull() throws EsterDaoException {
        final MemberEsterCriteria memberEsterCriteria = new MemberEsterCriteria();
        memberEsterCriteria.setLoginOrEmail(null);

        this.memberEsterDao.existMemberEsterByLoginOrEmail(memberEsterCriteria);
    }

    /**
     * Test d'un membre ESTER qui n'existe pas
     *
     * @throws EsterDaoException
     */
    @Test
    @Rollback(true)
    public void testExistMemberEsterFalse() throws EsterDaoException {
        final MemberEsterCriteria memberEsterCriteria = new MemberEsterCriteria();
        memberEsterCriteria.setLoginOrEmail("nexistepas@gmail.com");

        final boolean exist = this.memberEsterDao.existMemberEsterByLoginOrEmail(memberEsterCriteria);

        Assert.assertFalse(exist);
    }

    /**
     * Test d'un membre ESTER avec son adresse mail qui existe
     *
     * @throws EsterDaoException
     */
    @Test
    @Rollback(true)
    public void testExistMemberEsterTrueWithEmail() throws EsterDaoException {
        final MemberEsterCriteria memberEsterCriteria = new MemberEsterCriteria();
        memberEsterCriteria.setLoginOrEmail("adm.root.ester@gmail.com");

        final boolean exist = this.memberEsterDao.existMemberEsterByLoginOrEmail(memberEsterCriteria);

        Assert.assertTrue(exist);
    }

    /**
     * Test d'un membre ESTER avec son identifiant qui existe
     *
     * @throws EsterDaoException
     */
    @Test
    @Rollback(true)
    public void testExistMemberEsterTrueWithIdentifiant() throws EsterDaoException {
        final MemberEsterCriteria memberEsterCriteria = new MemberEsterCriteria();
        memberEsterCriteria.setLoginOrEmail("AdminESTER");

        final boolean exist = this.memberEsterDao.existMemberEsterByLoginOrEmail(memberEsterCriteria);

        Assert.assertTrue(exist);
    }

    /**
     * Test d'un critère null
     *
     * @throws EsterDaoException
     */
    @Test(expected = EsterDaoException.class)
    @Rollback(true)
    public void testFindMemberEsterWithCriteriaIsNull() throws EsterDaoException {
        this.memberEsterDao.findMemberEsterByLoginOrEmail(null);
    }

    /**
     * Test d'un critère avec champ null
     *
     * @throws EsterDaoException
     */
    @Test(expected = EsterDaoException.class)
    @Rollback(true)
    public void testFindMemberEsterWithLoginOrEmailIsNull() throws EsterDaoException {
        final MemberEsterCriteria memberEsterCriteria = new MemberEsterCriteria();
        memberEsterCriteria.setLoginOrEmail(null);

        this.memberEsterDao.findMemberEsterByLoginOrEmail(memberEsterCriteria);
    }

    /**
     * Test d'un membre ESTER qui n'a pas été retrouvé
     *
     * @throws EsterDaoException
     */
    @Test
    @Rollback(true)
    public void testFindMemberEsterNull() throws EsterDaoException {
        final MemberEsterCriteria memberEsterCriteria = new MemberEsterCriteria();
        memberEsterCriteria.setLoginOrEmail("nexistepas@gmail.com");

        final MemberEsterBean member = this.memberEsterDao.findMemberEsterByLoginOrEmail(memberEsterCriteria);

        Assert.assertNull(member);
    }

    /**
     * Test d'un membre ESTER qui a été retrouvé par son adresse mail
     *
     * @throws EsterDaoException
     */
    @Test
    @Rollback(true)
    public void testFindMemberEsterNotNullWithEmail() throws EsterDaoException {
        final MemberEsterCriteria memberEsterCriteria = new MemberEsterCriteria();
        memberEsterCriteria.setLoginOrEmail("adm.root.ester@gmail.com");

        final MemberEsterBean member = this.memberEsterDao.findMemberEsterByLoginOrEmail(memberEsterCriteria);

        Assert.assertNotNull(member);
        Assert.assertEquals("adm.root.ester@gmail.com", member.getEmail());
    }

    /**
     * Test d'un membre ESTER qui a été retrouvé par son identifiant
     *
     * @throws EsterDaoException
     */
    @Test
    @Rollback(true)
    public void testFindMemberEsterNotNullWithIdentifiant() throws EsterDaoException {
        final MemberEsterCriteria memberEsterCriteria = new MemberEsterCriteria();
        memberEsterCriteria.setLoginOrEmail("AdminESTER");

        final MemberEsterBean member = this.memberEsterDao.findMemberEsterByLoginOrEmail(memberEsterCriteria);

        Assert.assertNotNull(member);
        Assert.assertEquals("AdminESTER", member.getLogin());
    }
}

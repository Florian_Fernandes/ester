package fr.univangers.masterinfo.ester.dao.data;

import fr.univangers.masterinfo.ester.dao.bean.QuestionBean;
import fr.univangers.masterinfo.ester.dao.bean.QuestionOptionBean;
import fr.univangers.masterinfo.ester.dao.bean.QuestionType;
import fr.univangers.masterinfo.ester.dao.exception.EsterDaoException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

/**
 * Ajouter des données fictives sur les questions
 *
 * @version 1.0
 * @date 01/11/2020
 */
@Component
public class QuestionData extends AbstractData<QuestionBean> {

    /**
     * La data question option
     */
    @Autowired
    private QuestionOptionData questionOptionData;

    /**
     * Question sur l'echelle de borg
     *
     * @return la question
     * @throws EsterDaoException
     */
    public QuestionBean getQuestionEchelleBorg() throws EsterDaoException {
        final QuestionBean echelleBorg = new QuestionBean();
        echelleBorg.setCode("AQ48");
        echelleBorg.setName(
                "Comment évaluez-vous l'intensité des efforts physiques de votre travail au cours d'une journée de travail ?");
        echelleBorg.setType(QuestionType.SELECT);
        echelleBorg.setScore(2);
        echelleBorg.setPosition(1);

        final List<QuestionOptionBean> questionsOptions = new ArrayList<>();
        questionsOptions.add(this.questionOptionData.getQuestionOptionPasEffort());
        questionsOptions.add(this.questionOptionData.getQuestionOptionExtremementLeger1());
        questionsOptions.add(this.questionOptionData.getQuestionOptionExtremementLeger2());
        questionsOptions.add(this.questionOptionData.getQuestionOptionTresLeger1());
        questionsOptions.add(this.questionOptionData.getQuestionOptionTresLeger2());
        questionsOptions.add(this.questionOptionData.getQuestionOptionLeger1());
        questionsOptions.add(this.questionOptionData.getQuestionOptionLeger2());
        questionsOptions.add(this.questionOptionData.getQuestionOptionPeuDur1());
        questionsOptions.add(this.questionOptionData.getQuestionOptionPeuDur2());
        questionsOptions.add(this.questionOptionData.getQuestionOptionDur1());
        questionsOptions.add(this.questionOptionData.getQuestionOptionDur2());
        questionsOptions.add(this.questionOptionData.getQuestionOptionTresDur1());
        questionsOptions.add(this.questionOptionData.getQuestionOptionTresDur2());
        questionsOptions.add(this.questionOptionData.getQuestionOptionExtremementDur());
        questionsOptions.add(this.questionOptionData.getQuestionOptionEpuisant());

        for (final QuestionOptionBean questionOption : questionsOptions) {
            questionOption.setQuestion(echelleBorg);
            echelleBorg.getQuestionOptions().add(questionOption);
        }

        return echelleBorg;
    }

    /**
     * Question sur repeter action
     *
     * @return la question
     * @throws EsterDaoException
     */
    public QuestionBean getQuestionRepeterAction() throws EsterDaoException {
        final QuestionBean repeterAction = new QuestionBean();
        repeterAction.setCode("AQ49");
        repeterAction.setName(
                "Votre travail nécessite-t-il de répéter les mêmes actions plus de 2 à 4 fois environ par minute ?");
        repeterAction.setType(QuestionType.RADIO);
        repeterAction.setScore(2);
        repeterAction.setPosition(2);

        final List<QuestionOptionBean> questionsOptions = new ArrayList<>();
        questionsOptions.add(this.questionOptionData.getQuestionOptionJamais());
        questionsOptions.add(this.questionOptionData.getQuestionOptionRarement());
        questionsOptions.add(this.questionOptionData.getQuestionOptionSouvent());
        questionsOptions.add(this.questionOptionData.getQuestionOptionTresSouvent());

        for (final QuestionOptionBean questionOption : questionsOptions) {
            questionOption.setQuestion(repeterAction);
            repeterAction.getQuestionOptions().add(questionOption);
        }

        return repeterAction;
    }

    /**
     * Question sur travailler bras en l'air
     *
     * @return la question
     * @throws EsterDaoException
     */
    public QuestionBean getQuestionTravaillerBrasAir() throws EsterDaoException {
        final QuestionBean travaillerBrasAir = new QuestionBean();
        travaillerBrasAir.setCode("AQ573");
        travaillerBrasAir.setName(
                "Travaillez-vous avec un ou deux bras en l'air (au-dessus des épaules) régulièrement ou de manière prolongée ?");
        travaillerBrasAir.setType(QuestionType.RADIO);
        travaillerBrasAir.setScore(2);
        travaillerBrasAir.setPosition(3);

        final List<QuestionOptionBean> questionsOptions = new ArrayList<>();
        questionsOptions.add(this.questionOptionData.getQuestionOptionJamais());
        questionsOptions.add(this.questionOptionData.getQuestionOptionRarement());
        questionsOptions.add(this.questionOptionData.getQuestionOptionSouvent());
        questionsOptions.add(this.questionOptionData.getQuestionOptionTresSouvent());

        for (final QuestionOptionBean questionOption : questionsOptions) {
            questionOption.setQuestion(travaillerBrasAir);
            travaillerBrasAir.getQuestionOptions().add(questionOption);
        }

        return travaillerBrasAir;
    }

    /**
     * Question sur fléchir les coudes
     *
     * @return la question
     * @throws EsterDaoException
     */
    public QuestionBean getQuestionFlechirCoude() throws EsterDaoException {
        final QuestionBean flechirCoude = new QuestionBean();
        flechirCoude.setCode("AQ576");
        flechirCoude
                .setName("Fléchissez-vous le(s) coude(s) régulièrement ou de manière prolongée ?");
        flechirCoude.setType(QuestionType.RADIO);
        flechirCoude.setScore(1);
        flechirCoude.setPosition(4);

        final List<QuestionOptionBean> questionsOptions = new ArrayList<>();
        questionsOptions.add(this.questionOptionData.getQuestionOptionJamais());
        questionsOptions.add(this.questionOptionData.getQuestionOptionRarement());
        questionsOptions.add(this.questionOptionData.getQuestionOptionSouvent());
        questionsOptions.add(this.questionOptionData.getQuestionOptionTresSouvent());

        for (final QuestionOptionBean questionOption : questionsOptions) {
            questionOption.setQuestion(flechirCoude);
            flechirCoude.getQuestionOptions().add(questionOption);
        }

        return flechirCoude;
    }

    /**
     * Question sur presser les objets
     *
     * @return la question
     * @throws EsterDaoException
     */
    public QuestionBean getQuestionPresserObjet() throws EsterDaoException {
        final QuestionBean presserObjet = new QuestionBean();
        presserObjet.setCode("AQ579");
        presserObjet.setName(
                "Pressez ou prenez-vous fermement des objets ou des pièces entre le pouce et l'index ?");
        presserObjet.setType(QuestionType.RADIO);
        presserObjet.setScore(2);
        presserObjet.setPosition(5);

        final List<QuestionOptionBean> questionsOptions = new ArrayList<>();
        questionsOptions.add(this.questionOptionData.getQuestionOptionJamais());
        questionsOptions.add(this.questionOptionData.getQuestionOptionRarement());
        questionsOptions.add(this.questionOptionData.getQuestionOptionSouvent());
        questionsOptions.add(this.questionOptionData.getQuestionOptionTresSouvent());

        for (final QuestionOptionBean questionOption : questionsOptions) {
            questionOption.setQuestion(presserObjet);
            presserObjet.getQuestionOptions().add(questionOption);
        }

        return presserObjet;
    }

    /**
     * Question sur l'influence du travail
     *
     * @return la question
     * @throws EsterDaoException
     */
    public QuestionBean getQuestionInfluencerTravail() throws EsterDaoException {
        final QuestionBean influencerTravail = new QuestionBean();
        influencerTravail.setCode("AQ588");
        influencerTravail
                .setName("J'ai la possibilité d'influencer le déroulement de mon travail.");
        influencerTravail.setType(QuestionType.RADIO);
        influencerTravail.setScore(2);
        influencerTravail.setPosition(6);

        final List<QuestionOptionBean> questionsOptions = new ArrayList<>();
        questionsOptions.add(this.questionOptionData.getQuestionOptionPasDuToutDac());
        questionsOptions.add(this.questionOptionData.getQuestionOptionPasDac());
        questionsOptions.add(this.questionOptionData.getQuestionOptionDac());
        questionsOptions.add(this.questionOptionData.getQuestionOptionToutAFaitDac());

        for (final QuestionOptionBean questionOption : questionsOptions) {
            questionOption.setQuestion(influencerTravail);
            influencerTravail.getQuestionOptions().add(questionOption);
        }

        return influencerTravail;
    }

    /**
     * Question sur les taches
     *
     * @return la question
     * @throws EsterDaoException
     */
    public QuestionBean getQuestionColleguesAidentTaches() throws EsterDaoException {
        final QuestionBean collegues = new QuestionBean();
        collegues.setCode("AQ5826");
        collegues
                .setName("Les collègues avec qui je travaille m'aident à mener mes tâches à bien.");
        collegues.setType(QuestionType.RADIO);
        collegues.setScore(2);
        collegues.setPosition(7);

        final List<QuestionOptionBean> questionsOptions = new ArrayList<>();
        questionsOptions.add(this.questionOptionData.getQuestionOptionPasDuToutDac());
        questionsOptions.add(this.questionOptionData.getQuestionOptionPasDac());
        questionsOptions.add(this.questionOptionData.getQuestionOptionDac());
        questionsOptions.add(this.questionOptionData.getQuestionOptionToutAFaitDac());

        for (final QuestionOptionBean questionOption : questionsOptions) {
            questionOption.setQuestion(collegues);
            collegues.getQuestionOptions().add(questionOption);
        }

        return collegues;
    }
}

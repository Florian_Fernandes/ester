package fr.univangers.masterinfo.ester.dao.impl;

import com.mongodb.MongoException;
import fr.univangers.masterinfo.ester.dao.bean.EmployeeBean;
import fr.univangers.masterinfo.ester.dao.bean.QuestionBean;
import fr.univangers.masterinfo.ester.dao.bean.ResultBean;
import fr.univangers.masterinfo.ester.dao.contract.ResultDaoContract;
import fr.univangers.masterinfo.ester.dao.criteria.ResultCriteria;
import fr.univangers.masterinfo.ester.dao.exception.EsterDaoException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Implémentation de le DAO resultat
 *
 * @author Matthew COYLE
 * @version 1.0
 * @date 14/10/2020
 */
@Transactional
@Repository
public class ResultDaoImpl extends AbstractDaoImpl<ResultBean> implements ResultDaoContract {
    /**
     * Log de la classe
     */
    private static final Logger LOG = LogManager
            .getLogger(ReferenceDaoImpl.class);

    /**
     * Le constructeur
     */
    public ResultDaoImpl() {
        super(ResultBean.class);
    }

    @SuppressWarnings("unchecked")
    @Override
    public int getScoreGlobalByEmployeeAndQuestionnary(final ResultCriteria resultCriteria)
            throws EsterDaoException {
        ResultBean result = new ResultBean();

        if (resultCriteria == null) {
            throw new EsterDaoException("Resultat criteria is null", "Erreur dans la recherche en base de données");
        }
        try {
            final StringBuilder sb = new StringBuilder();
            sb.append("{");
            sb.append("'res_employee':'" + resultCriteria.getIdEmployee() + "'");
            sb.append(",");
            sb.append("'res_questionnary':'" + resultCriteria.getIdQuestionnary() + "'");
            sb.append("}");

            result = (ResultBean) this.entityManager.createNativeQuery(sb.toString(), ResultBean.class).getSingleResult();

            System.out.println(sb);
        } catch (final MongoException e) {
            throw new EsterDaoException(e.getMessage(), "Impossible d'accéder à la base de données... Contactez un administrateur");
        } catch (final Exception e) {
            throw new EsterDaoException(e.getMessage(), "Erreur dans la recherche en base de données");
        }
        return result.getScoreGlobal();
    }

    @SuppressWarnings("unchecked")
    @Override
    public ResultBean getResultByEmployeeAndQuestionnary(final ResultCriteria resultCriteria)
            throws EsterDaoException {
        ResultBean result = new ResultBean();
        if (resultCriteria == null) {
            throw new EsterDaoException("Resultat criteria is null", "Erreur dans la recherche en base de données");
        }
        try {
            final StringBuilder sb = new StringBuilder();
            sb.append("{");
            sb.append("'res_employee':'" + resultCriteria.getIdEmployee() + "'");
            sb.append(",");
            sb.append("'res_questionnary':'" + resultCriteria.getIdQuestionnary() + "'");
            sb.append("}");
            result = (ResultBean) this.entityManager
                    .createNativeQuery(sb.toString(), ResultBean.class).getSingleResult();
            System.out.println(sb);
        } catch (final MongoException e) {
            throw new EsterDaoException(e.getMessage(), "Impossible d'accéder à la base de données... Contactez un administrateur");
        } catch (final Exception e) {
            throw new EsterDaoException(e.getMessage(), "Erreur dans la recherche en base de données");
        }
        return result;
    }


    /**
     * METHODE A JOUR ET UTILE (recuperer les scores dans une bdd de reference)
     **/
    @SuppressWarnings("unchecked")
    @Override
    public List<Long> getlistScoreResults(String tableReferenceName, String variables) throws EsterDaoException {
        ArrayList<Long> scores = new ArrayList<>();
        String table = tableReferenceName;

        List<String> colonnes;
        if(variables != null) {
            colonnes = Arrays.asList(variables.split(";"));
        }
        else {
            colonnes = new ArrayList<>();
        }

        String nativeQueryCount;
        Long resultCount;
        //on parcours toutes les possibilités de resultats jusqu'a 99
        int borneMin = 99;
        int borneMax = 0;

        try {
            for (int i = 0; i < 99; i++) {
                for (String colonne : colonnes) {
                    nativeQueryCount = "db." + table + ".count( {" + colonne + ": " + i + "} )";
                    resultCount = (Long) this.entityManager.createNativeQuery(nativeQueryCount).getSingleResult();

                    if (resultCount != 0) {
                        //on atteint la premiere valeur de resultat possible de la BBD de reference
                        if (i < borneMin) borneMin = i;
                        borneMax = i;
                    }
                    if (i >= scores.size()) {
                        scores.add(resultCount);
                    } else {
                        scores.set(i, resultCount + scores.get(i));
                    }
                }
            }
            //on ajoute dans le tableau sont ceux qui n'ont pas de score (score = 99) -> position : 99
            int resultat99 = 0;
            for (String colonne: colonnes) {
                nativeQueryCount = "db." + table + ".count( {" + colonne + ": " + 99 + "} )";
                resultCount = (Long) this.entityManager.createNativeQuery(nativeQueryCount).getSingleResult();
                resultat99 += resultCount;
            }
            scores.add((long) resultat99);

            //on ajoute les bornes max et min dans le tableau -> position : 100 et 101
            scores.add((long) borneMin);
            scores.add((long) borneMax);

            //on ajoute le nombres de colonne en 102
            scores.add((long) colonnes.size());
            LOG.info("tailles core {}", (long) scores.size());
            //la derniere entrée dans le tableau est le nombre de personne total (utile pour mettre les resultats sous pourcentage dans la JSP) position -> 103
            //le nombre de lignes dans la base
            nativeQueryCount = "db." + table + ".count()";
            Long nbLignes = (Long) this.entityManager.createNativeQuery(nativeQueryCount).getSingleResult();
            scores.add(nbLignes);
        } catch (final MongoException e) {
            throw new EsterDaoException(e.getMessage(), "Impossible d'accéder à la base de données... Contactez un administrateur");
        } catch (final Exception e) {
            throw new EsterDaoException(e.getMessage());
        }

        return scores;
    }

    @SuppressWarnings("unchecked")
    @Override
    public long getPercentageValueVariable(String tableReferenceName, String variable, int value) throws EsterDaoException {
        float percentage = 0;

        try {
            String nativeQueryValueCount = "db." + tableReferenceName + ".count( {" + variable + ": " + value + "} )";
            String nativeQueryTotalCount = "db." + tableReferenceName + ".count()";
            Long resultCount = (Long) this.entityManager.createNativeQuery(nativeQueryValueCount).getSingleResult();
            Long totalCount = (Long) this.entityManager.createNativeQuery(nativeQueryTotalCount).getSingleResult();
            percentage = (resultCount*100)/totalCount;
        } catch (final MongoException e) {
            throw new EsterDaoException(e.getMessage(), "Impossible d'accéder à la base de données... Contactez un administrateur");
        } catch (final Exception e) {
            throw new EsterDaoException(e.getMessage());
        }
        return (long)percentage;
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<QuestionBean> getQuestionsPerGroup(final ResultCriteria resultCriteria)
            throws EsterDaoException {
        List<QuestionBean> questionBeans = new ArrayList<QuestionBean>();
        if (resultCriteria == null) {
            throw new EsterDaoException("Resultat criteria is null");
        }
        try {
            final StringBuilder sb = new StringBuilder();
            sb.append("{");
            sb.append("'que_questionnary':'" + resultCriteria.getIdQuestionnary() + "'");
            sb.append("}");
            questionBeans = this.entityManager
                    .createNativeQuery(sb.toString(), QuestionBean.class).getResultList();
            System.out.println(sb);

        } catch (final MongoException e) {
            throw new EsterDaoException(e.getMessage(), "Impossible d'accéder à la base de données... Contactez un administrateur");
        } catch (final Exception e) {
            throw new EsterDaoException(e.getMessage());
        }
        return questionBeans;
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<EmployeeBean> getEmployeeWithAnswer(final ResultCriteria resultCriteria)
            throws EsterDaoException {
        List<EmployeeBean> employeeBeans = new ArrayList<EmployeeBean>();
        if (resultCriteria == null) {
            throw new EsterDaoException("Resultat criteria is null");
        }
        try {
            final StringBuilder sb = new StringBuilder();
            sb.append("{");
            sb.append("'questionnariesAnswered':'" + resultCriteria.getIdQuestionnary() + "'");
            sb.append("}");
            employeeBeans = this.entityManager
                    .createNativeQuery(sb.toString(), EmployeeBean.class).getResultList();
            System.out.println(sb);

        } catch (final MongoException e) {
            throw new EsterDaoException(e.getMessage(), "Impossible d'accéder à la base de données... Contactez un administrateur");
        } catch (final Exception e) {
            throw new EsterDaoException(e.getMessage());
        }
        return employeeBeans;
    }
}
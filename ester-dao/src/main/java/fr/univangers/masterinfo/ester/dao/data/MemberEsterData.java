package fr.univangers.masterinfo.ester.dao.data;

import fr.univangers.masterinfo.ester.dao.bean.MemberEsterBean;
import fr.univangers.masterinfo.ester.dao.bean.UserGroup;
import fr.univangers.masterinfo.ester.dao.bean.UserRole;
import org.springframework.stereotype.Component;

import java.util.Date;

/**
 * Ajouter des données fictives sur les membre ester
 *
 * @version 1.0
 * @date 01/11/2020
 */
@Component
public class MemberEsterData extends AbstractData<MemberEsterBean> {

    /**
     * Crée un utilisateur admin pour la démo
     *
     * @return Un utilisateur admin pour la démo
     */
    public MemberEsterBean getUserAdmin() {

        final MemberEsterBean user = new MemberEsterBean();
        user.setLogin("AdminESTER");
        user.setRole(UserRole.ADMIN);
        user.setGroup(UserGroup.MEMBRE_ESTER);
        user.setConnectionNumber(0);
        user.setConnectionTime(0);
        user.setDateCreation(new Date());
        user.setEmail("adm.root.ester@gmail.com");
        user.setPassword("AdminESTER49");

        return user;
    }

    /**
     * Crée un utilisateur assistant pour la démo
     *
     * @return Un utilisateur assistant pour la démo
     */
    public MemberEsterBean getUserPluridisciplinaire() {

        final MemberEsterBean user = new MemberEsterBean();
        user.setLogin("PluridisciplinaireESTER");
        user.setRole(UserRole.PLURIDISCIPLINAIRE);
        user.setGroup(UserGroup.MEMBRE_ESTER);
        user.setConnectionNumber(10);
        user.setConnectionTime(10);
        user.setDateCreation(new Date());
        user.setEmail("Pluridisciplinaire.ester@gmail.com");
        user.setPassword("Pluridisciplinaire49");

        return user;
    }


    /**
     * Crée un utilisateur médecin pour la démo
     *
     * @return Un utilisateur médecin pour la démo
     */
    public MemberEsterBean getUserMedecin() {

        final MemberEsterBean user = new MemberEsterBean();
        user.setLogin("MedecinESTER");
        user.setRole(UserRole.MEDECIN);
        user.setGroup(UserGroup.MEMBRE_ESTER);
        user.setConnectionNumber(20);
        user.setConnectionTime(20);
        user.setDateCreation(new Date());
        user.setEmail("medecin.ester@gmail.com");
        user.setPassword("MedecinESTER49");

        return user;
    }

}

package fr.univangers.masterinfo.ester.dao.bean;

/**
 * Les différents OS supportés par l'application
 *
 * @version 1.0
 * @date 01/11/2020
 */
public enum Environment {
    /**
     * OS Linux
     */
    LINUX("bash", "-c"),

    /**
     * OS Windows
     */
    WINDOWS("cmd.exe", "/c");

    /**
     * Le nom de l'éxécutable
     */
    private final String[] commands = new String[3];

    /**
     * Constructeur
     *
     * @param commands
     */
    Environment(final String... commands) {
        this.commands[0] = commands[0];
        this.commands[1] = commands[1];
    }

    /**
     * @return the commands
     */
    public String[] getCommands() {
        return this.commands;
    }
}

package fr.univangers.masterinfo.ester.dao.bean;

import javax.persistence.*;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

/**
 * Bean qui représente un email
 *
 * @version 1.0
 * @date 04/10/2020
 */
@Entity
@Table(name = "t_mail")
public class MailBean extends AbstractBean {

    /**
     * Serial Version UID
     */
    private static final long serialVersionUID = 1L;

    /**
     * Date d'envoi
     */
    @Column(name = "mai_date_send")
    @Temporal(TemporalType.DATE)
    private Date dateSend;

    /**
     * Expéditeur
     */
    @Column(name = "mai_from")
    private String from;

    /**
     * Destinataires
     */
    @Column(name = "mai_to")
    @ElementCollection
    private Set<String> to = new HashSet<>();

    /**
     * Destinataires mises en copies
     */
    @Column(name = "mai_cc")
    @ElementCollection
    private Set<String> cc = new HashSet<>();

    /**
     * Sujet du mail
     */
    @Column(name = "mai_subject")
    private String subject;

    /**
     * Message
     */
    @Column(name = "mai_msg")
    private String msg;

    /**
     * @return the dateSend
     */
    public Date getDateSend() {
        return this.dateSend;
    }

    /**
     * @param dateSend the dateSend to set
     */
    public void setDateSend(final Date dateSend) {
        this.dateSend = dateSend;
    }

    /**
     * @return the from
     */
    public String getFrom() {
        return this.from;
    }

    /**
     * @param from the from to set
     */
    public void setFrom(final String from) {
        this.from = from;
    }

    /**
     * @return the to
     */
    public Set<String> getTo() {
        return this.to;
    }

    /**
     * @param to the to to set
     */
    public void setTo(final Set<String> to) {
        this.to = to;
    }

    /**
     * @return the cc
     */
    public Set<String> getCc() {
        return this.cc;
    }

    /**
     * @param cc the cc to set
     */
    public void setCc(final Set<String> cc) {
        this.cc = cc;
    }

    /**
     * @return the subject
     */
    public String getSubject() {
        return this.subject;
    }

    /**
     * @param subject the subject to set
     */
    public void setSubject(final String subject) {
        this.subject = subject;
    }

    /**
     * @return the msg
     */
    public String getMsg() {
        return this.msg;
    }

    /**
     * @param msg the msg to set
     */
    public void setMsg(final String msg) {
        this.msg = msg;
    }
}

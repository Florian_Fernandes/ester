package fr.univangers.masterinfo.ester.dao.contract;

import fr.univangers.masterinfo.ester.dao.bean.QuestionnaryBean;
import fr.univangers.masterinfo.ester.dao.exception.EsterDaoException;

/**
 * Contrat d'une DAO questionnaire
 *
 * @author Marwa KHADJI
 * @version 1.0
 * @date 25/10/2020
 */
public interface QuestionnaryDaoContract extends AbstractDaoContract<QuestionnaryBean> {

    QuestionnaryBean findQuestionnaryByName(final String name) throws EsterDaoException;
}

package fr.univangers.masterinfo.ester.dao.data;

import fr.univangers.masterinfo.ester.dao.bean.QuestionOptionBean;
import fr.univangers.masterinfo.ester.dao.exception.EsterDaoException;
import org.springframework.stereotype.Component;

/**
 * Ajouter des données fictives sur les options des questions
 *
 * @version 1.0
 * @date 01/11/2020
 */
@Component
public class QuestionOptionData extends AbstractData<QuestionOptionBean> {

    /**
     * @return l'option question
     * @throws EsterDaoException
     */
    public QuestionOptionBean getQuestionOptionPasEffort() throws EsterDaoException {
        final QuestionOptionBean pasEffort = new QuestionOptionBean();
        pasEffort.setScore(0);
        pasEffort.setCoeff(1);
        pasEffort.setValue("Pas d'effort du tout");
        pasEffort.setCode("6");
        pasEffort.setPosition(1);

        return pasEffort;
    }

    /**
     * @return l'option question
     * @throws EsterDaoException
     */
    public QuestionOptionBean getQuestionOptionExtremementLeger1() throws EsterDaoException {
        final QuestionOptionBean extremementLeger1 = new QuestionOptionBean();
        extremementLeger1.setScore(0);
        extremementLeger1.setCoeff(1);
        extremementLeger1.setValue("Extrêmement léger");
        extremementLeger1.setCode("7");
        extremementLeger1.setPosition(2);

        return extremementLeger1;
    }

    /**
     * @return l'option question
     * @throws EsterDaoException
     */
    public QuestionOptionBean getQuestionOptionExtremementLeger2() throws EsterDaoException {
        final QuestionOptionBean extremementLeger2 = new QuestionOptionBean();
        extremementLeger2.setScore(0);
        extremementLeger2.setCoeff(1);
        extremementLeger2.setValue("");
        extremementLeger2.setCode("8");
        extremementLeger2.setPosition(3);

        return extremementLeger2;
    }

    /**
     * @return l'option question
     * @throws EsterDaoException
     */
    public QuestionOptionBean getQuestionOptionTresLeger1() throws EsterDaoException {
        final QuestionOptionBean tresLeger1 = new QuestionOptionBean();
        tresLeger1.setScore(0);
        tresLeger1.setCoeff(1);
        tresLeger1.setValue("Très léger");
        tresLeger1.setCode("9");
        tresLeger1.setPosition(4);

        return tresLeger1;
    }

    /**
     * @return l'option question
     * @throws EsterDaoException
     */
    public QuestionOptionBean getQuestionOptionTresLeger2() throws EsterDaoException {
        final QuestionOptionBean tresLeger2 = new QuestionOptionBean();
        tresLeger2.setScore(0);
        tresLeger2.setCoeff(1);
        tresLeger2.setValue("");
        tresLeger2.setCode("10");
        tresLeger2.setPosition(5);

        return tresLeger2;
    }

    /**
     * @return l'option question
     * @throws EsterDaoException
     */
    public QuestionOptionBean getQuestionOptionLeger1() throws EsterDaoException {
        final QuestionOptionBean leger1 = new QuestionOptionBean();
        leger1.setScore(0);
        leger1.setCoeff(1);
        leger1.setValue("Léger");
        leger1.setCode("11");
        leger1.setPosition(6);

        return leger1;
    }

    /**
     * @return l'option question
     * @throws EsterDaoException
     */
    public QuestionOptionBean getQuestionOptionLeger2() throws EsterDaoException {
        final QuestionOptionBean leger2 = new QuestionOptionBean();
        leger2.setScore(0);
        leger2.setCoeff(1);
        leger2.setValue("");
        leger2.setCode("12");
        leger2.setPosition(7);

        return leger2;
    }

    /**
     * @return l'option question
     * @throws EsterDaoException
     */
    public QuestionOptionBean getQuestionOptionPeuDur1() throws EsterDaoException {
        final QuestionOptionBean peuDur1 = new QuestionOptionBean();
        peuDur1.setScore(0);
        peuDur1.setCoeff(1);
        peuDur1.setValue("Un peu dur");
        peuDur1.setCode("13");
        peuDur1.setPosition(8);

        return peuDur1;
    }

    /**
     * @return l'option question
     * @throws EsterDaoException
     */
    public QuestionOptionBean getQuestionOptionPeuDur2() throws EsterDaoException {
        final QuestionOptionBean peuDur2 = new QuestionOptionBean();
        peuDur2.setScore(0);
        peuDur2.setCoeff(1);
        peuDur2.setValue("");
        peuDur2.setCode("14");
        peuDur2.setPosition(9);

        return peuDur2;
    }

    /**
     * @return l'option question
     * @throws EsterDaoException
     */
    public QuestionOptionBean getQuestionOptionDur1() throws EsterDaoException {
        final QuestionOptionBean dur1 = new QuestionOptionBean();
        dur1.setScore(2);
        dur1.setCoeff(1);
        dur1.setValue("Dur");
        dur1.setCode("15");
        dur1.setPosition(10);

        return dur1;
    }

    /**
     * @return l'option question
     * @throws EsterDaoException
     */
    public QuestionOptionBean getQuestionOptionDur2() throws EsterDaoException {
        final QuestionOptionBean dur2 = new QuestionOptionBean();
        dur2.setScore(2);
        dur2.setCoeff(1);
        dur2.setValue("");
        dur2.setCode("16");
        dur2.setPosition(11);

        return dur2;
    }

    /**
     * @return l'option question
     * @throws EsterDaoException
     */
    public QuestionOptionBean getQuestionOptionTresDur1() throws EsterDaoException {
        final QuestionOptionBean tresDur1 = new QuestionOptionBean();
        tresDur1.setScore(2);
        tresDur1.setCoeff(1);
        tresDur1.setValue("Très dur");
        tresDur1.setCode("17");
        tresDur1.setPosition(12);

        return tresDur1;
    }

    /**
     * @return l'option question
     * @throws EsterDaoException
     */
    public QuestionOptionBean getQuestionOptionTresDur2() throws EsterDaoException {
        final QuestionOptionBean tresDur2 = new QuestionOptionBean();
        tresDur2.setScore(2);
        tresDur2.setCoeff(1);
        tresDur2.setValue("");
        tresDur2.setCode("18");
        tresDur2.setPosition(13);

        return tresDur2;
    }

    /**
     * @return l'option question
     * @throws EsterDaoException
     */
    public QuestionOptionBean getQuestionOptionExtremementDur() throws EsterDaoException {
        final QuestionOptionBean extremementDur = new QuestionOptionBean();
        extremementDur.setScore(2);
        extremementDur.setCoeff(1);
        extremementDur.setValue("Extremement dur");
        extremementDur.setCode("19");
        extremementDur.setPosition(14);

        return extremementDur;
    }

    /**
     * @return l'option question
     * @throws EsterDaoException
     */
    public QuestionOptionBean getQuestionOptionEpuisant() throws EsterDaoException {
        final QuestionOptionBean epuisant = new QuestionOptionBean();
        epuisant.setScore(2);
        epuisant.setCoeff(1);
        epuisant.setValue("Epuisant");
        epuisant.setCode("20");
        epuisant.setPosition(15);

        return epuisant;
    }

    /**
     * @return l'option question
     * @throws EsterDaoException
     */
    public QuestionOptionBean getQuestionOptionJamais() throws EsterDaoException {
        final QuestionOptionBean jamais = new QuestionOptionBean();
        jamais.setCode("1");
        jamais.setCoeff(1);
        jamais.setValue("Jamais");
        jamais.setScore(0);
        jamais.setPosition(1);

        return jamais;
    }

    /**
     * @return l'option question
     * @throws EsterDaoException
     */
    public QuestionOptionBean getQuestionOptionRarement() throws EsterDaoException {
        final QuestionOptionBean rarement = new QuestionOptionBean();
        rarement.setCode("2");
        rarement.setCoeff(1);
        rarement.setValue("Rarement (< 2 heures/jour)");
        rarement.setScore(0);
        rarement.setPosition(2);

        return rarement;
    }

    /**
     * @return l'option question
     * @throws EsterDaoException
     */
    public QuestionOptionBean getQuestionOptionSouvent() throws EsterDaoException {
        final QuestionOptionBean souvent = new QuestionOptionBean();
        souvent.setCode("3");
        souvent.setCoeff(1);
        souvent.setValue("Souvent (2 à 4 heures/jour)");
        souvent.setScore(2);
        souvent.setPosition(3);

        return souvent;
    }

    /**
     * @return l'option question
     * @throws EsterDaoException
     */
    public QuestionOptionBean getQuestionOptionTresSouvent() throws EsterDaoException {
        final QuestionOptionBean tresSouvent = new QuestionOptionBean();
        tresSouvent.setCode("4");
        tresSouvent.setCoeff(1);
        tresSouvent.setValue("La plupart du temps (> 4 heures/jour)");
        tresSouvent.setScore(2);
        tresSouvent.setPosition(4);

        return tresSouvent;
    }

    /**
     * @return l'option question
     * @throws EsterDaoException
     */
    public QuestionOptionBean getQuestionOptionPasDuToutDac() throws EsterDaoException {
        final QuestionOptionBean pasDuToutDac = new QuestionOptionBean();
        pasDuToutDac.setCode("1");
        pasDuToutDac.setCoeff(1);
        pasDuToutDac.setScore(0);
        pasDuToutDac.setValue("Pas du tout d'accord");
        pasDuToutDac.setPosition(1);

        return pasDuToutDac;
    }

    /**
     * @return l'option question
     * @throws EsterDaoException
     */
    public QuestionOptionBean getQuestionOptionPasDac() throws EsterDaoException {
        final QuestionOptionBean pasDac = new QuestionOptionBean();
        pasDac.setCode("2");
        pasDac.setCoeff(1);
        pasDac.setScore(0);
        pasDac.setValue("Pas d'accord");
        pasDac.setPosition(2);

        return pasDac;
    }

    /**
     * @return l'option question
     * @throws EsterDaoException
     */
    public QuestionOptionBean getQuestionOptionDac() throws EsterDaoException {
        final QuestionOptionBean dac = new QuestionOptionBean();
        dac.setCode("3");
        dac.setCoeff(1);
        dac.setScore(2);
        dac.setValue("D'accord");
        dac.setPosition(3);

        return dac;
    }

    /**
     * @return l'option question
     * @throws EsterDaoException
     */
    public QuestionOptionBean getQuestionOptionToutAFaitDac() throws EsterDaoException {
        final QuestionOptionBean toutAFaitDac = new QuestionOptionBean();
        toutAFaitDac.setCode("4");
        toutAFaitDac.setCoeff(1);
        toutAFaitDac.setScore(2);
        toutAFaitDac.setValue("Tout à fait d'accord");
        toutAFaitDac.setPosition(4);

        return toutAFaitDac;
    }
}

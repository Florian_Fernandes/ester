package fr.univangers.masterinfo.ester.dao.bean;

import javax.persistence.*;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

/**
 * Bean qui represente un résultat
 *
 * @author Mathew COYLE
 * @version 1.0
 * @date 14/10/2020
 */
@Entity
@Table(name = "t_result")
public class ResultBean extends AbstractBean {

    /**
     * Serial Version UID
     */
    private static final long serialVersionUID = 1L;

    /**
     * Score global du résultat Somme des scores de chaque reponse
     */
    @Column(name = "res_score_global")
    private int scoreGlobal;

    /**
     * La date du résultat produit
     */
    @Temporal(TemporalType.DATE)
    @Column(name = "res_date_result")
    private Date dateResult;

    /**
     * Le salarié qui a produit le résultat
     */
    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE}, fetch = FetchType.EAGER)
    @JoinColumn(name = "res_employee")
    private EmployeeBean employee;

    /**
     * Le questionnaire associé
     */
    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE}, fetch = FetchType.EAGER)
    @JoinColumn(name = "res_questionnary")
    private QuestionnaryBean questionnary;

    /**
     * L'ensemble des réponses du questionnaire
     */
    @OneToMany(mappedBy = "result", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private Set<AnswerBean> answers = new HashSet<>();

    /**
     * @return the scoreGlobal
     */
    public int getScoreGlobal() {
        return this.scoreGlobal;
    }

    /**
     * @param scoreGlobal the scoreGlobal to set
     */
    public void setScoreGlobal(int scoreGlobal) {
        this.scoreGlobal = scoreGlobal;
    }

    /**
     * @return the dateResult
     */
    public Date getDateResult() {
        return this.dateResult;
    }

    /**
     * @param dateResult the dateResult to set
     */
    public void setDateResult(final Date dateResult) {
        this.dateResult = dateResult;
    }

    /**
     * @return the employee
     */
    public EmployeeBean getEmployee() {
        return this.employee;
    }

    /**
     * @param employee the employee to set
     */
    public void setEmployee(final EmployeeBean employee) {
        this.employee = employee;
    }

    /**
     * @return the questionnary
     */
    public QuestionnaryBean getQuestionnary() {
        return this.questionnary;
    }

    /**
     * @param questionnary the questionnary to set
     */
    public void setQuestionnary(final QuestionnaryBean questionnary) {
        this.questionnary = questionnary;
    }

    /**
     * @return the answers
     */
    public Set<AnswerBean> getAnswers() {
        return this.answers;
    }

    /**
     * @param answers the answers to set
     */
    public void setAnswers(final Set<AnswerBean> answers) {
        this.answers = answers;
    }
}

package fr.univangers.masterinfo.ester.dao.data;

import fr.univangers.masterinfo.ester.dao.bean.QuestionBean;
import fr.univangers.masterinfo.ester.dao.bean.QuestionnaryBean;
import fr.univangers.masterinfo.ester.dao.exception.EsterDaoException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Ajouter des données fictives sur les questionnaires
 *
 * @version 1.0
 * @date 01/11/2020
 */
@Component
public class QuestionnaryData extends AbstractData<QuestionnaryBean> {

    /**
     * Data sur les questions
     */
    @Autowired
    private QuestionData questionData;

    /**
     * Questionnaire chronique
     *
     * @return questionnaire
     * @throws EsterDaoException
     */
    public QuestionnaryBean getQuestionnaryChronique() throws EsterDaoException {
        final QuestionnaryBean chronique = new QuestionnaryBean();
        chronique.setVersion(1);
        chronique.setCode("evalRiskTMS_SMS-MSChronique");
        chronique.setName("Eval-Risk-TMS : Risque de SMS-MS chronique");
        chronique.setDateSubmission(new Date());
        chronique.setDateLastModified(new Date());

        final List<QuestionBean> questions = new ArrayList<>();
        questions.add(this.questionData.getQuestionEchelleBorg());
        questions.add(this.questionData.getQuestionRepeterAction());
        questions.add(this.questionData.getQuestionTravaillerBrasAir());
        questions.add(this.questionData.getQuestionFlechirCoude());
        questions.add(this.questionData.getQuestionPresserObjet());
        questions.add(this.questionData.getQuestionInfluencerTravail());
        questions.add(this.questionData.getQuestionColleguesAidentTaches());

        for (final QuestionBean question : questions) {
            question.setQuestionnary(chronique);
            chronique.getQuestions().add(question);
        }

        return chronique;
    }
}

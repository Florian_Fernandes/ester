package fr.univangers.masterinfo.ester.dao.impl;

import fr.univangers.masterinfo.ester.dao.bean.UserBean;
import fr.univangers.masterinfo.ester.dao.contract.UserDaoContract;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;

/**
 * Implémentation de la DAO user
 *
 * @author Matthew COYLE
 * @version 1.0
 * @date 14/10/2020
 */
@Transactional
@Repository
public class UserDaoImpl extends AbstractDaoImpl<UserBean> implements UserDaoContract {

    /**
     * Le constructeur
     */
    public UserDaoImpl() {
        super(UserBean.class);
    }
}

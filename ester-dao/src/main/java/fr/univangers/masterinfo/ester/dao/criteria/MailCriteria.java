package fr.univangers.masterinfo.ester.dao.criteria;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

/**
 * Filtre pour interroger les données d'un salarié
 *
 * @version 1.0
 * @date 04/10/2020
 */
public class MailCriteria extends AbstractCriteria {

    /**
     * Date d'envoi
     */
    private Date dateSend;

    /**
     * Expéditeur
     */
    private String from;

    /**
     * Destinataires
     */
    private Set<String> to = new HashSet<>();

    /**
     * Destinataires mises en copies
     */
    private Set<String> cc = new HashSet<>();

    /**
     * Sujet du mail
     */
    private String subject;

    /**
     * @return the dateSend
     */
    public Date getDateSend() {
        return this.dateSend;
    }

    /**
     * @param dateSend the dateSend to set
     */
    public void setDateSend(final Date dateSend) {
        this.dateSend = dateSend;
    }

    /**
     * @return the from
     */
    public String getFrom() {
        return this.from;
    }

    /**
     * @param from the from to set
     */
    public void setFrom(final String from) {
        this.from = from;
    }

    /**
     * @return the to
     */
    public Set<String> getTo() {
        return this.to;
    }

    /**
     * @param to the to to set
     */
    public void setTo(final Set<String> to) {
        this.to = to;
    }

    /**
     * @return the cc
     */
    public Set<String> getCc() {
        return this.cc;
    }

    /**
     * @param cc the cc to set
     */
    public void setCc(final Set<String> cc) {
        this.cc = cc;
    }

    /**
     * @return the subject
     */
    public String getSubject() {
        return this.subject;
    }

    /**
     * @param subject the subject to set
     */
    public void setSubject(final String subject) {
        this.subject = subject;
    }
}

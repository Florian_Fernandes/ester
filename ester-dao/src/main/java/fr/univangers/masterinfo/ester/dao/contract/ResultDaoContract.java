package fr.univangers.masterinfo.ester.dao.contract;

import fr.univangers.masterinfo.ester.dao.bean.EmployeeBean;
import fr.univangers.masterinfo.ester.dao.bean.QuestionBean;
import fr.univangers.masterinfo.ester.dao.bean.ResultBean;
import fr.univangers.masterinfo.ester.dao.criteria.ResultCriteria;
import fr.univangers.masterinfo.ester.dao.exception.EsterDaoException;

import java.util.List;

/**
 * Le contrat d'une DAO resultat
 */
public interface ResultDaoContract extends AbstractDaoContract<ResultBean> {


    /**
     * Récupérer le score global par enmployé et par questionnaire
     *
     * @param resultCriteria
     * @return
     * @throws EsterDaoException
     */
    public int getScoreGlobalByEmployeeAndQuestionnary(final ResultCriteria resultCriteria) throws EsterDaoException;

    /**
     * Récupérer une liste de score
     *
     *
     * @return
     * @throws EsterDaoException
     */
    List<Long> getlistScoreResults(String tableReferenceName, String variables) throws EsterDaoException;

    /**
     * Récupérer le pourcentage d'apparition de la valeur de la variable
     *
     * @param tableReferenceName
     * @param variable
     * @param value
     * @return
     * @throws EsterDaoException
     */
    long getPercentageValueVariable(String tableReferenceName, String variable, int value) throws EsterDaoException;

    /**
     * Récupérer les Questions d'un questionnaire
     *
     * @param resultCriteria
     * @return
     * @throws EsterDaoException
     */
    List<QuestionBean> getQuestionsPerGroup(ResultCriteria resultCriteria) throws EsterDaoException;

    /**
     * Récupérer les résultat d'un salarié
     *
     * @param resultCriteria
     * @return
     * @throws EsterDaoException
     */
    ResultBean getResultByEmployeeAndQuestionnary(ResultCriteria resultCriteria) throws EsterDaoException;

    /**
     * Récupérer employee avec leur reponses
     *
     * @param resultCriteria
     * @return
     * @throws EsterDaoException
     */
    List<EmployeeBean> getEmployeeWithAnswer(ResultCriteria resultCriteria) throws EsterDaoException;
}
package fr.univangers.masterinfo.ester.dao.criteria;

/**
 * Filtre pour interroger les données d'une vidéo
 *
 * @version 1.0
 * @date 04/10/2020
 */
public class VideoCriteria extends AbstractCriteria {

    /**
     * Code unique de la vidéo
     */
    private String code;

    /**
     * @return the code
     */
    public String getCode() {
        return this.code;
    }

    /**
     * @param code the code to set
     */
    public void setCode(final String code) {
        this.code = code;
    }
}

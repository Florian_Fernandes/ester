package fr.univangers.masterinfo.ester.dao.contract;

import fr.univangers.masterinfo.ester.dao.bean.ReferenceBean;
import fr.univangers.masterinfo.ester.dao.criteria.ReferenceCriteria;
import fr.univangers.masterinfo.ester.dao.exception.EsterDaoException;

/**
 * Contrat d'une DAO de données de références
 *
 * @version 1.0
 * @date 01/11/2020
 */
public interface ReferenceDaoContract extends AbstractDaoContract<ReferenceBean> {

    /**
     * Ajouter des données de références
     *
     * @param criteria
     * @throws EsterDaoException
     */
    void addReference(ReferenceCriteria criteria) throws EsterDaoException;

    /**
     * Mettre à jour des données de références
     *
     * @param criteria
     * @throws EsterDaoException
     */
    void updateReference(ReferenceCriteria criteria) throws EsterDaoException;

    /**
     * Mettre à jour des données de références
     *
     * @param reference
     * @throws EsterDaoException
     */
    public void removeReferenceTable(final ReferenceBean reference) throws EsterDaoException;

    /**
     * Trouve et renvoie le ReferenceBean trouvé dans la base de données, sinon null
     *
     * @param id
     * @throws EsterDaoException
     */
    ReferenceBean findReference(String id) throws EsterDaoException;
}

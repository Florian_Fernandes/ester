package fr.univangers.masterinfo.ester.dao.impl;

import fr.univangers.masterinfo.ester.dao.bean.AnswerBean;
import fr.univangers.masterinfo.ester.dao.contract.AnswerDaoContract;
import fr.univangers.masterinfo.ester.dao.criteria.AnswerCriteria;
import fr.univangers.masterinfo.ester.dao.exception.EsterDaoException;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

/**
 * Implémentation de le DAO reponse
 *
 * @author Matthew COYLE
 * @version 1.0
 * @date 14/10/2020
 */
@Transactional
@Repository
public class AnswerDaoImpl extends AbstractDaoImpl<AnswerBean> implements AnswerDaoContract {

    /**
     * Le constructeur
     */
    public AnswerDaoImpl() {
        super(AnswerBean.class);
    }

    /**
     * Fonction qui retourn un Answer avec un Résultat et un employé
     */
    @SuppressWarnings("unchecked")
    @Override
    public List<AnswerBean> getListAnwserByResulat(AnswerCriteria answerCriteria) throws EsterDaoException {
        List<AnswerBean> answers = new ArrayList<AnswerBean>();
        if (answerCriteria == null) {
            throw new EsterDaoException("Resultat criteria is null", "Erreur dans la recherche en base de données");
        }
        try {
            final StringBuilder sb = new StringBuilder();
            sb.append("{");
            sb.append("'ans_result':'" + answerCriteria.getIdResultat() + "'");
            sb.append("}");
            sb.append("'ans_employee':'" + answerCriteria.getIdEmployee() + "'");
            sb.append("}");
            answers = this.entityManager.createNativeQuery(sb.toString(), AnswerBean.class).getResultList();

        } catch (final Exception e) {
            throw new EsterDaoException(e.getMessage(), "Erreur dans la recherche en base de données");
        }
        return answers;
    }
}

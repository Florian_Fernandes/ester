package fr.univangers.masterinfo.ester.dao.data;

import fr.univangers.masterinfo.ester.dao.bean.AbstractBean;
import org.springframework.stereotype.Component;

/**
 * Ajouter des données fictives dans la BD
 *
 * @param <T> l'entité
 * @version 1.0
 * @date 01/11/2020
 */
@Component
public abstract class AbstractData<T extends AbstractBean> {

}

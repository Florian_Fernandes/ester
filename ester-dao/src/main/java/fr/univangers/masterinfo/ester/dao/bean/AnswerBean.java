package fr.univangers.masterinfo.ester.dao.bean;

import com.mongodb.lang.Nullable;

import javax.persistence.*;
import java.util.Set;

/**
 * Bean qui represente une réponse donnée par un salarié
 *
 * @author Matthew COYLE
 * @version 1.0
 * @date 14/10/2020
 */
@Entity
@Table(name = "t_answer")
public class AnswerBean extends AbstractBean {

    /**
     * Serial Version UID
     */
    private static final long serialVersionUID = 1L;

    /**
     * Le salarié qui a produit la réponse
     */
    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE}, fetch = FetchType.EAGER)
    @JoinColumn(name = "ans_employee")
    private EmployeeBean employee;

    /**
     * Les réponses options sélectionnées par le salarié
     */
    @ManyToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE}, fetch = FetchType.EAGER)
    @JoinColumn(name = "ans_response")
    private Set<QuestionOptionBean> response;

    /**
     *
     */
    @Nullable()
    @JoinColumn(name = "ans_responseID")
    private String responseID;

    /**
     * Le résultat associé
     */
    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE}, fetch = FetchType.EAGER)
    @JoinColumn(name = "ans_result")
    private ResultBean result;

    /**
     * reponse courte ou longue du l'employé
     */
    @Nullable()
    @Column(name = "reponsesEmpl")
    private String reponsesEmploye;

    /**
     * @return reponse de l'employé
     */
    public String getReponsesEmploye() {
        return this.reponsesEmploye;
    }

    /**
     * @param reponsesEmploye the employee to set
     */
    public void setReponsesEmploye(final String reponsesEmploye) {
        this.reponsesEmploye = reponsesEmploye;
    }

    /**
     * @return the employee
     */
    public EmployeeBean getEmployee() {
        return this.employee;
    }

    /**
     * @param employee the employee to set
     */
    public void setEmployee(final EmployeeBean employee) {
        this.employee = employee;
    }

    /**
     * @return the response
     */
    public Set<QuestionOptionBean> getResponse() {
        return this.response;
    }

    /**
     * @param responses the response to set
     */
    public void setResponse(final Set<QuestionOptionBean> responses) {
        this.response = responses;
    }

    public String getResponseID() {
        return this.responseID;
    }

    public void setResponseID(String responseID) {
        this.responseID = responseID;
    }

    /**
     * @return the result
     */
    public ResultBean getResult() {
        return this.result;
    }

    /**
     * @param result the result to set
     */
    public void setResult(final ResultBean result) {
        this.result = result;
    }

    /*	*//**
     * @return attribut pour questions courtes et longues
     */
    /*	public QuestionBean getResponseCourteOuLongue() {return responseCourteOuLongue;}*/

    /**
     * @param responseCourteOuLongue the result to set
     */
    /*	public void setResponseCourteOuLongue(QuestionBean responseCourteOuLongue) {this.responseCourteOuLongue = responseCourteOuLongue;}*/

}

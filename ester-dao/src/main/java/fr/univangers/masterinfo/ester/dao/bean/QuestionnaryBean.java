package fr.univangers.masterinfo.ester.dao.bean;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.*;

/**
 * Bean qui représente un questionnaire
 *
 * @author Marwa KHADJI
 * @version 1.0
 * @date 25/10/2020
 */
@Entity
@Table(name = "t_questionnary")
public class QuestionnaryBean extends AbstractBean {

    /**
     * Trier les questionnaires par ordre alphabétique
     */
    public static final Comparator<QuestionnaryBean> COMPARATOR_QUESTIONNARY = new Comparator<QuestionnaryBean>() {

        @Override
        public int compare(final QuestionnaryBean q1, final QuestionnaryBean q2) {
            return q1.name.compareTo(q2.name);
        }
    };
    /**
     * Serial Version UID
     */
    private static final long serialVersionUID = 1L;
    /**
     * Version du questionnaire
     */
    @Column(name = "que_version")
    private int version;

    /**
     * Code questionnaire
     */
    @Column(name = "que_code")
    private String code;

    /**
     * Nom du questionnaire
     */
    @Column(name = "que_name")
    private String name;

    /**
     * Nom du questionnaire
     */
    @Column(name = "que_published")
    private boolean published;

    /**
     * Date de soumission du questionnaire
     */
    @Temporal(TemporalType.DATE)
    @Column(name = "que_date_submission")
    private Date dateSubmission;

    /**
     * Date de dernière modification du questionnaire
     */
    @Temporal(TemporalType.DATE)
    @Column(name = "que_date_last_modified")
    private Date dateLastModified;

    /**
     * Ensemble des questions
     */
    @OneToMany(mappedBy = "questionnary", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @OrderBy("position")
    private Set<QuestionBean> questions = new TreeSet<>(QuestionBean.COMPARATOR_QUESTION);

    /**
     * Ensemble de résultats
     */
    @OneToMany(mappedBy = "questionnary", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JsonIgnore
    private Set<ResultBean> results = new HashSet<>();

    /**
     * Employés qui ont répondu au questionnaire
     */
    @ManyToMany(mappedBy = "questionnariesAnswered", cascade = {CascadeType.PERSIST,
            CascadeType.MERGE}, fetch = FetchType.EAGER)
    @JsonIgnore
    private Set<EmployeeBean> employeeAnswered = new HashSet<>();

    /**
     * Employés qui n'ont pas répondu au questionnaire
     */
    @ManyToMany(mappedBy = "questionnariesNoAnswered", cascade = {CascadeType.PERSIST,
            CascadeType.MERGE}, fetch = FetchType.EAGER)
    @JsonIgnore
    private Set<EmployeeBean> employeeNoAnswered = new HashSet<>();

    /**
     * Liste des Membres Ester administrant le questionnaire
     */
    @ManyToMany(cascade = {CascadeType.PERSIST,
            CascadeType.MERGE}, fetch = FetchType.EAGER)
    @JsonIgnore
    private Set<MemberEsterBean> memberAdmins = new HashSet<>();

    /**
     * Les données de références associées à ce questionnaire
     */
    @Column(name = "reference")
    private String reference;

    /**
     * le createur du questionnaire
     */
    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE}, fetch = FetchType.EAGER)
    @JoinColumn(name = "que_creator")
    @JsonIgnore
    private MemberEsterBean creator;

    /**
     * @return the version
     */
    public int getVersion() {
        return this.version;
    }

    /**
     * @param version the version to set
     */
    public void setVersion(final int version) {
        this.version = version;
    }

    /**
     * @return the code
     */
    public String getCode() {
        return this.code;
    }

    /**
     * @param code the code to set
     */
    public void setCode(final String code) {
        this.code = code;
    }

    /**
     * @return the name
     */
    public String getName() {
        return this.name;
    }

    /**
     * @param name the name to set
     */
    public void setName(final String name) {
        this.name = name;
    }

    /**
     * @return the dateSubmission
     */
    public Date getDateSubmission() {
        return this.dateSubmission;
    }

    /**
     * @param dateSubmission the dateSubmission to set
     */
    public void setDateSubmission(final Date dateSubmission) {
        this.dateSubmission = dateSubmission;
    }

    /**
     * @return the dateLastModified
     */
    public Date getDateLastModified() {
        return this.dateLastModified;
    }

    /**
     * @param dateLastModified the dateLastModified to set
     */
    public void setDateLastModified(final Date dateLastModified) {
        this.dateLastModified = dateLastModified;
    }

    /**
     * @return the questions
     */
    public Set<QuestionBean> getQuestions() {
        return this.questions;
    }

    /**
     * @param questions the questions to set
     */
    public void setQuestions(final Set<QuestionBean> questions) {
        this.questions = questions;
    }

    /**
     * @param position
     * @return la question avec la position passée en paramètre
     */
    public QuestionBean getQuestion(int position) {
        for (QuestionBean question : questions) {
            if (question.getPosition() == position) {
                return question;
            }
        }

        return null;
    }

    /**
     * @return the results
     */
    public Set<ResultBean> getResults() {
        return this.results;
    }

    /**
     * @param results the results to set
     */
    public void setResults(final Set<ResultBean> results) {
        this.results = results;
    }

    /**
     * @return the employeeAnswered
     */
    public Set<EmployeeBean> getEmployeeAnswered() {
        return this.employeeAnswered;
    }

    /**
     * @param employeeAnswered the employeeAnswered to set
     */
    public void setEmployeeAnswered(final Set<EmployeeBean> employeeAnswered) {
        this.employeeAnswered = employeeAnswered;
    }

    /**
     * @return the employeeNoAnswered
     */
    public Set<EmployeeBean> getEmployeeNoAnswered() {
        return this.employeeNoAnswered;
    }

    /**
     * @param employeeNoAnswered the employeeNoAnswered to set
     */
    public void setEmployeeNoAnswered(final Set<EmployeeBean> employeeNoAnswered) {
        this.employeeNoAnswered = employeeNoAnswered;
    }

    /**
     * @return the memberAdmins
     */
    public Set<MemberEsterBean> getMemberAdmins() {
        return this.memberAdmins;
    }

    /**
     * @param memberAdmins the memberAdmins to set
     */
    public void setMemberAdmins(final Set<MemberEsterBean> memberAdmins) {
        this.memberAdmins = memberAdmins;
    }

    /**
     * @return the creator
     */
    public MemberEsterBean getCreator() {
        return this.creator;
    }

    /**
     * @param creator the creator to set
     */
    public void setCreator(final MemberEsterBean creator) {
        this.creator = creator;
    }

    /**
     * @return published
     */
    public boolean getPublished() {
        return published;
    }

    /**
     * @param published the creator to set
     */
    public void setPublished(boolean published) {
        this.published = published;
    }

    /**
     * @return la base de référence liée au questionnaire
     */
    public String getReference() {
        return this.reference;
    }

    /**
     * associe une base de référence au questionnaire
     *
     * @param ref
     */
    public void setReference(String ref) {
        this.reference = ref;
    }
}
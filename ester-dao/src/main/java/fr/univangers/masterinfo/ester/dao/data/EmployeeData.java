package fr.univangers.masterinfo.ester.dao.data;

import fr.univangers.masterinfo.ester.dao.bean.EmployeeBean;
import fr.univangers.masterinfo.ester.dao.bean.UserGroup;
import fr.univangers.masterinfo.ester.dao.bean.UserRole;
import fr.univangers.masterinfo.ester.dao.bean.UserSexe;
import org.springframework.stereotype.Component;

import java.util.Date;

/**
 * Ajouter des données fictives sur les salariés
 *
 * @version 1.0
 * @date 01/11/2020
 */
@Component
public class EmployeeData extends AbstractData<EmployeeBean> {

    /**
     * Crée un salarié homme pour la démo
     *
     * @return Un salarié homme pour la démo
     */
    public EmployeeBean getEmployeeHomme() {

        final EmployeeBean user = new EmployeeBean();
        user.setLogin("abcd1234");
        user.setRole(UserRole.SALARIE);
        user.setGroup(UserGroup.SALARIE);
        user.setConnectionNumber(0);
        user.setConnectionTime(0);
        user.setDateCreation(new Date());
        user.setSexe(UserSexe.HOMME);
        user.setBirth(new Date());
        user.setPcs("Agriculteurs et éleveurs, salariés de leur exploitation");
        user.setPoste("Culture et production animale, chasse et services annexes");
        user.setCommentaires("Je suis éleveur de mouton");
        user.setRegion("Pays-de-la-Loire");
        user.setDepartement("Loire-Atlantique");

        return user;
    }

    /**
     * Crée une salarié femme pour la démo
     *
     * @return Une salarié femme pour la démo
     */
    public EmployeeBean getEmployeeFemme() {

        final EmployeeBean user = new EmployeeBean();
        user.setLogin("dcba4321");
        user.setRole(UserRole.SALARIE);
        user.setGroup(UserGroup.SALARIE);
        user.setConnectionNumber(0);
        user.setConnectionTime(0);
        user.setDateCreation(new Date());
        user.setSexe(UserSexe.FEMME);
        user.setBirth(new Date());
        user.setPcs("Agriculteurs et éleveurs, salariés de leur exploitation");
        user.setPoste("Culture et production animale, chasse et services annexes");
        user.setCommentaires("Je suis éleveur de mouton");
        user.setRegion("Pays-de-la-Loire");
        user.setDepartement("Loire-Atlantique");

        return user;
    }
}

package fr.univangers.masterinfo.ester.dao.criteria;

/**
 * Filtre pour interroger les données d'une reponse
 *
 * @author Matthew COYLE
 * @version 1.0
 * @date 14/10/2020
 */
public class AnswerCriteria extends AbstractCriteria {
    /**
     * Id employee
     */
    private String idEmployee;

    /**
     * Id reponses
     */
    private String idReponses;


    /**
     * Id resultat
     */
    private String idResultat;

    /**
     * @return idEmployee
     */
    public String getIdEmployee() {
        return this.idEmployee;
    }

    /**
     * @param idEmployee
     */
    public void setIdEmployee(final String idEmployee) {
        this.idEmployee = idEmployee;
    }

    /**
     * @return idReponses
     */
    public String getIdReponses() {
        return this.idReponses;
    }

    /**
     * @param idReponses
     */

    public void setIdQuestionnary(final String idReponses) {
        this.idReponses = idReponses;
    }

    /**
     * @return idResultat
     */
    public String getIdResultat() {
        return this.idResultat;
    }

    /**
     * @param idResultat
     */
    public void setIdResultat(String idResultat) {
        this.idResultat = idResultat;
    }

}

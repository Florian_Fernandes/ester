package fr.univangers.masterinfo.ester.dao.contract;

import fr.univangers.masterinfo.ester.dao.bean.QuestionOptionBean;

import java.util.Set;

/**
 * Contrat d'une DAO question option
 *
 * @author Marwa KHADJI
 * @version 1.0
 * @date 25/10/2020
 */
public interface QuestionOptionDaoContract extends AbstractDaoContract<QuestionOptionBean> {

    Set<QuestionOptionBean> getAllQuestionOptionsChecked(String idQuestion);
}
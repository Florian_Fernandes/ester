package fr.univangers.masterinfo.ester.dao.contract;

import fr.univangers.masterinfo.ester.dao.bean.UserBean;

/**
 * Le contrat d'une DAO user
 *
 * @author Matthew COYLE
 * @version 1.0
 * @date 14/10/2020
 */
public interface UserDaoContract extends AbstractDaoContract<UserBean> {

}

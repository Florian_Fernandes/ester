package fr.univangers.masterinfo.ester.dao.bean;

/**
 * Les catégories de personnes
 *
 * @version 1.0
 * @date 04/10/2020
 */
public enum UserGroup {

    /**
     * Salarié
     */
    SALARIE("Salarié"),

    /**
     * Membre ESTER (préventeur, infirmier, etc.)
     */
    MEMBRE_ESTER("Membre ESTER");

    /**
     * Le nom de la catégorie
     */
    private String name;

    /**
     * Constructeur
     *
     * @param name
     */
    private UserGroup(final String name) {
        this.name = name;
    }

    /**
     * @return the name
     */
    public String getName() {
        return this.name;
    }
}
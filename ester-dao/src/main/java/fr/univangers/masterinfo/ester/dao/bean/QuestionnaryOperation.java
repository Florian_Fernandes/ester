package fr.univangers.masterinfo.ester.dao.bean;

/**
 * Les différents types de questions
 *
 * @author Marwa KHADJI
 * @version 1.0
 * @date 25/10/2020
 */
public enum QuestionnaryOperation {

    GET_LIST,

    GET_ONE,

    SAVE,

    UPDATE,

    DELETE;
}

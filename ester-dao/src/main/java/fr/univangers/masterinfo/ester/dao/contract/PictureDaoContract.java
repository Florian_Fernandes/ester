package fr.univangers.masterinfo.ester.dao.contract;

import fr.univangers.masterinfo.ester.dao.bean.PictureBean;
import fr.univangers.masterinfo.ester.dao.criteria.PictureCriteria;
import fr.univangers.masterinfo.ester.dao.exception.EsterDaoException;

/**
 * Le contrat d'une DAO user
 *
 * @author Matthew COYLE
 * @version 1.0
 * @date 14/10/2020
 */
public interface PictureDaoContract extends AbstractDaoContract<PictureBean> {

    /**
     * Retrouver une image selon des critères
     *
     * @param pictureCriteria
     * @return L'utilisateur selon des critères
     * @throws EsterDaoException
     */
    PictureBean findPictureByCode(PictureCriteria pictureCriteria) throws EsterDaoException;
}

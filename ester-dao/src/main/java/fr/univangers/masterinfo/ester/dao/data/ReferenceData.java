package fr.univangers.masterinfo.ester.dao.data;

import fr.univangers.masterinfo.ester.dao.bean.FileFormat;
import fr.univangers.masterinfo.ester.dao.bean.ReferenceBean;
import fr.univangers.masterinfo.ester.dao.exception.EsterDaoException;
import org.springframework.stereotype.Component;

import java.io.File;

/**
 * Ajouter des données fictives sur les données de références
 *
 * @version 1.0
 * @date 01/11/2020
 */
@Component
public class ReferenceData extends AbstractData<ReferenceBean> {

    /**
     * Données de références cosali.csv
     *
     * @param root
     * @return l'objet référence
     * @throws EsterDaoException
     */
    public ReferenceBean getReferenceCosali(final String root) throws EsterDaoException {
        final ReferenceBean reference = new ReferenceBean();
        reference.setName("t_cosali");
        reference.setFormat(FileFormat.TSV);
        reference.setFile(new File(root + "WEB-INF/data/references/cosali.csv"));

        return reference;
    }
}

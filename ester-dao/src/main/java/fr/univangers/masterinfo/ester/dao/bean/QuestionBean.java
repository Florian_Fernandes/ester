package fr.univangers.masterinfo.ester.dao.bean;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.Comparator;
import java.util.Set;
import java.util.TreeSet;

/**
 * Bean qui représente une question
 *
 * @author Marwa KHADJI
 * @version 1.0
 * @date 25/10/2020
 */
@Entity
@Table(name = "t_question")
public class QuestionBean extends AbstractBean implements Comparable<QuestionBean> {

    /**
     * Trier les questions par ordre de position
     */
    public static final Comparator<QuestionBean> COMPARATOR_QUESTION = new Comparator<>() {

        @Override
        public int compare(final QuestionBean q1, final QuestionBean q2) {
            return Integer.compare(q1.position, q2.position);
        }
    };
    /**
     * Serial Version UID
     */
    private static final long serialVersionUID = 1L;
    /**
     * Le code de la question qui va servir à l'export
     */
    @Column(name = "que_code")
    private String code;

    /**
     * L'intitulé de la question
     */
    @Column(name = "que_name")
    private String name;

    /**
     * Le type de la question sous forme d'énumération
     */
    @Column(name = "que_type")
    @Enumerated(EnumType.STRING)
    private QuestionType type;

    /**
     * Le score pondéré que l'on peut atteindre pour cette question
     */
    @Column(name = "que_score")
    private int score;

    /**
     * Le seuil permettant d'evaluer le risque
     */
    @Column(name = "que_seuil")
    private int seuil;

    /**
     * Position de la question pour l'affichage
     */
    @Column(name = "que_position")
    private int position;

    /**
     * Variable de référence à comparer
     */
    @Column(name = "que_variable")
    private String variable;

    /**
     * Les options pour la question
     */
    @OneToMany(mappedBy = "question", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @OrderBy("position")
    private Set<QuestionOptionBean> questionOptions = new TreeSet<>(
            QuestionOptionBean.COMPARATOR_QUESTION_OPTION);

    /**
     * Le questionnaire qui référence la question
     */
    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE}, fetch = FetchType.EAGER)
    @JoinColumn(name = "que_questionnary")
    @JsonIgnore
    private QuestionnaryBean questionnary;

    /**
     * @return the code
     */
    public String getCode() {
        return this.code;
    }

    /**
     * @param code the code to set
     */
    public void setCode(final String code) {
        this.code = code;
    }

    /**
     * @return the name
     */
    public String getName() {
        return this.name;
    }

    /**
     * @param name the name to set
     */
    public void setName(final String name) {
        this.name = name;
    }

    /**
     * @return the type
     */
    public QuestionType getType() {
        return this.type;
    }

    /**
     * @param type the type to set
     */
    public void setType(final QuestionType type) {
        this.type = type;
    }

    /**
     * @return the score
     */
    public int getScore() {
        return this.score;
    }

    /**
     * @param score the score to set
     */
    public void setScore(final int score) {
        this.score = score;
    }

    /**
     * @return the seuil
     */
    public int getSeuil() {
        return this.seuil;
    }

    /**
     * @param seuil the seuil to set
     */
    public void setSeuil(final int seuil) {
        this.seuil = seuil;
    }

    /**
     * @return the position
     */
    public int getPosition() {
        return this.position;
    }

    /**
     * @param position the position to set
     */
    public void setPosition(final int position) {
        this.position = position;
    }

    /**
     * @return variable de référence
     */
    public String getVariable() {
        return this.variable;
    }

    /**
     * @param variable
     */
    public void setVariable(final String variable) {
        this.variable = variable;
    }

    /**
     * @return the questionOptions
     */
    public Set<QuestionOptionBean> getQuestionOptions() {
        return this.questionOptions;
    }

    /**
     * @param questionOptions the questionOptions to set
     */
    public void setQuestionOptions(final Set<QuestionOptionBean> questionOptions) {
        this.questionOptions = questionOptions;
    }

    /**
     * @param position
     * @return la sous-question avec la position passée en paramètre
     */
    public QuestionOptionBean getQuestionOption(int position) {
        for (QuestionOptionBean questionOption : questionOptions) {
            if (questionOption.getPosition() == position) {
                return questionOption;
            }
        }

        return null;
    }

    /**
     * @return the questionnary
     */
    public QuestionnaryBean getQuestionnary() {
        return this.questionnary;
    }

    /**
     * @param questionnary the questionnary to set
     */
    public void setQuestionnary(final QuestionnaryBean questionnary) {
        this.questionnary = questionnary;

    }

    @Override
    public int compareTo(QuestionBean bean) {
        return (int)(this.position - bean.getPosition());
    }
}
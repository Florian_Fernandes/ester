package fr.univangers.masterinfo.ester.dao.contract;

import fr.univangers.masterinfo.ester.dao.bean.QuestionBean;

/**
 * Contrat d'une DAO question
 *
 * @author Marwa KHADJI
 * @version 1.0
 * @date 25/10/2020
 */
public interface QuestionDaoContract extends AbstractDaoContract<QuestionBean> {

}

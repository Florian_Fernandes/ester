package fr.univangers.masterinfo.ester.dao.bean;

import org.hibernate.ogm.datastore.mongodb.options.GridFSBucket;
import org.hibernate.ogm.datastore.mongodb.type.GridFS;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * Bean qui représente une vidéo
 *
 * @version 1.0
 * @date 04/10/2020
 */
@Entity
@Table(name = "t_video")
public class VideoBean extends AbstractBean {

    /**
     * Nom de la collection qui contient les vidéos
     */
    public static final String COLLECTION_VIDEO_GALLERY = "t_video_gallery";
    /**
     * Serial version UID
     */
    private static final long serialVersionUID = 1L;
    /**
     * L'objet qui contient le flux vidéo
     */
    @GridFSBucket(COLLECTION_VIDEO_GALLERY)
    private GridFS gridFs;

    /**
     * Code unique de la vidéo
     */
    @Column(name = "vid_code")
    private String code;

    /**
     * @return the gridFs
     */
    public GridFS getGridFs() {
        return this.gridFs;
    }

    /**
     * @param gridFs the gridFs to set
     */
    public void setGridFs(final GridFS gridFs) {
        this.gridFs = gridFs;
    }

    /**
     * @return the code
     */
    public String getCode() {
        return this.code;
    }

    /**
     * @param code the code to set
     */
    public void setCode(final String code) {
        this.code = code;
    }
}

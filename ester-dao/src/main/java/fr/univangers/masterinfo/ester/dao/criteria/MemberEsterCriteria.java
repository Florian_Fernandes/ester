package fr.univangers.masterinfo.ester.dao.criteria;

import fr.univangers.masterinfo.ester.dao.bean.UserRole;

import java.util.ArrayList;
import java.util.List;

/**
 * Filtre pour interroger les données d'un membre ESTER
 *
 * @version 1.0
 * @date 04/10/2020
 */
public class MemberEsterCriteria extends AbstractCriteria {

    /**
     * Le login ou l'email
     */
    private String loginOrEmail;

    /**
     * L'email
     */
    private String email;

    /**
     * Le login
     */
    private String login;

    /**
     * La liste des roles
     */
    private List<UserRole> roles = new ArrayList<>();

    /**
     * @return the roles
     */
    public List<UserRole> getRoles() {
        return this.roles;
    }

    /**
     * @param roles the roles to set
     */
    public void setRoles(List<UserRole> roles) {
        this.roles = roles;
    }

    /**
     * @return the loginOrEmail
     */
    public String getLoginOrEmail() {
        return this.loginOrEmail;
    }

    /**
     * @param loginOrEmail the loginOrEmail to set
     */
    public void setLoginOrEmail(final String loginOrEmail) {
        this.loginOrEmail = loginOrEmail;
    }

    /**
     * @return the email
     */
    public String getEmail() {
        return email;
    }

    /**
     * @param email the email to set
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * @return the login
     */
    public String getLogin() {
        return login;
    }

    /**
     * @param login the login to set
     */
    public void setLogin(String login) {
        this.login = login;
    }
}

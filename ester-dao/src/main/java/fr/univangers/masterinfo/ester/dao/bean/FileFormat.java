package fr.univangers.masterinfo.ester.dao.bean;

/**
 * Les différents types de fichiers acceptés dans l'application
 *
 * @version 1.0
 * @date 01/11/2020
 */
public enum FileFormat {
    /**
     * Format JSON
     */
    JSON("json"),

    /**
     * Format CSV
     */
    CSV("csv"),

    /**
     * Format TSV
     */
    TSV("tsv");

    /**
     * Le nom du format
     */
    private String name;

    /**
     * Constructeur
     *
     * @param name
     */
    FileFormat(final String name) {
        this.name = name;
    }

    /**
     * @return the name
     */
    public String getName() {
        return this.name;
    }

    /**
     * @param name the name to set
     */
    public void setName(final String name) {
        this.name = name;
    }
}

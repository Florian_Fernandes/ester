package fr.univangers.masterinfo.ester.dao.impl;

import com.mongodb.MongoException;
import fr.univangers.masterinfo.ester.dao.bean.Environment;
import fr.univangers.masterinfo.ester.dao.bean.ReferenceBean;
import fr.univangers.masterinfo.ester.dao.contract.ReferenceDaoContract;
import fr.univangers.masterinfo.ester.dao.criteria.ReferenceCriteria;
import fr.univangers.masterinfo.ester.dao.exception.EsterDaoException;
import fr.univangers.masterinfo.ester.utils.ProcessUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Repository;

import javax.persistence.TypedQuery;
import javax.transaction.Transactional;
import java.util.HashMap;
import java.util.Map;

/**
 * Implémentation de le DAO de référence
 *
 * @version 1.0
 * @date 04/10/2020
 */
@Transactional
@Repository
public class ReferenceDaoImpl extends AbstractDaoImpl<ReferenceBean>
        implements ReferenceDaoContract {
    private static final Logger LOG = LogManager.getLogger(ReferenceDaoImpl.class);

    /**
     * Le constructeur
     */
    public ReferenceDaoImpl() {
        super(ReferenceBean.class);
    }

    @Override
    public void delete() throws EsterDaoException {
        // On supprime toutes les tables créés
        for (final ReferenceBean reference : this.read()) {
            final String sqlNative = String.format("db.%s.remove({ })", reference.getName());
            this.entityManager.createNativeQuery(sqlNative).executeUpdate();
        }

        super.delete();
    }

    @Override
    public void removeReferenceTable(final ReferenceBean reference) {
        final String sqlNative = String.format("db.%s.drop()", reference.getName());
        this.entityManager.createNativeQuery(sqlNative).executeUpdate();
    }

    @Override
    public void addReference(final ReferenceCriteria criteria) throws EsterDaoException {


        final String[] exec = Environment.LINUX.getCommands();


        final Map<String, Object> properties = new HashMap<>();
        properties.put(ProcessUtils.HOST_KEY, HOST_VALUE);
        properties.put(ProcessUtils.PORT_KEY, PORT_VALUE);
        properties.put(ProcessUtils.DB_KEY, DB_VALUE);
        properties.put(ProcessUtils.FILE_PATH_KEY, criteria.getFile().getAbsolutePath());
        properties.put(ProcessUtils.FILE_FORMAT_KEY, criteria.getFormatfile().getName());
        properties.put(ProcessUtils.COLLECTION_KEY, criteria.getCollection());
        properties.put(ProcessUtils.APPEND_KEY, false);
        properties.put(ProcessUtils.EXEC_KEY, exec);

        ProcessUtils.execMongoImportCommand(properties);
    }

    @Override
    public void updateReference(final ReferenceCriteria criteria) throws EsterDaoException {
        final String[] exec = Environment.LINUX.getCommands();


        final Map<String, Object> properties = new HashMap<>();
        properties.put(ProcessUtils.HOST_KEY, HOST_VALUE);
        properties.put(ProcessUtils.PORT_KEY, PORT_VALUE);
        properties.put(ProcessUtils.DB_KEY, DB_VALUE);
        properties.put(ProcessUtils.FILE_PATH_KEY, criteria.getFile().getAbsolutePath());
        properties.put(ProcessUtils.FILE_FORMAT_KEY, criteria.getFormatfile().getName());
        properties.put(ProcessUtils.COLLECTION_KEY, criteria.getCollection());
        properties.put(ProcessUtils.APPEND_KEY, true);
        properties.put(ProcessUtils.EXEC_KEY, exec);

        ProcessUtils.execMongoImportCommand(properties);
    }

    @Override
    public ReferenceBean findReference(String id) throws EsterDaoException {
        if (StringUtils.isBlank(id)) {
            return null;
        }

        try {
            final StringBuilder sb = new StringBuilder();

            sb.append(" FROM " + this.clazz.getName() + " AS reference ");
            sb.append(" WHERE reference.id = :id ");

            final TypedQuery<ReferenceBean> query = this.entityManager.createQuery(sb.toString(),
                    this.clazz);
            query.setParameter("id", id);

            final ReferenceBean reference = query.getResultList().stream().findFirst().orElse(null);

            return reference;

        } catch (final MongoException e) {
            throw new EsterDaoException(e.getMessage(), "Impossible d'accéder à la base de données... Contactez un administrateur");
        } catch (final Exception e) {
            throw new EsterDaoException(e.getMessage());
        }
    }
}

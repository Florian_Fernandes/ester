package fr.univangers.masterinfo.ester.dao.impl;

import fr.univangers.masterinfo.ester.dao.bean.QuestionBean;
import fr.univangers.masterinfo.ester.dao.contract.QuestionDaoContract;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;

/**
 * Implémentation de le DAO question
 */
@Transactional
@Repository
public class QuestionDaoImpl extends AbstractDaoImpl<QuestionBean> implements QuestionDaoContract {

    /**
     * Le constructeur
     */
    public QuestionDaoImpl() {
        super(QuestionBean.class);
    }
}

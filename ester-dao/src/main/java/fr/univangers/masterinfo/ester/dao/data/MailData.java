package fr.univangers.masterinfo.ester.dao.data;

import fr.univangers.masterinfo.ester.dao.bean.MailBean;
import org.springframework.stereotype.Component;

/**
 * Ajouter des données fictives sur les mails
 *
 * @version 1.0
 * @date 01/11/2020
 */
@Component
public class MailData extends AbstractData<MailBean> {

}

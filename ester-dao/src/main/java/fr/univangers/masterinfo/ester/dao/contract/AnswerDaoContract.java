package fr.univangers.masterinfo.ester.dao.contract;

import fr.univangers.masterinfo.ester.dao.bean.AnswerBean;
import fr.univangers.masterinfo.ester.dao.criteria.AnswerCriteria;
import fr.univangers.masterinfo.ester.dao.exception.EsterDaoException;

import java.util.List;

/**
 * Le contrat d'une DAO réponse
 *
 * @author Matthew COYLE
 * @version 1.0
 * @date 14/10/2020
 */
public interface AnswerDaoContract extends AbstractDaoContract<AnswerBean> {

    public List<AnswerBean> getListAnwserByResulat(AnswerCriteria answerCriteria) throws EsterDaoException;
}

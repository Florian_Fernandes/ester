package fr.univangers.masterinfo.ester.dao.impl;

import com.mongodb.MongoException;
import fr.univangers.masterinfo.ester.dao.bean.VideoBean;
import fr.univangers.masterinfo.ester.dao.contract.VideoDaoContract;
import fr.univangers.masterinfo.ester.dao.criteria.VideoCriteria;
import fr.univangers.masterinfo.ester.dao.exception.EsterDaoException;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Repository;

import javax.persistence.TypedQuery;
import javax.transaction.Transactional;

/**
 * Implémentation de la DAO user
 *
 * @author Matthew COYLE
 * @version 1.0
 * @date 14/10/2020
 */
@Transactional
@Repository
public class VideoDaoImpl extends AbstractDaoImpl<VideoBean> implements VideoDaoContract {

    /**
     * Log de la classe
     */
    private static final Logger LOG = LogManager.getLogger(VideoDaoImpl.class);

    /**
     * Le constructeur
     */
    public VideoDaoImpl() {
        super(VideoBean.class);
    }

    @Override
    public void delete() throws EsterDaoException {
        this.entityManager
                .createNativeQuery(
                        String.format("db.%s.chunks.remove({ })", VideoBean.COLLECTION_VIDEO_GALLERY))
                .executeUpdate();

        this.entityManager
                .createNativeQuery(
                        String.format("db.%s.files.remove({ })", VideoBean.COLLECTION_VIDEO_GALLERY))
                .executeUpdate();

        super.delete();
    }

    @Override
    public VideoBean findVideoByCode(final VideoCriteria videoCriteria) throws EsterDaoException {

        if (videoCriteria == null) {
            throw new EsterDaoException("Video criteria is null");
        }

        if (StringUtils.isBlank(videoCriteria.getCode())) {
            throw new EsterDaoException("Code is null");
        }

        try {
            final StringBuilder sb = new StringBuilder();

            sb.append(" FROM " + this.clazz.getName() + " AS video ");
            sb.append(" WHERE video.code = :code ");

            final TypedQuery<VideoBean> query = this.entityManager.createQuery(sb.toString(),
                    this.clazz);
            query.setParameter("code", videoCriteria.getCode());

            final VideoBean video = query.getResultList().stream().findFirst().orElse(null);

            return video;
        } catch (final MongoException e) {
            throw new EsterDaoException(e.getMessage(), "Impossible d'accéder à la base de données... Contactez un administrateur");
        } catch (final Exception e) {
            throw new EsterDaoException(e.getMessage());
        }
    }
}

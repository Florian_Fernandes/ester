package fr.univangers.masterinfo.ester.dao.contract;

import fr.univangers.masterinfo.ester.dao.bean.EmployeeBean;
import fr.univangers.masterinfo.ester.dao.criteria.EmployeeCriteria;
import fr.univangers.masterinfo.ester.dao.exception.EsterDaoException;

/**
 * Le contrat d'une DAO salarié
 *
 * @version 1.0
 * @date 04/10/2020
 */
public interface EmployeeDaoContract extends AbstractDaoContract<EmployeeBean> {

    /**
     * Vérifier si un salarié existe selon son identifiant
     *
     * @param employeeCriteria
     * @return vrai si le salarié existe. Faux dans le cas contraire
     * @throws EsterDaoException
     */
    boolean existEmployeeById(EmployeeCriteria employeeCriteria) throws EsterDaoException;

    /**
     * Vérifier si un salarié existe selon son email
     *
     * @param employeeCriteria
     * @return vrai si le salarié existe. Faux dans le cas contraire
     * @throws EsterDaoException
     */
    boolean existEmployeeByEmail(EmployeeCriteria employeeCriteria) throws EsterDaoException;

    /**
     * Vérifier si un salarié existe selon son login
     *
     * @param employeeCriteria
     * @return vrai si le salarié existe. Faux dans le cas contraire
     * @throws EsterDaoException
     */
    boolean existEmployeeByLogin(EmployeeCriteria employeeCriteria) throws EsterDaoException;

    /**
     * Retrouver un salarié selon son login
     *
     * @param employeeCriteria
     * @return le salarié respectant les critères
     * @throws EsterDaoException
     */
    EmployeeBean findEmployee(EmployeeCriteria employeeCriteria) throws EsterDaoException;

    /**
     * Retrouver un salarié selon son ID
     *
     * @param employeeCriteria
     * @return
     * @throws EsterDaoException
     */

    EmployeeBean findEmployeeByID(final EmployeeCriteria employeeCriteria) throws EsterDaoException;
}

package fr.univangers.masterinfo.ester.dao.impl;

import fr.univangers.masterinfo.ester.dao.bean.MailBean;
import fr.univangers.masterinfo.ester.dao.contract.MailDaoContract;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;

/**
 * Implémentation de le DAO mail
 *
 * @version 1.0
 * @date 04/10/2020
 */
@Transactional
@Repository
public class MailDaoImpl extends AbstractDaoImpl<MailBean> implements MailDaoContract {

    /**
     * Le constructeur
     */
    public MailDaoImpl() {
        super(MailBean.class);
    }
}

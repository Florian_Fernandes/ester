package fr.univangers.masterinfo.ester.dao.bean;

/**
 * Les différents types de questions
 *
 * @author Marwa KHADJI
 * @version 1.0
 * @date 25/10/2020
 */
public enum QuestionType {

    /**
     * Réponse courte
     */
    SHORTANSWER,

    /**
     * Réponse longue
     */
    LONGANSWER,

    /**
     * Case à cocher
     */
    CHECKBOX,

    /**
     * Bouton radio
     */
    RADIO,

    /**
     * Liste à choix
     */
    SELECT,

    /**
     * Image
     */
    PICTURE,

    /**
     * Vidéo
     */
    VIDEO;
}

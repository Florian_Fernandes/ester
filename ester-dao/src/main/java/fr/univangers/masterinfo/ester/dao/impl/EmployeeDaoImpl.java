package fr.univangers.masterinfo.ester.dao.impl;

import com.mongodb.MongoException;
import fr.univangers.masterinfo.ester.dao.bean.EmployeeBean;
import fr.univangers.masterinfo.ester.dao.contract.EmployeeDaoContract;
import fr.univangers.masterinfo.ester.dao.criteria.EmployeeCriteria;
import fr.univangers.masterinfo.ester.dao.exception.EsterDaoException;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Repository;

import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.transaction.Transactional;

/**
 * Implémentation de le DAO salarié
 *
 * @version 1.0
 * @date 04/10/2020
 */
@Transactional
@Repository
public class EmployeeDaoImpl extends AbstractDaoImpl<EmployeeBean> implements EmployeeDaoContract {

    /**
     * Log de la classe
     */
    private static final Logger LOG = LogManager.getLogger(EmployeeDaoImpl.class);

    /**
     * Le constructeur
     */
    public EmployeeDaoImpl() {
        super(EmployeeBean.class);
    }

    @Override
    public boolean existEmployeeById(final EmployeeCriteria employeeCriteria) throws EsterDaoException {

        if (employeeCriteria == null) {
            throw new EsterDaoException("Employee criteria is null", "Erreur dans la recherche en base de données pour trouver l'id d'un salarié");
        }

        if (StringUtils.isBlank(employeeCriteria.getLogin())) {
            throw new EsterDaoException("Login is null", "Erreur dans la recherche en base de données pour trouver  l'id d'un salarié");
        }

        try {
            final StringBuilder sb = new StringBuilder();
            sb.append(" SELECT COUNT(*) ");
            sb.append(" FROM " + this.clazz.getName() + " AS user ");
            sb.append(" WHERE user.login = :login ");

            final Query query = this.entityManager.createQuery(sb.toString());
            query.setParameter("login", employeeCriteria.getLogin());

            @SuppressWarnings("unchecked") final Integer count = (Integer) query.getResultList().stream().findFirst().orElse(0);

            return count > 0;
        } catch (final MongoException e) {
            throw new EsterDaoException(e.getMessage(), "Impossible d'accéder à la base de données... Contactez un administrateur");
        } catch (final Exception e) {
            throw new EsterDaoException(e.getMessage(), "Erreur dans la recherche en base de données pour trouver l'id d'un salarié");
        }
    }

    @Override
    public EmployeeBean findEmployee(final EmployeeCriteria employeeCriteria) throws EsterDaoException {

        if (employeeCriteria == null) {
            throw new EsterDaoException("Employee criteria is null", "Erreur dans la recherche en base de données pour trouver un salarié");
        }

        if (StringUtils.isBlank(employeeCriteria.getLogin())) {
            throw new EsterDaoException("Login is null", "Erreur dans la recherche en base de données pour trouver un salarié");
        }

        try {
            final StringBuilder sb = new StringBuilder();

            sb.append(" FROM " + this.clazz.getName() + " AS user ");
            sb.append(" WHERE user.login = :login ");

            final TypedQuery<EmployeeBean> query = this.entityManager.createQuery(sb.toString(),
                    this.clazz);
            query.setParameter("login", employeeCriteria.getLogin());

            final EmployeeBean user = query.getResultList().stream().findFirst().orElse(null);

            return user;
        } catch (final MongoException e) {
            throw new EsterDaoException(e.getMessage(), "Impossible d'accéder à la base de données... Contactez un administrateur");
        } catch (final Exception e) {
            throw new EsterDaoException(e.getMessage(), "Erreur dans la recherche en base de données pour trouver un salarié");
        }
    }

    @Override
    public EmployeeBean findEmployeeByID(final EmployeeCriteria employeeCriteria) throws EsterDaoException {

        if (employeeCriteria == null) {
            throw new EsterDaoException("Employee criteria is null");
        }

        if (StringUtils.isBlank(employeeCriteria.getId())) {
            throw new EsterDaoException("Login is null");
        }

        try {
            final StringBuilder sb = new StringBuilder();

            sb.append(" FROM " + this.clazz.getName() + " AS user ");
            sb.append(" WHERE user.id = :id ");

            final TypedQuery<EmployeeBean> query = this.entityManager.createQuery(sb.toString(),
                    this.clazz);
            query.setParameter("id", employeeCriteria.getId());

            final EmployeeBean user = query.getResultList().stream().findFirst().orElse(null);


            return user;
        } catch (final MongoException e) {
            throw new EsterDaoException(e.getMessage(), "Impossible d'accéder à la base de données... Contactez un administrateur");
        } catch (final Exception e) {
            throw new EsterDaoException(e.getMessage());
        }
    }

    @Override
    public boolean existEmployeeByEmail(final EmployeeCriteria employeeCriteria)
            throws EsterDaoException {
        if (employeeCriteria == null) {
            throw new EsterDaoException("Employee criteria is null", "Erreur dans la recherche en base de données pour trouver un salarié");
        }

        if (StringUtils.isBlank(employeeCriteria.getEmail())) {
            throw new EsterDaoException("Email is null", "Erreur dans la recherche en base de données pour trouver un salarié");
        }

        try {
            final StringBuilder sb = new StringBuilder();
            sb.append(" SELECT COUNT(*) ");
            sb.append(" FROM " + this.clazz.getName() + " AS user ");
            sb.append(" WHERE user.email = :mail ");

            final Query query = this.entityManager.createQuery(sb.toString());
            query.setParameter("mail", employeeCriteria.getEmail());

            @SuppressWarnings("unchecked") final Integer count = (Integer) query.getResultList().stream().findFirst().orElse(0);


            return count > 0;
        } catch (final MongoException e) {
            throw new EsterDaoException(e.getMessage(), "Impossible d'accéder à la base de données... Contactez un administrateur");
        } catch (final Exception e) {
            throw new EsterDaoException(e.getMessage(), "Erreur dans la recherche en base de données pour trouver un salarié");
        }
    }

    @Override
    public boolean existEmployeeByLogin(EmployeeCriteria employeeCriteria) throws EsterDaoException {
        if (employeeCriteria == null) {
            throw new EsterDaoException("Employee criteria is null", "Erreur dans la recherche en base de données pour trouver un salarié");
        }

        if (StringUtils.isBlank(employeeCriteria.getLogin())) {
            throw new EsterDaoException("Login is null", "Erreur dans la recherche en base de données pour trouver un salarié");
        }

        try {
            final StringBuilder sb = new StringBuilder();
            sb.append(" SELECT COUNT(*) ");
            sb.append(" FROM " + this.clazz.getName() + " AS user ");
            sb.append(" WHERE user.login = :login ");

            final Query query = this.entityManager.createQuery(sb.toString());
            query.setParameter("login", employeeCriteria.getLogin());

            @SuppressWarnings("unchecked") final Integer count = (Integer) query.getResultList().stream().findFirst().orElse(0);


            return count > 0;
        } catch (final MongoException e) {
            throw new EsterDaoException(e.getMessage(), "Impossible d'accéder à la base de données... Contactez un administrateur");
        } catch (final Exception e) {
            throw new EsterDaoException(e.getMessage(), "Erreur dans la recherche en base de données pour trouver un salarié");
        }
    }
}

package fr.univangers.masterinfo.ester.dao.impl;

import com.mongodb.MongoException;
import fr.univangers.masterinfo.ester.dao.bean.MemberEsterBean;
import fr.univangers.masterinfo.ester.dao.contract.MemberEsterDaoContract;
import fr.univangers.masterinfo.ester.dao.criteria.MemberEsterCriteria;
import fr.univangers.masterinfo.ester.dao.exception.EsterDaoException;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Repository;

import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.transaction.Transactional;
import java.util.List;

/**
 * Implémentation de le DAO membre ESTER
 *
 * @version 1.0
 * @date 04/10/2020
 */
@Transactional
@Repository
public class MemberEsterDaoImpl extends AbstractDaoImpl<MemberEsterBean>
        implements MemberEsterDaoContract {

    /**
     * Log de la classe
     */
    private static final Logger LOG = LogManager.getLogger(MemberEsterDaoImpl.class);

    /**
     * Le constructeur
     */
    public MemberEsterDaoImpl() {
        super(MemberEsterBean.class);
    }

    @Override
    public boolean existMemberEsterByLoginOrEmail(final MemberEsterCriteria memberEsterCriteria)
            throws EsterDaoException {

        if (memberEsterCriteria == null) {
            throw new EsterDaoException("Member ester criteria is null", "Erreur dans la recherche en base de données pour trouver un membre ester");
        }

        if (StringUtils.isBlank(memberEsterCriteria.getLoginOrEmail())) {
            throw new EsterDaoException("Login or email is null", "Erreur dans la recherche en base de données pour trouver un membre ester");
        }

        try {
            final StringBuilder sb = new StringBuilder();
            sb.append(" SELECT COUNT(*) ");
            sb.append(" FROM " + this.clazz.getName() + " AS user ");
            sb.append(" WHERE user.login = :login ");
            sb.append(" OR user.email = :email ");

            final Query query = this.entityManager.createQuery(sb.toString());
            query.setParameter("login", memberEsterCriteria.getLoginOrEmail());
            query.setParameter("email", memberEsterCriteria.getLoginOrEmail());

            @SuppressWarnings("unchecked") final int count = (int) query.getResultList().stream().findFirst().orElse(0);

            return count > 0;
        } catch (final MongoException e) {
            throw new EsterDaoException(e.getMessage(), "Impossible d'accéder à la base de données... Contactez un administrateur");
        } catch (final Exception e) {
            throw new EsterDaoException(e.getMessage(), "Erreur dans la recherche en base de données pour trouver un membre ester");
        }
    }

    @Override
    public boolean existMemberEsterByMail(MemberEsterCriteria memberEsterCriteria) throws EsterDaoException {
        if (memberEsterCriteria == null) {
            throw new EsterDaoException("Member ester criteria is null", "Erreur dans la recherche en base de données pour trouver un membre ester");
        }

        if (StringUtils.isBlank(memberEsterCriteria.getEmail())) {
            throw new EsterDaoException("Email is null", "Erreur dans la recherche en base de données pour trouver un membre ester");
        }

        try {
            final StringBuilder sb = new StringBuilder();
            sb.append(" SELECT COUNT(*) ");
            sb.append(" FROM " + this.clazz.getName() + " AS user ");
            sb.append(" WHERE user.email = :email ");

            final Query query = this.entityManager.createQuery(sb.toString());
            query.setParameter("email", memberEsterCriteria.getEmail());

            @SuppressWarnings("unchecked") final int count = (int) query.getResultList().stream().findFirst().orElse(0);

            return count > 0;
        } catch (final MongoException e) {
            throw new EsterDaoException(e.getMessage(), "Impossible d'accéder à la base de données... Contactez un administrateur");
        } catch (final Exception e) {
            throw new EsterDaoException(e.getMessage(), "Erreur dans la recherche en base de données pour trouver un membre ester");
        }
    }

    @Override
    public boolean existMemberEsterByLogin(MemberEsterCriteria memberEsterCriteria) throws EsterDaoException {
        if (memberEsterCriteria == null) {
            throw new EsterDaoException("Member ester criteria is null", "Erreur dans la recherche en base de données  pour trouver un membre ester");
        }

        if (StringUtils.isBlank(memberEsterCriteria.getLogin())) {
            throw new EsterDaoException("Login is null", "Erreur dans la recherche en base de données  pour trouver un membre ester");
        }

        try {
            final StringBuilder sb = new StringBuilder();
            sb.append(" SELECT COUNT(*) ");
            sb.append(" FROM " + this.clazz.getName() + " AS user ");
            sb.append(" WHERE user.login = :login ");

            final Query query = this.entityManager.createQuery(sb.toString());
            query.setParameter("login", memberEsterCriteria.getLogin());

            @SuppressWarnings("unchecked") final int count = (int) query.getResultList().stream().findFirst().orElse(0);

            return count > 0;
        } catch (final MongoException e) {
            throw new EsterDaoException(e.getMessage(), "Impossible d'accéder à la base de données... Contactez un administrateur");
        } catch (final Exception e) {
            throw new EsterDaoException(e.getMessage(), "Erreur dans la recherche en base de données pour trouver un membre ester");
        }
    }


    @Override
    public MemberEsterBean findMemberEsterByLoginOrEmail(
            final MemberEsterCriteria memberEsterCriteria) throws EsterDaoException {

        if (memberEsterCriteria == null) {
            throw new EsterDaoException("Member ester criteria is null", "Erreur dans la recherche en base de données pour trouver un membre ester");
        }

        if (StringUtils.isBlank(memberEsterCriteria.getLoginOrEmail())) {
            throw new EsterDaoException("Login or email is null", "Erreur dans la recherche en base de données pour trouver un membre ester");
        }

        try {
            final StringBuilder sb = new StringBuilder();

            sb.append(" FROM " + this.clazz.getName() + " AS user ");
            sb.append(" WHERE user.login = :login ");
            sb.append(" OR user.email = :email ");

            final TypedQuery<MemberEsterBean> query = this.entityManager.createQuery(sb.toString(),
                    this.clazz);
            query.setParameter("login", memberEsterCriteria.getLoginOrEmail());
            query.setParameter("email", memberEsterCriteria.getLoginOrEmail());

            final MemberEsterBean user = query.getResultList().stream().findFirst().orElse(null);

            return user;

        } catch (final MongoException e) {
            throw new EsterDaoException(e.getMessage(), "Impossible d'accéder à la base de données... Contactez un administrateur");
        } catch (final Exception e) {
            throw new EsterDaoException(e.getMessage(), "Erreur dans la recherche en base de données pour trouver un membre ester");
        }
    }

    @Override
    public List<MemberEsterBean> findMemberEsterByRoles(
            final MemberEsterCriteria memberEsterCriteria) throws EsterDaoException {

        if (memberEsterCriteria == null) {
            throw new EsterDaoException("Member ester criteria is null", "Erreur dans la recherche en base de données pour trouver un membre ester");
        }

        if (memberEsterCriteria.getRoles().isEmpty()) {
            throw new EsterDaoException("Roles is empty", "Erreur dans la recherche en base de données pour trouver un membre ester");
        }

        try {
            final StringBuilder sb = new StringBuilder();

            sb.append(" FROM " + this.clazz.getName() + " AS user ");
            sb.append(" WHERE user.role IN (:roles) ");

            final TypedQuery<MemberEsterBean> query = this.entityManager.createQuery(sb.toString(),
                    this.clazz);
            query.setParameter("roles", memberEsterCriteria.getRoles());

            final List<MemberEsterBean> users = query.getResultList();


            return users;
        } catch (final MongoException e) {
            throw new EsterDaoException(e.getMessage(), "Impossible d'accéder à la base de données... Contactez un administrateur");
        } catch (final Exception e) {
            throw new EsterDaoException(e.getMessage(), "Erreur dans la recherche en base de données pour trouver un membre ester");
        }
    }


    public MemberEsterBean findMembreEsterById(MemberEsterCriteria memberEsterCriteria) throws EsterDaoException {

        if (memberEsterCriteria == null) {
            throw new EsterDaoException("Member ester criteria is null", "Erreur dans la recherche en base de données pour trouver un membre ester");
        }

        if (StringUtils.isBlank(memberEsterCriteria.getId())) {
            throw new EsterDaoException("ID is null", "Erreur dans la recherche en base de données pour trouver un membre ester");
        }

        try {
            final StringBuilder sb = new StringBuilder();

            sb.append(" FROM " + this.clazz.getName() + " AS user ");
            sb.append(" WHERE user.id = :id ");


            final TypedQuery<MemberEsterBean> query = this.entityManager.createQuery(sb.toString(),
                    this.clazz);
            query.setParameter("id", memberEsterCriteria.getId());

            final MemberEsterBean user = query.getResultList().stream().findFirst().orElse(null);


            return user;
        } catch (final MongoException e) {
            throw new EsterDaoException(e.getMessage(), "Impossible d'accéder à la base de données... Contactez un administrateur");
        } catch (final Exception e) {
            throw new EsterDaoException(e.getMessage(), "Erreur dans la recherche en base de données pour trouver un membre ester");
        }

    }

    @Override
    public boolean existMembreById(MemberEsterCriteria memberEsterCriteria) throws EsterDaoException {

        if (memberEsterCriteria == null) {
            throw new EsterDaoException("Member ester criteria is null", "Erreur dans la recherche en base de données pour trouver un membre ester");
        }

        if (StringUtils.isBlank(memberEsterCriteria.getId())) {
            return false;
        }

        try {
            final StringBuilder sb = new StringBuilder();
            sb.append(" SELECT COUNT(*) ");
            sb.append(" FROM " + this.clazz.getName() + " AS user ");
            sb.append(" WHERE user.id = :id ");

            final Query query = this.entityManager.createQuery(sb.toString());
            query.setParameter("id", memberEsterCriteria.getId());

            @SuppressWarnings("unchecked") final int count = (int) query.getResultList().stream().findFirst().orElse(0);


            return (count > 0);
        } catch (final MongoException e) {
            throw new EsterDaoException(e.getMessage(), "Impossible d'accéder à la base de données... Contactez un administrateur");
        } catch (final Exception e) {
            throw new EsterDaoException(e.getMessage(), "Erreur dans la recherche en base de données pour trouver un membre ester");
        }
    }

    public boolean existMemberEsterByID(final MemberEsterCriteria memberEsterCriteria)
            throws EsterDaoException {

        if (memberEsterCriteria == null) {
            throw new EsterDaoException("Member ester criteria is null", "Erreur dans la recherche en base de données pour trouver un membre ester");
        }

        if (StringUtils.isBlank(memberEsterCriteria.getId())) {
            throw new EsterDaoException("ID is null", "Erreur dans la recherche en base de données pour trouver un membre ester");
        }

        try {
            final StringBuilder sb = new StringBuilder();
            sb.append(" SELECT COUNT(*) ");
            sb.append(" FROM " + this.clazz.getName() + " AS user ");
            sb.append(" WHERE user.id = :id ");

            final Query query = this.entityManager.createQuery(sb.toString());
            query.setParameter("id", memberEsterCriteria.getId());

            @SuppressWarnings("unchecked") final int count = (int) query.getResultList().stream().findFirst().orElse(0);


            return count > 0;
        } catch (final MongoException e) {
            throw new EsterDaoException(e.getMessage(), "Impossible d'accéder à la base de données... Contactez un administrateur");
        } catch (final Exception e) {
            throw new EsterDaoException(e.getMessage(), "Erreur dans la recherche en base de données pour trouver un membre ester");
        }
    }

    public MemberEsterBean findMemberEsterByID(final MemberEsterCriteria memberEsterCriteria) throws EsterDaoException {

        if (memberEsterCriteria == null) {
            throw new EsterDaoException("Member ester criteria is null", "Erreur dans la recherche en base de données pour trouver un membre ester");
        }

        if (StringUtils.isBlank(memberEsterCriteria.getId())) {
            throw new EsterDaoException("ID is null", "Erreur dans la recherche en base de données pour trouver un membre ester");
        }

        try {
            final StringBuilder sb = new StringBuilder();

            sb.append(" FROM " + this.clazz.getName() + " AS user ");
            sb.append(" WHERE user.id = :id ");

            final TypedQuery<MemberEsterBean> query = this.entityManager.createQuery(sb.toString(),
                    this.clazz);
            query.setParameter("id", memberEsterCriteria.getId());

            final MemberEsterBean user = query.getResultList().stream().findFirst().orElse(null);


            return user;
        } catch (final MongoException e) {
            throw new EsterDaoException(e.getMessage(), "Impossible d'accéder à la base de données... Contactez un administrateur");
        } catch (final Exception e) {
            throw new EsterDaoException(e.getMessage(), "Erreur dans la recherche en base de données pour trouver un membre ester");
        }
    }


    public List<MemberEsterBean> findAllExceptCurrentUser(MemberEsterCriteria memberEsterCriteria) throws EsterDaoException {
        if (memberEsterCriteria == null) {
            throw new EsterDaoException("Member ester criteria is null", "Erreur dans la recherche en base de données pour trouver un membre ester");
        }

        if (StringUtils.isBlank(memberEsterCriteria.getId())) {
            throw new EsterDaoException("ID is null", "Erreur dans la recherche en base de données pour trouver un membre ester");
        }

        try {
            final StringBuilder sb = new StringBuilder();

            sb.append(" FROM " + this.clazz.getName() + " AS user ");
            sb.append(" WHERE user.id != :id ");

            final TypedQuery<MemberEsterBean> query = this.entityManager.createQuery(sb.toString(),
                    this.clazz);
            query.setParameter("id", memberEsterCriteria.getId());

            final List<MemberEsterBean> users = query.getResultList();


            return users;
        } catch (final MongoException e) {
            throw new EsterDaoException(e.getMessage(), "Impossible d'accéder à la base de données... Contactez un administrateur");
        } catch (final Exception e) {
            throw new EsterDaoException(e.getMessage(), "Erreur dans la recherche en base de données pour trouver un membre ester");
        }
    }
}

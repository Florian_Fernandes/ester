package fr.univangers.masterinfo.ester.dao.contract;

import fr.univangers.masterinfo.ester.dao.bean.VideoBean;
import fr.univangers.masterinfo.ester.dao.criteria.VideoCriteria;
import fr.univangers.masterinfo.ester.dao.exception.EsterDaoException;

/**
 * Le contrat d'une DAO user
 *
 * @author Matthew COYLE
 * @version 1.0
 * @date 14/10/2020
 */
public interface VideoDaoContract extends AbstractDaoContract<VideoBean> {

    /**
     * Retrouver une vidéo selon des critères
     *
     * @param videoCriteria
     * @return L'utilisateur selon des critères
     * @throws EsterDaoException
     */
    VideoBean findVideoByCode(VideoCriteria videoCriteria) throws EsterDaoException;
}

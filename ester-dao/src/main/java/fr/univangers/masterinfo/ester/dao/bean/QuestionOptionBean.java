package fr.univangers.masterinfo.ester.dao.bean;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.mongodb.lang.Nullable;

import javax.persistence.*;
import java.util.Comparator;
import java.util.HashSet;
import java.util.Set;

/**
 * Bean qui représente une option question
 *
 * @author Marwa KHADJI
 * @version 1.0
 * @date 25/10/2020
 */
@Entity
@Table(name = "t_question_option")
public class QuestionOptionBean extends AbstractBean implements Comparable<QuestionOptionBean> {

    /**
     * Trier les options de questions par ordre de position
     */
    public static final Comparator<QuestionOptionBean> COMPARATOR_QUESTION_OPTION = new Comparator<QuestionOptionBean>() {

        @Override
        public int compare(final QuestionOptionBean q1, final QuestionOptionBean q2) {
            return Integer.compare(q1.position, q2.position);
        }
    };
    /**
     * Serial Version UID
     */
    private static final long serialVersionUID = 1L;
    /**
     * Le code pour l'export
     */
    @Column(name = "que_code")
    private String code;

    /**
     * L'intitulé de la proposition
     */
    @Column(name = "que_value")
    private String value;

    /**
     * Le score de la proposition
     */
    @Column(name = "que_score")
    private int score;

    /**
     * Le coefficient de la proposition
     */
    @Column(name = "que_coeff")
    private int coeff;

    /**
     * Position de l'option pour l'affichage
     */
    @Column(name = "que_position")
    private int position;

    /**
     * Valeur de la variable de référence
     */
    @Column(name = "que_valeur")
    @Nullable
    private int valeur;

    /**
     * La question auquel appartient l'option
     */
    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE}, fetch = FetchType.EAGER)
    @JoinColumn(name = "que_question")
    @JsonIgnore
    private QuestionBean question;

    /**
     * L'ensemble des réponses choisi par les salariés
     */
    @ManyToMany(mappedBy = "response", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JsonIgnore
    private Set<AnswerBean> answers = new HashSet<>();

    /**
     * @return the code
     */
    public String getCode() {
        return this.code;
    }

    /**
     * @param code the code to set
     */
    public void setCode(final String code) {
        this.code = code;
    }

    /**
     * @return the value
     */
    public String getValue() {
        return this.value;
    }

    /**
     * @param value the value to set
     */
    public void setValue(final String value) {
        this.value = value;
    }

    /**
     * @return the score
     */
    public int getScore() {
        return this.score;
    }

    /**
     * @param score the score to set
     */
    public void setScore(final int score) {
        this.score = score;
    }

    /**
     * @return the coeff
     */
    public int getCoeff() {
        return this.coeff;
    }

    /**
     * @param coeff the coeff to set
     */
    public void setCoeff(final int coeff) {
        this.coeff = coeff;
    }

    /**
     * @return the position
     */
    public int getPosition() {
        return this.position;
    }

    /**
     * @param position the position to set
     */
    public void setPosition(final int position) {
        this.position = position;
    }

    /**
     * @return valeur de la variable de référence
     */
    public int getValeur() {
        return this.valeur;
    }
    //public int getValeur() { return 0;};

    /**
     * @param valeur
     */
    public void setValeur(final int valeur) {
        this.valeur = valeur;
    }

    /**
     * @return the question
     */
    public QuestionBean getQuestion() {
        return this.question;
    }

    /**
     * @param question the question to set
     */
    public void setQuestion(final QuestionBean question) {
        this.question = question;
    }

    /**
     * @return the answers
     */
    public Set<AnswerBean> getAnswers() {
        return this.answers;
    }

    /**
     * @param answers the answers to set
     */
    public void setAnswers(final Set<AnswerBean> answers) {
        this.answers = answers;
    }


    @Override
    public int compareTo(QuestionOptionBean o) {
        return this.value.compareTo(o.value);
    }
}
package fr.univangers.masterinfo.ester.dao.criteria;

/**
 * Filtre pour interroger les données d'un resultat
 *
 * @author Matthew COYLE
 * @version 1.0
 * @date 14/10/2020
 */
public class ResultCriteria extends AbstractCriteria {
    /**
     * Id employee
     */
    private String idEmployee;

    /**
     * Id questionnaire
     */
    private String idQuestionnary;

    /**
     * @return idEmployee
     */
    public String getIdEmployee() {
        return this.idEmployee;
    }

    /**
     * @param idEmployee
     */
    public void setIdEmployee(final String idEmployee) {
        this.idEmployee = idEmployee;
    }

    /**
     * @return idQuestionnary
     */
    public String getIdQuestionnary() {
        return this.idQuestionnary;
    }

    /**
     * @param idQuestionnary
     */

    public void setIdQuestionnary(final String idQuestionnary) {
        this.idQuestionnary = idQuestionnary;
    }

}

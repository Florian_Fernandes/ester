package fr.univangers.masterinfo.ester.dao.data;

import fr.univangers.masterinfo.ester.dao.bean.ResultBean;
import fr.univangers.masterinfo.ester.dao.exception.EsterDaoException;
import org.springframework.stereotype.Component;

import java.util.Date;

/**
 * Ajouter des données fictives sur les résultats
 *
 * @version 1.0
 * @date 01/11/2020
 */
@Component
public class ResultData extends AbstractData<ResultBean> {

    /**
     * Résultat
     *
     * @return résultat
     * @throws EsterDaoException
     */
    public ResultBean getResult() throws EsterDaoException {
        final ResultBean result = new ResultBean();
        result.setDateResult(new Date());
        result.setScoreGlobal(0);

        return result;
    }
}
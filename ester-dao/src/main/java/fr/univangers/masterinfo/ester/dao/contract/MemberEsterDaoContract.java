package fr.univangers.masterinfo.ester.dao.contract;

import fr.univangers.masterinfo.ester.dao.bean.MemberEsterBean;
import fr.univangers.masterinfo.ester.dao.criteria.MemberEsterCriteria;
import fr.univangers.masterinfo.ester.dao.exception.EsterDaoException;

import java.util.List;

/**
 * Le contrat d'une DAO membre ESTER
 *
 * @version 1.0
 * @date 04/10/2020
 */
public interface MemberEsterDaoContract extends AbstractDaoContract<MemberEsterBean> {

    /**
     * Vérifier si un membre ESTER existe selon des critères
     *
     * @param memberEsterCriteria
     * @return vrai si le membre ESTER existe. Faux dans le cas contraire
     * @throws EsterDaoException
     */
    boolean existMemberEsterByLoginOrEmail(MemberEsterCriteria memberEsterCriteria)
            throws EsterDaoException;

    /**
     * Vérifier si un membre ESTER existe selon son mail
     *
     * @param memberEsterCriteria
     * @return vrai si le membre ESTER existe. Faux dans le cas contraire
     * @throws EsterDaoException
     */
    boolean existMemberEsterByMail(MemberEsterCriteria memberEsterCriteria)
            throws EsterDaoException;

    /**
     * Vérifier si un membre ESTER existe selon son login
     *
     * @param memberEsterCriteria
     * @return vrai si le membre ESTER existe. Faux dans le cas contraire
     * @throws EsterDaoException
     */
    boolean existMemberEsterByLogin(MemberEsterCriteria memberEsterCriteria)
            throws EsterDaoException;

    /**
     * Retrouver un membre ESTER selon des critères
     *
     * @param memberEsterCriteria
     * @return L'utilisateur selon des critères
     * @throws EsterDaoException
     */
    MemberEsterBean findMemberEsterByLoginOrEmail(MemberEsterCriteria memberEsterCriteria)
            throws EsterDaoException;

    /**
     * Retrouver des membre ESTER selon leurs roles
     *
     * @param memberEsterCriteria
     * @return Les membres ester
     * @throws EsterDaoException
     */
    List<MemberEsterBean> findMemberEsterByRoles(MemberEsterCriteria memberEsterCriteria)
            throws EsterDaoException;

    MemberEsterBean findMembreEsterById(MemberEsterCriteria memberEsterCriteria)
            throws EsterDaoException;

    /**
     * verifier l'existance un membre ESTER selon son ID
     *
     * @param memberEsterCriteria
     * @return Les membres ester
     * @throws EsterDaoException
     */
    boolean existMembreById(MemberEsterCriteria memberEsterCriteria)
            throws EsterDaoException;

    /**
     * Retourner la liste de tous les membres ester à l'exception de celui dont l'ID est spécifié dans memberEsterCriteria
     *
     * @param memberEsterCriteria
     * @return
     * @throws EsterDaoException
     */
    List<MemberEsterBean> findAllExceptCurrentUser(MemberEsterCriteria memberEsterCriteria)
            throws EsterDaoException;
}

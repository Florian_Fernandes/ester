package fr.univangers.masterinfo.ester.dao.contract;

import fr.univangers.masterinfo.ester.dao.bean.AbstractBean;
import fr.univangers.masterinfo.ester.dao.bean.FileFormat;
import fr.univangers.masterinfo.ester.dao.exception.EsterDaoException;

import java.io.File;
import java.util.List;

/**
 * Le contrat CRUD d'une DAO générique
 *
 * @param <T>
 * @version 1.0
 * @date 04/10/2020
 */
public interface AbstractDaoContract<T extends AbstractBean> {

    /**
     * Exporter les données dans un fichier selon le format souhaité
     *
     * @param format
     * @return le fichier
     * @throws EsterDaoException
     */
    File exportToFile(FileFormat format) throws EsterDaoException;

    /**
     * Importer les données à partir d'un fichier selon le format souhaité
     *
     * @param format
     * @param file
     * @param append
     * @throws EsterDaoException
     */
    void importFromFile(final File file, final FileFormat format, final boolean append)
            throws EsterDaoException;

    /**
     * Sauvegarder une entité
     *
     * @param entity
     * @throws EsterDaoException
     */
    void create(final T entity) throws EsterDaoException;

    /**
     * Récupérer une entité selon un identifiant
     *
     * @param id
     * @return l'entité correpond au critère
     * @throws EsterDaoException
     */
    T read(final String id) throws EsterDaoException;

    /**
     * Récupérer toutes les entités
     *
     * @return une liste d'entités
     * @throws EsterDaoException
     */
    List<T> read() throws EsterDaoException;

    /**
     * Mettre à jour une entité
     *
     * @param entity
     * @throws EsterDaoException
     */
    void update(final T entity) throws EsterDaoException;

    /**
     * Supprimer une entité
     *
     * @param entity
     * @throws EsterDaoException
     */
    void delete(T entity) throws EsterDaoException;

    /**
     * Supprimer toutes les entités
     *
     * @throws EsterDaoException
     */
    void delete() throws EsterDaoException;
}
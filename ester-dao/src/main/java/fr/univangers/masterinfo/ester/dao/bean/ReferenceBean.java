package fr.univangers.masterinfo.ester.dao.bean;

import javax.persistence.*;
import java.io.File;
import java.util.HashSet;
import java.util.Set;

/**
 * Bean qui représente des données de références portées sur des questionnaires
 */
@Entity
@Table(name = "t_reference")
public class ReferenceBean extends AbstractBean {

    /**
     * Serial Version UID
     */
    private static final long serialVersionUID = 1L;

    /**
     * Le nom de la collection, table contenant ces données de références
     */
    @Column(name = "ref_name")
    private String name;

    /**
     * string de la liste des variables
     */
    @Column(name = "ref_variables")
    private String variables;

    /**
     * Le fichier qui contient les données de références Ce fichier sera converti en une table
     */
    @Transient
    private transient File file;

    /**
     * Le format du fichier (CSV, JSON, etc.)
     */
    @Transient
    private transient FileFormat format;

    /**
     * Doit ajouter ou écraser les données ?
     */
    @Transient
    private transient boolean append;

    /**
     * Les questionnaires associés à ces données de références
     */
    @ManyToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE}, fetch = FetchType.EAGER)
    private Set<QuestionnaryBean> questionnaries = new HashSet<>();

    /**
     * @return the name
     */
    public String getName() {
        return this.name;
    }

    /**
     * @param name the name to set
     */
    public void setName(final String name) {
        this.name = name;
    }

    public String getVariables() {
        return this.variables;
    }

    public void setVariables(final String variables) {
        this.variables = variables;
    }

    /**
     * @return the questionnaries
     */
    public Set<QuestionnaryBean> getQuestionnaries() {
        return this.questionnaries;
    }

    /**
     * @param questionnaries the questionnaries to set
     */
    public void setQuestionnaries(final Set<QuestionnaryBean> questionnaries) {
        this.questionnaries = questionnaries;
    }

    /**
     * @return the format
     */
    public FileFormat getFormat() {
        return this.format;
    }

    /**
     * @param format the format to set
     */
    public void setFormat(final FileFormat format) {
        this.format = format;
    }

    /**
     * @return the file
     */
    public File getFile() {
        return this.file;
    }

    /**
     * @param file the file to set
     */
    public void setFile(final File file) {
        this.file = file;
    }

    /**
     * @return the append
     */
    public boolean isAppend() {
        return this.append;
    }

    /**
     * @param append the append to set
     */
    public void setAppend(final boolean append) {
        this.append = append;
    }
}

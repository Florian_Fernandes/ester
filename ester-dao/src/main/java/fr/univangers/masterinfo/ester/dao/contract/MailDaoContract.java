package fr.univangers.masterinfo.ester.dao.contract;

import fr.univangers.masterinfo.ester.dao.bean.MailBean;

/**
 * Le contrat d'une DAO mail
 *
 * @version 1.0
 * @date 04/10/2020
 */
public interface MailDaoContract extends AbstractDaoContract<MailBean> {

}

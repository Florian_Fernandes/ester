package fr.univangers.masterinfo.ester.dao.bean;

import javax.persistence.*;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

/**
 * Bean qui représente un employé
 *
 * @version 1.0
 * @date 04/10/2020
 */
@Entity
@Table(name = "t_employee")
public class EmployeeBean extends UserBean implements Comparable<EmployeeBean> {


    /**
     * Serial Version UID
     */
    private static final long serialVersionUID = 1L;
    /**
     * Le sexe de l'individu (homme ou femme)
     */
    @Column(name = "emp_exe")
    @Enumerated(EnumType.STRING)
    private UserSexe sexe;
    /**
     * L'année de naissance (AAAA)
     */
    @Column(name = "emp_birth")
    @Temporal(TemporalType.DATE)
    private Date birth;
    /**
     * Professions et catégories socioprofessionnelles
     */
    @Column(name = "emp_pcs")
    private String pcs;
    /**
     * Nomenclature d'activités française
     */
    @Column(name = "emp_naf")
    private String naf;
    /**
     * Le poste du salarié
     */
    @Column(name = "emp_poste")
    private String poste;
    /**
     * Liste des questionnaires repondus
     */
    @ManyToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE}, fetch = FetchType.EAGER)
    private Set<QuestionnaryBean> questionnariesAnswered = new HashSet<>();
    /**
     * Liste des questionnaires repondus
     */
    @ManyToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE}, fetch = FetchType.EAGER)
    private Set<QuestionnaryBean> questionnariesNoAnswered = new HashSet<>();
    /**
     * Commentaires sur le poste de travail
     */
    @Column(name = "emp_commentaires")
    private String commentaires;
    /**
     * Région d'activité. Ex : Pays de la Loire
     */
    @Column(name = "emp_region")
    private String region;
    /**
     * Département d'activité. Ex : Loire-Atlantique
     */
    @Column(name = "emp_departement")
    private String departement;
    /**
     * Les résultats produits pour chaque questionnaire
     */
    @OneToMany(mappedBy = "employee", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private Set<ResultBean> results = new HashSet<>();
    /**
     * Les réponses données pour l'ensemble des questionnaires
     */
    @OneToMany(mappedBy = "employee", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private Set<AnswerBean> answers = new HashSet<>();

    /* sert à trier une liste d'employés selon le login avec Collection.sort("listEmployees") */
    public int compareTo(EmployeeBean e1) {
        return this.getLogin().compareTo(e1.getLogin());
    }

    /**
     * @return the sexe
     */
    public UserSexe getSexe() {
        return this.sexe;
    }

    /**
     * @param sexe the sexe to set
     */
    public void setSexe(final UserSexe sexe) {
        this.sexe = sexe;
    }

    /**
     * @return the birth
     */
    public Date getBirth() {
        return this.birth;
    }

    /**
     * @param birth the birth to set
     */
    public void setBirth(final Date birth) {
        this.birth = birth;
    }

    /**
     * @return the pcs
     */
    public String getPcs() {
        return this.pcs;
    }

    /**
     * @param pcs the pcs to set
     */
    public void setPcs(final String pcs) {
        this.pcs = pcs;
    }

    /**
     * @return the naf
     */
    public String getNaf() {
        return this.naf;
    }

    /**
     * @param naf the naf to set
     */
    public void setNaf(final String naf) {
        this.naf = naf;
    }

    /**
     * @return the poste
     */
    public String getPoste() {
        return this.poste;
    }

    /**
     * @param poste the poste to set
     */
    public void setPoste(final String poste) {
        this.poste = poste;
    }

    /**
     * @return the questionnariesAnswered
     */
    public Set<QuestionnaryBean> getQuestionnariesAnswered() {
        return this.questionnariesAnswered;
    }

    /**
     * @param questionnariesAnswered the questionnariesAnswered to set
     */
    public void setQuestionnariesAnswered(final Set<QuestionnaryBean> questionnariesAnswered) {
        this.questionnariesAnswered = questionnariesAnswered;
    }

    /**
     * @return the questionnariesNoAnswered
     */
    public Set<QuestionnaryBean> getQuestionnariesNoAnswered() {
        return this.questionnariesNoAnswered;
    }

    /**
     * @param questionnariesNoAnswered the questionnariesNoAnswered to set
     */
    public void setQuestionnariesNoAnswered(final Set<QuestionnaryBean> questionnariesNoAnswered) {
        this.questionnariesNoAnswered = questionnariesNoAnswered;
    }

    /**
     * @return the commentaires
     */
    public String getCommentaires() {
        return this.commentaires;
    }

    /**
     * @param commentaires the commantaires to set
     */
    public void setCommentaires(final String commentaires) {
        this.commentaires = commentaires;
    }

    /**
     * @return the region
     */
    public String getRegion() {
        return this.region;
    }

    /**
     * @param region the region to set
     */
    public void setRegion(final String region) {
        this.region = region;
    }

    /**
     * @return the departement
     */
    public String getDepartement() {
        return this.departement;
    }

    /**
     * @param departement the departement to set
     */
    public void setDepartement(final String departement) {
        this.departement = departement;
    }

    /**
     * @return the results
     */
    public Set<ResultBean> getResults() {
        return this.results;
    }

    /**
     * @param results the results to set
     */
    public void setResults(final Set<ResultBean> results) {
        this.results = results;
    }

    /**
     * @return the answers
     */
    public Set<AnswerBean> getAnswers() {
        return this.answers;
    }

    /**
     * @param answers the answers to set
     */
    public void setAnswers(final Set<AnswerBean> answers) {
        this.answers = answers;
    }

    /**
     *
     */
    public void removequestionnary(QuestionnaryBean q) {
        this.questionnariesAnswered.remove(q);
        this.questionnariesNoAnswered.remove(q);
        this.results.remove(q);
    }
}
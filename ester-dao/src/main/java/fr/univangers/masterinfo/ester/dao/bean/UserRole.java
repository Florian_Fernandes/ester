package fr.univangers.masterinfo.ester.dao.bean;

/**
 * Les rôles joués par les utilisateurs leur offrant des privilèges
 *
 * @version 1.0
 * @date 04/10/2020
 */
public enum UserRole {

    /**
     * Administrateur
     */
    ADMIN("Administrateur"),

    /**
     * Médecin
     */
    MEDECIN("Médecin"),

    /**
     * Pluridisciplinaire
     */
    PLURIDISCIPLINAIRE("Pluridisciplinaire"),

    /**
     * Salarié
     */
    SALARIE("Salarié");

    /**
     * Nom du rôle
     */
    private String name;

    /**
     * Constructeur
     *
     * @param name
     */
    private UserRole(final String name) {
        this.name = name;
    }

    /**
     * @return the name
     */
    public String getName() {
        return this.name;
    }
}
package fr.univangers.masterinfo.ester.dao.criteria;

/**
 * Filtre pour interroger les données d'un salarié
 *
 * @version 1.0
 * @date 04/10/2020
 */
public class EmployeeCriteria extends AbstractCriteria {

    /**
     * Le login
     */
    private String login;

    /**
     * l'email
     */
    private String email;

    /**
     * @return the login
     */
    public String getLogin() {
        return this.login;
    }

    /**
     * @param login the login to set
     */
    public void setLogin(final String login) {
        this.login = login;
    }

    /**
     * @return the email
     */
    public String getEmail() {
        return this.email;
    }

    /**
     * @param email the email to set
     */
    public void setEmail(final String email) {
        this.email = email;
    }
}

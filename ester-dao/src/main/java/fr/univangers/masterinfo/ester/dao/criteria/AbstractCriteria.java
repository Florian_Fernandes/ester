package fr.univangers.masterinfo.ester.dao.criteria;

/**
 * Filtre pour interroger la BD
 *
 * @version 1.0
 * @date 04/10/2020
 */
public abstract class AbstractCriteria {

    /**
     * L'identifiant de l'entité
     */
    protected String id;

    /**
     * @return the id
     */
    public String getId() {
        return this.id;
    }

    /**
     * @param id the id to set
     */
    public void setId(final String id) {
        this.id = id;
    }
}

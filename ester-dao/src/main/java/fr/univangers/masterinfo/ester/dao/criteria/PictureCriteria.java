package fr.univangers.masterinfo.ester.dao.criteria;

/**
 * Filtre pour interroger les données d'une photo
 *
 * @version 1.0
 * @date 04/10/2020
 */
public class PictureCriteria extends AbstractCriteria {

    /**
     * Code unique de la photo
     */
    private String code;

    /**
     * @return the code
     */
    public String getCode() {
        return this.code;
    }

    /**
     * @param code the code to set
     */
    public void setCode(final String code) {
        this.code = code;
    }
}

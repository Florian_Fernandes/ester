package fr.univangers.masterinfo.ester.dao.bean;

/**
 * Les différents sexe : homme ou femme
 *
 * @version 1.0
 * @date 04/10/2020
 */
public enum UserSexe {

    /**
     * Homme
     */
    HOMME("Homme"),

    /**
     * Femme
     */
    FEMME("Femme"),

    /**
     * Autre
     */
    AUTRE("Autre");

    /**
     * Le nom du sexe
     */
    private String name;

    /**
     * Constructeur
     *
     * @param name
     */
    private UserSexe(final String name) {
        this.name = name;
    }

    /**
     * @return the name
     */
    public String getName() {
        return this.name;
    }
}
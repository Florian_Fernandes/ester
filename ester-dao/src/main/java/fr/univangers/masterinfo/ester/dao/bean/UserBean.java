package fr.univangers.masterinfo.ester.dao.bean;

import javax.persistence.*;
import java.util.Date;

/**
 * Bean qui représente un utilisateur
 *
 * @version 1.0
 * @date 04/10/2020
 */
@MappedSuperclass
public abstract class UserBean extends AbstractBean {

    /**
     * Serial Version UID
     */
    private static final long serialVersionUID = 1L;

    /**
     * Le login
     */
    @Column(name = "use_login")
    protected String login;

    /**
     * L'adresse mail
     */
    @Column(name = "use_email")
    protected String email;

    /**
     * Le role
     */
    @Column(name = "use_role")
    @Enumerated(EnumType.STRING)
    protected UserRole role;

    /**
     * le groupe auquel il appartient
     */
    @Column(name = "use_group")
    @Enumerated(EnumType.STRING)
    protected UserGroup group;

    /**
     * Nombre de connexion au site
     */
    @Column(name = "use_connection_number")
    protected int connectionNumber;

    /**
     * Durée de connexion au site (en seconde)
     */
    @Column(name = "use_connection_time")
    protected long connectionTime;

    /**
     * Date de création de l'utilisateur
     */
    @Column(name = "use_date_creation")
    @Temporal(TemporalType.DATE)
    protected Date dateCreation;

    /**
     * @return the login
     */
    public String getLogin() {
        return this.login;
    }

    /**
     * @param login the login to set
     */
    public void setLogin(final String login) {
        this.login = login;
    }

    /**
     * @return the email
     */
    public String getEmail() {
        return this.email;
    }

    /**
     * @param email the email to set
     */
    public void setEmail(final String email) {
        this.email = email;
    }

    /**
     * @return the role
     */
    public UserRole getRole() {
        return this.role;
    }

    /**
     * @param role the role to set
     */
    public void setRole(final UserRole role) {
        this.role = role;
    }

    /**
     * @return the group
     */
    public UserGroup getGroup() {
        return this.group;
    }

    /**
     * @param group the group to set
     */
    public void setGroup(final UserGroup group) {
        this.group = group;
    }

    /**
     * @return the connectionNumber
     */
    public int getConnectionNumber() {
        return this.connectionNumber;
    }

    /**
     * @param connectionNumber the connectionNumber to set
     */
    public void setConnectionNumber(final int connectionNumber) {
        this.connectionNumber = connectionNumber;
    }

    /**
     * @return the connectionTime
     */
    public long getConnectionTime() {
        return this.connectionTime;
    }

    /**
     * @param connectionTime the connectionTime to set
     */
    public void setConnectionTime(final long connectionTime) {
        this.connectionTime = connectionTime;
    }

    /**
     * @return the dateCreation
     */
    public Date getDateCreation() {
        return this.dateCreation;
    }

    /**
     * @param dateCreation the dateCreation to set
     */
    public void setDateCreation(final Date dateCreation) {
        this.dateCreation = dateCreation;
    }
}

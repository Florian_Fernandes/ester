package fr.univangers.masterinfo.ester.dao.impl;

import com.mongodb.MongoException;
import fr.univangers.masterinfo.ester.dao.bean.QuestionnaryBean;
import fr.univangers.masterinfo.ester.dao.contract.QuestionnaryDaoContract;
import fr.univangers.masterinfo.ester.dao.exception.EsterDaoException;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Repository;

import javax.persistence.TypedQuery;
import javax.transaction.Transactional;

/**
 * Implémentation de le DAO questionnaire
 *
 * @author Marwa KHADJI
 * @version 1.0
 * @date 25/10/2020
 */
@Transactional
@Repository
public class QuestionnaryDaoImpl extends AbstractDaoImpl<QuestionnaryBean>
        implements QuestionnaryDaoContract {

    /**
     * Le constructeur
     */
    public QuestionnaryDaoImpl() {
        super(QuestionnaryBean.class);
    }

    @Override
    public QuestionnaryBean findQuestionnaryByName(final String name) throws EsterDaoException {

        if (StringUtils.isBlank(name)) {
            throw new EsterDaoException("Questionnary's name is null");
        }

        try {
            final StringBuilder sb = new StringBuilder();

            sb.append(" FROM " + this.clazz.getName() + " AS questionnary ");
            sb.append(" WHERE questionnary.name = :name ");

            final TypedQuery<QuestionnaryBean> query = this.entityManager.createQuery(sb.toString(),
                    this.clazz);
            query.setParameter("name", name);

            final QuestionnaryBean questionnary = query.getResultList().stream().findFirst().orElse(null);

            return questionnary;

        } catch (final MongoException e) {
            throw new EsterDaoException(e.getMessage(), "Impossible d'accéder à la base de données... Contactez un administrateur");
        } catch (final Exception e) {
            throw new EsterDaoException(e.getMessage());
        }
    }
}

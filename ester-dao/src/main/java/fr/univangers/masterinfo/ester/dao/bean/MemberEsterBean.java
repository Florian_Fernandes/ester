package fr.univangers.masterinfo.ester.dao.bean;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

/**
 * Bean qui représente un membre ESTER
 *
 * @version 1.0
 * @date 04/10/2020
 */
@Entity
@Table(name = "t_member_ester")
public class MemberEsterBean extends UserBean {

    /**
     * Serial Version UID
     */
    private static final long serialVersionUID = 1L;

    /**
     * Le mot de passe pour se connecter au site
     */
    @Column(name = "mem_password")
    private String password;


    /**
     * Liste des questionnaires qu'il a crée
     */
    @OneToMany(mappedBy = "creator", cascade = {CascadeType.PERSIST,
            CascadeType.MERGE}, fetch = FetchType.EAGER)
    private Set<QuestionnaryBean> questionnariesCreated = new HashSet<>();

    /**
     * @return the password
     */
    public String getPassword() {
        return this.password;
    }

    /**
     * @param password the password to set
     */
    public void setPassword(final String password) {
        this.password = password;
    }

    /**
     * @return the questionnariesCreated
     */
    public Set<QuestionnaryBean> getQuestionnariesCreated() {
        return this.questionnariesCreated;
    }

    /**
     * @param questionnariesCreated the questionnariesCreated to set
     */
    public void setQuestionnariesCreated(final Set<QuestionnaryBean> questionnariesCreated) {
        this.questionnariesCreated = questionnariesCreated;
    }
}

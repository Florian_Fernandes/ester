package fr.univangers.masterinfo.ester.dao.impl;

import com.mongodb.MongoException;
import fr.univangers.masterinfo.ester.dao.bean.AbstractBean;
import fr.univangers.masterinfo.ester.dao.bean.Environment;
import fr.univangers.masterinfo.ester.dao.bean.FileFormat;
import fr.univangers.masterinfo.ester.dao.contract.AbstractDaoContract;
import fr.univangers.masterinfo.ester.dao.exception.EsterDaoException;
import fr.univangers.masterinfo.ester.utils.ProcessUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.ogm.OgmSession;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Table;
import javax.transaction.Transactional;
import java.io.File;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Implémentation du CRUD générique
 *
 * @param <T> l'entité
 * @version 1.0
 * @date 04/10/2020
 */
@Transactional
@Repository
public abstract class AbstractDaoImpl<T extends AbstractBean> implements AbstractDaoContract<T> {

    /**
     * Valeur host mongodb
     */
    public static final String HOST_VALUE = "localhost";
    /**
     * Valeur port mongodb
     */
    public static final String PORT_VALUE = "27017";
    /**
     * Valeur base mongodb
     */
    public static final String DB_VALUE = "ester-db";
    /**
     * Log de la classe
     */
    private static final Logger LOG = LogManager.getLogger(AbstractDaoImpl.class);
    /**
     * Entity is null
     */
    private static final String ENTITY_IS_NULL = "Entity is null";

    /**
     * Les méta-données de l'entité
     */
    protected final Class<T> clazz;

    /**
     * L'entité manageur qui gère les entités
     */
    @PersistenceContext
    protected EntityManager entityManager;

    /**
     * @param clazz
     */
    public AbstractDaoImpl(final Class<T> clazz) {
        this.clazz = clazz;
    }

    /**
     * Récupère la session Hibernate OGM à partir de l'entité manageur
     *
     * @return la session Hibernate OGM
     */
    protected OgmSession getOgmSession() {
        return this.entityManager.unwrap(OgmSession.class);
    }

    @Override
    public File exportToFile(final FileFormat format) throws EsterDaoException {
        return null;
    }

    @Override
    public void importFromFile(final File file, final FileFormat format, final boolean append)
            throws EsterDaoException {
        try {
            final String filePath = file.getAbsolutePath();
            final String fileFormat = format.getName();
            final Table table = this.clazz.getAnnotation(Table.class);
            final String collection = table.name();

            final String[] exec = Environment.LINUX.getCommands();

            final Map<String, Object> properties = new HashMap<>();
            properties.put(ProcessUtils.HOST_KEY, HOST_VALUE);
            properties.put(ProcessUtils.PORT_KEY, PORT_VALUE);
            properties.put(ProcessUtils.DB_KEY, DB_VALUE);
            properties.put(ProcessUtils.FILE_PATH_KEY, filePath);
            properties.put(ProcessUtils.FILE_FORMAT_KEY, fileFormat);
            properties.put(ProcessUtils.COLLECTION_KEY, collection);
            properties.put(ProcessUtils.APPEND_KEY, append);
            properties.put(ProcessUtils.EXEC_KEY, exec);

            ProcessUtils.execMongoImportCommand(properties);
        } catch (final MongoException e) {
            throw new EsterDaoException(e.getMessage(), "Impossible d'accéder à la base de données... Contactez un administrateur");
        } catch (final Exception e) {
            throw new EsterDaoException(e.getMessage());
        }

    }

    @Override
    public T read(final String id) throws EsterDaoException {
        if (id == null) {
            throw new EsterDaoException("Id is null");
        }

        try {
            return this.entityManager.find(this.clazz, id);
        } catch (final MongoException e) {
            throw new EsterDaoException(e.getMessage(), "Impossible d'accéder à la base de données... Contactez un administrateur");
        } catch (final Exception e) {
            throw new EsterDaoException(e.getMessage());
        }
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<T> read() throws EsterDaoException {
        try {
            return this.entityManager.createQuery("FROM " + this.clazz.getName()).getResultList();
        } catch (final MongoException e) {
            throw new EsterDaoException(e.getMessage(), "Impossible d'accéder à la base de données... Contactez un administrateur");
        } catch (final Exception e) {
            throw new EsterDaoException(e.getMessage());
        }
    }

    @Override
    public void create(final T entity) throws EsterDaoException {
        if (entity == null) {
            throw new EsterDaoException(ENTITY_IS_NULL);
        }

        try {
            this.entityManager.persist(entity);
        } catch (final MongoException e) {
            throw new EsterDaoException(e.getMessage(), "Impossible d'accéder à la base de données... Contactez un administrateur");
        } catch (final Exception e) {
            throw new EsterDaoException(e.getMessage());
        }
    }

    @Override
    public void update(final T entity) throws EsterDaoException {
        if (entity == null) {
            throw new EsterDaoException(ENTITY_IS_NULL);
        }

        try {
            this.entityManager.merge(entity);
        } catch (final MongoException e) {
            throw new EsterDaoException(e.getMessage(), "Impossible d'accéder à la base de données... Contactez un administrateur");
        } catch (final Exception e) {
            throw new EsterDaoException(e.getMessage());
        }
    }

    @Override
    public void delete(final T entity) throws EsterDaoException {
        if (entity == null) {
            throw new EsterDaoException(ENTITY_IS_NULL);
        }

        try {
            this.entityManager.remove(
                    this.entityManager.contains(entity) ? entity : this.entityManager.merge(entity));
        } catch (final MongoException e) {
            throw new EsterDaoException(e.getMessage(), "Impossible d'accéder à la base de données... Contactez un administrateur");
        } catch (final Exception e) {
            throw new EsterDaoException(e.getMessage());
        }
    }


    @Override
    public void delete() throws EsterDaoException {
        try {
            final Table table = this.clazz.getAnnotation(Table.class);
            final String sqlNative = String.format("db.%s.remove({ })", table.name());
            this.entityManager.createNativeQuery(sqlNative).executeUpdate();
        } catch (final MongoException e) {
            throw new EsterDaoException(e.getMessage(), "Impossible d'accéder à la base de données... Contactez un administrateur");
        } catch (final Exception e) {
            throw new EsterDaoException(e.getMessage());
        }
    }
}

package fr.univangers.masterinfo.ester.dao.criteria;

import fr.univangers.masterinfo.ester.dao.bean.FileFormat;
import fr.univangers.masterinfo.ester.dao.bean.QuestionnaryBean;

import java.io.File;

/**
 * Filtre pour interroger les données de références
 *
 * @version 1.0
 * @date 01/11/2020
 */
public class ReferenceCriteria extends AbstractCriteria {

    private File file;

    private String collection;

    private FileFormat formatfile;

    private QuestionnaryBean questionnary;

    private String variables;

    public String getVariables() {
        return variables;
    }

    public void setVariables(String variables) {
        this.variables = variables;
    }

    /**
     * @return the questionnary
     */
    public QuestionnaryBean getQuestionnary() {
        return this.questionnary;
    }

    /**
     * @param questionnary the questionnary to set
     */
    public void setQuestionnary(final QuestionnaryBean questionnary) {
        this.questionnary = questionnary;
    }

    /**
     * @return the file
     */
    public File getFile() {
        return this.file;
    }

    /**
     * @param file the file to set
     */
    public void setFile(final File file) {
        this.file = file;
    }

    /**
     * @return the collection
     */
    public String getCollection() {
        return this.collection;
    }

    /**
     * @param collection the collection to set
     */
    public void setCollection(final String collection) {
        this.collection = collection;
    }

    /**
     * @return the formatfile
     */
    public FileFormat getFormatfile() {
        return this.formatfile;
    }

    /**
     * @param formatfile the formatfile to set
     */
    public void setFormatfile(final FileFormat formatfile) {
        this.formatfile = formatfile;
    }
}

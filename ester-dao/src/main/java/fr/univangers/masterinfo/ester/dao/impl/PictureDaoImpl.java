package fr.univangers.masterinfo.ester.dao.impl;

import fr.univangers.masterinfo.ester.dao.bean.PictureBean;
import fr.univangers.masterinfo.ester.dao.contract.PictureDaoContract;
import fr.univangers.masterinfo.ester.dao.criteria.PictureCriteria;
import fr.univangers.masterinfo.ester.dao.exception.EsterDaoException;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Repository;

import javax.persistence.TypedQuery;
import javax.transaction.Transactional;

/**
 * Implémentation de la DAO user
 *
 * @author Matthew COYLE
 * @version 1.0
 * @date 14/10/2020
 */
@Transactional
@Repository
public class PictureDaoImpl extends AbstractDaoImpl<PictureBean> implements PictureDaoContract {

    /**
     * Log de la classe
     */
    private static final Logger LOG = LogManager.getLogger(PictureDaoImpl.class);

    /**
     * Le constructeur
     */
    public PictureDaoImpl() {
        super(PictureBean.class);
    }

    @Override
    public void delete() throws EsterDaoException {
        this.entityManager
                .createNativeQuery(
                        String.format("db.%s.chunks.remove({ })", PictureBean.COLLECTION_PICTURE_GALLERY))
                .executeUpdate();

        this.entityManager
                .createNativeQuery(
                        String.format("db.%s.files.remove({ })", PictureBean.COLLECTION_PICTURE_GALLERY))
                .executeUpdate();

        super.delete();
    }

    @Override
    public PictureBean findPictureByCode(final PictureCriteria pictureCriteria)
            throws EsterDaoException {

        if (pictureCriteria == null) {
            throw new EsterDaoException("Picture criteria is null", "Erreur dans la recherche en base de données pour trouver votre image");
        }

        if (StringUtils.isBlank(pictureCriteria.getCode())) {
            throw new EsterDaoException("Code is null", "Erreur dans la recherche en base de données pour trouver votre image");
        }

        try {
            final StringBuilder sb = new StringBuilder();

            sb.append(" FROM " + this.clazz.getName() + " AS picture ");
            sb.append(" WHERE picture.code = :code ");

            final TypedQuery<PictureBean> query = this.entityManager.createQuery(sb.toString(),
                    this.clazz);
            query.setParameter("code", pictureCriteria.getCode());

            final PictureBean picture = query.getResultList().stream().findFirst().orElse(null);

            return picture;
        } catch (final Exception e) {
            throw new EsterDaoException(e.getMessage(), "Erreur dans la recherche en base de données pour trouver votre image");
        }
    }
}

#!/bin/bash

if [ "$1" = "--vps" ] || [ "$1" = "-v" ]
then
  echo "Stoping services"
  systemctl stop tomcat9

  echo "Building with maven"
  mvn -f pom.xml clean install -DskipTests

  echo "Moving war file into tomcat webapps"
  rm /var/lib/tomcat9/webapps/ester-web.war
  rm -r /var/lib/tomcat9/webapps/ester-web
  mv ester-web/target/ester-web.war /var/lib/tomcat9/webapps/ester-web.war

  echo "Restart services"
  systemctl start tomcat9

else
  echo "Stoping services"
  sudo systemctl stop tomcat9

  echo "Building with maven"
  sudo mvn -f pom.xml clean install -DskipTests

  echo "Moving war file into tomcat webapps"
  sudo rm /var/lib/tomcat9/webapps/ester-web.war
  sudo rm -r /var/lib/tomcat9/webapps/ester-web
  sudo mv ester-web/target/ester-web.war /var/lib/tomcat9/webapps/ester-web.war

  echo "Restart services"
  sudo systemctl start tomcat9
fi

echo "Checking services"
red=`tput setaf 1`
green=`tput setaf 2`
reset=`tput sgr0`
TOMCAT="$(systemctl is-active tomcat9)"
if [ "${TOMCAT}" = "active" ]; then
    echo "${green}Tomcat has been correctly restarted${reset}"
else
    echo "${red}[ERROR]Tomcat is not running.... so exiting${reset}"
    exit 1
fi
MONGOD="$(systemctl is-active mongod)"
if [ "${MONGOD}" = "active" ]; then
    echo "${green}Mongo has been correctly restarted${reset}"
else
    echo "${red}[ERROR]Mongo is not running.... so exiting${reset}"
    exit 1
fi
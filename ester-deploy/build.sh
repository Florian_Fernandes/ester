#!/bin/bash

###################################################################################################################################################################
#Nom		: build.sh																		#
#Arguments 	: /                                                                               									#        
#Description	: Construction des différents outils pour déployer l'application                  									#
#Pré-condition	: Construction valable sur une distribution Debian GNU/Linux 10 (buster)                                     					#
#Date 		: 07/02/2021																		#
#Auteurs 	: Théo MAHAUDA, Moahmed OUHIRRA, Anas TAGUENITI													#
#Version	: 1.0																			#
###################################################################################################################################################################

###################################################################################################################################################################
#																					#
# Git : https://www.digitalocean.com/community/tutorials/how-to-install-git-on-debian-10 										#
# 												                							#
###################################################################################################################################################################

git clone https://gitlab.com/tmahauda/ester-webapp-java-ee-2020.git

###################################################################################################################################################################
#																					#
# Maven : https://linuxize.com/post/how-to-install-apache-maven-on-debian-10/ 											#
# 												                							#
###################################################################################################################################################################

mvn -f ester-webapp-java-ee-2020/pom.xml clean install -DskipTests

###################################################################################################################################################################
#																					#
# Tomcat : http://objis.com/tutoriel-tomcat-n2-deploiement-war/ 													#
# 												                							#
###################################################################################################################################################################

yes | cp -rf ester-webapp-java-ee-2020/ester-web/target/ester-web.war /opt/tomcat/webapps/ROOT.war

rm -rf ester-webapp-java-ee-2020/

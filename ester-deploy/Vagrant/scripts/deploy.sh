#!/bin/bash
export DEBIAN_FRONTEND=noninteractive
apt-get -qq update -y

# some utils
agt-get install ufw -y 
apt-get install wget -y 
apt-get install git -y

# clone the project
git clone https://gitlab.com/Florian_Fernandes/ester.git /ester-app

# jdk java
apt-get install openjdk-11-jdk -y

# maven
apt-get install maven -y

# mongodb
apt-get install gnupg -y 
wget -qO - https://www.mongodb.org/static/pgp/server-5.0.asc | sudo apt-key add -
echo "deb http://repo.mongodb.org/apt/debian buster/mongodb-org/5.0 main" | sudo tee /etc/apt/sources.list.d/mongodb-org-5.0.list

apt-get update -y 
apt-get install -y mongodb-org

echo "mongodb-org hold" | dpkg --set-selections
echo "mongodb-org-server hold" | dpkg --set-selections
echo "mongodb-org-shell hold" | dpkg --set-selections
echo "mongodb-org-mongos hold" | dpkg --set-selections
echo "mongodb-org-tools hold" | dpkg --set-selections

# nginx
apt-get install nginx -y

# tomcat
apt-get install tomcat9 -y 
systemctl daemon-reload

# mock bdd with fake data
service mongod start
mongoimport --host localhost --port 27017 --db=ester-db --collection=t_answer --drop --type json --file /ester-app/ester-deploy/mongodb/t_answer.json --jsonArray
mongoimport --host localhost --port 27017 --db=ester-db --collection=t_cosali --drop --type json --file /ester-app/ester-deploy/mongodb/t_cosali.json --jsonArray
mongoimport --host localhost --port 27017 --db=ester-db --collection=t_employee --drop --type json --file /ester-app/ester-deploy/mongodb/t_employee.json --jsonArray
mongoimport --host localhost --port 27017 --db=ester-db --collection=t_member_ester --drop --type json --file /ester-app/ester-deploy/mongodb/t_member_ester.json --jsonArray
mongoimport --host localhost --port 27017 --db=ester-db --collection=t_question_option --drop --type json --file /ester-app/ester-deploy/mongodb/t_question_option.json --jsonArray
mongoimport --host localhost --port 27017 --db=ester-db --collection=t_question --drop --type json --file /ester-app/ester-deploy/mongodb/t_question.json --jsonArray
mongoimport --host localhost --port 27017 --db=ester-db --collection=t_questionnary --drop --type json --file /ester-app/ester-deploy/mongodb/t_questionnary.json --jsonArray
mongoimport --host localhost --port 27017 --db=ester-db --collection=t_reference --drop --type json --file /ester-app/ester-deploy/mongodb/t_reference.json --jsonArray
mongoimport --host localhost --port 27017 --db=ester-db --collection=t_result --drop --type json --file /ester-app/ester-deploy/mongodb/t_result.json --jsonArray

# build the project with maven
mvn -f /ester-app/pom.xml clean install -DskipTests

# copy the war file into the tomcat folder
cp /ester-app/ester-web/target/ester-web.war  /var/lib/tomcat9/webapps/ester-web.war

# start tomcat service
service tomcat9 start

# services at boot
systemctl enable tomcat9
systemctl enable mongod

echo "Checking services"
red=`tput setaf 1`
green=`tput setaf 2`
reset=`tput sgr0`
TOMCAT="$(systemctl is-active tomcat9)"
if [ "${TOMCAT}" = "active" ]; then
    echo "${green}Tomcat has been correctly restarted${reset}"
else
    echo "${red}[ERROR]Tomcat is not running.... so exiting${reset}"
    exit 1
fi
MONGOD="$(systemctl is-active mongod)"
if [ "${MONGOD}" = "active" ]; then
    echo "${green}Mongo has been correctly restarted${reset}"
else
    echo "${red}[ERROR]Mongo is not running.... so exiting${reset}"
    exit 1
fi


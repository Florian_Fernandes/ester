#!/bin/bash

###################################################################################################################################################################
#Nom		: install.sh																		#
#Arguments 	: /                                                                               									#        
#Description	: Installation des différents outils pour déployer l'application                  									#
#Pré-condition	: Installation valable sur une distribution Debian GNU/Linux 10 (buster)                                     					#
#Date 		: 07/02/2021																		#
#Auteurs 	: Théo MAHAUDA, Moahmed OUHIRRA, Anas TAGUENITI													#
#Version	: 1.0																			#
###################################################################################################################################################################

###################################################################################################################################################################
#																					#
# ufw : https://www.digitalocean.com/community/tutorials/how-to-set-up-a-firewall-with-ufw-on-debian-10 								#
# 												                							#
###################################################################################################################################################################

apt-get update -y

agt-get install ufw -y

###################################################################################################################################################################
#																					#
# wget : https://avroblog.com/en/blog/content/how-to-install-wget-in-Debian-system 											#
# 												                							#
###################################################################################################################################################################

apt-get update -y

apt-get install wget -y

###################################################################################################################################################################
#																					#
# Java 11 : https://www.digitalocean.com/community/tutorials/how-to-install-java-with-apt-on-debian-10 								#
# 												                							#
###################################################################################################################################################################

apt-get update -y

apt-get install openjdk-11-jdk -y

###################################################################################################################################################################
#																					#
# Maven : https://linuxize.com/post/how-to-install-apache-maven-on-debian-10/ 											#
# 												                							#
###################################################################################################################################################################

apt-get update -y

apt-get install maven -y

###################################################################################################################################################################
#																					#
# Git : https://www.digitalocean.com/community/tutorials/how-to-install-git-on-debian-10 										#
# 												                							#
###################################################################################################################################################################

apt-get update -y

apt-get install git -y 

###################################################################################################################################################################
#																					#
# Nginx : https://www.digitalocean.com/community/tutorials/how-to-install-nginx-on-debian-10 									#
# 												                							#
###################################################################################################################################################################

apt-get update -y

apt-get install nginx -y 

###################################################################################################################################################################
#																					#
# MongoDB : https://docs.mongodb.com/manual/tutorial/install-mongodb-on-debian/ 											#
# 												                							#
###################################################################################################################################################################

apt-get update -y

apt-get install gnupg -y

wget -qO - https://www.mongodb.org/static/pgp/server-4.4.asc | apt-key add -

echo "deb http://repo.mongodb.org/apt/debian buster/mongodb-org/4.4 main" | tee /etc/apt/sources.list.d/mongodb-org-4.4.list

apt-get update -y

apt-get install mongodb-org -y 

echo "mongodb-org hold" | dpkg --set-selections
echo "mongodb-org-server hold" | dpkg --set-selections
echo "mongodb-org-shell hold" | dpkg --set-selections
echo "mongodb-org-mongos hold" | dpkg --set-selections
echo "mongodb-org-tools hold" | dpkg --set-selections

###################################################################################################################################################################
#																					#
# Tomcat : https://www.digitalocean.com/community/tutorials/how-to-install-apache-tomcat-9-on-debian-10 								#
# 												                							#
###################################################################################################################################################################

apt-get update -y

rm -rf /opt/tomcat

mkdir -p /opt/tomcat

wget https://downloads.apache.org/tomcat/tomcat-9/v9.0.43/bin/apache-tomcat-9.0.43.tar.gz

tar -xvzf apache-tomcat-9.0.43.tar.gz -C /opt/tomcat --strip-components=1

rm -rf apache-tomcat-9.0.43.tar.gz

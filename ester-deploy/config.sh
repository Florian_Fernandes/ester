#!/bin/bash

###################################################################################################################################################################
#Nom		: config.sh																		#
#Arguments 	: /                                                                               									#        
#Description	: Configuration des différents outils pour déployer l'application                  								#
#Pré-condition	: Configuration valable sur une distribution Debian GNU/Linux 10 (buster)                                     					#
#Date 		: 07/02/2021																		#
#Auteurs 	: Théo MAHAUDA, Moahmed OUHIRRA, Anas TAGUENITI													#
#Version	: 1.0																			#
###################################################################################################################################################################

###################################################################################################################################################################
#																					#
# Java 11 : https://www.digitalocean.com/community/tutorials/how-to-install-java-with-apt-on-debian-10 								#
# 												                							#
###################################################################################################################################################################

export JAVA_HOME=/usr/lib/jvm/java-11-openjdk-amd64

export PATH=$PATH:$JAVA_HOME

###################################################################################################################################################################
#																					#
# Nginx : https://www.atlantic.net/vps-hosting/how-to-setup-tomcat-with-nginx-as-a-reverse-proxy-on-ubuntu-18-04/ 							#
# 												                							#
###################################################################################################################################################################

ufw allow 'Nginx Full'
ufw allow 'Nginx HTTP'
ufw allow 'Nginx HTTPS'

yes | cp -rf nginx/default /etc/nginx/sites-available/default

systemctl start nginx 

###################################################################################################################################################################
#																					#
# MongoDB : https://docs.mongodb.com/manual/tutorial/install-mongodb-on-debian/ 											#
# 												                							#
###################################################################################################################################################################

systemctl start mongod

###################################################################################################################################################################
#																					#
# Tomcat : https://www.digitalocean.com/community/tutorials/how-to-install-apache-tomcat-9-on-debian-10 								#
# 												                							#
###################################################################################################################################################################

groupadd tomcat

useradd -s /bin/false -g tomcat -d /opt/tomcat tomcat

chgrp -R tomcat /opt/tomcat

chmod -R g+r /opt/tomcat/conf
chmod g+x /opt/tomcat/conf

chown -R tomcat /opt/tomcat/webapps/ /opt/tomcat/work/ /opt/tomcat/temp/ /opt/tomcat/logs/

yes | cp -rf tomcat/tomcat.service /etc/systemd/system/tomcat.service
yes | cp -rf tomcat/tomcat-users.xml /opt/tomcat/conf/tomcat-users.xml
yes | cp -rf tomcat/context.xml /opt/tomcat/webapps/manager/META-INF/context.xml
yes | cp -rf tomcat/context.xml /opt/tomcat/webapps/host-manager/META-INF/context.xml


ufw allow 8080

systemctl start tomcat

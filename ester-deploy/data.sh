#!/bin/bash


mongoimport --host localhost --port 27017 --db=ester-db --collection=t_answer --drop --type json --file mongodb/clear.json --jsonArray
mongoimport --host localhost --port 27017 --db=ester-db --collection=cosali --drop --type json --file mongodb/clear.json --jsonArray
mongoimport --host localhost --port 27017 --db=ester-db --collection=t_employee --drop --type json --file mongodb/data_test/t_employee.json --jsonArray
mongoimport --host localhost --port 27017 --db=ester-db --collection=t_member_ester --drop --type json --file mongodb/data_test/t_member_ester.json --jsonArray
mongoimport --host localhost --port 27017 --db=ester-db --collection=t_question_option --drop --type json --file mongodb/data_test/t_question_option.json --jsonArray
mongoimport --host localhost --port 27017 --db=ester-db --collection=t_question --drop --type json --file mongodb/data_test/t_question.json --jsonArray
mongoimport --host localhost --port 27017 --db=ester-db --collection=t_questionnary --drop --type json --file mongodb/data_test/t_questionnary.json --jsonArray
mongoimport --host localhost --port 27017 --db=ester-db --collection=t_reference --drop --type json --file mongodb/clear.json --jsonArray
mongoimport --host localhost --port 27017 --db=ester-db --collection=t_result --drop --type json --file mongodb/clear.json --jsonArray

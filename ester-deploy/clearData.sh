#!/bin/bash

mongoimport --host localhost --port 27017 --db=ester-db --collection=t_answer --drop --type json --file mongodb/clear.json --jsonArray
mongoimport --host localhost --port 27017 --db=ester-db --collection=t_cosali --drop --type json --file mongodb/clear.json --jsonArray
mongoimport --host localhost --port 27017 --db=ester-db --collection=t_employee --drop --type json --file mongodb/clear.json --jsonArray
mongoimport --host localhost --port 27017 --db=ester-db --collection=t_member_ester --drop --type json --file mongodb/t_member_ester_single.json --jsonArray
mongoimport --host localhost --port 27017 --db=ester-db --collection=t_question_option --drop --type json --file mongodb/clear.json --jsonArray
mongoimport --host localhost --port 27017 --db=ester-db --collection=t_question --drop --type json --file mongodb/clear.json --jsonArray
mongoimport --host localhost --port 27017 --db=ester-db --collection=t_questionnary --drop --type json --file mongodb/clear.json --jsonArray
mongoimport --host localhost --port 27017 --db=ester-db --collection=t_reference --drop --type json --file mongodb/clear.json --jsonArray
mongoimport --host localhost --port 27017 --db=ester-db --collection=t_result --drop --type json --file mongodb/clear.json --jsonArray